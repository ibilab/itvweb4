package net.infobank.itv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;

@SpringBootApplication(exclude = {MultipartAutoConfiguration.class})
public class ItvWeb4Application {

	public static void main(String[] args) {
		SpringApplication.run(ItvWeb4Application.class, args);
	}

}
