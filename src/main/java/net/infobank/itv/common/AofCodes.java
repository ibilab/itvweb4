package net.infobank.itv.common;

public class AofCodes {
    public static final String HH_GCD			= "EL906";	// 시구분.
    public static final String MI_GCD			= "EL907";	// 5분구분.
    public static final String MI_1_GCD			= "EL908";	// 1분구분.

    public static final String VOTE_STATUS_NAME = "EL003";  // 투표 상태 명 
    public static final String SAVE_MSG_GCD		= "EL005";	// 보관함 종류
    public static final String VOTE_DATA		= "EL010";	// 투표 데이터
    public static final String VOTE_USER_DUP	= "EL011";	// 중복, 다중  처리
    public static final String VOTE_KEYWORD_DUP	= "EL012";	// 중복 무효화
    public static final String VOTE_MT			= "EL013";	// 투표 MT전송 여부

    public static final String SNS_LIST			= "EL001";	// MSG 출처
    public static final String TWITTER_CODE			= "EL107";	// MSG 출처
}
