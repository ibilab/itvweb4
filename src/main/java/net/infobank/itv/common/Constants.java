package net.infobank.itv.common;

public class Constants {
    public static final String OPERATOR = "operator";
    public static final String OPERATOR_PASSWORD_INCORRECT = "아이디 또는 비밀번호가 틀렸습니다. 확인해주세요.";
    public static final String OPERATOR_LOGIN_REQUIRED = "로그인 정보가 없습니다. \\n 로그인 하여야 합니다.";

    public static final String LOGIN_PAGE = "/login";

    public static void init() {
    }
}
