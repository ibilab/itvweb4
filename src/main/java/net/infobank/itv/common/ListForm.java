package net.infobank.itv.common;

import lombok.Data;

import java.util.List;

@Data
public class ListForm {

    private int page_num = 1;
    private int page_count;
    private int total_row_count;
    private int start_index;
    private int page_limit = 10;
    private int start_page;
    private int end_page;
    private int start_limit;
    private int row_count = 15;
    private String search_field = "";
    private String search_kw = "";

    private String excel = "N";

    private List list;
    private String pageNavigator;

    public int getStart_limit() {
        return (this.page_num - 1) * this.row_count;
    }

    public void setTotal_row_count(int totalRow_count) {
        this.total_row_count = totalRow_count;
        int row_count = getRow_count();
        setPage_count((totalRow_count - 1) / row_count + 1);
        setStart_index(totalRow_count - (getPage_num() - 1) * row_count);
    }

    public String getPageNavigator() {
        StringBuffer pn = new StringBuffer();
        int page_num = getPage_num();
        String numberChar = " ";
        pn.append("<div class=\"paging\"><div class=\"inner\">");
        String first = "처음";
        String end = "마지막";
        String pre = "이전";
        String next = "다음";

        if (page_num % page_limit > 0)
            start_page = (page_num / page_limit) * page_limit + 1;
        else start_page = page_num - page_limit + 1;

        end_page = start_page + (page_limit - 1);

        if (page_num != 1) pn.append("<a href='javascript:goPage(1);' class=\"first\">" + first + "</a>" + numberChar);
        else pn.append(numberChar);

        if (page_num > page_limit)
            pn.append("<a href='javascript:goPage(" + (start_page - 1) + ");' class=\"prev\">" + pre + "</a>");
        else pn.append(numberChar);

        for (int i = start_page; i <= end_page; i++) {
            if (i > page_count) break;
            if (i != page_num) pn.append(numberChar + "<a href='javascript:goPage(" + i + ");'>" + i + "</a>");
            else pn.append(numberChar + "<a href='javascript:goPage(" + i + ");' class=\"on\">" + i + "</a>");
        }

        if (end_page < page_count)
            pn.append(numberChar + "<a href='javascript:goPage(" + (end_page + 1) + ");'class=\"next\">" + next + "</a>");
        else pn.append(numberChar);

        if (page_num < page_count)
            pn.append(numberChar + "<a href='javascript:goPage(" + page_count + ");' class=\"last\">" + end + "</a>");
        else pn.append(numberChar);

        pn.append("</div></div>");

        return pn.toString();
    }
}
