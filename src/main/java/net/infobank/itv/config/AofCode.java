package net.infobank.itv.config;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.CodeForm;
import net.infobank.itv.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Order(1)
@Component
public class AofCode implements ApplicationRunner {
	public static HashMap<String, ArrayList<CodeForm>> allCodes = new HashMap<String, ArrayList<CodeForm>>();

	private final CodeService codeService;

	public AofCode(CodeService codeService) {
		this.codeService = codeService;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("시스템 코드 loading...");

		allCodes.clear();

		List<CodeForm> list = codeService.getAllCodeList();
		ArrayList<CodeForm> code_list = null;
		int code_cnt = 0;
		for ( CodeForm code : list) {
			if ("0".equals(code.getCode())) {
				code_list = new ArrayList<CodeForm>();
				setCodes(code.getGroup_cd(), code_list);
			} else {
				code_cnt++;
				code_list.add(code);
			}
		}
		log.info("시스템 코드 완료 (총: "+ code_cnt +"개).");
	}

	public void setCodes(String group_cd, ArrayList<CodeForm> codes){
		allCodes.put(group_cd, codes);
	}

	public static ArrayList<CodeForm> getCodes(String group_cd){
		return allCodes.get(group_cd);
	}

	public static CodeForm getCode(String group_cd, String code) {
		ArrayList<CodeForm> list = getCodes(group_cd);

		if(list != null && code != null && !code.equals("")) {
			for(CodeForm form : list) {
				if(form.getCode().equals(code)) {
					return form;
				}
			}
		}
		return new CodeForm();
	}

	/*
	 *  group_cd 의 코드명리스트를 String으로 반환한다.
	 *  "EL001", "|"
	 *  "문자|콩|......"
	 */
	public static String getCodeNamesToString(String group_cd, String delims){
		ArrayList arrCode = AofCode.getCodes(group_cd);
		String retutnVal = "";
		for(int i=0 ; i<arrCode.size() ; i++){
			retutnVal += delims + ((CodeForm)arrCode.get(i)).getCode_nm();
		}
		return retutnVal.substring(delims.length());
	}

	/*
	 *  group_cd 의 코드명리스트를 String으로 반환한다.
	 *  "EL001", "|", "001|002"
	 *  "문자|콩"
	 */
	public static String getCodeNamesToString(String group_cd, String delims, String code_list){
		ArrayList<CodeForm> arrCode = AofCode.getCodes(group_cd);
		String[] code_lists = code_list.split("\\|");
		return arrCode.stream()
				.filter(code -> Arrays.asList(code_lists).contains(code.getCode()))
				.map(CodeForm::getCode_nm)
				.collect(Collectors.joining(delims));
	}

	public static String getUseCode(String group_cd, String delims, String code_list){
		ArrayList<CodeForm> arrCode = AofCode.getCodes(group_cd);
		String[] code_lists = code_list.split("\\|");
		return arrCode.stream()
			.filter(code -> Arrays.asList(code_lists).contains(code.getCode()))
			.map(CodeForm::getCode)
			.collect(Collectors.joining(delims));
	}

	public static List<Map<String, Object>> getUseCodeAsListMap(String group_cd, String delims, String code_list){
		ArrayList<CodeForm> arrCode = AofCode.getCodes(group_cd);
		String[] code_lists = code_list.split("\\|");
		return arrCode.stream()
				.filter(code -> Arrays.asList(code_lists).contains(code.getCode()))
				.map(code ->{
					return new HashMap<String, Object>(){{
						put("codeNm",code.getCode_nm());
						put("code",code.getCode());
					}};
				})
				.collect(Collectors.toList());
	}
}
