package net.infobank.itv.config;

import net.infobank.itv.common.Constants;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();

        // 기존 로그인정보 삭제
        if (session.getAttribute(Constants.OPERATOR) != null) {
            session.removeAttribute(Constants.OPERATOR);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HttpSession session = request.getSession();
//        String dest = (String) session.getAttribute("dest");

        //response.sendRedirect("/main");
//        response.sendRedirect(dest != null ? (String) dest : "/login" );
    }
}
