package net.infobank.itv.config;

import net.infobank.itv.common.Constants;
import net.infobank.itv.form.MenuForm;
import net.infobank.itv.form.OperatorForm;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.List;

@Component
public class OperatorInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);
        List<MenuForm> menu = (List<MenuForm>) session.getAttribute("fullMenus");

        if(operator == null ) {
            request.setAttribute("error_message", Constants.OPERATOR);
            request.getRequestDispatcher("/error").forward(request, response);
//            response.sendRedirect("/error");      // setAttribute에 값이 안간다.
//            throw new Exception();                // 오류 로그 발생한다.
            return false;
        }
        if(menu != null) {
            long auth = 0;
            String uri = request.getRequestURI();
            final String mainURI = uri.split("/")[1];
            auth = menu.stream()
                    .map(MenuForm::getMenu_url)
                    .filter(e -> e.contains(mainURI))
                    .count();
            if(mainURI.equals("SendAction.do"))
                auth = 1;
            if(auth < 1) {
                request.setAttribute("error_message", Constants.OPERATOR);
                request.getRequestDispatcher("/error").forward(request, response);
                return false;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        System.out.println("post handle");
//        HttpSession session = request.getSession();
//        String dest = (String) session.getAttribute("dest");

//        OperatorVO operator = (OperatorVO) session.getAttribute(Constants.OPERATOR);
//        System.out.println(request.getRequestURI());
//        System.out.println(operator);
//        if( operator != null) {
//            System.out.println("ASDF");
//
//            // 모든 에러는 에러 발생시키고 /error/error 에서 받고 ASDF 일때는 login 으로 이동시켜버림...
//            throw new Exception("ASDF");
//            //response.sendRedirect("/errors");
////            request.getRequestDispatcher("/errors/infoMessage").forward(request, response);
//        }
    }
}
