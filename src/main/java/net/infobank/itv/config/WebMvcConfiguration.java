package net.infobank.itv.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan(basePackages = {"net.infobank.itv"})
public class WebMvcConfiguration implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/login.do");
		registry.addInterceptor(new OperatorInterceptor()).excludePathPatterns("/login/**")
				.excludePathPatterns("/UserAction.do/**")
				.excludePathPatterns("/logout.do")
				.excludePathPatterns("/page/**")
		.excludePathPatterns("/error").excludePathPatterns("/login.do").excludePathPatterns("/")
		.excludePathPatterns("/js/**").excludePathPatterns("/css/**").excludePathPatterns("/img/**")
		.excludePathPatterns("/icon/**").excludePathPatterns("/mmsfiles/**").excludePathPatterns("/favicon.ico").excludePathPatterns("/file/**");
	}
	private final int MAX_SIZE = 10 * 1024 * 1024;
	@Bean
	public MultipartResolver multipartResolver(){
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(MAX_SIZE);
		multipartResolver.setMaxUploadSizePerFile(MAX_SIZE);
		multipartResolver.setMaxInMemorySize(0);
		return multipartResolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/file/**").addResourceLocations("D:\\upload\\");
	}
}
