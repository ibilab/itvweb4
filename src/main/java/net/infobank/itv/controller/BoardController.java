package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.common.Constants;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.ChannelForm;
import net.infobank.itv.form.MyPhraseForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.service.BoardService;
import net.infobank.itv.service.ChannelService;
import net.infobank.itv.service.MyPhraseService;
import net.infobank.itv.service.ProgramService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.DupUtil;
import net.infobank.itv.util.StringUtil;
import org.apache.catalina.WebResourceRoot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Slf4j
@Controller
@RequestMapping(value="/BoardAction.do")
public class BoardController {

    private final ProgramService programService;
    private final BoardService boardService;
    private final ChannelService channelService;
    private final MyPhraseService myPhraseService;

    public BoardController(ProgramService programService, BoardService boardService, ChannelService channelService, MyPhraseService myPhraseService) {
        this.programService = programService;
        this.boardService = boardService;
        this.channelService = channelService;
        this.myPhraseService = myPhraseService;
    }

    @RequestMapping(value = "")
    public String board(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response, BoardForm form) {
        String next = "errors/404";
        switch(cmd) {
            case "forms" :
                next = forms(request, response);
                break;
            case "mmsforms" :
                next = mmsforms(request, response);
                break;
            case "sendBox" :
                next = sendBox(request, response);
                break;
            case "saveBox" :
                next = saveBox(request, response);
                break;
            case "sso" :
//                next = list(request, response, form);
                break;
        }
        return next;
    }

    public String forms(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session  = request.getSession();
        OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);

        // 프로그램 세션 설정 CHECK
        if("Y".equals(request.getParameter("changePgmSession"))){
            int ch_key = Integer.parseInt(request.getParameter("ch_key"));
            int pgm_key = Integer.parseInt(request.getParameter("pgm_key"));
            // 프로그램 세션 설정
            operator.setCh_key(ch_key);
            operator.setPgm_key(pgm_key);
            ChannelForm channel = channelService.getChInfo(ch_key);
            String billing_code = channel == null ? "" : channel.getBilling_code();
            operator.setBilling_code(billing_code);

            request.setAttribute("pgmTopList", programService.getTopProgramList(operator.getCh_key()));
        }

        request.setAttribute("sns_lists", AofCode.getCodeNamesToString(AofCodes.SNS_LIST, "|", operator.getOper_sns()));

        return "layout/board/boardSearch";
    }


    public String mmsforms(HttpServletRequest request, HttpServletResponse response) {
        return "layout/board/boardMmsSearch";
    }

    public String sendBox(HttpServletRequest request, HttpServletResponse response) {
        return "layout/send/sendSearch";
    }

    public String saveBox(HttpServletRequest request, HttpServletResponse response) {

        String st_type = request.getParameter("st_type");
        String menu_title = AofCode.getCode(AofCodes.SAVE_MSG_GCD, st_type).getCode_nm();

        request.setAttribute("menu_title", menu_title);
        return "layout/board/boardSaveBox";
    }

    @RequestMapping(value = "/list")
    public String list(HttpServletRequest request, HttpServletResponse response, BoardForm form) {
        OperatorForm operator = CommonUtils.getOperator(request);
        form.setPgm_key(operator.getPgm_key());
        String next = "";

        // 엑셀 or saveBoxList
        String excel = request.getParameter("excel");
        String st_type = request.getParameter("st_type");
        String list_type = request.getParameter("list_type");

        try {
            form.setExcel(excel);
            if ("saveBox".equals(list_type)) {
                boardService.getSaveBoardList(form, st_type);
                next = "board/boardSaveBoxList";
            } else if ("mmslist".equals(list_type)) {
                form.setMsg_count(1);
                boardService.getBoardList(form);
                next = "board/boardMmsList";
            } else {
                boardService.getBoardList(form);
                next = "board/boardList";
            }
            if(!StringUtil.isEmpty(excel)) {
                next = "board/boardListExcel";
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("BoardForm", form);
        return next;
    }


    @RequestMapping(value = "/listBoardEachUser")
    public String listBoardEachUser(HttpServletRequest request, HttpServletResponse response, BoardForm form) {
        OperatorForm operator = CommonUtils.getOperator(request);
        form.setPgm_key(operator.getPgm_key());
        String next = "";

        try {
            next = "board/boardListEachUser";
            boardService.getBoardListEachUser(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("BoardForm", form);
        return next;
    }
//
//    @RequestMapping(value = "/listSendMsgEachUser")
//    public String listSendMsgEachUser(HttpServletRequest request, HttpServletResponse response, BoardForm form) {
//        OperatorForm operator = CommonUtils.getOperator(request);
//        form.setPgm_key(operator.getPgm_key());
//        String next = "";
//
//        try {
//            next = "board/boardSendListEachUser";
//            boardService.getListSendMsgEachUser(form);
//        } catch (Exception e) {
//            log.error("Exception : ", e);
//            request.setAttribute("msg", e);
//            return "errors/errorMessage";
//        }
//
//        request.setAttribute("BoardForm", form);
//        return next;
//    }

    @RequestMapping(value = "/addPrize")
    public @ResponseBody String addPrize(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");

            boardService.addPrize(pgm_key, msg_com, user_id);

            result.addProperty("message", "당첨내역 업데이트에 성공하였습니다.");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "당첨내역 업데이트에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/setMsgRead")
    public @ResponseBody String setMsgRead(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String board_key = request.getParameter("board_key");
            String table_name = request.getParameter("table_name");
            String msg_read = request.getParameter("msg_read");

            boardService.setMsgRead(table_name, board_key, msg_read);

            result.addProperty("action", "list();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "읽음 표시 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/copyMsg")
    public @ResponseBody String copyMsg(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        String set ="복사";
        try {
            String board_key = request.getParameter("board_key");
            String table_name = request.getParameter("table_name");
            String save_type = request.getParameter("save_type");
            String save_box_type = request.getParameter("save_box_type");
            if("move".equals(save_type)) {
                set = "이동";
            }

            boardService.copyMsg(table_name, board_key, save_type, save_box_type);

            result.addProperty("message", "선택된 메세지 " + set + "에 성공하였습니다.");
            result.addProperty("action", "list();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "선택된 메세지 " + set + "에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/copySelectMsg")
    public @ResponseBody String copySelectMsg(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        String set = "복사";
        try {
            String save_type = request.getParameter("save_type");
            String save_box_type = request.getParameter("save_box_type");
            String[] chk_board_key = request.getParameterValues("chk_board_key[]");
            if("move".equals(save_type)) {
                set = "이동";
            }

            boardService.copySelectMsg(chk_board_key, save_type, save_box_type);

            result.addProperty("message", "선택된 메세지 " + set + "에 성공하였습니다.");
            result.addProperty("action", "list();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "선택된 메세지 " + set + "에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/showMsg")
    public @ResponseBody String showMsg(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String board_key = request.getParameter("board_key");
            String table_name = request.getParameter("table_name");
            String msg_show = request.getParameter("msg_show");

            boardService.showMsg(table_name, board_key, msg_show);

            String set ="";
            if("move".equals(msg_show)) {
                set = "숨김";
            } else {
                set = "보임";
            }
//            result.addProperty("message", "선택된 메세지 " + set + "에 성공하였습니다.");
            result.addProperty("action", "list();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", e.getMessage());
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/sendMsg")
    public String sendMsg(HttpServletRequest request, HttpServletResponse response, BoardForm form) throws Exception {
        OperatorForm operator = CommonUtils.getOperator(request);
        form.setPgm_key(operator.getPgm_key());

        String[] chk_board_key = form.getChk_board_key();
        ArrayList<BoardForm> list = new ArrayList<BoardForm>();

        for (int i=0 ; i<chk_board_key.length ; i++) {
            String[] chkKey = chk_board_key[i].split(";");
            BoardForm board = new BoardForm();
            board.setBoard_key(Integer.parseInt(chkKey[0]));
            board.setMsg_com(chkKey[3]);
            board.setMsg_userid(chkKey[1]);
            board.setTable_name(chkKey[2]);
            if(chkKey.length > 4)
                board.setMsg_key(chkKey[4]);
            list.add(board);
        }

        // 레인보우 핸드폰 케이스 체크 변수
        int rainbow_phone_case_y = 0;
        int rainbow_phone_case_n = 0;
        // msg_userid 로 distinct 처리
        ArrayList<BoardForm> distinct_result = (ArrayList<BoardForm>) DupUtil.deduplication(list, BoardForm::getMsg_userid);
        ArrayList<BoardForm> resultList = new ArrayList<BoardForm>();

        for (BoardForm board: distinct_result) {
            if(board.getMsg_com().equals("013")) {
                String userId = board.getMsg_userid();
                boolean isPhone = Pattern.matches("^01(?:0|1|[6-9])(?:\\d{3}|\\d{4})\\d{4}$", userId);
                if(isPhone){
                    resultList.add(board);
                    rainbow_phone_case_y++;
                }else {
                    rainbow_phone_case_n++;
                }
            } else {
                resultList.add(board);
            }
        }
        form.setList(resultList);

        request.setAttribute("rainbow_phone_case_y", rainbow_phone_case_y);
        request.setAttribute("rainbow_phone_case_n", rainbow_phone_case_n);
        request.setAttribute("BoardForm", form);

        // TODO 장문 전송가능 판단
        // Functions.getSendSMSMaxLength

        // 상용구 목록
        List<MyPhraseForm> phlist = myPhraseService.list(form.getPgm_key());
        request.setAttribute("phlist", phlist);

        return "base/board/boardSendMsg";
    }

    @RequestMapping(value = "/sendMsgBox")
    public String sendMsgBox(HttpServletRequest request, HttpServletResponse response, BoardForm form) throws Exception {
        String selected = request.getParameter("selected");

        // 상용구 목록
        List<MyPhraseForm> phlist = myPhraseService.list(form.getPgm_key());
        request.setAttribute("phlist", phlist);

        // 사용자 목록 가져오기
        boardService.getSaveBoxMsgList(form, selected);

        if(!"Y".equals(selected)) {
            request.setAttribute("sendMsgType", "전체");
        } else {
            request.setAttribute("sendMsgType", "선택");
        }

        request.setAttribute("BoardForm", form);
        return "base/send/sendMsgSaveBox";
    }


    @RequestMapping(value = "/deleteSaveBoxAllMsg")
    public @ResponseBody String deleteSaveBoxAllMsg(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String st_type = request.getParameter("st_type");

            boardService.deleteSaveBoxAllMsg(pgm_key, st_type);

            result.addProperty("message", "메세지 삭제에 성공 성공하였습니다.");
            result.addProperty("action", "list();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "메세지 삭제에 실패 실패하였습니다.");
        }
        return gson.toJson(result);
    }
}
