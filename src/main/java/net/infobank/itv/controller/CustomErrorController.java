package net.infobank.itv.controller;

import net.infobank.itv.common.Constants;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {
	private String VIEW_PATH = "base/errors/";

	@RequestMapping(value = "/error")
	public String handleError(HttpServletRequest request) {
		String next = "error";
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		String reason = (String) request.getAttribute("error_message");

		if( reason != null) {
			if(Constants.OPERATOR.equals(reason)) {
				request.setAttribute("msg", Constants.OPERATOR_LOGIN_REQUIRED);
				request.setAttribute("next", Constants.LOGIN_PAGE);
				next = "infoMessage";
			}
		}

		if (status != null) {
			int statusCode = Integer.valueOf(status.toString());
			if (statusCode == HttpStatus.NOT_FOUND.value()) {
				next = "404";
			}
		}

		// error/error.jsp 로 통합하자.... from 500.jsp
		return VIEW_PATH + next;
	}

	@Override
	public String getErrorPath() {
		return null;
	}
}
