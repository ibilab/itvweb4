package net.infobank.itv.controller;

import net.infobank.itv.form.EmoticonDto;
import net.infobank.itv.form.MultipartEmoticonDto;
import net.infobank.itv.service.EmoticonService;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Controller
@RequestMapping("/EmoticonAction.do")
public class EmoticonController {
	private final EmoticonService service;

	public EmoticonController(EmoticonService service) {
		this.service = service;
	}

	@GetMapping("")
	public String emoticonManager(){
		return "layout/emoticon/emoticonManager";
	}

	@GetMapping("/emoticon")
	@ResponseBody
	public EmoticonDto getEmoticonList(@RequestParam("page")int page, @RequestParam("rowCount") int rowCount){
		EmoticonDto dto = new EmoticonDto();
		dto.setPage_num(page);
		dto.setRow_count(rowCount);
		return service.getList(dto);
	}

	@PostMapping("/emoticon")
	@ResponseBody
	public Map<String, Object> addEmoticon(@ModelAttribute EmoticonDto dto){
		Map<String, Object> map = new HashMap<>();
		map.put("result", service.addEmoticon(dto));
		map.put("message", "등록이 완료되었습니다.");
		return map;
	}

	@PutMapping("/emoticon")
	@ResponseBody
	public Map<String, Object> modifyEmoticon(@ModelAttribute EmoticonDto dto){
		Map<String, Object> map = new HashMap<>();
		map.put("result", service.modifyEmoticon(dto));
		map.put("message", "수정이 완료되었습니다.");
		return map;
	}

	@PutMapping("/emoticon/{idx}")
	@ResponseBody
	public Map<String, Object> removeEmoticon(@PathVariable int idx, @ModelAttribute EmoticonDto dto, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String emoticonPath = request.getServletContext().getRealPath(".") + "/../emoticon/";
		String[] path = dto.getEmoPath().split("/");
		dto.setEmoPath(emoticonPath + path[path.length-1]);
		map.put("result", service.removeEmoticon(idx, dto));
		map.put("message", "삭제가 완료되었습니다.");
		return map;
	}

	@GetMapping({"/emoticon/check/{idx}", "/emoticon/check"})
	@ResponseBody
	public Map<String, Object> check(@PathVariable("idx") Optional<String> optionIdx, @RequestParam("emoClass") String emoClass, @RequestParam("emoCode") String emoCode){
		String idx = optionIdx.orElse("0");
		Map<String, Object> map = new HashMap<>();
		EmoticonDto dto = new EmoticonDto();
		dto.setEmoCode(emoCode);
		dto.setEmoClass(emoClass);
		dto.setEmoIdx(idx);
		boolean check = service.check(dto);
		map.put("result", check);
		map.put("btnText", "사용가능");
		if(!check){
			map.put("message", "중복된 코드 조합(대분류+소분류) 입니다.");
			map.put("btnText", "사용불가");
		}
		return map;
	}

	@PostMapping(path = "/emoticon/image")
	@ResponseBody
	public Map<String, Object> uploadImage(@RequestParam("operId") String operId,
										   @RequestParam("pgmKey") int pgmKey,
										   @RequestParam("pgmKey") String uploadFilePath,
										   @RequestParam("file") MultipartFile file,
											MultipartHttpServletRequest request){
		final Map<String, Object> map = new HashMap<>();
		if(! Objects.requireNonNull(file.getContentType()).startsWith("image")){
			map.put("result", false);
			map.put("message", "이미지 파일(PNG, JPG, JPEG, GIF)만 업로드가 가능합니다.");
			return map;
		}

		int EMOTICON_UPLOAD_MAX_SIZE = 1024 * 1024; // 1MB
		if(file.getSize() > EMOTICON_UPLOAD_MAX_SIZE) {
			map.put("result", false);
			map.put("message", "이미지 파일(JPG, GIF, PNG) 또는 1M 미만 업로드가 가능합니다.");
			return map;
		}
		ServletContext scontext = request.getServletContext();

		final String realFolder = scontext.getRealPath(".");
		MultipartEmoticonDto dto = new MultipartEmoticonDto(operId, pgmKey, uploadFilePath, file, realFolder);
		String fullPath = service.emoticonImageWrite(dto);
		if( fullPath.isEmpty()) {
			map.put("result", false);
			map.put("message", "이미지 추가에 실패하였습니다.");
			return map;
		}

		map.put("result", true);
		map.put("fullPath", fullPath);
		map.put("message", "이미지 추가 성공");

		return map;
	}

	@GetMapping(value = "/emoticon/{imageName}.{extension}", produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] getImage(
			@PathVariable(name = "imageName") String imageName,
			@PathVariable(name = "extension") String extension,
			HttpServletRequest request) throws IOException {
		String imagePath = request.getServletContext().getRealPath(".") + "/../emoticon/" + imageName + "." + extension;
		InputStream imageStream = new FileInputStream(imagePath);
		byte[] imageByteArray = IOUtils.toByteArray(imageStream);
		imageStream.close();

		return imageByteArray;
	}
}
