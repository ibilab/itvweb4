package net.infobank.itv.controller;

import lombok.AllArgsConstructor;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.EntireDto;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.service.EntireService;
import net.infobank.itv.util.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/EntireBoardAction.do")
@AllArgsConstructor
public class EntireBoardController {

	private final EntireService service;

	@GetMapping("")
	public String page(){
		return "layout/entire/entire";
	}

	@GetMapping("/info")
	@ResponseBody
	public Map<String, Object> getInfo(HttpServletRequest request){
		final Map<String, Object> map = new HashMap<>();
		OperatorForm operator = CommonUtils.getOperator(request);
		map.put("sns_lists", AofCode.getUseCodeAsListMap(AofCodes.SNS_LIST, "|", operator.getOper_sns()));

		return map;
	}

	@GetMapping("/list")
	@ResponseBody
	public Map<String, Object> list(@ModelAttribute EntireDto dto, HttpServletRequest request) {
		final Map<String, Object> map = new HashMap<>();
		LocalDate startDate = LocalDate.parse(dto.getStartDate(), DateTimeFormatter.ISO_DATE);
		LocalDate endDate = LocalDate.parse(dto.getEndDate(), DateTimeFormatter.ISO_DATE);
		if(ChronoUnit.DAYS.between(startDate, endDate) > 2) {
			map.put("result", false);
			map.put("message", "전체프로그램 조회는 최대 3일 까지만 지원 합니다.");
			return map;
		}
		OperatorForm operator = CommonUtils.getOperator(request);

		dto.setChKey(operator.getCh_key());
		EntireDto result = service.getBoardList(dto);

		map.put("result", true);
		map.put("board", result);
		return map;
	}

	@GetMapping("/list/excel")
	public String excel(@ModelAttribute EntireDto dto, HttpServletRequest request, ModelMap map) {
		OperatorForm operator = CommonUtils.getOperator(request);
		dto.setExcel("Y");
		dto.setChKey(operator.getCh_key());
		EntireDto result = service.getBoardList(dto);
		map.put("board", result);
		return "entire/EntireBoardListExcel";
	}


}
