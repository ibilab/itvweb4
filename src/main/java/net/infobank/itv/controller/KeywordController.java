package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.KeywordForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.service.KeywordService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping(value="/KeywordAction.do")
public class KeywordController {

    private final KeywordService keywordService;

    public KeywordController(KeywordService keywordService) {
        this.keywordService = keywordService;
    }

    @RequestMapping(value = "")
    public String keyword(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("cmd = " + cmd);
        String next = "errors/404";
        switch(cmd) {
            case "defaultforms" :
                next = defaultforms(request, response);
                break;
            case "filterforms" :
                next = filterforms(request, response);
                break;
            case "newforms" :
                next = newforms(request, response);
                break;
            case "spamforms" :
                next = spamforms(request, response);
                break;
        }
        System.out.println(next);
        return next;
    }

    public String defaultforms(HttpServletRequest request, HttpServletResponse response) {

        return "layout/keyword/defaultForm";
    }

    public String filterforms(HttpServletRequest request, HttpServletResponse response) {

        return "layout/keyword/filterForm";
    }

    public String newforms(HttpServletRequest request, HttpServletResponse response) {

        return "layout/keyword/newForm";
    }

    public String spamforms(HttpServletRequest request, HttpServletResponse response) {

        return "layout/board/boardSearch";
    }

    @RequestMapping(value = "/defaultList")
    public String defaultlist(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        KeywordForm form = new KeywordForm();
        form.setPgm_key(operator.getPgm_key());
        form.setMo_word("기본");

        String next = "";
        try {
            next = "keyword/defaultList";
            keywordService.list(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("KeywordForm", form);
        return next;
    }

    @RequestMapping(value = "/defaultWrite")
    public String defaultWrite(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        KeywordForm form = new KeywordForm();
        form.setPgm_key(operator.getPgm_key());
        form.setMo_word("기본");

        String next = "";
        try {
            String keyword_idx = request.getParameter("keyword_idx");
            if(!StringUtil.isEmpty(keyword_idx)) {
                form.setKeyword_idx(Integer.parseInt(keyword_idx));
                form = keywordService.get(form);
            }
            next = "base/keyword/defaultWrite";
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("Keyword", form);
        return next;
    }

    @RequestMapping(value = "/delete")
    public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String keyword_idx = request.getParameter("keyword_idx");

            keywordService.delete(keyword_idx);

            result.addProperty("action", "initFormPage();");
            result.addProperty("message", "성공적으로 삭제 되었습니다.");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "삭제 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/save")
    public @ResponseBody String save(HttpServletRequest request, HttpServletResponse response, KeywordForm form) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        String action = "수정";

        try {
            int keyword_idx = form.getKeyword_idx();
            if( keyword_idx <= 0) {
                action = "등록";
                keywordService.insert(form);
            } else {
                keywordService.update(form);
            }

            result.addProperty("action", "parent.initFormPage();");
            result.addProperty("message", "성공적으로 " + action + " 되었습니다.");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", action  + " 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/newList")
    public String newlist(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        KeywordForm form = new KeywordForm();
        form.setPgm_key(operator.getPgm_key());
        form.setMo_word("처음");

        String next = "";
        try {
            next = "keyword/newList";
            keywordService.list(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("KeywordForm", form);
        return next;
    }

    @RequestMapping(value = "/newWrite")
    public String newWrite(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        KeywordForm form = new KeywordForm();
        form.setPgm_key(operator.getPgm_key());
        form.setMo_word("처음");

        String next = "";
        try {
            String keyword_idx = request.getParameter("keyword_idx");
            if(!StringUtil.isEmpty(keyword_idx)) {
                form.setKeyword_idx(Integer.parseInt(keyword_idx));
                form = keywordService.get(form);
            }
            next = "base/keyword/newWrite";
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("Keyword", form);
        return next;
    }

    @RequestMapping(value = "/filterList")
    public String filterList(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        KeywordForm form = new KeywordForm();
        form.setPgm_key(operator.getPgm_key());
        form.setMo_word(null);

        String next = "";
        try {
            next = "keyword/filterList";
            keywordService.list(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("KeywordForm", form);
        return next;
    }

    @RequestMapping(value = "/filterWrite")
    public String filterWrite(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        KeywordForm form = new KeywordForm();
        form.setPgm_key(operator.getPgm_key());

        String next = "";
        try {
            String keyword_idx = request.getParameter("keyword_idx");
            if(!StringUtil.isEmpty(keyword_idx)) {
                form.setKeyword_idx(Integer.parseInt(keyword_idx));
                form = keywordService.get(form);
            }
            next = "base/keyword/filterWrite";
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("Keyword", form);
        return next;
    }


    @RequestMapping(value = "/filterDupCount")
    public @ResponseBody String filterDupCount(HttpServletRequest request, HttpServletResponse response, KeywordForm form) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        String action = "수정";

        try {
            int count = keywordService.filterDupCount(form);
            if (count > 0) {
                result.addProperty("result", "true");
            } else {
                result.addProperty("result", "false");
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", action  + " 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }
}
