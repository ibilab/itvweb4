package net.infobank.itv.controller;

import net.infobank.itv.common.Constants;
import net.infobank.itv.service.ChannelService;
import net.infobank.itv.service.MenuService;
import net.infobank.itv.service.OperatorService;
import net.infobank.itv.service.ProgramService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.form.MenuForm;
import net.infobank.itv.form.OperatorForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.List;

@Controller
public class LoginController {

	@Autowired
	private OperatorService operatorService;
	@Autowired
	private MenuService menuService;
	@Autowired
	private ChannelService channelService;
	@Autowired
	private ProgramService programService;

	@GetMapping(value = "/")
	public String toLogin(HttpServletRequest request) {
		HttpSession session = request.getSession();

		return "base/main/login";
//        if (session.getAttribute("login") != null) {
//            return "main/main";
//        } else {
//            return "redirect:" + Constants.LOGIN_PAGE;
//        }
	}

	@GetMapping(value = "/login")
	public String loginForm(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();

		// requestUrl 가 null 이 아니거나 referer 가 null이 아닐경우
		if (request.getRequestURI() != null && request.getHeader("referer") != null) {
			session.setAttribute("dest", request.getHeader("referer"));
		}

//        System.out.println(request.getRequestURI());
//        System.out.println(request.getHeader("referer"));

		return "base/main/login";
	}

	@PostMapping(value = "/login.do")
	public String login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String next = "";
		HttpSession session = request.getSession();

		String oper_id = request.getParameter("oper_id");
		String oper_pw = request.getParameter("oper_pw");

		OperatorForm operator = operatorService.login(oper_id);

		if(operator == null) {
			// TODO 메시지 보여주고 로그인 페이지로
			System.out.println("no id");
			request.setAttribute("msg", Constants.OPERATOR_PASSWORD_INCORRECT);
			request.setAttribute("next", Constants.LOGIN_PAGE);
			next = "base/errors/infoMessage";
		} else if(!oper_pw.equals(operator.getOper_pw())) {
			// TODO 메시지 보여주고 로그인 페이지로
			request.setAttribute("msg", Constants.OPERATOR_PASSWORD_INCORRECT);
			request.setAttribute("next",Constants.LOGIN_PAGE);
			try {
				// TODO 로그인 로그
			} catch(Exception e) {
			}
			next = "base/errors/infoMessage";
		} else {
			// TODO 현재 시간에 맞는 pgm 가져와서 operator에 추가... controller? service?
			// board.getPgm_name.now_pgm

			session.setAttribute(Constants.OPERATOR, operator);
			// TODO 로그인 로그

			// channel
			session.setAttribute("chTopList", channelService.getTopChannelList(operator));
			// program
			session.setAttribute("pgmTopList", programService.getTopProgramList(operator.getCh_key()));
			// program info
			session.setAttribute("programInfo", programService.getProgramInfo(operator.getPgm_key()));

			// 메뉴 가져오기
			int group_level = operator.getGroup_level();

			session.setAttribute("topMenus", menuService.getTopMenuList(group_level));
			List<MenuForm> fullMenus = menuService.getFullMenuList(group_level);
			session.setAttribute("fullMenus", fullMenus);
			// 우측 탭정보
			session.setAttribute("session_tabs", "1|1|1");

			CommonUtils.getMenuPathInfo(request, fullMenus);

//            MenuModel mmodel = new MenuModel();
//            request.getSession().setAttribute("topMenus", mmodel.getTopMenus(user));
//            request.getSession().setAttribute("fullMenus", mmodel.getFullMenus(user));

			// forwarding
//            next = "redirect:" + "main";
			next = "layout/main/main";
//            System.out.println(request.getHeader("referer"));
//            String dest = (String) session.getAttribute("dest");
		}
		return next;
	}

	@GetMapping(value = "/logout.do")
	public String logout(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		String next = "base/main/login";
		session.invalidate();

		return next;
	}

	@GetMapping(value = "/main")
	public String main(Model model, HttpServletRequest request, HttpServletResponse response) {
		return "layout/main/main";
	}
}
