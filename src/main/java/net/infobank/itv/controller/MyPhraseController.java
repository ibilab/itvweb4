package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.MyPhraseForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.service.MyPhraseService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping(value="/MyPhraseAction.do")
public class MyPhraseController {

	@Autowired
	MyPhraseService myPhraseService;

	@RequestMapping(value = "")
	public String forms(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response) {
		String next = "errors/404";
		switch(cmd) {
			case "myPhraseBox" :
				next = "layout/myphrase/myPhraseForm";
				break;
		}
		return next;
	}

	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response) {
		String next = "";
		List<MyPhraseForm> list = null;

		try {
			int pgm_key = Integer.parseInt(request.getParameter("pgm_key"));

			list = myPhraseService.list(pgm_key);
			next = "myphrase/myPhraseList";
		} catch (Exception e) {
			log.error("Exception : ", e);
			request.setAttribute("msg", e);
			return "errors/errorMessage";
		}

		request.setAttribute("list", list);
		return next;
	}

	@GetMapping(value = "/boilerplate")
	@ResponseBody
	public Map<String, Object> getList(HttpServletRequest request, HttpServletResponse response) {
		String next = "";
		List<MyPhraseForm> list = null;
		Map<String, Object> map = new HashMap<>();
		OperatorForm operator = CommonUtils.getOperator(request);
		try {
			int pgm_key = operator.getPgm_key();

			list = myPhraseService.list(pgm_key);
			map.put("list", list);
			map.put("result", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", false);
			map.put("message", e.getMessage());
		}

		return map;
	}

	@RequestMapping(value = "/write")
	public String write(HttpServletRequest request, HttpServletResponse response) {
		OperatorForm operator = CommonUtils.getOperator(request);
		MyPhraseForm form = new MyPhraseForm();
		form.setPgm_key(operator.getPgm_key());

		String next = "";
		try {
			String myphrase_idx = request.getParameter("myphrase_idx");
			if(!StringUtil.isEmpty(myphrase_idx)) {
				form.setMyphrase_idx(Integer.parseInt(myphrase_idx));
				form = myPhraseService.get(form);
			}
			next = "base/myphrase/myphraseWrite";
		} catch (Exception e) {
			log.error("Exception : ", e);
			request.setAttribute("msg", e);
			return "errors/errorMessage";
		}

		request.setAttribute("MyPhrase", form);
		return next;
	}

	@RequestMapping(value = "/delete")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response) {
		Gson gson = new Gson();
		JsonObject result = new JsonObject();

		try {
			int myphrase_idx = Integer.parseInt(request.getParameter("myphrase_idx"));

			myPhraseService.delete(myphrase_idx);

			result.addProperty("action", "initPage();");
			result.addProperty("message", "성공적으로 삭제 되었습니다.");
		} catch (Exception e) {
			log.error("Exception : ", e);
			result.addProperty("message", "삭제 처리 실패하였습니다.");
		}
		return gson.toJson(result);
	}


	@RequestMapping(value = "/save")
	public @ResponseBody String save(HttpServletRequest request, HttpServletResponse response, MyPhraseForm form) {
		Gson gson = new Gson();
		JsonObject result = new JsonObject();
		String action = "수정";

		try {
			int myphrase_idx = form.getMyphrase_idx();
			if( myphrase_idx <= 0) {
				action = "등록";
				myPhraseService.insert(form);
			} else {
				myPhraseService.update(form);
			}

			result.addProperty("action", "parent.initPage();");
			result.addProperty("message", "성공적으로 " + action + " 되었습니다.");
		} catch (Exception e) {
			log.error("Exception : ", e);
			result.addProperty("message", action  + " 처리 실패하였습니다.");
		}
		return gson.toJson(result);
	}

}
