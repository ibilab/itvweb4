package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.common.Constants;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.service.OperatorService;
import net.infobank.itv.service.UserService;
import net.infobank.itv.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping(value="/UserAction.do")
public class OperatorController {

    @Autowired
    OperatorService operatorService;

    @RequestMapping(value = "/updateOperPW")
    public @ResponseBody String updateOperPW(HttpServletRequest request, HttpServletResponse response, @ModelAttribute OperatorForm operator) {
        String id = operator.getOper_id();
        String pw = operator.getOper_pw();

        String result = "false";
        if(operatorService.updateOperPW(id, pw) > 0) {
            result = "true";
            OperatorForm oper = CommonUtils.getOperator(request);
            oper.setOper_pw(pw);
            request.setAttribute(Constants.OPERATOR, oper);
        }

        return result;
    }
}
