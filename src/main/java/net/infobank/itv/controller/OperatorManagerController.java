package net.infobank.itv.controller;

import net.infobank.itv.common.AofCodes;
import net.infobank.itv.common.Constants;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.ChannelForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.ProgramForm;
import net.infobank.itv.form.ProgramLightForm;
import net.infobank.itv.service.ChannelService;
import net.infobank.itv.service.OperatorService;
import net.infobank.itv.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/OperatorMng.do")
public class OperatorManagerController <T> {

	private final ChannelService channelService;
	private final ProgramService programService;
	private final OperatorService operatorService;

	public OperatorManagerController(ChannelService channelService, ProgramService programService, OperatorService operatorService){
		this.channelService = channelService;
		this.programService = programService;
		this.operatorService = operatorService;
	}

	@GetMapping("")
	public String operatorManager(HttpServletRequest request, ModelMap model){
		HttpSession session  = request.getSession();
		OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);

		// 프로그램 세션 설정 CHECK
		if("Y".equals(request.getParameter("changePgmSession"))){
			int ch_key = Integer.parseInt(request.getParameter("ch_key"));
			int pgm_key = Integer.parseInt(request.getParameter("pgm_key"));
			// 프로그램 세션 설정
			operator.setCh_key(ch_key);
			operator.setPgm_key(pgm_key);
			ChannelForm channel = channelService.getChInfo(ch_key);
			String billing_code = channel == null ? "" : channel.getBilling_code();
			operator.setBilling_code(billing_code);
			model.put("pgmTopList", programService.getTopProgramList(operator.getCh_key()));
		}
		model.put("groupList", operatorService.getGroupList(operator.getGroup_level()).getList());
		model.put("sns_lists", AofCode.getUseCode(AofCodes.SNS_LIST, "|", operator.getOper_sns()));
		model.put("sns_lists_nm", AofCode.getCodeNamesToString(AofCodes.SNS_LIST, "|", operator.getOper_sns()));
		session.removeAttribute("updateUser");
		return "layout/operator/OperMng";
	}

	@GetMapping("/channel")
	@ResponseBody
	public List<ChannelForm> getChannelList(HttpSession session){
		OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);
		return channelService.getTopChannelList(operator);
	}

	@GetMapping("/users")
	@ResponseBody
	public OperatorForm getGroupLevelUserList(@RequestParam("pageNum") int pageNum,
											  @RequestParam("rowCount") int rowCount,
											  @RequestParam("ch_key") int ch_key,
											  HttpSession session){
		OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);
		operator.setPage_num(pageNum);
		operator.setRow_count(rowCount);
		return operatorService.getOperatorFormList(operator, ch_key);
	}

	@PostMapping("/users")
	@ResponseBody
	public Map<String, Object> addUser(OperatorForm operatorForm, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		map.put("result",operatorService.addUser(operatorForm));
		map.put("message", "등록이 완료되었습니다.");
		return map;
	}

	@PutMapping("/users")
	@ResponseBody
	public T modifyUsers(OperatorForm operatorForm, HttpSession session, HttpServletResponse response){
		String user = session.getAttribute("updateUser").toString();
		Map<String, Object> map = new HashMap<>();
		if(! user.equals(operatorForm.getOper_id()) || user.isEmpty()) {
			String responseText = "잘못된 요청입니다.\n확인 후 다시 시도해주시기 바랍니다.";
			response.setStatus(400);
			return (T)responseText;
		}
		map.put("result",operatorService.modifyUser(operatorForm));
		map.put("message", "수정이 완료되었습니다.");
		session.removeAttribute("updateUser");
		return (T)map;
	}

	@PostMapping("/users/{id}")
	@ResponseBody
	public Map<String, Object> setUpdateUser(@PathVariable("id") String id, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		session.setAttribute("updateUser", id);
		map.put("result", "success");
		return map;
	}

	@PutMapping("/users/{id}")
	@ResponseBody
	public Map<String, Object> removeUser(@PathVariable("id") String id){
		Map<String, Object> map = new HashMap<>();
		map.put("result", operatorService.modifyUserStatus(id));
		map.put("message", "삭제가 완료되었습니다.");
		return map;
	}

	@GetMapping("/users/check/{id}")
	@ResponseBody
	public int getUserInfo(@PathVariable("id") String operId, HttpSession session){
		return operatorService.isUsed(operId);
	}

	@GetMapping("/program/{chKey}")
	@ResponseBody
	public List<ProgramLightForm> getProgram(@PathVariable("chKey") int chKey){
		return programService.getLightList(chKey);
	}
}
