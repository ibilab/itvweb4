package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.ProgramForm;
import net.infobank.itv.service.ChannelService;
import net.infobank.itv.service.ProgramService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Slf4j
@Controller
@RequestMapping(value="/PgmAction.do")
public class ProgramController {

    @Autowired
    ProgramService programService;
    @Autowired
    ChannelService channelService;

    @RequestMapping(value = "")
    public String forms(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response) {
        String next = "errors/404";
        switch(cmd) {
            case "mng" :
                next = mng(request, response);
                break;
            case "endmng" :
                next = endmng(request, response);
                break;
        }
        return next;
    }

    public String mng(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        request.setAttribute("Channels", channelService.getTopChannelList(operator));
        return "layout/program/programMng";
    }

    public String endmng(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        request.setAttribute("Channels", channelService.getTopChannelList(operator));
        return "layout/program/programEndMng";
    }

    @RequestMapping(value = "/list")
    public String list(HttpServletRequest request, HttpServletResponse response, ProgramForm form) {
        String next = "";
        try {
            List<ProgramForm> list = programService.list(form);
            next = "program/programList";
            request.setAttribute("list", list);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        return next;
    }


    @RequestMapping(value = "/write")
    public String write(HttpServletRequest request, HttpServletResponse response) {
        ProgramForm form = new ProgramForm();

        String next = "";
        try {
            String pgm_key = request.getParameter("pgm_key");
            if(!StringUtil.isEmpty(pgm_key)) {
                form = programService.getProgramInfo(Integer.parseInt(pgm_key));
                request.setAttribute("Program", form);
            }

            request.setAttribute("hh_gcd", AofCode.getCodes(AofCodes.HH_GCD));
            request.setAttribute("mi_1_gcd", AofCode.getCodes(AofCodes.MI_1_GCD));

            request.setAttribute("sns_lists", AofCode.getCodes(AofCodes.SNS_LIST));

            next = "base/program/programWrite";

        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        return next;
    }

    @RequestMapping(value = "/getPgmTimeDup")
    public @ResponseBody String getPgmTimeDup(HttpServletRequest request, HttpServletResponse response, ProgramForm form) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            int count = programService.getPgmTimeDup(form);

            if (count > 0) {
                result.addProperty("data", "true");
            } else {
                result.addProperty("data", form.getPgm_key() + "");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("data", "error");
            result.addProperty("msg", e.toString());
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/save")
    public @ResponseBody String save(HttpServletRequest request, HttpServletResponse response, ProgramForm form) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            int pgm_key = form.getPgm_key();
            if( pgm_key <= 0) {
                programService.insert(request, form);
            } else {
                programService.update(request, form);
            }
            result.addProperty("message", " 프로그램 저장에 성공하였습니다.");
            result.addProperty("action", "reload();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", " 프로그램 저장에 실패하였습니다.");
        }

        return gson.toJson(result);
    }

    @RequestMapping(value = "/delete")
    public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            int pgm_key = Integer.parseInt(request.getParameter("pgm_key"));

            programService.delete(pgm_key);
            result.addProperty("message", "삭제 되었습니다.");
            result.addProperty("action", "reload();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "삭제 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/endList")
    public String endList(HttpServletRequest request, HttpServletResponse response, ProgramForm form) {
        String next = "";
        try {
            form.setIsvalid("0");
            List<ProgramForm> list = programService.list(form);
            next = "program/programEndList";
            request.setAttribute("list", list);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        return next;
    }


    @RequestMapping(value = "/withTime")
    public @ResponseBody String withTime(HttpServletRequest request) {
        int ch_key = Integer.parseInt(request.getParameter("ch_key"));

        List<ProgramForm> list = programService.getTopProgramList(ch_key);
        Gson gson = new Gson();
        String json = gson.toJson(list);

        return json;
    }
}
