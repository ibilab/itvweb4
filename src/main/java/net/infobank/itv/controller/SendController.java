package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.SendForm;
import net.infobank.itv.service.SendService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping(value="/SendAction.do")
public class SendController {

    @Autowired
    SendService sendService;

    @RequestMapping(value = "/list")
    public String list(HttpServletRequest request, HttpServletResponse response, SendForm form) {
        OperatorForm operator = CommonUtils.getOperator(request);
        form.setPgm_key(operator.getPgm_key());
        String next = "";

        String excel = request.getParameter("excel");

        try {
            if(!StringUtil.isEmpty(excel)) {
                form.setExcel(excel);
                next = "send/sendListExcel";
            } else {
                next = "send/sendList";
            }
            System.out.println(next);
            sendService.getSendList(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("SendForm", form);
        return next;
    }

    @RequestMapping(value = "/listSendMsgEachUser")
    public String listSendMsgEachUser(HttpServletRequest request, HttpServletResponse response, SendForm form) {
        OperatorForm operator = CommonUtils.getOperator(request);
        form.setPgm_key(operator.getPgm_key());
        String next = "";

        try {
            next = "send/sendListEachUser";
            sendService.getListSendMsgEachUser(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("BoardForm", form);
        return next;
    }

    @RequestMapping(value = "/sendMsgSave")
    public @ResponseBody String sendMsgSave(HttpServletRequest request, HttpServletResponse response, BoardForm form) throws Exception {
        // 선택 답장
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        String[] msg_userid = request.getParameterValues("msg_userid");
        String[] board_key = request.getParameterValues("board_key");
        String[] msg_com = request.getParameterValues("msg_com");
        String[] table_name = request.getParameterValues("table_name");

        String mt_msg = form.getMt_msg().replaceAll("\n", "");
        form.setMt_msg(mt_msg);

        int iSendResult_001 = 0; // 문자 정상 발송 카운트
        int iSendResult_007 = 0; // 카카오톡 정상 발송 카운트
        int iSendResult_013 = 0; // 레인보우 정상 발송 카운트

        try {
            for (int i = 0; i < msg_userid.length; i++) {
                form.setMsg_userid(msg_userid[i]);
                form.setBoard_key(Integer.parseInt(board_key[i]));
                form.setMsg_com(msg_com[i]);
                form.setTable_name(table_name[i]);

                boolean sendResult = sendService.sendMsgSave(request, form);
                if (sendResult) {
                    switch (msg_com[i]) {
                        case "001":
                            iSendResult_001++;
                            break;
                        case "007":
                            iSendResult_007++;
                            break;
                        case "013":
                            iSendResult_013++;
                            break;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            iSendResult_001 = -1;
            iSendResult_007 = -1;
            iSendResult_013 = -1;

        }
        result.addProperty("result_001", iSendResult_001);
        result.addProperty("result_007", iSendResult_007);
        result.addProperty("result_013", iSendResult_013);

        return gson.toJson(result);
    }


    @RequestMapping(value = "sendSmsMsg")
    public @ResponseBody String sendSmsMsg(HttpServletRequest request, HttpServletResponse response) {
        // 유저 정보에서 답장
        OperatorForm operator = CommonUtils.getOperator(request);
        String oper_id = operator.getOper_id();
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String mt_msg = request.getParameter("mt_msg");
            String comment_id = request.getParameter("comment_id");
            String table_name = request.getParameter("table_name");
            String board_key = request.getParameter("board_key");

            BoardForm form = new BoardForm();
            form.setPgm_key(Integer.parseInt(pgm_key));
            form.setMsg_com(msg_com);
            form.setMsg_userid(user_id);
            form.setMt_msg(mt_msg);
            form.setComment_id(comment_id);
            form.setTable_name(table_name);
            form.setBoard_key(Integer.parseInt(board_key));

            //  TODO 고릴라 처리...?
            if(sendService.sendMsgSave(request, form)) {
                result.addProperty("action", "location.reload();");
                result.addProperty("message", "메세지 보내기에 성공 했습니다.");
            } else {
                result.addProperty("message", "메세지 보내기에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "메세지 보내기에 실패하였습니다.");
        }
        return gson.toJson(result);
    }
}
