package net.infobank.itv.controller;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.TwitterDto;
import net.infobank.itv.form.YoutubeForm;
import net.infobank.itv.service.SnsService;
import net.infobank.itv.util.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;

@Slf4j
@Controller
@RequestMapping(value="/SnsAction.do")
public class SnsController {

    private final SnsService snsService;


    public SnsController(SnsService snsService) {
        this.snsService = snsService;
    }

    @RequestMapping(value = "")
    public String sns(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response) {
        String next = "errors/404";
        switch(cmd) {
            case "youtubeMng" :
                next = "layout/sns/youtubeMng";
                break;
            case "twMng" :
                next = "layout/sns/twitter";
                break;
        }
        return next;
    }

    @RequestMapping(value = "/youtubeList")
    public String youtubeList(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        YoutubeForm form = new YoutubeForm();
        form.setPgm_key(operator.getPgm_key());

        String next = "";
        try {
            next = "sns/youtubeList";
            snsService.searchYoutubeList(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("YoutubeForm", form);
        return next;
    }

    @GetMapping("/twitter")
    @ResponseBody
    public Map<String, Object> twitter(HttpServletRequest request){
        OperatorForm operator = CommonUtils.getOperator(request);
        Map<String, Object> map = new HashMap<>();
        map.put("result", true);
        map.put("twitter", snsService.getTwitterInfo(operator.getPgm_key()));
        return map;
    }

    @PostMapping("/twitter")
    @ResponseBody
    public Map<String, Object> addTwitter(@ModelAttribute TwitterDto dto,HttpServletRequest request){
        OperatorForm operator = CommonUtils.getOperator(request);
        Map<String, Object> map = new HashMap<>();
        dto.setPgmKey(operator.getPgm_key());
        int result = snsService.addTwitter(dto);
        map.put("result", false);
        map.put("message", "저장에 실패하였습니다..\n확인후 다시 시도해 주시기 바랍니다.");
        if(result == 1) {
            map.put("result", true);
            map.put("message", "저장이 완료되었습니다.");
        }

        return map;
    }

    @PutMapping("/twitter")
    @ResponseBody
    public Map<String, Object> modifyTwitter(@ModelAttribute TwitterDto dto, HttpServletRequest request){
        OperatorForm operator = CommonUtils.getOperator(request);
        Map<String, Object> map = new HashMap<>();
        dto.setPgmKey(operator.getPgm_key());
        int result = snsService.modifyTwitter(dto);

        map.put("result", false);
        map.put("message", "수정에 실패하였습니다.\n확인후 다시 시도해 주시기 바랍니다.");
        if(result == 1) {
            map.put("result", true);
            map.put("message", "수정이 완료되었습니다.");
        }

        return map;
    }

    @PutMapping("/twitter/{id}")
    @ResponseBody
    public Map<String, Object> removeTwitter(@PathVariable("id") Optional<Integer> optionalInt, HttpServletRequest request){
        int id = optionalInt.orElse(0);
        OperatorForm operator = CommonUtils.getOperator(request);
        Map<String, Object> map = new HashMap<>();
        map.put("result", false);
        map.put("message", "삭제에 실패하였습니다.\n확인후 다시 시도해 주시기 바랍니다.");
        if(operator.getPgm_key() != id){
            return map;
        }

        int result = snsService.removeTwitter(operator.getPgm_key());
        if(result == 1) {
            map.put("result", true);
            map.put("message", "삭제되었습니다.");
        }
        return map;
    }

    @GetMapping("/twitter/url")
    @ResponseBody
    public Map<String, Object> getTwitterAuthURI(HttpSession session){
        final String CONSUMER_KEY = AofCode.getCode(AofCodes.TWITTER_CODE ,"1").getCode_nm();
        final String CONSUMER_SECRET = AofCode.getCode(AofCodes.TWITTER_CODE ,"2").getCode_nm();
        RequestToken requestToken = snsService.getRequestToken(CONSUMER_KEY, CONSUMER_SECRET);
        String url = requestToken.getAuthorizationURL();
        Map<String, Object> map = new HashMap<>();
        map.put("consumerKey", CONSUMER_KEY);
        map.put("consumerSecret", CONSUMER_SECRET);
        session.setAttribute("requestToken", requestToken);
        map.put("url", url);
        return map;
    }

    @PostMapping("/twitter/oauth")
    @ResponseBody
    public Map<String, Object> getAccessToken(@RequestParam("oauth_token")String oAuthToken
                                            , @RequestParam("oauth_verifier") String oAuthVerifier
                                            , HttpSession session){
        RequestToken requestToken = (RequestToken)session.getAttribute("requestToken");
        final String CONSUMER_KEY = AofCode.getCode(AofCodes.TWITTER_CODE ,"1").getCode_nm();
        final String CONSUMER_SECRET = AofCode.getCode(AofCodes.TWITTER_CODE ,"2").getCode_nm();
        Map<String, Object> map = new HashMap<>();
        if(requestToken == null){
            map.put("message", "잘못된 접근입니다.");
            map.put("result", false);
            return map;
        }

        AccessToken accessToken = snsService.getAccessToken(oAuthToken,oAuthVerifier,CONSUMER_KEY,CONSUMER_SECRET, requestToken);
        map.put("result", true);
        map.put("accessToken", accessToken.getToken());
        map.put("accessTokenSecret", accessToken.getTokenSecret());
        map.put("consumerKey", CONSUMER_KEY);
        map.put("consumerSecret", CONSUMER_SECRET);
        return map;
    }


}
