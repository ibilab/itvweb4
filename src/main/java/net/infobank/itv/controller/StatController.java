package net.infobank.itv.controller;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.StatForm;
import net.infobank.itv.form.YoutubeForm;
import net.infobank.itv.service.ChannelService;
import net.infobank.itv.service.StatService;
import net.infobank.itv.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping(value="/StatAction.do")
public class StatController {

    @Autowired
    private StatService statService;
    @Autowired
    private ChannelService channelService;

    @RequestMapping(value = "")
    public String mng(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response) {
        String next = "errors/404";
        switch(cmd) {
            case "statDayMng" :
                next = statDayMng(request, response);
                break;
            case "statPgmMng" :
                next = statPgmMng(request, response);
                break;
            case "statComDayMng" :
                next = statComDayMng(request, response);
                break;
        }
        return next;
    }

    public String statDayMng(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        request.setAttribute("Channels", channelService.getTopChannelList(operator));
        return "layout/stat/statDayMng";
    }

    public String statPgmMng(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        request.setAttribute("Channels", channelService.getTopChannelList(operator));
        return "layout/stat/statPgmMng";
    }

    public String statComDayMng(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        request.setAttribute("Channels", channelService.getTopChannelList(operator));
        return "layout/stat/statComDayMng";
    }

    @RequestMapping(value = "statDay")
    public String statDay(HttpServletRequest request, HttpServletResponse response, StatForm form) {
        statService.statDay(request, form);
        return "base/stat/statDayList";
    }

    @RequestMapping(value = "statPgm")
    public String statPgm(HttpServletRequest request, HttpServletResponse response, StatForm form) {
        statService.statPgm(request, form);
        return "base/stat/statPgmList";
    }

    @RequestMapping(value = "statComDay")
    public String statComDay(HttpServletRequest request, HttpServletResponse response, StatForm form) {
        statService.statComDay(request, form);
        return "base/stat/statComDayList";
    }
}
