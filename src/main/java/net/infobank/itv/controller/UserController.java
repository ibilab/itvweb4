package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.MyPhraseForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.UserForm;
import net.infobank.itv.service.BoardService;
import net.infobank.itv.service.MyPhraseService;
import net.infobank.itv.service.UserService;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
@RequestMapping(value="/UserAction.do")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    BoardService boardService;
    @Autowired
    MyPhraseService myPhraseService;

    @RequestMapping(value = "/setUserImage")
    public @ResponseBody String setUserImage(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String user_id = request.getParameter("user_id");
            String msg_com = request.getParameter("msg_com");
            String user_pic = request.getParameter("user_pic");

            if(userService.setUserImage(pgm_key, user_id, msg_com, user_pic) > 0) {
                result.addProperty("action", "list();");
                // TODO 사용자 이미지 수정
                //result.addProperty("action", "list();");
            } else {
                result.addProperty("message", "사용자 이미지 업데이트 실패 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "사용자 이미지 업데이트 실패 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "showUserInfo")
    public String showUserInfo(HttpServletRequest request, HttpServletResponse response) {
        String next = "base/user/userInfo";
        OperatorForm operator = CommonUtils.getOperator(request);
        String user_id = request.getParameter("user_id");
        String msg_com = request.getParameter("msg_com");
        String board_key = request.getParameter("board_key");
        try {
            UserForm userInfo = userService.getUserInfo(request, user_id, msg_com);
            request.setAttribute("userInfo", userInfo);

            // 상용구 목록
            List<MyPhraseForm> phlist = myPhraseService.list(operator.getPgm_key());
            request.setAttribute("phlist", phlist);

            // history 에 사용될 yyyyMM 목록 만들기
            DateUtil dutil = new DateUtil();
            ArrayList monthList = new ArrayList();
            for (int i = 0; i < 12; i++) {
                monthList.add(i, dutil.getString("yyyyMM"));
                dutil.addMonth(-1);
            }
            request.setAttribute("BackList", monthList);
        } catch (Exception e) {
            log.error("Exception : ", e);
        }

        return next;
    }

    @RequestMapping(value = "updateUserEditNick")
    public @ResponseBody String updateUserEditNick(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String edit_nick = request.getParameter("edit_nick");

            if(userService.updateUserEditNick(pgm_key, msg_com, user_id, edit_nick) > 0) {
//                result.addProperty("message", "닉 변경 제한에 성공하였습니다.");
            } else {
                result.addProperty("message", "닉 변경 제한에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "닉 변경 제한에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "updateUserAllowMt")
    public @ResponseBody String updateUserAllowMt(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String allow_mt = request.getParameter("allow_mt");

            if(userService.updateUserAllowMt(pgm_key, msg_com, user_id, allow_mt) > 0) {
//                result.addProperty("message", "MT 수신 여부  변경에 성공하였습니다.");
            } else {
                result.addProperty("message", "MT 수신 여부  변경에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "MT 수신 여부  변경에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "updateUserShowMsg")
    public @ResponseBody String updateUserShowMsg(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String show_msg = request.getParameter("show_msg");

            if(userService.updateUserShowMsg(pgm_key, msg_com, user_id, show_msg) > 0) {
//                result.addProperty("message", "자동숨김 설정에 성공하였습니다.");
            } else {
                result.addProperty("message", "자동숨김 설정에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "자동숨김 설정에 실패하였습니다.");
        }
        return gson.toJson(result);
    }


    @RequestMapping(value = "updateUserNick")
    public @ResponseBody String updateUserNick(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String user_nick = request.getParameter("user_nick");

            if(userService.updateUserNick(pgm_key, msg_com, user_id, user_nick) > 0) {
                result.addProperty("message", "닉네임 변경에 성공하였습니다.");
                result.addProperty("action", "reloadBoardList();");
            } else {
                result.addProperty("message", "닉네임 변경에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "닉네임 변경에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "updateUserInfo")
    public @ResponseBody String updateUserInfo(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String user_info = request.getParameter("user_info");

            if(userService.updateUserInfo(pgm_key, msg_com, user_id, user_info) > 0) {
                result.addProperty("message", "정보 변경에 성공하였습니다.");
                result.addProperty("action", "reloadBoardList();");
            } else {
                result.addProperty("message", "정보 변경에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "정보 변경에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "updateUserIcon")
    public @ResponseBody String updateUserIcon(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String user_icon = request.getParameter("user_icon");

            if(userService.updateUserIcon(pgm_key, msg_com, user_id, user_icon) > 0) {
                result.addProperty("action", "reloadBoardList();");
            } else {
                result.addProperty("message", "아이콘 설정에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "아이콘 설정에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "updateUserGift")
    public @ResponseBody String updateUserGift(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            String pgm_key = request.getParameter("pgm_key");
            String msg_com = request.getParameter("msg_com");
            String user_id = request.getParameter("user_id");
            String user_image = request.getParameter("user_image");

            if(userService.updateUserGift(pgm_key, msg_com, user_id, user_image) > 0) {
                result.addProperty("action", "reloadBoardList();");
            } else {
                result.addProperty("message", "선물 아이콘 설정에 실패하였습니다.");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "선물 아이콘 설정에 실패하였습니다.");
        }
        return gson.toJson(result);
    }

}
