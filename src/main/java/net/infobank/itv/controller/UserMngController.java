package net.infobank.itv.controller;

import net.infobank.itv.common.AofCodes;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.UserDto;
import net.infobank.itv.service.UserMngService;
import net.infobank.itv.util.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/UserMngAction.do")
public class UserMngController {
	private final int MESSAGE_MAX_SIZE = 80;
	private final UserMngService service;

	public UserMngController(UserMngService service) {
		this.service = service;
	}

	@GetMapping("")
	public String page(@RequestParam("cmd")String cmd, HttpServletRequest request, ModelMap model){
		String page = "";
		OperatorForm operator = CommonUtils.getOperator(request);
		switch (cmd){
			case "UserMng" :
				page = "layout/user/attendUser";
				model.put("sns_lists", AofCode.getUseCodeAsListMap(AofCodes.SNS_LIST, "|", operator.getOper_sns()));
				break;
			case "UserBlackMng" :
				page = "layout/user/blackList";
				break;
		}
		return page;
	}

	@GetMapping("/users")
	@ResponseBody
	public Map<String, Object> getUsers(@ModelAttribute UserDto dto, @RequestParam("page_num") int pageNum, HttpServletRequest request){
		Map<String, Object> map = new HashMap<>();
		OperatorForm operator = CommonUtils.getOperator(request);
		dto.setPgmKey(operator.getPgm_key());
		dto.setPage_num(pageNum);
		map.put("user", service.getAttendUserList(dto));
		return map;
	}

	@GetMapping("/users/excel")
	public String getUsersExcel(@ModelAttribute UserDto dto, @RequestParam("page_num") int pageNum, HttpServletRequest request, ModelMap model){
		OperatorForm operator = CommonUtils.getOperator(request);
		dto.setPgmKey(operator.getPgm_key());
		dto.setPage_num(pageNum);
		model.put("user", service.getAttendUserList(dto));
		return "user/attendUserListExcel";
	}

	@GetMapping("/users/message")
	public String getMessageForm(@ModelAttribute UserDto dto, HttpServletRequest request, ModelMap map){
		OperatorForm operator = CommonUtils.getOperator(request);
		dto.setPgmKey(operator.getPgm_key());
		return "base/user/sendAllMessage";
	}

	@GetMapping("/users/message/info")
	@ResponseBody
	public Map<String, Object> getMessageInfo(@ModelAttribute UserDto dto, HttpServletRequest request){
		Map<String, Object> map = new HashMap<>();
		OperatorForm operator = CommonUtils.getOperator(request);
		dto.setPgmKey(operator.getPgm_key());
		map.put("totalCount", service.getTotalCount(dto));
		map.put("searchOption", dto);
		map.put("msgSize", MESSAGE_MAX_SIZE);
		map.put("info", operator);
		return map;
	}

	@PostMapping("/users/message")
	@ResponseBody
	public Map<String, Object> sendMessage(@ModelAttribute UserDto dto, @RequestParam("message") String message, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		OperatorForm operator = CommonUtils.getOperator(request);
		dto.setPgmKey(operator.getPgm_key());
		int totalCount = service.send(dto, message, operator);
		map.put("totalCount", totalCount);
		return map;
	}
}
