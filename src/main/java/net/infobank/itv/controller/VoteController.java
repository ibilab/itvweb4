package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.form.VoteForm;
import net.infobank.itv.service.VoteService;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Controller
@RequestMapping(value="/VoteAction.do")
public class VoteController {

    @Autowired
    private VoteService voteService;

    @RequestMapping(value = "")
    public String vote(@RequestParam("cmd") String cmd, HttpServletRequest request, HttpServletResponse response) {
        String next = "errors/404";
        switch(cmd) {
            case "mng" :
                next = mng(request, response);
                break;
            case "result" :
                next = result(request, response);
                break;
        }
        return next;
    }

    public String mng(HttpServletRequest request, HttpServletResponse response) {

        return "layout/vote/voteMng";
    }

    @RequestMapping(value = "/list")
    public String list(HttpServletRequest request, HttpServletResponse response, VoteForm form) {
        String next = "";

        try {
            form = voteService.list(form);
            next = "base/vote/voteList";
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("VoteForm", form);
        return next;
    }

    @RequestMapping(value = "/write")
    public String write(HttpServletRequest request, HttpServletResponse response) {
        VoteForm form = new VoteForm();

        String next = "";
        try {
            request.setAttribute("vote_user_dup", AofCode.getCodes(AofCodes.VOTE_USER_DUP));
            request.setAttribute("vote_emo", AofCode.getCodes(AofCodes.VOTE_DATA));

            request.setAttribute("hh_gcd", AofCode.getCodes(AofCodes.HH_GCD));
            request.setAttribute("mi_1_gcd", AofCode.getCodes(AofCodes.MI_1_GCD));

            request.setAttribute("sns_lists", AofCode.getCodes(AofCodes.SNS_LIST));

            String vote_key = request.getParameter("vote_key");
            if(!StringUtil.isEmpty(vote_key)) {
                form.setVote_key(Integer.parseInt(vote_key));
                form = voteService.get(form);

                request.setAttribute("Vote", form);
            }

            next = "base/vote/voteWrite";
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        return next;
    }

    @RequestMapping(value = "/save")
    public @ResponseBody String save(HttpServletRequest request, HttpServletResponse response, VoteForm form) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        String action = "수정";

        try {
            int vote_key = form.getVote_key();

            String[] items = request.getParameterValues("vote_item");
            String[] vote_keyword = request.getParameterValues("vote_keyword");
            form.setVote_items(StringUtil.getCommaList(items).replaceAll("'", "").replaceAll(",", "\\|"));
            form.setVote_keywords(StringUtil.getCommaList(vote_keyword).replaceAll("'", "").replaceAll(",", "\\|"));

            if( vote_key <= 0) {
                action = "등록";
                String start_type = request.getParameter("start_type");
                if("NOW".equals(start_type)) {
                    Date now = new Date();
                    SimpleDateFormat ymd = new SimpleDateFormat("yyyyMMddHHmmss");
                    form.setVote_stm( ymd.format(now) );
                }

                voteService.insert(form);
                result.addProperty("message", "성공적으로 " + action + " 되었습니다.");
            } else {
                if(voteService.update(form)) {
                    result.addProperty("message", "수정 되었습니다.");
                } else {
                    result.addProperty("message", "수정 실패(다른곳에서 이미 수정됨).");
                }
            }

            result.addProperty("action", "reload();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", action  + " 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/ending")
    public @ResponseBody String ending(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            int vote_key = Integer.parseInt(request.getParameter("vote_key"));
            int sub_key = Integer.parseInt(request.getParameter("sub_key"));

            if(voteService.ending(vote_key, sub_key)) {
                result.addProperty("message", "종료 되었습니다.");
            } else {
                result.addProperty("message", "종료 실패(다른곳에서 이미 수정됨).");
            }
            result.addProperty("action", "reload();");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "종료 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/delete")
    public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            int vote_key = Integer.parseInt(request.getParameter("vote_key"));
            int sub_key = Integer.parseInt(request.getParameter("sub_key"));

            if(voteService.delete(vote_key, sub_key)) {
                result.addProperty("message", "삭제 되었습니다.");
                result.addProperty("action", "reloadInsert();");
            } else {
                result.addProperty("message", "삭제 실패(다른곳에서 이미 수정됨).");
                result.addProperty("action", "reload();");
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "삭제 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    public String result(HttpServletRequest request, HttpServletResponse response) {

        return "layout/vote/voteResult";
    }
    @RequestMapping(value = "/resultList")
    public String resultList(HttpServletRequest request, HttpServletResponse response) {

        try {
            int vote_key = Integer.parseInt(request.getParameter("vote_key"));

            voteService.resultList(request, vote_key);

        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }
        return "base/vote/voteResultList";
    }
}
