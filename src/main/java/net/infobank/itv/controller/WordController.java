package net.infobank.itv.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.form.LimitWordForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.service.WordService;
import net.infobank.itv.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping(value="/WordMng.do")
public class WordController {

    @Autowired
    private WordService wordService;

    @RequestMapping(value = "")
    public String keyword(HttpServletRequest request, HttpServletResponse response) {
        String next = "layout/word/limitWordMng";
        return next;
    }

    @RequestMapping(value = "/limitWordList")
    public String limitWordList(HttpServletRequest request, HttpServletResponse response) {
        OperatorForm operator = CommonUtils.getOperator(request);
        LimitWordForm form = new LimitWordForm();
        form.setPgm_key(operator.getPgm_key());


        String next = "";
        try {
            next = "word/limitWordList";
            wordService.limitWordList(form);
        } catch (Exception e) {
            log.error("Exception : ", e);
            request.setAttribute("msg", e);
            return "errors/errorMessage";
        }

        request.setAttribute("WordForm", form);
        return next;
    }

    @RequestMapping(value = "/deleteLimitWord")
    public @ResponseBody String deleteLimitWord(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            int lword_key = Integer.parseInt(request.getParameter("lword_key"));

            wordService.deleteLimitWord(lword_key);

            result.addProperty("action", "initPage();");
            result.addProperty("message", "성공적으로 삭제 되었습니다.");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "삭제 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

    @RequestMapping(value = "/insertLimitWord")
    public @ResponseBody String insertLimitWord(HttpServletRequest request, HttpServletResponse response, LimitWordForm form) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();

        try {
            wordService.insertLimitWord(form);

            result.addProperty("action", "initPage();");
//            result.addProperty("message", "성공적으로 등록 되었습니다.");
        } catch (Exception e) {
            log.error("Exception : ", e);
            result.addProperty("message", "등록 처리 실패하였습니다.");
        }
        return gson.toJson(result);
    }

}
