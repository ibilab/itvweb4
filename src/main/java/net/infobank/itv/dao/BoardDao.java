package net.infobank.itv.dao;

import net.infobank.itv.form.BoardForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class BoardDao {
    final String nameSpace = "Board";

    @Autowired
    private SqlSession sqlSession;

    public String checkTable(String table) {
        return sqlSession.selectOne(nameSpace + ".checkTable", table);
    }

    public List<BoardForm> searchBoard(BoardForm form) {
        return sqlSession.selectList(nameSpace + ".searchBoard", form);
    }

    public int countBoard(BoardForm form) {
        return sqlSession.selectOne(nameSpace + ".countBoard", form);
    }

    public int distinctBoard(BoardForm form) {
        return sqlSession.selectOne(nameSpace + ".distinctBoard", form);
    }

    public int setMsgRead(HashMap<String, String> map) {
        return sqlSession.update(nameSpace + ".setMsgRead", map);
    }

    public int addPrize(HashMap<String, String> map) {
        return sqlSession.update(nameSpace + ".addPrize", map);
    }

    public int copyMsg(HashMap<String, String> map) {
        return sqlSession.update(nameSpace + ".copyMsg", map);
    }

    public int showMsg(HashMap<String, String> map) {
        return sqlSession.update(nameSpace + ".showMsg", map);
    }

    public int updateReturnMsg(BoardForm form) {
        return sqlSession.update(nameSpace + ".updateReturnMsg", form);
    }

    public List<BoardForm> searchSaveBoard(BoardForm form) {
        return sqlSession.selectList(nameSpace + ".searchSaveBoard", form);
    }

    public List<BoardForm> searchSaveBoardUsers(BoardForm form) {
        return sqlSession.selectList(nameSpace + ".searchSaveBoardUsers", form);
    }

    public int countSaveBoard(BoardForm form) {
        return sqlSession.selectOne(nameSpace + ".countSaveBoard", form);
    }

    public int deleteSaveBoxAllMsg(HashMap<String, String> map) {
        return sqlSession.update(nameSpace + ".deleteSaveBoxAllMsg", map);
    }
}
