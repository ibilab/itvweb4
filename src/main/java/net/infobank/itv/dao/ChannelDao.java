package net.infobank.itv.dao;

import net.infobank.itv.form.ChannelForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.ProgramForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class ChannelDao {
    String nameSpace = "Channel";

    @Autowired
    private SqlSession sqlSession;

    public List<ChannelForm> getTopChannelList(OperatorForm operator) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("ch_key", operator.getCh_key());
        map.put("br_key", operator.getBr_key());
        map.put("level", operator.getGroup_level());
        return sqlSession.selectList(nameSpace+ ".topList", map);
    }

    public ChannelForm getChInfo(int pgm_key) {
        return sqlSession.selectOne(nameSpace + ".getChInfo", pgm_key);
    }
}
