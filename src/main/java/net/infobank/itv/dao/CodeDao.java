package net.infobank.itv.dao;

import net.infobank.itv.form.CodeForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CodeDao {
    String nameSpace = "Code";

    @Autowired
    private SqlSession sqlSession;

    public List<CodeForm> getAllCodeList() {
        return sqlSession.selectList(nameSpace + ".getAllCodeList");
    }

}
