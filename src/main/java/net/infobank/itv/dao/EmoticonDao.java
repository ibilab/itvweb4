package net.infobank.itv.dao;

import net.infobank.itv.form.EmoticonDto;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmoticonDao {
	private final SqlSession sqlSession;

	public EmoticonDao(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	public int insertEmoticon(EmoticonDto dto){
		return sqlSession.insert("emoticon.insertEmoticon", dto);
	}

	public List<EmoticonDto> selectEmoticonList(EmoticonDto dto){
		return sqlSession.selectList("emoticon.selectEmoticonList", dto);
	}

	public int selectDuplicate(EmoticonDto dto){
		return sqlSession.selectOne("emoticon.selectDuplicate", dto);
	}

	public int selectTotalCount(){
		return sqlSession.selectOne("emoticon.selectTotalCount");
	}

	public int updateEmoticon(EmoticonDto dto){
		return sqlSession.update("emoticon.updateEmoticon", dto);
	}

	public int deleteEmoticon(int idx) {
		return sqlSession.delete("emoticon.deleteEmoticon", idx);
	}
}
