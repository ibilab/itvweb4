package net.infobank.itv.dao;

import lombok.AllArgsConstructor;
import net.infobank.itv.form.EntireDto;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@AllArgsConstructor
public class EntireDao {
	private final SqlSession session;

	public List<Integer> selectProgramKeyList(int chKey){
		return session.selectList("entire.selectProgramKeyList", chKey);
	}

	public List<EntireDto> selectBoardList(EntireDto dto) {
		return session.selectList("entire.selectBoardList", dto);
	}

	public int selectTotalCount(EntireDto dto){
		return session.selectOne("entire.selectTotalCount", dto);
	}
}
