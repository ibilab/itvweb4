package net.infobank.itv.dao;

import net.infobank.itv.form.ImBoardInfoForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ImBoardInfoDao {
    String nameSpace = "ImBoardInfo";

    @Autowired
    private SqlSession sqlSession;


    public List<ImBoardInfoForm> list(int pgm_key) {
        return sqlSession.selectList(nameSpace + ".list", pgm_key);
    }

    public int count(ImBoardInfoForm form) {
        return sqlSession.selectOne(nameSpace + ".count", form);
    }

    public void update(ImBoardInfoForm form) {
        sqlSession.update(nameSpace + ".update", form);
    }
    public int insert(ImBoardInfoForm form) {
        return sqlSession.insert(nameSpace + ".insert", form);
    }

}
