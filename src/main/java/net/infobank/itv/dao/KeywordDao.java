package net.infobank.itv.dao;

import net.infobank.itv.form.KeywordForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KeywordDao {
    String nameSpace = "Keyword";

    @Autowired
    private SqlSession sqlSession;

    public List<KeywordForm> list(KeywordForm form) {
        return sqlSession.selectList(nameSpace + ".list", form);
    }

    public KeywordForm get(KeywordForm form) {
        return sqlSession.selectOne(nameSpace + ".get", form);
    }

    public void delete(String keyword_idx) {
        sqlSession.update(nameSpace + ".delete", keyword_idx);
    }

    public void insert(KeywordForm form) {
        sqlSession.insert(nameSpace + ".insert", form);
    }

    public void update(KeywordForm form) {
        sqlSession.update(nameSpace + ".update", form);
    }

    public int filterDupCount(KeywordForm form) {
        return sqlSession.selectOne(nameSpace + ".getDupCount", form);
    }
}
