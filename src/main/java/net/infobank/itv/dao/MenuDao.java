package net.infobank.itv.dao;

import net.infobank.itv.form.MenuForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MenuDao {
    String nameSpace = "Menu";

    @Autowired
    private SqlSession sqlSession;

    public List<MenuForm> getTopMenuList(int group_level) {
        return sqlSession.selectList(nameSpace + ".getTopMenuList", group_level);
    }

    public List<MenuForm> getFullMenuList(int group_level) {
        return sqlSession.selectList(nameSpace + ".getFullMenuList", group_level);
    }
}
