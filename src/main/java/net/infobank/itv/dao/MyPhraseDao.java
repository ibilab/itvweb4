package net.infobank.itv.dao;

import net.infobank.itv.form.MyPhraseForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MyPhraseDao {
    String nameSpace = "MyPhrase";

    @Autowired
    private SqlSession sqlSession;

    public List<MyPhraseForm> list(int pgm_key) {
        return sqlSession.selectList(nameSpace + ".list", pgm_key);
    }

    public MyPhraseForm get(MyPhraseForm form) {
        return sqlSession.selectOne(nameSpace + ".get", form);
    }

    public void delete(int myphrase_idx) {
        sqlSession.update(nameSpace + ".delete", myphrase_idx);
    }

    public void insert(MyPhraseForm form) {
        sqlSession.insert(nameSpace + ".insert", form);
    }

    public void update(MyPhraseForm form) {
        sqlSession.update(nameSpace + ".update", form);
    }
}
