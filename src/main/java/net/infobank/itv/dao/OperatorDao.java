package net.infobank.itv.dao;

import net.infobank.itv.form.OperatorForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OperatorDao {
    String nameSpace = "Operator";

    private final SqlSession sqlSession;

    public OperatorDao(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    public OperatorForm findById(String oper_id) {
        return sqlSession.selectOne(nameSpace+ ".findById", oper_id);
    }

    public int updateOperPW(String id, String pw) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("oper_id", id);
        map.put("oper_pw", pw);
        return sqlSession.update(nameSpace+ ".updateOperPW", map);
    }

    public int insertOperator(OperatorForm oper){
        return sqlSession.insert(nameSpace + ".insertOperator", oper);
    }

    public int updateOperator(OperatorForm opf){
        return sqlSession.update(nameSpace + ".updateOperator", opf);
    }

    public int updateUserStatus(String id){
        return sqlSession.update(nameSpace + ".updateUserStatus", id);
    }

    public List<OperatorForm> selectUserList(Map<String, Object> map){
        return sqlSession.selectList(nameSpace + ".selectUserList", map);
    }

    public int selectUserCount(Map<String, Object> map) {
        return sqlSession.selectOne(nameSpace + ".selectUserCount", map);
    }

    public List<OperatorForm> selectGroupList(int groupLevel){
        return sqlSession.selectList(nameSpace + ".selectGroupList", groupLevel);
    }

    public int selectDuplicateUser(String operId){
        return sqlSession.selectOne(nameSpace + ".selectDuplicateUser", operId);
    }
}
