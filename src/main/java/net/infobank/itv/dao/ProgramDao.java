package net.infobank.itv.dao;

import net.infobank.itv.form.ProgramForm;
import net.infobank.itv.form.ProgramLightForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class ProgramDao {
    String nameSpace = "Program";

    @Autowired
    private SqlSession sqlSession;

    public List<ProgramForm> getTopProgramList(int ch_key) {
        return sqlSession.selectList(nameSpace + ".topList", ch_key);
    }
    public List<ProgramLightForm> getLightList(int ch_key) {
        return sqlSession.selectList(nameSpace + ".lightList", ch_key);
    }

    public List<Integer> selectProgramKeyList(int chKey) {
        return sqlSession.selectList(nameSpace + ".selectProgramKeyList", chKey);
    }

    public ProgramForm getChInfo(int pgm_key) {
        return sqlSession.selectOne(nameSpace + ".getChInfo", pgm_key);
    }

    public ProgramForm getProgramInfo(int pgm_key) {
        return sqlSession.selectOne(nameSpace + ".getProgramInfo", pgm_key);
    }

    public List<ProgramForm> list(ProgramForm form) {
        return sqlSession.selectList(nameSpace + ".list", form);
    }

    public List<ProgramForm> getDupList(HashMap<String, String> map) {
        return sqlSession.selectList(nameSpace + ".dupList", map);
    }

    public int insert(ProgramForm form) {
        sqlSession.insert(nameSpace + ".insert", form);
        return sqlSession.selectOne(nameSpace + ".lastId");
    }

    public void update(ProgramForm form) {
        sqlSession.update(nameSpace + ".update", form);
    }

    public void delete(int pgm_key) {
        sqlSession.update(nameSpace + ".delete", pgm_key);
    }

    public void endListUpdate() {
        sqlSession.update(nameSpace + ".endListUpdate");
    }
}
