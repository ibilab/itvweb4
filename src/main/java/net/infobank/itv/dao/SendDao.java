package net.infobank.itv.dao;

import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.SendDto;
import net.infobank.itv.form.SendForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class SendDao {
    String nameSpace = "Send";

    @Autowired
    private SqlSession sqlSession;

    public List<SendForm> searchSendList(SendForm form) {
        return sqlSession.selectList(nameSpace + ".searchSendList", form);
    }

    public int countSendList(SendForm form) {
        return sqlSession.selectOne(nameSpace + ".countSendList", form);
    }

    public int sendMsg(BoardForm form, String oper_id) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", form.getPgm_key());
        map.put("user_id", form.getMsg_userid());
        map.put("msg_com", form.getMsg_com());
        map.put("mt_msg", form.getMt_msg());
        map.put("comment_id", form.getComment_id());
        map.put("msg_key", form.getMsg_key());
        map.put("oper_id", oper_id);
        return sqlSession.insert(nameSpace + ".sendMsg", map);
    }

    public int insertSendMsg(List<SendDto> list) {
        return sqlSession.insert(nameSpace + ".insertSendMsg", list);
    }


    public String checkTable(String table) {
        return sqlSession.selectOne(nameSpace + ".checkTable", table);
    }
}
