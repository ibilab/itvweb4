package net.infobank.itv.dao;

import net.infobank.itv.form.TwitterDto;
import net.infobank.itv.form.YoutubeForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SnsDao {
    String nameSpace = "Sns";


    private final SqlSession sqlSession;

    public SnsDao(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    public List<YoutubeForm> searchYoutubeList(YoutubeForm form) {
        return sqlSession.selectList(nameSpace + ".searchYoutube", form);
    }

    public int countYoutube(YoutubeForm form) {
        return sqlSession.selectOne(nameSpace + ".countYoutube", form);
    }

    public TwitterDto selectTwitter(int programKey){
        return  sqlSession.selectOne(nameSpace + ".selectTwitterInfo", programKey);
    }

    public int insertTwitter(TwitterDto dto) {
        return sqlSession.insert(nameSpace + ".insertTwitter", dto);
    }

    public int updateTwitter(TwitterDto dto) {
        return sqlSession.update(nameSpace + ".updateTwitterInfo", dto);
    }

    public int deleteTwitter(int programKey){
        return sqlSession.delete(nameSpace + ".deleteTwitter", programKey);
    }

}
