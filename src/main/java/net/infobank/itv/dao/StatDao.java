package net.infobank.itv.dao;

import net.infobank.itv.form.StatForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatDao {
    String nameSpace = "Stat";

    @Autowired
    private SqlSession sqlSession;

    public List<StatForm> statDay(StatForm form) {
        return sqlSession.selectList(nameSpace + ".searchStatDay", form);
    }

    public List<StatForm> statPgm(StatForm form) {
        return sqlSession.selectList(nameSpace + ".searchStatPgm", form);
    }

    public List<StatForm> statComDay(StatForm form) {
        return sqlSession.selectList(nameSpace + ".searchStatComDay", form);
    }
}
