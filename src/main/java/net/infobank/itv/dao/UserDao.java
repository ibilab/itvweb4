package net.infobank.itv.dao;

import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.UserForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
public class UserDao {
    String nameSpace = "User";

    @Autowired
    private SqlSession sqlSession;

    public String checkAllowMt(BoardForm form) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", form.getPgm_key());
        map.put("user_id", form.getMsg_userid());
        map.put("msg_com", form.getMsg_com());
        return sqlSession.selectOne(nameSpace + ".checkAllowMt", map) + "";
    }

    public int setUserImage(String pgm_key, String user_id, String msg_com, String user_pic) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("user_pic", user_pic);
        return sqlSession.update(nameSpace + ".setUserImage", map);
    }

    public UserForm getUserInfo(String pgm_key, String user_id, String msg_com) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        return sqlSession.selectOne(nameSpace + ".getUserInfo", map);
    }

    public int updateUserEditNick(String pgm_key, String msg_com, String user_id, String edit_nick) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("edit_nick", edit_nick);
        return sqlSession.update(nameSpace + ".updateUserEditNick", map);
    }

    public int updateUserAllowMt(String pgm_key, String msg_com, String user_id, String allow_mt) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("allow_mt", allow_mt);
        return sqlSession.update(nameSpace + ".updateUserAllowMt", map);
    }

    public int updateUserShowMsg(String pgm_key, String msg_com, String user_id, String show_msg) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("show_msg", show_msg);
        return sqlSession.update(nameSpace + ".updateUserShowMsg", map);
    }

    public int updateUserNick(String pgm_key, String msg_com, String user_id, String user_nick) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("user_nick", user_nick);
        return sqlSession.update(nameSpace + ".updateUserNick", map);
    }

    public int updateUserInfo(String pgm_key, String msg_com, String user_id, String user_info) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("user_info", user_info);
        return sqlSession.update(nameSpace + ".updateUserInfo", map);
    }

    public int updateUserIcon(String pgm_key, String msg_com, String user_id, String user_icon) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("user_icon", user_icon);
        return sqlSession.update(nameSpace + ".updateUserIcon", map);
    }

    public int updateUserGift(String pgm_key, String msg_com, String user_id, String user_image) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("pgm_key", pgm_key);
        map.put("user_id", user_id);
        map.put("msg_com", msg_com);
        map.put("user_image", user_image);
        return sqlSession.update(nameSpace + ".updateUserGift", map);
    }

    public void createUserTable(int pgm_key) {
        sqlSession.update(nameSpace + ".createUserTable", pgm_key);
    }
}
