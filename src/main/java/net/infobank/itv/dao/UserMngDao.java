package net.infobank.itv.dao;

import lombok.AllArgsConstructor;
import net.infobank.itv.form.UserDto;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@AllArgsConstructor
public class UserMngDao {
	private final SqlSession session;

	public List<UserDto> selectAttendUserList(UserDto dto){
		return session.selectList("User.selectAttendUserList", dto);
	}

	public int selectAttendUserCount(UserDto dto){
		return session.selectOne("User.selectAttendUserCount", dto);
	}

	public List<String> selectMessageSendList(UserDto dto){
		return session.selectList("User.selectMessageSendList", dto);
	}
}
