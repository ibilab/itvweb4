package net.infobank.itv.dao;

import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.VoteForm;
import net.infobank.itv.form.VoteResultForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VoteDao {
    String nameSpace = "Vote";

    @Autowired
    private SqlSession sqlSession;

    public List<VoteForm> searchVote(VoteForm form) {
        return sqlSession.selectList(nameSpace + ".searchVote", form);
    }

    public int countVote(VoteForm form) {
        return sqlSession.selectOne(nameSpace + ".countVote", form);
    }

    public VoteForm get(VoteForm form) {
        return sqlSession.selectOne(nameSpace + ".get", form);
    }

    public void updateStatus(VoteForm form) {
        sqlSession.update(nameSpace + ".updateStatus", form);
    }

    public void delete(VoteForm form) {
        sqlSession.update(nameSpace + ".delete", form);
    }

    public void insert(VoteForm form) {
        sqlSession.insert(nameSpace + ".insert", form);
    }

    public void update(VoteForm form) {
        sqlSession.update(nameSpace + ".update", form);
    }

    public List<VoteResultForm> resultList(int vote_key) {
        return sqlSession.selectList(nameSpace + ".resultList", vote_key);
    }
}
