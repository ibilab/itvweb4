package net.infobank.itv.dao;

import net.infobank.itv.form.LimitWordForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WordDao {
    String nameSpace = "Word";

    @Autowired
    private SqlSession sqlSession;

    public List<LimitWordForm> searchLimitWord(LimitWordForm form) {
        return sqlSession.selectList(nameSpace + ".searchLimitWord", form);
    }

    public int countLimitWord(LimitWordForm form) {
        return sqlSession.selectOne(nameSpace + ".countLimitWord", form);
    }

    public void deleteLimitWord(int lword_key) {
        sqlSession.update(nameSpace + ".deleteLimitWord", lword_key);
    }

    public void insertLimitWord(LimitWordForm form) {
        sqlSession.insert(nameSpace + ".insertLimitWord", form);
    }
}
