package net.infobank.itv.form;

import lombok.Data;
import net.infobank.itv.common.ListForm;

import java.util.ArrayList;

@Data
public class BoardForm extends ListForm {

    // DB
    private int board_key;
    private int pgm_key;
    private String msg_type;
    private String msg_num;
    private String msg_emo;
    private String msg_username;
    private String msg_userid;
    private String msg_com;
    private String msg_date;
    private String msg_title;
    private String msg_data;
    private int msg_count;
    private String msg_mmsurl;
    private int msg_show;
    private int msg_read;
    private int msg_rtn;
    private int emo_type;
    private int emo_code;
    private int emo_class;

    // select
    private int mo_day;
    private int mo_month;
    private int mo_total;
    private String table_name;
    private String user_icon;
    private int user_grade;
    private String comment_id;
    private String user_phone_dash;
    private String user_nick;
    private String user_image;
    private String msg_time;
    private String[] chk_board_key;
    private String msg_key;

    private int tot_cnt2;

    // TODO delete
    private String show_last_4_number;
    private String last_phone_number;

    // search
    private int st_type;
    private int show_hide_msg;
    private int read_msg_table;
    private String start_dt;
    private String start_hh;
    private String start_mi;
    private String end_dt;
    private String end_hh;
    private String end_mi;
    private String search_new_user;
    private ArrayList<String> month_list;

    // mt
    private String mt_msg;
}
