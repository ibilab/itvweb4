package net.infobank.itv.form;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChannelForm implements Serializable {

    private int ch_key;
    private String ch_name;
    private String ch_info;
    private String billing_code;

    private String br_key;
    private final static long serialVersion = 1L;
}
