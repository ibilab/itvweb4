package net.infobank.itv.form;

import lombok.Data;

@Data
public class CodeForm {
    private String group_cd;
    private String code;
    private String code_nm;
    private String code_info;
    private int disp_order;
    private String use_yn="Y";
}
