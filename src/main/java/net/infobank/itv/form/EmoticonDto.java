package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.infobank.itv.common.ListForm;

@Data
@AllArgsConstructor
public class EmoticonDto extends ListForm {
	private String emoIdx;
    private String emoClass;
	private String emoCode;
	private String emoPath;
	private String emoUse;
	private String emoRegdate;

	public EmoticonDto (){
		super();
	}
}
