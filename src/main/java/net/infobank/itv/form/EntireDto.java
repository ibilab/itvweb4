package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.infobank.itv.common.ListForm;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntireDto extends ListForm {
	private int boardKey;
	private int chKey;
	private String commentId;
	private String lastPhoneNumber;
	private String moDay;
	private String moTotal;
	private String msgCom;
	private String msgData;
	private String msgMmsurl;
	private String msgTime;
	private String msgUserid;
	private String msgUsername;
	private int pgmKey;
	private String pgmName;
	private String userGrade;
	private String userIcon;
	private String userImage;
	private String userNick;
	private String userPhoneDash;
	private String userPic;
	private String startDate;
	private String endDate;
	private String searchCondition;
	private String searchValue;
	private String searchField;
	private String excel;
	private List<String> boardList;
	private List<Integer> pgmKeyList;
}
