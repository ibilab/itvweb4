package net.infobank.itv.form;

import lombok.Data;

@Data
public class ImBoardInfoForm {

    private int idx_im;
    private String msg_com;
    private String prog_cd;
    private String prog_name;
    private String multi_id;

    private int pgm_key;
    private int isvalid;
    private int last_key;

}
