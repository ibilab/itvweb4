package net.infobank.itv.form;

import lombok.Data;
import net.infobank.itv.common.ListForm;

@Data
public class KeywordForm extends ListForm {

    private int keyword_idx;
    private int pgm_key;
    private String oper_id;
    private String mo_word;
    private String mt_subject;
    private String mt_msg;
    private String mt_use;
    private String regdate;
}
