package net.infobank.itv.form;

import lombok.Data;
import net.infobank.itv.common.ListForm;

@Data
public class LimitWordForm extends ListForm {
    private int lword_key;
    private int pgm_key;
    private String word_limt;
    private String word_replace;
    private String word_use;
}
