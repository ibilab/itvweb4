package net.infobank.itv.form;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class MenuForm implements Serializable {
    private final static long serialVersion = 1L;
    private int menu_key;
    private int menu_level;
    private String menu_name;
    private String menu_url;
    private String menu_info;
    private String menu_order;
    private String menu_group;

    private boolean topMenu;
    private boolean middleMenu;

    public boolean isTopMenu() {
        return menu_order.endsWith("000");
    }

    public boolean isMiddleMenu() {
        return menu_order.endsWith("00");
    }
}
