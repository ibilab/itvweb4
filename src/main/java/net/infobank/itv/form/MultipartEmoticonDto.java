package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
public class MultipartEmoticonDto {
	private String operId;
	private int pgmKey;
	private String uploadFilePath;
	private MultipartFile file;
	private String realFolder;
}
