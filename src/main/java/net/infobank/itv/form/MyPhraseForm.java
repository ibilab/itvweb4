package net.infobank.itv.form;

import lombok.Data;

@Data
public class MyPhraseForm {

    private int myphrase_idx;
    private int pgm_key;
    private String oper_id;
    private String title;
    private String msg;
    private String regdate;
}
