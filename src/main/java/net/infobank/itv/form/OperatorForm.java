package net.infobank.itv.form;

import lombok.Data;
import lombok.ToString;
import net.infobank.itv.common.ListForm;

@Data
@ToString
public class OperatorForm extends ListForm {
    private String oper_id;
    private String group_key;
    private int pgm_key;
    private String oper_name;
    private String oper_pw;
    private int sns_count;
    private String oper_sns;
    private String oper_use;
    private int group_level;
    private String group_name;
    private String sns_list;
    private String oper_info;

    // 채널/프로그램 세팅에 따라 값이 유동적
    private String br_key;
    private String billing_code;
    private int ch_key;         // 로그인시 현재 진행 채널...
    private String ch_name;
    private String pgm_name;    // 로그인시 현재 진행 프로그램명...
    private String pgm_sns;
    private String pgm_mo;
}
