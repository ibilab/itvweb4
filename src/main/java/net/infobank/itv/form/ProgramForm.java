package net.infobank.itv.form;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProgramForm implements Serializable {
    private final static long serialVersion = 1L;
    private int pgm_key;
    private String pgm_name;
    private int ch_key;
    private String pgm_mo;
    private String pgm_emo;
    private String pgm_stm;
    private String pgm_etm;
    private String pgm_week;

    private String pgm_sdate;
    private String pgm_edate;

    private String reserved = "0";
    private int sns_count;

    private String isvalid = "1";
    private String mac_use;

    private String br_key;
    private String sns_list;

    private List<ImBoardInfoForm> imBoardInfoList;

    public String getStart_hh() {
        return this.pgm_stm.substring(0, 2);
    }
    public String getStart_mi() {
        return this.pgm_stm.replace(":", "").substring(2, 4);
    }
    public String getEnd_hh() {
        return this.pgm_etm.substring(0, 2);
    }
    public String getEnd_mi() {
        return this.pgm_etm.replace(":", "").substring(2, 4);
    }
}
