package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProgramLightForm {
	private String pgmKey;
	private String pgmName;
	private String pgmStm;
}
