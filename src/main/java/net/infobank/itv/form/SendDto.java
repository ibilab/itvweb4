package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendDto {
	private int idx;
	private int chKey;
	private int pgmKey;
	private String msgKey;
	private String msgCom;
	private String userId;
	private String title;
	private String msg;
	private int imageCount;
	private String imageMmsurl;
	private String imageMmsfname;
	private String operId;
	private String regdate;
	private String tranStatus;
	private String callback;
	private String refKey;
	private String commentId;
}
