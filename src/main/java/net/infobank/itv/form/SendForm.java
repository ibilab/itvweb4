package net.infobank.itv.form;

import lombok.Data;
import net.infobank.itv.common.ListForm;

import java.util.ArrayList;

@Data
public class SendForm extends ListForm {

    private int send_key;
    private int ch_key;
    private int pgm_key;
    private String msg_com;
    private String user_id;
    private String title;
    private String msg;
    private String comment_id;
    private String regdate;

    private String user_nick;
    private String user_icon;
    private String user_grade;
    private String user_name;

    // search
    private String start_dt;
    private String start_hh;
    private String start_mi;
    private String end_dt;
    private String end_hh;
    private String end_mi;
    private String excel = "N";
    private ArrayList<String> month_list;

    private String table_name;
}
