package net.infobank.itv.form;

import lombok.Data;

@Data
public class StatForm {

    // DB
    private int idx;
    private int ch_key;
    private int pgm_key;
    private String msg_com;
    private String stat_new;
    private String stat_tot;
    private String stat_mo;
    private String stat_mt;
    private String stat_date;

    // STAT 결과
    private String reg_day;
    private String pgm_name;
    private String msg_com_nm;
    private int mo_cnt;
    private int mt_cnt;
    private int mo_avg_cnt;
    private int new_mem_cnt;
    private int old_mem_cnt;
    private int mo_dis_cnt;


    // Search
    private String start_dt;
    private String end_dt;
    private String group_by = "";
}
