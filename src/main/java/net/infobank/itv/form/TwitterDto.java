package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TwitterDto {
	private int pgmKey;
	private String twUserId;
	private String twUserName;
	private String twAccessToken;
	private String twAccessTokenSecret;
	private String twCustomerKey;
	private String twCustomerSecret;
}
