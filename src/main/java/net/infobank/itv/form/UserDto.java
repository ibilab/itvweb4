package net.infobank.itv.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.infobank.itv.common.ListForm;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends ListForm {
	private String userId;
	private String userName;
	private int pgmKey;
	private String msgCom;
	private String userIcon;
	private String userNick;
	private String userRegDt;
	private String userGrade;
	private int allowMt;
	private String moDay;
	private String moMonth;
	private String moTotal;
	private String mtDay;
	private String mtMonth;
	private String mtTotal;
	private String showMsg;
	private String userPic;
	private String userInfo;
	private String preloginDt;
	private String lastloginDt;
	private String editNick;
	private String userImage;
	private String userIdx;
	private String countryIso;
	private String state;
	private String lastModify;
	private String lastUpdateField;
	private String userAgreement1;
	private String userAgreement2;
	private String userAttendType;
	private String userAttendLocation;
	private String userBirth;
	private String startDate;
	private String endDate;
	private String searchMsgCom;
	private String searchPeriodName;
	private String searchLogin;
	private String searchKeyword;
	private String excel;
	private String codeName;
}
