package net.infobank.itv.form;

import lombok.Data;

@Data
public class UserForm {

    private int pgm_key;
    private String user_id;
    private String user_name;
    private String msg_com;
    private String user_nick;
    private int mo_day;
    private int mo_month;
    private int mo_total;
    private String user_reg_dt;
    private String lastlogin_dt;
    private String user_icon;
    private String user_image;
    private String user_grade;
    private String user_info;
    private String edit_nick;
    private String allow_mt;
    private String show_msg;
    private String user_pic;
    private String pz_day;
    private String pz_month;
    private String pz_total;

}
