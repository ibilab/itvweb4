package net.infobank.itv.form;

import lombok.Data;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.common.ListForm;
import net.infobank.itv.config.AofCode;

@Data
public class VoteForm extends ListForm {

    private int vote_key = 0;
    private int pgm_key;
    private String vote_regdate;
    private String vote_stm;
    private String vote_etm;
    private String vote_title;
    private int vote_count;
    private String vote_items;
    private String vote_keywords;

    private String vote_status;
    private String vote_status2;

    private int vote_emo;
    private int vote_user_dup;
    private int sub_key = 0;
    private String sns_list;
    private int sns_count;
    private String isvalid;

    private String start_dt;
    private String end_dt;

    public String getStart_hh() {
        if(this.vote_stm.length() > 9) {
            return this.vote_stm.substring(8, 10);
        } else {
            return "";
        }
    }
    public String getStart_mi() {
        if(this.vote_stm.length() > 11) {
            return this.vote_stm.substring(10, 12);
        } else {
            return "";
        }
    }
    public String getStart_ss() {
        if(this.vote_stm.length() > 13) {
            return this.vote_stm.substring(12, 14);
        } else {
            return "";
        }
    }

    public String getEnd_hh() {
        if(this.vote_etm.length() > 9) {
            return this.vote_etm.substring(8, 10);
        } else {
            return "";
        }
    }
    public String getEnd_mi() {
        if(this.vote_etm.length() > 11) {
            return this.vote_etm.substring(10, 12);
        } else {
            return "";
        }
    }
    public String getEnd_ss() {
        if(this.vote_etm.length() > 13) {
            return this.vote_etm.substring(12, 14);
        } else {
            return "";
        }
    }

    public String getVote_status_name() {
        return AofCode.getCode(AofCodes.VOTE_STATUS_NAME, this.vote_status).getCode_nm();
    }
}
