package net.infobank.itv.form;

import lombok.Data;

import java.text.DecimalFormat;

@Data
public class VoteResultForm {

    // DB
    private int vote_key = 0;
    private String msg_com;
    private String vote_rslt;

    // 결과 표시 내용
    private String vote_item;
    private String vote_result;
    private int rank = 1;
    private int total = 0;


    public int getSum() {
        String[] results = this.vote_result.split("\\|");
        int sum = 0;
        for (String result : results) {
            sum += Integer.parseInt(result);
        }
        return sum;
    }

    public String getRatio() {
        DecimalFormat df = new DecimalFormat("#####.##");
        double dou = 0.0;
        if(getSum() != 0 && this.total != 0 ) {
            dou =  (double)(getSum() / this.total) * 100;
        }
        return df.format(dou) + "%";
    }
}
