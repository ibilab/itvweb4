package net.infobank.itv.form;

import lombok.Data;
import net.infobank.itv.common.AofCodes;
import net.infobank.itv.common.ListForm;
import net.infobank.itv.config.AofCode;

@Data
public class YoutubeForm extends ListForm {

    private int live_key;
    private int pgm_key;
    private String live_regdate;
    private String live_stm;
    private String live_etm;
    private String live_status;     // 상태 1: 대기, 2:진행, 3:종료
    private String msg_ltm;
    private String video_code;

    public String getLive_status_name() {
        return AofCode.getCode(AofCodes.VOTE_STATUS_NAME, this.live_status).getCode_nm();
    }
}
