package net.infobank.itv.service;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.dao.BoardDao;
import net.infobank.itv.dao.SendDao;
import net.infobank.itv.dao.UserDao;
import net.infobank.itv.form.BoardForm;
import net.infobank.itv.util.DateUtil;
import net.infobank.itv.util.StringUtil;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Service
public class BoardService {

	private final BoardDao boardDao;
	private final UserDao userDao;
	private final SendDao sendDao;

	public BoardService(BoardDao boardDao, UserDao userDao, SendDao sendDao) {
		this.boardDao = boardDao;
		this.userDao = userDao;
		this.sendDao = sendDao;
	}

	public BoardForm getBoardList(BoardForm form) throws Exception {
		setSearchBoard(form);
		String excel = form.getExcel();

		form.setList(boardDao.searchBoard(form));
		if(!"Y".equals(excel)) {
			form.setTotal_row_count(boardDao.countBoard(form));
			form.setTot_cnt2(boardDao.distinctBoard(form));
		} else {
			form.setTotal_row_count(form.getList().size());
		}
		return form;
	}

	public void setSearchBoard(BoardForm form) throws Exception {
		form.setMonth_list(getMonth_list(form));

		form.setStart_dt(form.getStart_dt().replaceAll("-","")+form.getStart_hh() + form.getStart_mi());
		form.setEnd_dt(form.getEnd_dt().replaceAll("-","")+ form.getEnd_hh() + form.getEnd_mi());
	}

	public BoardForm getBoardListEachUser(BoardForm form) throws Exception {
		String month = form.getStart_dt();
		ArrayList<String> month_list = new ArrayList<>();
		if("last".equals(month)) {
			DateUtil dutil = new DateUtil();
			form.setEnd_dt(dutil.getString("yyyyMMdd") + "245959");
			dutil.addDay(-3);
			form.setStart_dt(dutil.getString("yyyyMMdd") + "000000");
			month_list.add("board");
		} else {
			form.setStart_dt(month + "01000000");
			form.setEnd_dt(month + "31245959");
			String chk = boardDao.checkTable("board_" + month);
			if(!StringUtil.isEmpty(chk)) {
				month_list.add("board_" + month);
			}
		}
		form.setShow_hide_msg(1);
		form.setMonth_list(month_list);

		if(month_list.size() > 0) {
			form.setList(boardDao.searchBoard(form));
			form.setTotal_row_count(boardDao.countBoard(form));
			form.setTot_cnt2(boardDao.distinctBoard(form));
		}

		return form;
	}

	public int setMsgRead(String table_name, String board_key, String msg_read) {
		HashMap<String, String> map = new HashMap<>();
		map.put("table_name", table_name);
		map.put("board_key", board_key);
		map.put("msg_read", msg_read);
		int result = boardDao.setMsgRead(map);
		return result;
	}

	// select 할 board_ 목록 가져오기
	public ArrayList<String> getMonth_list(BoardForm form) throws Exception {
		ArrayList<String> month_list = new ArrayList<>();
		month_list.add("board");

		LocalDate curDate = LocalDate.now();
		LocalDate startDate = LocalDate.parse(form.getStart_dt(), DateTimeFormatter.ISO_DATE);
		LocalDate endDate = LocalDate.parse(form.getEnd_dt(), DateTimeFormatter.ISO_DATE);

        if(ChronoUnit.DAYS.between(startDate, curDate) > 2 || ChronoUnit.DAYS.between(endDate, curDate) > 2) {

            DateUtil stDate = new DateUtil();
            DateUtil edDate = new DateUtil();
            DateUtil posMon = edDate;

            stDate.setYMD(form.getStart_dt(), "yyyy-MM-dd");
            edDate.setYMD(form.getEnd_dt(), "yyyy-MM-dd");
            if(startDate.getMonth() != curDate.getMonth() && endDate.getMonth() != curDate.getMonth()) {
                month_list.remove("board");
            }

            for(; stDate.getString("yyyyMM").compareTo(posMon.getString("yyyyMM")) <= 0; posMon.addMonth(-1)) {
                String tName = "board_" + posMon.getString("yyyyMM");

                String chk = boardDao.checkTable(tName);
                if(!StringUtil.isEmpty(chk)) {
                    month_list.add(tName);
                }
            }
		}

		return month_list;
	}

	public int addPrize(String pgm_key, String msg_com, String user_id) {
		HashMap<String, String> map = new HashMap<>();
		map.put("pgm_key", pgm_key);
		map.put("msg_com", msg_com);
		map.put("user_id", user_id);

		int result = boardDao.addPrize(map);
		return result;
	}

	public int copyMsg(String table_name, String board_key, String save_type, String save_box_type) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("table_name", table_name);
		map.put("board_key", board_key);
		map.put("save_type", save_type);
		map.put("save_box_type", save_box_type);

		int result = boardDao.copyMsg(map);

		if(result > 0 && "move".equals(save_type)) {
			map.put("msg_show", "0");
			boardDao.showMsg(map);
		}
		return result;
	}

	@Transactional
	public void copySelectMsg(String[] chk_board_key, String save_type, String save_box_type) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("save_type", save_type);
		map.put("save_box_type", save_box_type);

		try {
			for (int i = 0; i < chk_board_key.length; i++) {
				String board_key = chk_board_key[i].split(";")[0];
				String table_name = chk_board_key[i].split(";")[2];
				map.put("table_name", table_name);
				map.put("board_key", board_key);
				int result = boardDao.copyMsg(map);

				if (result > 0 && "move".equals(save_type)) {
					map.put("msg_show", "0");
					boardDao.showMsg(map);
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return;
	}

	public int showMsg(String table_name, String board_key, String msg_show) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("table_name", table_name);
		map.put("board_key", board_key);
		map.put("msg_show", msg_show);

		int result = boardDao.showMsg(map);

		return result;
	}

	public BoardForm getSaveBoardList(BoardForm form, String st_type) throws Exception {
		String excel = form.getExcel();

		form.setList(boardDao.searchSaveBoard(form));
		if(!"Y".equals(excel)) {
			form.setTotal_row_count(boardDao.countSaveBoard(form));
		} else {
			form.setTotal_row_count(form.getList().size());
		}

		return form;
	}

	public void getSaveBoxMsgList(BoardForm form, String selected) {

		if(!"Y".equals(selected)) {
			form.setChk_board_key(null);
		} else {
			String[] chk_board_key = form.getChk_board_key();
			String[] board_keys = new String[chk_board_key.length];
			ArrayList<BoardForm> list = new ArrayList<BoardForm>();

			for (int i=0 ; i<chk_board_key.length ; i++) {
				String[] chkKey = chk_board_key[i].split(";");
				board_keys[i] = chkKey[0];
			}
			form.setChk_board_key(board_keys);
		}

		List<BoardForm> list = boardDao.searchSaveBoardUsers(form);
		form.setList(list);
		form.setTotal_row_count(list.size());

		return;
	}

	public void deleteSaveBoxAllMsg(String pgm_key, String st_type) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("pgm_key", pgm_key);
		map.put("st_type", st_type);
		boardDao.deleteSaveBoxAllMsg(map);
	}
}
