package net.infobank.itv.service;

import net.infobank.itv.dao.ChannelDao;
import net.infobank.itv.form.ChannelForm;
import net.infobank.itv.form.OperatorForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChannelService {

    @Autowired
    private ChannelDao channelDAO;

    public List<ChannelForm> getTopChannelList(OperatorForm operator) {
        return channelDAO.getTopChannelList(operator);
    }

    public ChannelForm getChInfo(int ch_key) {
        return channelDAO.getChInfo(ch_key);
    }
}
