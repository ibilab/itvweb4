package net.infobank.itv.service;

import lombok.RequiredArgsConstructor;
import net.infobank.itv.dao.CodeDao;
import net.infobank.itv.form.CodeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CodeService {

    @Autowired
    private CodeDao codeDao;

    public List<CodeForm> getAllCodeList() {
        return codeDao.getAllCodeList();
    }

}
