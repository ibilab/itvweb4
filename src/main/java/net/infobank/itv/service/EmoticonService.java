package net.infobank.itv.service;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.dao.EmoticonDao;
import net.infobank.itv.form.EmoticonDto;
import net.infobank.itv.form.MultipartEmoticonDto;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
public class EmoticonService {
	private final String EMOTICON_SAVE_PATH = "/static/icon/emoticon/";
	private final String EMOTICON_REQUEST_PATH = "/icon/emoticon/";
	private final EmoticonDao dao;

	public EmoticonService(EmoticonDao dao) {
		this.dao = dao;
	}

	public EmoticonDto getList(EmoticonDto dto){
		final List<EmoticonDto> list = dao.selectEmoticonList(dto);
		dto.setList(list);
		dto.setTotal_row_count(dao.selectTotalCount());
		return dto;
	}

	public int addEmoticon(EmoticonDto dto){
		return dao.insertEmoticon(dto);
	}

	public int modifyEmoticon(EmoticonDto dto){
		return dao.updateEmoticon(dto);
	}

	public boolean check(EmoticonDto dto){
		int count = dao.selectDuplicate(dto);
		boolean result = true;
		if(count > 0) {
			result = false;
		}
		return result;
	}

	public int removeEmoticon(int idx, EmoticonDto dto){
		File file = new File(dto.getEmoPath());
		file.delete();
		return dao.deleteEmoticon(idx);
	}

	public String emoticonImageWrite(MultipartEmoticonDto dto){
		String realStaticPath = "";
		String fileName = "";
		String makeFileNaming = "";
		String newFileName = "";
		try {

			fileName = dto.getFile().getOriginalFilename();
			int type_idx = fileName.lastIndexOf(".");
			File folder = new File(dto.getRealFolder()+"/../emoticon");

			if(!folder.exists()){
				folder.mkdir();
			}

			String fileType_ = fileName.substring(type_idx);
			String fileName_ = fileName.substring(0, type_idx);
			String fileType_NoDot = fileName.substring(type_idx+1);

			// makeFileNaming = "selection_img_" + eventKey+ "_" + QuizModel.getTimeStamp();
			String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

			makeFileNaming = "emoticon_" + dto.getPgmKey() + "_" + dto.getOperId() + "_" + time;
			realStaticPath = folder.getAbsolutePath();

			// 저장된 원본 파일명
			String oldFilePath = realStaticPath + File.separator + fileName;

			// 파일 네이밍
			String newFilePath = realStaticPath + File.separator + makeFileNaming + fileType_;
			newFileName = makeFileNaming + fileType_;
			System.out.println(realStaticPath);
			System.out.println("- fileName : " + fileName);
			System.out.println("- makeFileNaming : " + makeFileNaming);
			System.out.println("- oldFile Path : " + oldFilePath);
			System.out.println("- newFile Path : " + newFilePath);
			System.out.println("- newFileName : " + newFileName);

			System.out.println("- operId : " + dto.getOperId());
			System.out.println("- pgmKey : " + dto.getPgmKey());
			File newFile = new File(newFilePath);
			dto.getFile().transferTo(newFile);
			//gif 아니면 리사이즈
			if(!fileType_NoDot.toLowerCase().equals("gif")) {
				BufferedImage imgSrc = ImageIO.read(new File(newFilePath));
				Image resizeImage = imgSrc.getScaledInstance(70, 70, Image.SCALE_DEFAULT);
				BufferedImage newImage = new BufferedImage(70, 70, BufferedImage.TYPE_INT_RGB);
				Graphics g = newImage.getGraphics();
				g.drawImage(resizeImage, 0, 0, null);
				g.dispose();
				ImageIO.write(newImage, fileType_NoDot, new File(newFilePath));
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return "";
		}
		return "/EmoticonAction.do/emoticon/" + newFileName;
	}

}
