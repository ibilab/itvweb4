package net.infobank.itv.service;

import lombok.AllArgsConstructor;
import net.infobank.itv.dao.EntireDao;
import net.infobank.itv.form.EntireDto;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class EntireService {
	private final EntireDao dao;
	private final ProgramService programService;

	public EntireDto getBoardList(EntireDto dto){
		final List<Integer> programKeyList = programService.getProgramKeyList(dto.getChKey());
		final List<String> boardList = new ArrayList<>();
		LocalDate curDate = LocalDate.now();
		LocalDate startDate = LocalDate.parse(dto.getStartDate(), DateTimeFormatter.ISO_DATE);
		LocalDate endDate = LocalDate.parse(dto.getEndDate(), DateTimeFormatter.ISO_DATE);

		if(startDate.getMonthValue() != curDate.getMonthValue() || endDate.getMonthValue() != curDate.getMonthValue()){
			if(startDate.getMonthValue() != endDate.getMonthValue()) {
				boardList.add("board_" + startDate.getYear() + (startDate.getMonthValue() < 10 ? "0" + startDate.getMonthValue() : startDate.getMonthValue()));
				boardList.add("board_" + endDate.getYear() + (endDate.getMonthValue() < 10 ? "0" + endDate.getMonthValue() : endDate.getMonthValue()));
			} else {
				boardList.add("board_" + startDate.getYear() + (startDate.getMonthValue() < 10 ? "0" + startDate.getMonthValue() : startDate.getMonthValue()));
			}
		} else {
			boardList.add("board");
		}
		if(dto.getSearchCondition().equals("DATA")) {
			dto.setSearchField("msg_data");
		} else if (dto.getSearchCondition().equals("NAME")){
			dto.setSearchField("msg_username");
		}
		dto.setBoardList(boardList);
		dto.setPgmKeyList(programKeyList);
		dto.setStartDate(dto.getStartDate().replaceAll("-", ""));
		dto.setEndDate(dto.getEndDate().replaceAll("-", ""));
		final List<EntireDto> list = dao.selectBoardList(dto);
		final int totalCount = dao.selectTotalCount(dto);
		dto.setList(list);
		dto.setTotal_row_count(totalCount);
		return dto;
	}


}
