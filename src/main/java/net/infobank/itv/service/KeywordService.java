package net.infobank.itv.service;

import net.infobank.itv.dao.KeywordDao;
import net.infobank.itv.dao.MyPhraseDao;
import net.infobank.itv.form.KeywordForm;
import net.infobank.itv.form.MyPhraseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KeywordService {

    @Autowired
    private KeywordDao keywordDao;

    public void list(KeywordForm form) {
        List<KeywordForm> list = keywordDao.list(form);
        form.setList(list);
        form.setTotal_row_count(list.size());
        return;
    }

    public KeywordForm get(KeywordForm form) {
        return keywordDao.get(form);
    }

    public void delete(String keyword_idx) {
        keywordDao.delete(keyword_idx);
    }

    public void insert(KeywordForm form) {
        keywordDao.insert(form);
    }

    public void update(KeywordForm form) {
        keywordDao.update(form);
    }

    public int filterDupCount(KeywordForm form) {
        return keywordDao.filterDupCount(form);
    }
}
