package net.infobank.itv.service;

import net.infobank.itv.dao.MenuDao;
import net.infobank.itv.form.MenuForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {

    @Autowired
    private MenuDao MenuDAO;

    public List<MenuForm> getTopMenuList(int group_level) {
        return MenuDAO.getTopMenuList(group_level);
    }

    public List<MenuForm> getFullMenuList(int group_level) {
        return MenuDAO.getFullMenuList(group_level);
    }
}
