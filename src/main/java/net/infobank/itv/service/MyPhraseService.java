package net.infobank.itv.service;

import net.infobank.itv.dao.MyPhraseDao;
import net.infobank.itv.form.MyPhraseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyPhraseService {

    @Autowired
    private MyPhraseDao myPhraseDao;

    public List<MyPhraseForm> list(int pgm_key) {
        return myPhraseDao.list(pgm_key);
    }

    public MyPhraseForm get(MyPhraseForm form) {
        return myPhraseDao.get(form);
    }

    public void delete(int myphrase_idx) {
        myPhraseDao.delete(myphrase_idx);
    }

    public void insert(MyPhraseForm form) {
        myPhraseDao.insert(form);
    }

    public void update(MyPhraseForm form) {
        myPhraseDao.update(form);
    }
}
