package net.infobank.itv.service;

import net.infobank.itv.dao.OperatorDao;
import net.infobank.itv.dao.ProgramDao;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.ProgramForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OperatorService {

    private final OperatorDao operatorDao;
    private final ProgramDao programDao;

    public OperatorService(OperatorDao operatorDao, ProgramDao programDao) {
        this.operatorDao = operatorDao;
        this.programDao = programDao;
    }

    public OperatorForm login(String oper_id) {
        OperatorForm operator = operatorDao.findById(oper_id);

        if(operator != null && operator.getPgm_key() != 0) {
            ProgramForm programForm = programDao.getChInfo(operator.getPgm_key());

            // TODO 프로그램 없을때 예외처리.... programForm == null;
            System.out.println(programForm);
            operator.setCh_key(programForm.getCh_key());
            operator.setBr_key(programForm.getBr_key());
            operator.setPgm_mo(programForm.getPgm_mo());

            // TODO 현재 시간에 맞는 pgm 가져와서 operator에 추가... controller? service?
            if(operator.getGroup_level() < 3) {

            }
//            a.pgm_name, a.ch_key, b.br_key, CAST(a.sns_list as char) as pgm_sns, b.billing_code
        }
        // TODO ch_key, br_key 등 세팅...
//        public LoginSession login(ActionForm form, int gb) throws Exception{
//            LoginSession user = null;
//
//            query = getQuery("member.login");
//
//            /*프로그램 세션 설정 CHECK*/
//            user = (LoginSession)dao.getBean(query, LoginSession.class, form);
//            if(user!=null && user.getPgm_key()!=null && !"".equals(user.getPgm_key())){
//                dao.setVO(getQuery("board.getPgm_name.ch_key"), user);
//                // 2010.07.23 chois79 현재 시간에 맞는 pgm 가져오기.
//                if(user.getGroup_level().compareTo("3") < 0) {
//                    dao.setVO(getQuery("board.getPgm_name.now_pgm"), user);
//                }
//
//            }
//            return user;
//        }

        return operator;
    }
    public int addUser(OperatorForm opf){
        return operatorDao.insertOperator(opf);
    }

    public int modifyUser(OperatorForm opf){
        return operatorDao.updateOperator(opf);
    }

    public int updateOperPW(String id, String pw) {
        return operatorDao.updateOperPW(id, pw);
    }

    public int modifyUserStatus(String id) {
        return  operatorDao.updateUserStatus(id);
    }

    public OperatorForm getOperatorFormList(OperatorForm opf, int chKey) {
        Map<String, Object> map = new HashMap<>();
        map.put("channelKey", chKey);
        map.put("groupLevel", opf.getGroup_level());
        map.put("start", opf.getStart_limit());
        map.put("end", opf.getRow_count());
        OperatorForm operatorForm = new OperatorForm();

        operatorForm.setList(operatorDao.selectUserList(map));
        operatorForm.setTotal_row_count(operatorDao.selectUserCount(map));
        return operatorForm;
    }

    public OperatorForm getGroupList(int groupLevel) {
        OperatorForm opf = new OperatorForm();
        opf.setList(operatorDao.selectGroupList(groupLevel));
        return opf;
    }

    public int isUsed(String operId){
        return operatorDao.selectDuplicateUser(operId);
    }

}
