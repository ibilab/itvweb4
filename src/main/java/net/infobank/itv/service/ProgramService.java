package net.infobank.itv.service;

import net.infobank.itv.dao.ImBoardInfoDao;
import net.infobank.itv.dao.ProgramDao;
import net.infobank.itv.dao.UserDao;
import net.infobank.itv.form.ImBoardInfoForm;
import net.infobank.itv.form.ProgramForm;
import net.infobank.itv.form.ProgramLightForm;
import net.infobank.itv.util.CommonUtils;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ProgramService {

    @Autowired
    private ProgramDao programDAO;
    @Autowired
    private UserDao userDAO;
    @Autowired
    private ImBoardInfoDao imBoardInfoDao;

    public List<ProgramForm> getTopProgramList(int ch_key) {
        return programDAO.getTopProgramList(ch_key);
    }

    public List<Integer> getProgramKeyList(int chKey){ return programDAO.selectProgramKeyList(chKey);}

    public List<ProgramLightForm> getLightList(int ch_key) {
        return programDAO.getLightList(ch_key);
    }

    public ProgramForm getProgramInfo(int pgm_key) {
        ProgramForm form = programDAO.getProgramInfo(pgm_key);
        form.setImBoardInfoList(imBoardInfoDao.list(pgm_key));
        return form;
    }

    public List<ProgramForm> list(ProgramForm form) {

        if("0".equals(form.getIsvalid())) {
            // 종료 목록은 가져오지만.. 여기서 종료 목록을 Refresh 한다.
            programDAO.endListUpdate();
        }
        return programDAO.list(form);
    }

    public int getPgmTimeDup(ProgramForm form) throws Exception {

//        pgm_sdate = pgm_sdate.replaceAll("-", "");
//        pgm_edate = pgm_edate.replaceAll("-", "");

        int isDup = 0;
        String pgm_key = form.getPgm_key() + "";
        String ch_key = form.getCh_key() + "";
        String pgm_stm = form.getPgm_stm();
        String pgm_etm = form.getPgm_etm();
        String pgm_edate = form.getPgm_edate();
        String pgm_sdate = form.getPgm_sdate();
        String pgm_week = form.getPgm_week();

        // 2일짜리 확인
        if(pgm_stm.compareTo(pgm_etm) > 1) {
            // 2일짜리
            isDup = checkPgmDupList(ch_key, pgm_sdate, pgm_edate, pgm_stm, "2400", pgm_week, pgm_key)
                    + checkPgmDupList(ch_key, getNextDate(pgm_sdate), getNextDate(pgm_edate), "0000", pgm_etm, getNextPgm_Week(pgm_week), pgm_key);
        } else {
            isDup = checkPgmDupList(ch_key, pgm_sdate, pgm_edate, pgm_stm, pgm_etm, pgm_week, pgm_key);
        }

        return isDup;
    }

    public int checkPgmDupList(String ch_key, String pgm_sdate, String pgm_edate, String pgm_stm, String pgm_etm, String pgm_week, String pgm_key) throws Exception {
        List<ProgramForm> pgms = new ArrayList<ProgramForm>();

        int isDup = 0;

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("ch_key", ch_key);
        map.put("pgm_key", pgm_key);

        pgms = programDAO.getDupList(map);

        for(ProgramForm pgm : pgms) {
            // 2일짜리 확인
            if(pgm.getPgm_stm().compareTo(pgm.getPgm_etm()) > 1 ) {
                isDup += checkPgmDup(pgm.getPgm_sdate(), pgm.getPgm_edate(), pgm.getPgm_stm(), "2400", pgm.getPgm_week(), pgm_sdate, pgm_edate, pgm_stm, pgm_etm, pgm_week)
                        + checkPgmDup(getNextDate(pgm.getPgm_sdate()), getNextDate(pgm.getPgm_edate()), "0000", pgm.getPgm_etm(), getNextPgm_Week(pgm.getPgm_week()), pgm_sdate, pgm_edate, pgm_stm, pgm_etm, pgm_week);
            } else {
                isDup += checkPgmDup(pgm.getPgm_sdate(), pgm.getPgm_edate(), pgm.getPgm_stm(), pgm.getPgm_etm(), pgm.getPgm_week(), pgm_sdate, pgm_edate, pgm_stm, pgm_etm, pgm_week);
            }
        }

        return isDup;
    }

    // DB자료, 입력자료
    public int checkPgmDup(String pgm_sdate, String pgm_edate, String pgm_stm, String pgm_etm
            , String pgm_week, String sdate, String edate, String stm, String etm, String week) throws Exception {
        int isDup = 0;
        String mon = "월";
        String tue = "화";
        String wed = "수";
        String thu = "목";
        String fri = "금";
        String sat = "토";
        String sun = "일";
        // date check
        if((sdate.compareTo(pgm_sdate) >= 0 && sdate.compareTo(pgm_edate) <= 0)
                || (edate.compareTo(pgm_sdate) >= 0 && sdate.compareTo(pgm_edate) <= 0)
                || (sdate.compareTo(pgm_sdate) <= 0 && edate.compareTo(pgm_edate) >= 0)) {

            // time check
            if((stm.compareTo(pgm_stm) > 0 && stm.compareTo(pgm_etm) < 0)
                    || (etm.compareTo(pgm_stm) > 0 && stm.compareTo(pgm_etm) < 0)
                    || (stm.compareTo(pgm_stm) <= 0 && etm.compareTo(pgm_etm) >= 0)) {

                // week check
                if(pgm_week.indexOf(mon) < 0 ) mon = "n";
                if(pgm_week.indexOf(tue) < 0 ) tue = "n";
                if(pgm_week.indexOf(wed) < 0 ) wed = "n";
                if(pgm_week.indexOf(thu) < 0 ) thu = "n";
                if(pgm_week.indexOf(fri) < 0 ) fri = "n";
                if(pgm_week.indexOf(sat) < 0 ) sat = "n";
                if(pgm_week.indexOf(sun) < 0 ) sun = "n";
                if(week.indexOf(mon) >= 0 ||  week.indexOf(tue) >= 0 || week.indexOf(wed) >= 0
                        || week.indexOf(thu) >= 0 ||  week.indexOf(fri) >= 0
                        || week.indexOf(sat) >= 0 ||  week.indexOf(sun) >= 0) isDup = 1;

            }
        }
        return isDup;
    }

    public String getNextDate(String date) throws ParseException {
        String rtnDate = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date fromDate = formatter.parse(date);

        Calendar cal1 = Calendar.getInstance ();
        cal1.setTime(fromDate);
        cal1.add ( Calendar.DAY_OF_MONTH, 1 );  // 1일후
        rtnDate = new java.text.SimpleDateFormat("yyyyMMdd").format(cal1.getTime());
        return rtnDate;
    }

    public String getNextPgm_Week(String pgm_week) {
        if(pgm_week.equals("월|화|수|목|금|토|일")) return pgm_week;
        pgm_week = pgm_week.replaceAll("일", "1");
        pgm_week = pgm_week.replaceAll("토", "일");
        pgm_week = pgm_week.replaceAll("금", "토");
        pgm_week = pgm_week.replaceAll("목", "금");
        pgm_week = pgm_week.replaceAll("수", "목");
        pgm_week = pgm_week.replaceAll("화", "수");
        pgm_week = pgm_week.replaceAll("월", "화");
        pgm_week = pgm_week.replaceAll("1", "월");
        return pgm_week;
    }

    @Transactional
    public void insert(HttpServletRequest request, ProgramForm form) {
        // sdate, edate '-' 처리
        if(!StringUtil.isEmpty(form.getPgm_sdate())){
            form.setPgm_sdate(form.getPgm_sdate().replaceAll("-", ""));
        }
        if(!StringUtil.isEmpty(form.getPgm_edate())){
            form.setPgm_edate(form.getPgm_edate().replaceAll("-", ""));
        }

        // mo, emo 웹 분리 작업
        makeMoEmo(form);

        // insert schedule
        int pgm_key = programDAO.insert(form);
        // create user_
        userDAO.createUserTable(pgm_key);

        processImBoardInfo(request, pgm_key);
    }

    @Transactional
    public void update(HttpServletRequest request, ProgramForm form) {
        // sdate, edate '-' 처리
        if(!StringUtil.isEmpty(form.getPgm_sdate())){
            form.setPgm_sdate(form.getPgm_sdate().replaceAll("-", ""));
        }
        if(!StringUtil.isEmpty(form.getPgm_edate())){
            form.setPgm_edate(form.getPgm_edate().replaceAll("-", ""));
        }

        // mo, emo 웹 분리 작업
        makeMoEmo(form);

        programDAO.update(form);

        int pgm_key = form.getPgm_key();
        processImBoardInfo(request, pgm_key);
    }

    public void processImBoardInfo(HttpServletRequest request, int pgm_key) {
        ArrayList<String> list = CommonUtils.getEnumerationString(request.getParameterNames(), "prog_cd");
        ImBoardInfoForm form = new ImBoardInfoForm();
        form.setPgm_key(pgm_key);

        for (String prog_param : list ) {
            String msg_com = prog_param.replace("prog_cd_", "");
            form.setMsg_com(msg_com);

            String prog_name = request.getParameter("pgm_name");
            String prog_cd = request.getParameter(prog_param);
            String multi_id = request.getParameter(prog_param.replace("prog_cd", "multi_id"));

            form.setProg_name(prog_name);
            form.setProg_cd(prog_cd);
            form.setMulti_id(multi_id);

            int count = imBoardInfoDao.count(form);

            if(count > 0) {
                imBoardInfoDao.update(form);
            } else {
                imBoardInfoDao.insert(form);
            }

        }
    }

    public void delete(int pgm_key) {
        programDAO.delete(pgm_key);
    }

    public void makeMoEmo(ProgramForm form) {
        if( form.getPgm_mo().length() > 5 && form.getPgm_mo().substring(0,1).equals("#") ) {
            form.setPgm_emo( form.getPgm_mo().substring(5) );
            form.setPgm_mo( form.getPgm_mo().substring(0, 5) );
        } else {
            form.setPgm_emo( "" );			// 빈값을 넣어줌
        }
    }

}
