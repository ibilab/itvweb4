package net.infobank.itv.service;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.common.Constants;
import net.infobank.itv.dao.BoardDao;
import net.infobank.itv.dao.SendDao;
import net.infobank.itv.dao.UserDao;
import net.infobank.itv.form.BoardForm;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.SendDto;
import net.infobank.itv.form.SendForm;
import net.infobank.itv.util.DateUtil;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class SendService {

    @Autowired
    private SendDao sendDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private BoardDao boardDao;


    public SendForm getSendList(SendForm form) throws Exception {
        setSearchSend(form);
        String excel = form.getExcel();

        form.setList(sendDao.searchSendList(form));
        if(!"Y".equals(excel)) {
            form.setTotal_row_count(sendDao.countSendList(form));
        } else {
            form.setTotal_row_count(form.getList().size());
        }
        System.out.println(form.getList());
        return form;
    }


    public void setSearchSend(SendForm form) throws Exception {
        form.setMonth_list(getMonth_list(form));

        form.setStart_dt(form.getStart_dt().replaceAll("-","") + form.getStart_hh() + form.getStart_mi());
        form.setEnd_dt(form.getEnd_dt().replaceAll("-","") + form.getEnd_hh() + form.getEnd_mi());
    }

    public boolean sendMsgSave(HttpServletRequest request, BoardForm form) {
        HttpSession session  = request.getSession();
        OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);

        try {

            String allow_mt = userDao.checkAllowMt(form);
            if (!"1".equals(allow_mt)) return false;

            sendDao.sendMsg(form, operator.getOper_id());

            // TODO 사용자 mt count 추가

            String check_table = boardDao.checkTable(form.getTable_name());
            if(!StringUtil.isEmpty(check_table)) {
                form.setMsg_rtn(1);
                boardDao.updateReturnMsg(form);
            }
        } catch (Exception e) {
            log.error("Error : ", e);
            return false;
        }

        return true;
    }

    public int multipleSend(List<SendDto> list) {
        return sendDao.insertSendMsg(list);
    }

    public SendForm getListSendMsgEachUser(SendForm form) throws Exception {
        String month = form.getStart_dt();
        ArrayList<String> month_list = new ArrayList<String>();
        if("last".equals(month)) {
            DateUtil dutil = new DateUtil();
            form.setEnd_dt(dutil.getString("yyyyMMdd") + "245959");
            dutil.addDay(-3);
            form.setStart_dt(dutil.getString("yyyyMMdd") + "000000");
            month_list.add("response_trlist");
        } else {
            form.setStart_dt(month + "01000000");
            form.setEnd_dt(month + "31245959");
            String chk = sendDao.checkTable("response_trlist_" + month);
            if(!StringUtil.isEmpty(chk)) {
                month_list.add("board_" + month);
            }
        }
        form.setMonth_list(month_list);

        if(month_list.size() > 0) {
            form.setList(sendDao.searchSendList(form));
            form.setTotal_row_count(sendDao.countSendList(form));
        }
        return form;
    }


    public ArrayList<String> getMonth_list(SendForm form) throws Exception {
        ArrayList<String> month_list = new ArrayList<String>();
        month_list.add("response_trlist");

        DateUtil stDate = new DateUtil();
        DateUtil endDate = new DateUtil();
        DateUtil posMon = endDate;

        stDate.setYMD(form.getStart_dt(), "yyyy-MM-dd");
        endDate.setYMD(form.getEnd_dt(), "yyyy-MM-dd");

        for(; stDate.getString("yyyyMM").compareTo(posMon.getString("yyyyMM")) <= 0; posMon.addMonth(-1)) {
            String tName = "response_trlist_" + posMon.getString("yyyyMM");

            String chk = sendDao.checkTable(tName);
            if(!StringUtil.isEmpty(chk)) {
                month_list.add(tName);
            }
        }

        return month_list;
    }
}
