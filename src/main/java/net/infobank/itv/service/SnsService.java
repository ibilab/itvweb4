package net.infobank.itv.service;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.dao.SnsDao;
import net.infobank.itv.form.TwitterDto;
import net.infobank.itv.form.YoutubeForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

@Service
@Slf4j
public class SnsService {

    private final SnsDao snsDao;

    public SnsService(SnsDao snsDao) {
        this.snsDao = snsDao;
    }

    public YoutubeForm searchYoutubeList(YoutubeForm form) {
        form.setList(snsDao.searchYoutubeList(form));
        form.setTotal_row_count(snsDao.countYoutube(form));
        return form;
    }

    public TwitterDto getTwitterInfo(int programKey){
        return snsDao.selectTwitter(programKey);
    }

    @Transactional
    public int addTwitter(TwitterDto dto) {
        return snsDao.insertTwitter(dto);
    }

    @Transactional
    public int modifyTwitter(TwitterDto dto) {
        return snsDao.updateTwitter(dto);
    }

    @Transactional
    public int removeTwitter(int programKey){
        return snsDao.deleteTwitter(programKey);
    }

    public RequestToken getRequestToken(String CONSUMER_KEY, String CONSUMER_SECRET) {
        Twitter twitter = new TwitterFactory().getInstance();;
        RequestToken requestToken = null;

		/*ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(CONSUMER_KEY);
		builder.setOAuthConsumerSecret(CONSUMER_SECRET);
		Configuration config = builder.build();

		TwitterFactory factory = new TwitterFactory(config);
		twitter = factory.getInstance();*/
        twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        try {
            requestToken = twitter.getOAuthRequestToken();
        } catch (TwitterException te) {
            te.printStackTrace();
        }
        return requestToken;
    }

    public AccessToken getAccessToken(String oauthToken, String oauth_verifier,
                                      String CONSUMER_KEY, String CONSUMER_SECRET,
                                      RequestToken requestToken) {
        if (requestToken == null) {
            System.out.print("requestToken == null");
        }
        if (oauthToken == null) {
            System.out.print("oauthToken == null");
        }

        TwitterFactory twitterFactory = new TwitterFactory();
        Twitter twitter = twitterFactory.getInstance();
        twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        AccessToken accessToken = null;

        if (requestToken.getToken().equals(oauthToken)) {
            try {
                accessToken = twitter.getOAuthAccessToken(requestToken, oauth_verifier);
                twitter.setOAuthAccessToken(accessToken);
            } catch (TwitterException te) {
                te.printStackTrace();
            }
        }
        return accessToken;
    }

    public void printStatuses() {
        ResponseList<Status> statuses;
        Paging page = new Paging();
        page.count(20);
        page.setPage(1);
        Twitter twitter = new TwitterFactory().getInstance();

        try {
            statuses = twitter.getHomeTimeline(page);
            for (Status status : statuses) {
                // status.getId()
                System.out.println(status.getUser().getScreenName() + ":"
                        + status.getText());
            }
        } catch (TwitterException te) {
            te.printStackTrace();
        }
    }

}
