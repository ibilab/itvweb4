package net.infobank.itv.service;

import net.infobank.itv.dao.StatDao;
import net.infobank.itv.form.StatForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class StatService {

    @Autowired
    private StatDao statDao;

    public StatForm statDay(HttpServletRequest request, StatForm form) {
        form.setStart_dt(form.getStart_dt().replaceAll("-", ""));
        form.setEnd_dt(form.getEnd_dt().replaceAll("-", ""));

        form.setGroup_by("stat_date");
        request.setAttribute("list", statDao.statDay(form));

        form.setGroup_by("");
        request.setAttribute("totallist", statDao.statDay(form));
        return form;
    }

    public StatForm statPgm(HttpServletRequest request, StatForm form) {
        form.setStart_dt(form.getStart_dt().replaceAll("-", ""));
        form.setEnd_dt(form.getEnd_dt().replaceAll("-", ""));

        form.setGroup_by("B.pgm_key");
        request.setAttribute("list", statDao.statPgm(form));

        form.setGroup_by("");
        request.setAttribute("totallist", statDao.statPgm(form));
        return form;
    }

    public StatForm statComDay(HttpServletRequest request, StatForm form) {
        form.setStart_dt(form.getStart_dt().replaceAll("-", ""));
        form.setEnd_dt(form.getEnd_dt().replaceAll("-", ""));

        form.setGroup_by("msg_com");
        request.setAttribute("list", statDao.statComDay(form));

        form.setGroup_by("");
        request.setAttribute("totallist", statDao.statComDay(form));
        return form;
    }
}
