package net.infobank.itv.service;

import lombok.AllArgsConstructor;
import net.infobank.itv.dao.UserMngDao;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.SendDto;
import net.infobank.itv.form.UserDto;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserMngService {
	private final UserMngDao dao;
	private final SendService sendService;

	public UserDto getAttendUserList(UserDto dto){
		List<UserDto> list = dao.selectAttendUserList(dto);
		int totalCount = getTotalCount(dto);
		UserDto result = new UserDto();
		result.setPage_num(dto.getPage_num());
		result.setList(list);
		result.setTotal_row_count(totalCount);
		return result;
	}

	public int getTotalCount(UserDto dto) {
		return dao.selectAttendUserCount(dto);
	}

	@Transactional
	public int send(UserDto dto, String message, OperatorForm operatorForm){
		dto.setAllowMt(1);
		List<String> list = dao.selectMessageSendList(dto);
		final List<SendDto> batchList = new ArrayList<>();
		int count = 0;
		String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

		for(int i = 0, len = list.size() ; i < len; i++){
			SendDto sendDto = new SendDto();
			sendDto.setChKey(operatorForm.getCh_key());
			sendDto.setPgmKey(operatorForm.getPgm_key());
			sendDto.setMsgCom("001");
			sendDto.setUserId(list.get(i));
			sendDto.setMsgKey(time + "_" + (i+1));
			Optional<String> o1 = Optional.ofNullable(operatorForm.getPgm_mo());
			Optional<String> o2 = Optional.ofNullable(operatorForm.getBilling_code());
			sendDto.setOperId(operatorForm.getOper_id());
			sendDto.setCallback(o1.orElse(""));
			sendDto.setRefKey(o2.orElse(""));
			sendDto.setMsg(message);
			batchList.add(sendDto);
			if(i % 1000 == 0 || i + 1 == len) {
				count += sendService.multipleSend(batchList);
				batchList.clear();
			}
		}
		return count;
	}
}
