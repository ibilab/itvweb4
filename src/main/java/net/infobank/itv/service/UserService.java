package net.infobank.itv.service;

import lombok.extern.slf4j.Slf4j;
import net.infobank.itv.dao.UserDao;
import net.infobank.itv.form.OperatorForm;
import net.infobank.itv.form.UserForm;
import net.infobank.itv.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public int setUserImage(String pgm_key, String user_id, String msg_com, String user_pic) throws Exception {
        return userDao.setUserImage(pgm_key, user_id, msg_com, user_pic);
    }

    public UserForm getUserInfo(HttpServletRequest request, String user_id, String msg_com) throws Exception {
        OperatorForm operator = CommonUtils.getOperator(request);
        String pgm_key = operator.getPgm_key() + "";
        // kakao.. ?

        UserForm userInfo = userDao.getUserInfo(pgm_key, user_id, msg_com);
        return userInfo;
    }

    public int updateUserEditNick(String pgm_key, String msg_com, String user_id, String edit_nick) throws Exception {
        return userDao.updateUserEditNick(pgm_key, msg_com, user_id, edit_nick);
    }

    public int updateUserAllowMt(String pgm_key, String msg_com, String user_id, String allow_mt) throws Exception {
        return userDao.updateUserAllowMt(pgm_key, msg_com, user_id, allow_mt);
    }

    public int updateUserShowMsg(String pgm_key, String msg_com, String user_id, String show_msg) throws Exception {
        // 카톡일 경우??

        return userDao.updateUserShowMsg(pgm_key, msg_com, user_id, show_msg);
    }

    public int updateUserNick(String pgm_key, String msg_com, String user_id, String user_nick) throws Exception {
        return userDao.updateUserNick(pgm_key, msg_com, user_id, user_nick);
    }

    public int updateUserInfo(String pgm_key, String msg_com, String user_id, String user_info) throws Exception {
        return userDao.updateUserInfo(pgm_key, msg_com, user_id, user_info);
    }

    public int updateUserIcon(String pgm_key, String msg_com, String user_id, String user_icon) {
        return userDao.updateUserIcon(pgm_key, msg_com, user_id, user_icon);
    }

    public int updateUserGift(String pgm_key, String msg_com, String user_id, String user_image) {
        return userDao.updateUserGift(pgm_key, msg_com, user_id, user_image);
    }

}
