package net.infobank.itv.service;

import net.infobank.itv.common.AofCodes;
import net.infobank.itv.config.AofCode;
import net.infobank.itv.dao.VoteDao;
import net.infobank.itv.form.VoteForm;
import net.infobank.itv.form.VoteResultForm;
import net.infobank.itv.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class VoteService {

    @Autowired
    private VoteDao voteDao;

    public VoteForm list(VoteForm form) {

        if(!StringUtil.isEmpty(form.getStart_dt())){
            form.setStart_dt(form.getStart_dt().replaceAll("-", "")+"000000");
        }
        if(!StringUtil.isEmpty(form.getEnd_dt())){
            form.setEnd_dt(form.getEnd_dt().replaceAll("-", "")+"240000");
        }
        form.setList(voteDao.searchVote(form));
        form.setTotal_row_count(voteDao.countVote(form));
        return form;
    }

    public VoteForm get(VoteForm form) {
        return voteDao.get(form);
    }

    public boolean ending(int vote_key, int sub_key) {
        VoteForm form = new VoteForm();
        form.setVote_key(vote_key);

        form = voteDao.get(form);

        if(sub_key == form.getSub_key()) {
            form.setVote_status("3");
            form.setVote_status2("3");
            voteDao.updateStatus(form);
            return true;
        } else {
            return false;
        }
    }

    public boolean delete(int vote_key, int sub_key) {
        VoteForm form = new VoteForm();
        form.setVote_key(vote_key);

        form = voteDao.get(form);

        if(sub_key == form.getSub_key()) {
            form.setVote_status("3");
            form.setVote_status2("3");
            voteDao.delete(form);
            return true;
        } else {
            return false;
        }
    }

    public boolean insert(VoteForm form) {
        voteDao.insert(form);
        return true;
    }

    public boolean update(VoteForm form) {
        VoteForm tmp = new VoteForm();
        tmp.setVote_key(form.getVote_key());

        tmp = voteDao.get(form);

        if(form.getSub_key() == tmp.getSub_key()) {
            voteDao.update(form);
            return true;
        } else {
            return false;
        }

    }

    public void resultList(HttpServletRequest request, int vote_key) throws Exception {
        VoteForm vote = new VoteForm();
        vote.setVote_key(vote_key);

        vote = voteDao.get(vote);

        vote.setVote_stm(StringUtil.changeDateFormat(vote.getVote_stm(), "yyyyMMddhhmmss", "yyyy-MM-dd hh:mm:ss"));
        vote.setVote_etm(StringUtil.changeDateFormat(vote.getVote_etm(), "yyyyMMddhhmmss", "yyyy-MM-dd hh:mm:ss"));
        request.setAttribute("vote_info",  vote);
        request.setAttribute("sns_lists", AofCode.getCodeNamesToString(AofCodes.SNS_LIST, "|", vote.getSns_list()));

        List<VoteResultForm> results = voteDao.resultList(vote_key);

        // PIVOT !!!!
        String[] vote_items = vote.getVote_items().split("\\|");
        List<VoteResultForm> rows = new ArrayList<VoteResultForm>();

        if (results.size() > 0) {
            for (int i = 0; i < vote_items.length; i++) {
                VoteResultForm row = new VoteResultForm();
                row.setVote_rslt("");
                rows.add(row);
            }
        }

        int total = 0;
        for( int i=0 ; i<results.size() ; i++) {
            String[] vote_results = results.get(i).getVote_rslt().split("\\|");

            if(vote_items.length != vote_results.length) {
                throw new Exception("투표항목수와 결과수가 다릅니다.");
            }

            for( int j=0 ; j<rows.size() ; j++) {
                VoteResultForm row = rows.get(j);
                total += Integer.parseInt(vote_results[j]);
                row.setVote_item(vote_items[j]);
                if ( i == 0 ) {
                    row.setVote_result(vote_results[j]);
                    row.setMsg_com(results.get(i).getMsg_com());
                } else {
                    row.setVote_result(row.getVote_result() + "|" + vote_results[j]);
                    row.setMsg_com(row.getMsg_com() + "|" + results.get(i).getMsg_com());
                }
            }
        }

        // rank, total
        if(results.size()>0) {
            for(int i=0; i< rows.size(); i++) {
                rows.get(i).setTotal(total);
                for(int j=0; j<rows.size(); j++){
                    VoteResultForm row1 = rows.get(i);
                    VoteResultForm row2 = rows.get(j);
                    if(row1.getSum() > row2.getSum()) {
                        rows.get(j).setRank(rows.get(j).getRank()+1);
                    }
                }
            }
        }

        request.setAttribute("results", rows);
    }
}
