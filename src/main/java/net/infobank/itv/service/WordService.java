package net.infobank.itv.service;

import net.infobank.itv.dao.WordDao;
import net.infobank.itv.form.LimitWordForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WordService {

    @Autowired
    private WordDao wordDao;

    public LimitWordForm limitWordList(LimitWordForm form) {

        form.setList(wordDao.searchLimitWord(form));
        form.setTotal_row_count(wordDao.countLimitWord(form));
        return form;
    }

    public void deleteLimitWord(int lword_key) {
        wordDao.deleteLimitWord(lword_key);
    }

    public void insertLimitWord(LimitWordForm form) {
        wordDao.insertLimitWord(form);
    }
}
