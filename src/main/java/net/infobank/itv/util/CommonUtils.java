package net.infobank.itv.util;

import net.infobank.itv.common.Constants;
import net.infobank.itv.form.MenuForm;
import net.infobank.itv.form.OperatorForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class CommonUtils {

    public static String getMenuPathInfo(HttpServletRequest request, List<MenuForm> menu) throws Exception {
        HttpSession session = request.getSession();
        String returnVal = "";
        String menu_order = CookieUtil.getCookie(request, "menu_order");

        if(menu_order.equals("")) {
            System.out.println("* !! Menu Order is Empty. Set to Default 1000");
            menu_order = "1000";
        }

        for(int i=0 ; i< menu.size() ; i++) {
            String order_1 = menu_order.substring(0,1);
            if(menu.get(i).getMenu_order().equals(order_1 + "000")) {
                returnVal = order_1 + "|" + menu.get(i).getMenu_name();
                session.setAttribute("menu_1_order", order_1);
                session.setAttribute("menu_1_name", menu.get(i).getMenu_name());
            }
            String order_2 = menu_order.substring(1,2);
            if(menu.get(i).getMenu_order().equals(order_1 + order_2 + "00")) {
                returnVal += order_2 + "|" + menu.get(i).getMenu_name();
                session.setAttribute("menu_2_order", order_2);
                session.setAttribute("menu_2_name", menu.get(i).getMenu_name());
            }
        }
        return returnVal;
    }

    public static OperatorForm getOperator(HttpServletRequest request) {
        HttpSession session = request.getSession();
        OperatorForm operator = (OperatorForm) session.getAttribute(Constants.OPERATOR);
        return operator;
    }


    public static ArrayList<String> getEnumerationString(Enumeration enums, String start) {
        ArrayList<String> result = new ArrayList<String>();

        while(enums.hasMoreElements()) {
            String name = (String) enums.nextElement();
            if("".equals(start) || name.startsWith(start)) {
                result.add(name);
            }
        }
        return result;
    }
}
