package net.infobank.itv.util;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {

    /**
     * setCookie
     *
     * @return
     * @throws Exception
     */
    public static void setCookie(HttpServletRequest request, HttpServletResponse response, String key, String value, int day) throws Exception {
        value = java.net.URLEncoder.encode(value.toString(), "UTF-8");
        Cookie cookie = new Cookie(key, value);
        cookie.setDomain(request.getServerName());
        cookie.setMaxAge(day * 24 * 60 * 60);   //30일
        // cookie.setPath("/");
        response.addCookie(cookie);
    }

    /**
     * getCookie
     *
     * @return cookie
     * @throws Exception
     */
    public static String getCookie(HttpServletRequest request, String key) throws Exception {
        Cookie[] cookies = request.getCookies();
        if (key == null) return null;
        String value = "";
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (key.equals(cookies[i].getName())) {
                    value = java.net.URLDecoder.decode(cookies[i].getValue(), "UTF-8");
                    break;
                }
            }
        }
        return value;
    }

}
