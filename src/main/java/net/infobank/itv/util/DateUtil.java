package net.infobank.itv.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil
{
    private Calendar cldr;
    private int thisYear;
    private int today;
    private int thisMonth;

    public DateUtil()
    {
        this.cldr = Calendar.getInstance();
        this.thisYear = this.cldr.get(1);
        this.today = this.cldr.get(5);
        this.thisMonth = (this.cldr.get(2) + 1);
    }

    public int getToday()
    {
        return this.today;
    }

    public int getThisMonth() {
        return this.thisMonth;
    }

    public int getThisYear() {
        return this.thisYear;
    }

    public int getYear()
    {
        return this.cldr.get(1);
    }

    public int getMonth()
    {
        return this.cldr.get(2) + 1;
    }
    public int getDay() {
        return this.cldr.get(5);
    }

    public void setMonthVal(String m)
    {
        if ((m != null) &&
                (!m.equals("")))
            this.cldr.add(2, Integer.parseInt(m) - getMonth());
    }

    public void addMonth(int i)
    {
        this.cldr.add(2, i);
    }

    public void setYearVal(String y)
    {
        if ((y != null) &&
                (!y.equals("")))
            this.cldr.add(1, Integer.parseInt(y) - getYear());
    }

    public void addYear(int i)
    {
        this.cldr.add(1, i);
    }

    public void setDayVal(String d) {
        if ((d != null) &&
                (!d.equals("")))
            this.cldr.add(5, Integer.parseInt(d) - getDay());
    }

    public void addDay(int i)
    {
        this.cldr.add(5, i);
    }

    public void setYMD(String yyyyMMdd, String pattern) throws ParseException {
        SimpleDateFormat fommatter = new SimpleDateFormat(pattern);
        this.cldr.setTime(fommatter.parse(yyyyMMdd));
    }

    public int getFirstDay()
    {
        this.cldr.set(5, 1);
        return this.cldr.get(7);
    }

    public int getLastDate()
    {
        return this.cldr.getActualMaximum(5);
    }

    public int getDayOfWeek()
    {
        return this.cldr.get(7);
    }

    public int getDayOfWeek(String d) {
        this.cldr.set(5, Integer.parseInt(d));
        return getDayOfWeek();
    }

    public String getString(String pattern)
    {
        SimpleDateFormat fommatter = new SimpleDateFormat(pattern);
        return fommatter.format(this.cldr.getTime());
    }

    public void initToday()
    {
        this.cldr.set(this.thisYear, this.thisMonth - 1, this.today);
    }

    public long getMilliseconds() {
        return this.cldr.getTimeInMillis();
    }

    public long getElapsedTime(DateUtil end_dt)
    {
        return end_dt.getMilliseconds() - getMilliseconds();
    }

    public static final String formatDate(String yyyyMMdd, String pattern1, String pattern2) throws ParseException {
        SimpleDateFormat fommatter = new SimpleDateFormat(pattern2);

        return fommatter.format(getDate(yyyyMMdd, pattern1));
    }

    public static final String formatDate(Date yyyyMMdd, String pattern) throws ParseException {
        SimpleDateFormat fommatter = new SimpleDateFormat(pattern);

        return fommatter.format(yyyyMMdd);
    }

    public static final String formatDate(String yyyyMMdd, String pattern) throws ParseException {
        SimpleDateFormat fommatter = new SimpleDateFormat(pattern);

        return fommatter.format(getDate(yyyyMMdd));
    }

    public static final Date getDate(String yyyyMMdd, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(yyyyMMdd);
    }

    public static final Date getDate(String yyyyMMdd) throws ParseException {
        return getDate(yyyyMMdd, "yyyyMMdd");
    }

    public static final String getLastYearBsl(String yyyyMMdd)
            throws ParseException
    {
        DateUtil trdt = new DateUtil();
        trdt.setYMD(yyyyMMdd, "yyyyMMdd");
        trdt.addYear(-1);
        return trdt.getString("yyyyMMdd");
    }

    public static final String getLastMonthBsl(String yyyyMMdd, int mon)
            throws ParseException
    {
        DateUtil trdt = new DateUtil();
        trdt.setYMD(yyyyMMdd, "yyyyMMdd");
        trdt.addMonth(mon);
        return trdt.getString("yyyyMMdd");
    }

    public static final String getLastDayBsl(String yyyyMMdd, int day)
            throws ParseException
    {
        DateUtil trdt = new DateUtil();
        trdt.setYMD(yyyyMMdd, "yyyyMMdd");
        trdt.addDay(day);
        return trdt.getString("yyyyMMdd");
    }

//    public static void main(String[] args) throws Exception {
//        DateUtil dd = new DateUtil();
//        dd.addDay(30);
//        System.out.println(dd.getMilliseconds());
//        dd.initToday();
//        System.out.println(dd.getMilliseconds());
//    }
}