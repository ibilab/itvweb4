package net.infobank.itv.util;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;

public class StringUtil
{
    public static final String toEUC_KR(String str)
    {
        return changeCharset(str, "ISO-8859-1", "EUC-KR");
    }

    public static final String toKR_EUC(String str) {
        return changeCharset(str, "EUC-KR", "ISO-8859-1");
    }

    public static final String toUTF8_EUC(String str) {
        return changeCharset(str, "UTF-8", "ISO-8859-1");
    }

    public static final String changeCharset(String str, String to_code, String form_code)
    {
        if (str == null) return null; try
    {
        return new String(str.getBytes(to_code), form_code); } catch (UnsupportedEncodingException e) {
    }
        return str;
    }

    public static final String getSysVal(String baseName, String key)
    {
        return getSysVal(baseName, key, null);
    }

    public static final String getSysVal(String baseName, String key, String charset) {
        ResourceBundle rb = ResourceBundle.getBundle(baseName);
        if (charset == null) {
            return rb.getString(key);
        }
        return changeCharset(rb.getString(key), "ISO-8859-1", charset);
    }

    public static final String[] split(String str, String chr)
    {
        String[] temp = (String[])null;

        if (str == null) return new String[0];
        if (str.trim().equals("")) return new String[0];

        Vector buf = new Vector();
        int pos = -1;
        while ((pos = str.indexOf(chr)) > -1) {
            buf.addElement(str.substring(0, pos));
            str = str.substring(pos + 1);
        }
        buf.addElement(str);
        temp = new String[buf.size()];
        for (int i = 0; i < buf.size(); i++) {
            temp[i] = ((String)buf.get(i));
        }
        return temp;
    }

    public static String[] getTokenData(String str, char _char)
    {
        String tempbuf = str;

        if (str == null) return null;
        if (str.length() == 0) return null;

        int j = 0;
        for (int i = 0; i < str.length(); i++) {
            if (tempbuf.charAt(i) == _char) {
                j++;
            }
        }

        String[] rtnbuf = new String[j + 1];

        j = 0;
        rtnbuf[0] = "";
        for (int i = 0; i < str.length(); i++) {
            if (tempbuf.charAt(i) == _char) {
                j++;
                rtnbuf[j] = "";
            }
            else {
                rtnbuf[j] = (rtnbuf[j] + String.valueOf(tempbuf.charAt(i)));
            }
        }

        return rtnbuf;
    }

    public static String voidNull(String param)
    {
        if (param == null)
            return "";
        if (param.trim().equals("")) {
            return "";
        }
        return param.trim();
    }

    public static final String getCommaList(ArrayList cds)
    {
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < cds.size(); i++) {
            if (i > 0) str.append(",");
            str.append("'");
            str.append(cds.get(i));
            str.append("'");
        }
        return str.toString();
    }

    public static final String getCommaList(String[] cds)
    {
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < cds.length; i++) {
            if (i > 0) str.append(",");
            str.append("'");
            str.append(cds[i]);
            str.append("'");
        }
        return str.toString();
    }

    public static final String getCommaListInt(String[] cds)
    {
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < cds.length; i++) {
            if (i > 0) str.append(",");
            str.append(cds[i]);
        }
        return str.toString();
    }

    public static final boolean isEmpty(String str)
    {
        return (str == null) || (str.trim().equals(""));
    }

    public static String changeDateFormat(String str, String from, String to) throws Exception {
        SimpleDateFormat fDate = new SimpleDateFormat(from);
        SimpleDateFormat tDate = new SimpleDateFormat(to);
        Date d = fDate.parse(str);
        return tDate.format(d);
    }

    public static void main(String[] args)
    {
        StringBuffer condition = new StringBuffer();
        String dd = null;
        dd = condition.toString();
        System.out.println(dd);
    }
}