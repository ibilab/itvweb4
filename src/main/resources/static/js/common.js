/*
 * 공통 자바스크립트 함수
 */

function emptyData(fobj, msg) {
    if (trim(fobj.value) == "" || trim(fobj.value) == "0") {
        alert(msg);
        if (fobj.type.toLowerCase() != "hidden" && !fobj.disabled) {
            fobj.focus();
        }
        return true;
    } else {
        return false;
    }
}

// 체크박스 목록
function getChks(fobj, delim) {
    var cds = "";
    if (delim == null) delim = ",";
    if (fobj == null) return cds;

    if (fobj.length) {
        for (var i = 0; i < fobj.length; i++) {
            if (fobj[i].checked) {
                if (cds != "") cds = cds + delim;
                cds = cds + fobj[i].value;
            }
        }
    } else {
        if (fobj.checked) {
            cds = fobj.value;
        }
    }

    return cds;
}

function showTooltip(board_key) {
    $("#tip_" + board_key).css("display","block");
    $("#msg_" + board_key).mouseleave(function(){
        $("#tip_" + board_key).css("display","none");
    });
}

function trim(s) {
    s = s.replace(/^\s*/, '').replace(/\s*$/, '');
    return s;
}

function resize_frame(name, height_size) {
    if(!height_size) height_size = 30;
    var oBody = eval(name).document.body;
    var oFrame = document.getElementById(name) == null ? document.getElementsByName(name)[0] : document.getElementById(name);

    //oFrame.style.width = oBody.scrollWidth + (oBody.offsetWidth-oBody.clientWidth);
    //oFrame.style.height = oBody.scrollHeight + (oBody.offsetHeight-oBody.clientHeight) + height_size;
    $("#" + name).css("height" , oBody.scrollHeight + (oBody.offsetHeight-oBody.clientHeight) + height_size);

    if(oFrame.style.width == '0px' || oFrame.style.height == '0px') {
        // 해당 iframe의 변경할 가로크기
        //oFrame.style.width = "146px";
        // 해당 iframe의 변경할 세로크기
        oFrame.style.height = "100px";
        // 크기 변경 후 상태바에 표시하는 오류 메세지
        window.status = 'iframe 크기 변경 실패';
    }
}

//input text 필드 엔터 처리함수 (onkeydown 이벤트)
function onEnter(callMethod, f) {
    if (event.keyCode == 13) {
        if (f) {
            callMethod(f);
        } else {
            callMethod();
        }
        event.returnValue = false;
    }
}

/* datepicker */
function setDatePicker(id, dateFormat = "yy-mm-dd") {
    $(id).datepicker({
        dateFormat: dateFormat,
        altField: id,
        showOtherMonths: true,
        selectOtherMonths: true,
        nextText: '다음 달', // next 아이콘의 툴팁.
        prevText: '이전 달', // prev 아이콘의 툴팁.
        monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayNamesMin: ["일", "월", "화", "수", "목", "금", "토",]
    });
}

function calculate_msglen(message) {
    var nbytes = 0;
    for (i=0; i<message.length; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            nbytes += 2;
        } else if (ch != '\r') {
            nbytes++;
        }
    }
    return nbytes;
}

/* ajax 공통 함수 */
function ajaxOnCreate(request, setting) {
    $("#progressbar").show();
}

function ajaxOnComplete(request, status) {
    $("#progressbar").hide();
}

function ajaxError(response, status, request) {
    alert(response.responseText);
}

function actionResult(response) {
    const result = JSON.parse(response);
    const action = result.action;
    const message = result.message;

    if(message != undefined) {
        alert(message);
    }
    if(action != undefined) {
        eval(action);
    }
}

function popUserInfo(user_id, msg_com, user_name, comment_id, board_key, table_name) {
    changeWidget(user_id);

    let data = {
        user_id : user_id,
        msg_com : msg_com,
        user_name : user_name,
        comment_id : comment_id,
        board_key : board_key,
        table_name : table_name,
    }
    try {
        setWidgetFrame("2", data);
    } catch(e) {
    }
}

var beforeID = "";
// 위젯 노출 / 비노출 처리 함수
function changeWidget(user_id){
    // console.log('user_id : ' + user_id);
    // console.log('beforeID : ' + beforeID);
    if(beforeID != user_id) {
        $('#container').addClass('on');
        $('#container>.aside').show();
    } else if($('#container').hasClass('on') == true) {
        $('#container').removeClass('on');
        $('#container>.aside').hide();
    } else {
        $('#container').addClass('on');
        $('#container>.aside').show();
    }
    beforeID = user_id;
}

function isUseFulVoteString(str) {
    var specialChars = '<>\'|"\\,';
    for(var i=0 ; i < str.length ; i++ ) {
        for(var j=0 ; j < specialChars.length ; j++) {
            if(str.charAt(i) == specialChars.charAt(j)) {
                return false;
            }
        }
    }
    return true;
}

function getDateStr(yyyyMMdd, delim){
    if(yyyyMMdd == "") return "";
    let buf = yyyyMMdd.substr(0,4) +delim;
    buf = buf + yyyyMMdd.substr(4,2) +delim;
    buf = buf + yyyyMMdd.substr(6,2);
    return buf;
}

// 좌측에 문자열 채우기
function fillString(str, len, fill) {
    str = str.toString();
    if(str.length < len) {
        for(let i=0 ; len - str.length ; i++ ) {
            str = fill + str;
        }
    }
    return str;
}