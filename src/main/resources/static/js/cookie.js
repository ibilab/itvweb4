/*
 * 쿠키관련 자바스크립트.
 * 관리자 페이지 menu, 강의실 left menu 에서 사용.
 */
 
function setCookie(cookieName,value,path,domain,secure) {
	   var expires = new Date();
	   expires.setMonth(expires.getMonth() + 1);
	   expires.setDate(expires.getDate() + 1);
	   path="/";
	   var cookiestr = escape(cookieName) + "=" + escape (value) +
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "") +
//		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
	   document.cookie = cookiestr;
	}
	
	function getCookie(cookieName) {
		var cookieValue = '';
		var posName = document.cookie.indexOf(escape(cookieName) + '=');
		if (posName != -1) {
			var posValue = posName + (escape(cookieName) + '=').length;
			var endPos = document.cookie.indexOf(';', posValue);
			if (endPos != -1) cookieValue = unescape(document.cookie.substring(posValue, endPos));
			else cookieValue = unescape(document.cookie.substring(posValue));
		}
		return (cookieValue);
	}