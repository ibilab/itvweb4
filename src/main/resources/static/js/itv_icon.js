//***********************************************************************************
//	file name	:	itv_icons.js
//	programmer	:	유명재(Yoo Myung-Jae)
//	project		:
//	description	:
//	version		:	2008-07-10 (v1.1)
//***********************************************************************************
//	note		:	Gift Icon 추가
//***********************************************************************************

var posX;
var posY;
var icon_form;

function ch_pos()
{
	posX = event.clientX + document.body.scrollLeft;
	posY = event.clientY + document.body.scrollTop;
}

function view_layer( form )
{
	icon_form = form;

	var obj = document.getElementById("layer_emoticon");
	obj.style.left = posX;
	obj.style.top  = posY;

	if (obj.style.visibility == "hidden"){
		obj.style.visibility = "visible";
	} else {
		obj.style.visibility = "hidden";
	}
}

function change_emoticon(num)
{
	var obj = document.getElementById("layer_emoticon");

	if ( num == '111' || num == '112' )
		num = '100';

	document.UserForm.emoticon.src = "../icon/position_" + num + ".gif";

	document.UserForm.imoti.value = num;
	obj.style.visibility = "hidden";
}

var position_number=101;
var position_max=103;
var set_position_number=position_number;
var PX = 4;
var PY = 1;
var set_position_name = new Array();
set_position_name[0] = "없음";
set_position_name[1] = "흑장미";
set_position_name[2] = "흑기사";
set_position_name[3] = "독수리";

document.write ("<div id=\"layer_emoticon\" border=\"0\" style=\"left:0;top:100px;position:absolute;visibility:hidden;z-index:9999;\">");
document.write ("<table id=\"subview01\" width=\"301\" bgcolor=\"#f0F0f2\" border=\"2\" bordercolor=\"#A5B6DE\" cellpadding=\"3\" cellspacing=\"0\" style=\"border-collapse:collapse\" onmouseover=\"document.all.layer_emoticon.style.visibility='visible'\" onmouseout=\"document.all.layer_emoticon.style.visibility='hidden'\">");
for(var i=0 ; i<PY ; i++) {
	document.write ("	<tr align=\"center\" > ");
	for(var j=0 ; j<PX ; j++) {
		if(position_number>position_max) set_position_number=100;
		else set_position_number=position_number;
		document.write ("		<td border=\"0\" onClick=\"javascript:change_emoticon('"+set_position_number+"')\" style=\"cursor:pointer\" >");
		document.write ("	              <img src=\"../icon/position_"+set_position_number+".gif\" width=\"25\" height=\"26\" style=\"margin:5px\" border=\"0\" > ");
		document.write ("<br/>");
		document.write (set_position_name[set_position_number-100]);
		document.write ("	              </td> ");
		position_number++;
	}
	document.write ("	</tr>");
}
document.write ("</table>");
document.write ("</div> ");

var i,j;
var icon_number=101, icon_row=4;
var I=new Array(5);
I[0]=0;
I[1]=92;
I[2]=184;
I[3]=276;
I[4]=368;


document.write ("<map name=\"emoticon_map\">")
for(i=0; i<5; i++)
{
	for(j=0; j<icon_row; j++)
	{
		document.write (" <area shape=\"rect\" coords='"+I[j]+","+I[i]+","+I[j+1]+","+I[i+1]+"' onfocus=\"this.blur()\" href=\"javascript:change_emoticon('"+icon_number+"')\"> ");
		icon_number++;
	}
}
document.write ("	</map> ");


//***********************************************************************************
//	icon GIFT
//***********************************************************************************




var posGx;
var posGy;
var giftType;

function gift_Pos()
{
	posGx = event.x + document.body.scrollLeft;
	posGy = event.y + document.body.scrollTop;
}

function gift_Layer( no )
{
	giftType = no;

	var obj = document.getElementById("layer_gift");
	obj.style.left = posGx - 270 +'px';
	// obj.style.left = posGx + 'px';
	obj.style.top  = posGy + 'px';

	//obj.style.left = 10000px;
	//obj.style.top  = 10000px;
	//alert(obj.style.top +"/"+obj.style.left);

	obj.style.zIndex="9999";
	if (obj.style.visibility == "hidden"){
		obj.style.visibility = "visible";
	} else {
		obj.style.visibility = "hidden";
	}
}

function trimVal(s){
	s = s.replace(/^\s*/,'').replace(/\s*$/, '');
	return s;
}


function gift_update( gicon )
{
	gicon = trimVal( gicon );
	if ( gicon == null || gicon.length <= 0 ) {
		gicon = "200;200;200";
	}

	arGift = new Array();
	arGift = gicon.split(";");

	if( arGift[0].length <= 0 ) 	arGift[0] = "200";
	if( arGift[1].length <= 0 ) 	arGift[1] = "200";
	if( arGift[2].length <= 0 ) 	arGift[2] = "200";

	document.UserForm.user_gift_1.src = "/icon/gift_" + arGift[0] + ".gif";
	document.UserForm.user_gift_1_value.value = arGift[0] ;

	document.UserForm.user_gift_2.src = "/icon/gift_" + arGift[1] + ".gif";
	document.UserForm.user_gift_2_value.value = arGift[1];

	document.UserForm.user_gift_3.src = "/icon/gift_" + arGift[2] + ".gif";
	document.UserForm.user_gift_3_value.value = arGift[2];

}


function gift_Change(num)
{
	var obj = document.getElementById("layer_gift");

	if ( num == "205" || num == "206" )
		num = "200";

	if( giftType == 1 ) {
		document.UserForm.user_gift_1.src = "/icon/gift_" + num + ".gif";
		document.UserForm.user_gift_1.width = 25;
		document.UserForm.user_gift_1.height = 25;
		document.UserForm.user_gift_1_value.value = num;
	} else if( giftType == 2 ) {
		document.UserForm.user_gift_2.src = "/icon/gift_" + num + ".gif";
		document.UserForm.user_gift_2.width = 25;
		document.UserForm.user_gift_2.height = 25;
		document.UserForm.user_gift_2_value.value = num;
	} else {
		document.UserForm.user_gift_3.src = "/icon/gift_" + num + ".gif";
		document.UserForm.user_gift_3.width = 25;
		document.UserForm.user_gift_3.height = 25;
		document.UserForm.user_gift_3_value.value = num;
	}
	obj.style.visibility = "hidden";
}

document.write ("<div id=\"layer_gift\" border=\"0\" style=\"left:-151;top:20;position:absolute;visibility:hidden\">");
document.write ("<table id=\"subview01\" width=\"251\" bgcolor=\"#FFFFFF\" border=\"1\" bordercolor=\"#A5B6DE\" cellpadding=\"3\" cellspacing=\"0\" style=\"border-collapse:collapse\" onmouseover=\"document.all.layer_gift.style.visibility='visible'\" onmouseout=\"document.all.layer_gift.style.visibility='hidden'\">");
document.write ("	<tr align=\"center\"> ");
document.write ("		<td border=\"0\" > ");
document.write ("	              <img src=\"/icon/gift_map.gif\" border=\"0\" usemap=\"#gift_map\" ></td> ");
document.write ("	</tr>");
document.write ("</table>");
document.write ("</div> ");

var Gi,Gj;
var gift_number=201, gift_row=3;
var GI=new Array(4);
GI[0]=0;
GI[1]=92;
GI[2]=184;
GI[3]=276;


document.write ("<map name=\"gift_map\">");
for(Gi=0; Gi<5; Gi++)
{
	for(Gj=0; Gj<gift_row; Gj++)
	{
		document.write (" <area shape=\"rect\" coords='"+GI[Gj]+","+GI[Gi]+","+GI[Gj+1]+","+GI[Gi+1]+"' onfocus=\"this.blur()\" href=\"javascript:gift_Change('"+gift_number+"')\"> ");
		gift_number++;
	}
}
document.write ("	</map> ");

