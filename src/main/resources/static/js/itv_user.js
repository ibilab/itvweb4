//***********************************************************************************
//	file name	:	itv_user.js
//	programmer	:	유명재(Yoo Myung-Jae)
//	project		:	
//	description	:	
//	version		:	2009-08-17
//***********************************************************************************
//	note		:	사용자 등록 Context 메뉴 추가 
//***********************************************************************************


var Layer_popup_close=2;
var status; 
var szUser_id;
var szImage_url;
var szMsg_com;
var szPgm_key;


function popup_Layer(event, popup_name, user_id, msg_com, pgm_key, image_url) 
{
	if( user_id == null || image_url == null  || pgm_key == null ) {
		alert( "이미지 설정 에러...  ");
	}
	
	szUser_id = user_id;
	szMsg_com = msg_com;
	szImage_url = image_url;
	szPgm_key = pgm_key;
	
     var main,_tmpx,_tmpy,_marginx,_marginy;
     main = document.getElementById(popup_name);
     if(Layer_popup_close==2)	{	status=1;		}
     
     main.style.display = '';
     // 툴팁부분 의존
     main.style.left = tt_x + "px";
     main.style.top = tt_y-150 + "px";
}  

function mouse_anchor(type) 
{ 
    if(type=="1"){   status="1";	}
    if(type=="0"){   status="0";	}
}

function Layer_popup_Off() 
{
	if(status=="0") {	document.getElementById("popup_table").style.display = "none";	}
	if(Layer_popup_close==2) {	status=null;	}
} 

function mouseout_close() 	{	window.setTimeout('Layer_popup_Off()',850);	}
if(Layer_popup_close==1)	{	document.onmousedown = Layer_popup_Off;	}
if(Layer_popup_close==2)	{	document.onmouseover=mouseout_close;	}
