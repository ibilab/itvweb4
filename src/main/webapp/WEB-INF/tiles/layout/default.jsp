<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="icon" href="data:;base64,iVBORw0KGgo=">
<script type="text/javascript" src="/js/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui.js"></script>
<link href="/css/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
// $(function() {
//     // input box 클릭시 파란색 boarder
//     $('.select_box>input,select').focusin(function(){
//         $(this).parent('.select_box').addClass('on');
//     });
//     $('.select_box>input,select').focusout(function(){
//         $(this).parent('.select_box').removeClass('on');
//     });
//     $('.select_b>.select_t').click(function() {
//         $(this).toggleClass('on');
//     });
//     $('.select_b').mouseleave(function(){
//         $(this).find('.select_t').removeClass('on');
//     });
// });
</script>
</head>
<body>
<tiles:insertAttribute name="body"/>
<script type="text/javascript" src="/js/common.js?v=${System.nanoTime()}"></script>
</body>
</html>