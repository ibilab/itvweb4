<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<footer id="footer">
    <div class="footer_inner">
        <div class="footer_logo">
            <img src="../img/common/img_footer_logo.gif" width="112" height="37" alt="m&amp;studio">
        </div>
        <div id="contactAddr" class="footer_info">
            E-mail: van@infobank.net<br>
            Tel: 031-628-1520<br>
            Copyright ⓒ Infobank.Corp. All Right Reserved.
        </div>
    </div>
</footer>