<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="icon" href="data:;base64,iVBORw0KGgo=">
<style>
    /* 상단 공지사항 메뉴 */
    #top_notice_bar
    {
        height:25px;
        /*position:fixed; */
        right:0; z-index:10;
        background-color: #24aae2;
        align:center;
    }

    /* 상단 공지사항 메뉴내에 TXT*/
    #top_notice_bar  h4
    {padding-top:6px; padding-left:10px; color: #ffffff; }
</style>
<div id="top_notice_bar">
    <a href="#" onclick = "javascript:oldNoticePopup()">
        <h4>[중요] 방송 참여 내역 보관 기간(6개월) 공지 드립니다.</h4>
    </a>
</div>
<form name="topPform" method="post" action="BoardAction.do" style="margin-bottom: 0;margin-top:0;">
    <header id="header">
        <h1><a href="#"><img src="../img/common/img_header_logo.gif" width="250" height="36" alt="m&amp;studio"/></a>
        </h1>

        <div id="arsDiv" style="position:absolute; top:10px; left:205px;">
            <img src="../img/common/img_ars_logo.png" width="60" height="60" alt="m&amp;studio"/>
        </div>

        <div id="arsDiv" style="position:absolute; top:20px; left:270px;">
            <div class="select_box" style="width:200px; height:45px;">
				<textarea id="ars_msg" name="ars_msg" rows="" cols=""
                          placeholder="불편 사항 및 추가기능을&#13;&#10;입력하고 '소통하기' 버튼을 눌러주세요^^."
                          maxlength="1000" style="width:98%; height:40px;border:0 none; outline:0 none;"></textarea>
            </div>
        </div>
        <div id="arsDiv" style="position:absolute; top:40px; left:485px;">
            <span class="btn_btype01" id="btnNew"> <a href="javascript:changeProgramSession(1);">소통하기 ☞</a></span>
        </div>

        <!-- <div id="updateDiv" style="position:absolute; top:5px; left:645px;">
            <img src="http://merge.test.co.kr:2189/img/notice/update_content_20180717.png" width="320" height="70" alt="" />
        </div> -->
        <div class="tnb">
            <div class="channel_box">
                <div class="inner">
                    <c:choose>
                    <c:when test="${operator.group_level <= 2}">
                        <!-- pd의 경우 -->
                        <div style="float: left">
                            <img src="../../img/common/menu/bg_channelprogram_txt.gif" width="48" height="32"
                                 alt="m&amp;studio"/>
                        </div>
                        <select id="ch_key" name="ch_key" class="channel_select select_b" onchange="getTopPgms()">
                            <c:forEach items="${chTopList}" var="row">
                            <option value="<c:out value='${row.ch_key}'/>" <c:if test='${operator.ch_key == row.ch_key}'> selected </c:if> ><c:out value='${row.ch_name}'/></option>
                            </c:forEach>
                        </select>
                        <select id="pgm_key" name="pgm_key" class="program_select select_b" onchange="changeProgramSession(this.value)">
                            <c:forEach items="${pgmTopList}" var="row">
                                <option value="<c:out value='${row.pgm_key}'/>" <c:if test='${operator.pgm_key == row.pgm_key}'> selected </c:if> ><c:out value='${row.pgm_name}'/></option>
                            </c:forEach>
                        </select>
                    </c:when>
                    <c:otherwise>
                        <!-- 작가의 경우 -->
                        <div class="channelprogram_txt">
                            <ul>
                                <li><c:out value='${operator.ch_name}'/></li>
                                <li><strong><c:out value='${operator.pgm_name}'/></strong></li>
                            </ul>
                        </div>
                    </c:otherwise>
                    </c:choose>
                    <p class="week_time">
                        <strong><c:out value="${fn:replace(programInfo.pgm_week, '|', '/')}"/></strong> <br>
                        <strong class="time"><c:out value="${programInfo.pgm_stm} ~ ${programInfo.pgm_etm}"/></strong>
                    </p>
                </div>
            </div>
            <div class="channel_num">#9999</div>
        </div>
        <c:set var="tmp"/>
        <div class="gnb">
            <!-- 메뉴 반복  : START -->
            <c:forEach var="full" items="${fullMenus}" varStatus="i">
            <c:set var="order" value="${full.menu_order }"/>
            <%
                String order = (String) pageContext.getAttribute("order");
                String order_1 = order.substring(0, 1);
                String order_2 = order.substring(1, 2);
                pageContext.setAttribute("order_1", order_1);
                pageContext.setAttribute("order_2", order_2);
            %>

            <!-- 메인메뉴 -->
            <c:choose>
                <c:when test="${full.topMenu }">
                    <c:set var="tmp" value="1"/>
                    <c:if test="${i.first}">
                        <ul>
                    </c:if>
                    <c:if test="${!i.first}">
                        </ul></li>
                        </ul></li>
                    </c:if>
                    <li>
<%--                    <a <c:if test="${order_1 == menu_1_order  }">class="on"</c:if> id="top_menu_${order_1}" href="#"><c:out value="${full.menu_name }"/><span class="arrow"></span></a>--%>
                    <a id="top_menu_${order_1}" class="top_menus" href="#"><c:out value="${full.menu_name }"/><span class="arrow"></span></a>
                </c:when>

                <c:when test="${full.middleMenu }">
                    <c:choose>
                        <c:when test="${tmp == 1 }">
                            <ul>
                            <c:set var="tmp" value="0"/>
                        </c:when>

                        <c:otherwise>
                            </ul></li>
                        </c:otherwise>
                    </c:choose>
                    <li>
                    <!-- 공지사항 작업. 2015.5.18 Lucas : START -->
                    <!-- 메뉴 depth-1에서 '공지사항'이 아닌 경우 -->
                    <c:if test="${full.menu_name != '공지사항'}"><!-- eg, menu_info : 1101, 3201 -->
                        <a href="javascript:goURL('<c:out value='${full.menu_url}'/>', '<c:out value='${full.menu_order}'/>');"
                            class="m<c:out value='${order_1}'/>_<c:out value='${order_2}'/>"><c:out value="${full.menu_name }"/></a>
                    </c:if>
                    <!-- 메뉴 depth-1에서 '공지사항'인 경우 -->
                    <c:if test="${full.menu_name == '공지사항'}"><!-- eg, 1000, 3200 -->
                        <c:choose>
                            <c:when test="${domainInfo == 'jejumbcnew.mand.co.kr' || domainInfo == 'mbcnew.mand.co.kr' || domainInfo == 'bbsinew.mand.co.kr' || domainInfo == 'bsnew.mand.co.kr' || domainInfo == 'gugakfmnew.mand.co.kr'	|| domainInfo == 'tbnnew.mand.co.kr' || domainInfo == 'kfmnew.mand.co.kr' || domainInfo == 'kbsnew.mand.co.kr' || domainInfo == 'cbsnew.mand.co.kr' || domainInfo == 'jjcbsnew.mand.co.kr' || domainInfo == 'merge.test.co.kr'}">
                                <a href="javascript:openForceNoticePopup();" class="m<c:out value='${order_1}'/>_<c:out value='${order_2}'/>"><c:out value="${full.menu_name }"/></a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:oldNoticePopup();" class="m<c:out value='${order_1}'/>_<c:out value='${order_2}'/>"><c:out value="${full.menu_name }"/></a>
                            </c:otherwise>
                        </c:choose>

                    </c:if>
                    <!-- 공지사항 작업. 2015.5.18 Lucas :  END-->
                    <ul>
                </c:when>
                <c:otherwise>
                    <li>
                    <!-- 공지사항 작업. 2015.5.18 Lucas : START -->
                    <!-- 메뉴 depth-2에서 '공지사항'이 아닌 경우 --><!-- eg, menu_info : 1101, 3201 -->
                    <c:if test="${full.menu_name != '공지사항'}">
                        <a href="javascript:goURL('${full.menu_url}', '${full.menu_order}');">- ${full.menu_name}</a>
                    </c:if>
                    <!-- 메뉴 depth-2에서 '공지사항'인 경우 -->
                    <c:if test="${full.menu_name == '공지사항'}">
                        <c:choose>
                            <c:when test="${domainInfo == 'bbsinew.mand.co.kr' || domainInfo == 'bsnew.mand.co.kr' || domainInfo == 'gugakfmnew.mand.co.kr'	|| domainInfo == 'tbnnew.mand.co.kr' || domainInfo == 'kfmnew.mand.co.kr' || domainInfo == 'kbsnew.mand.co.kr' || domainInfo == 'cbsnew.mand.co.kr' || domainInfo == 'jjcbsnew.mand.co.kr'}">
                                <a href="javascript:openForceNoticePopup();">- ${full.menu_name}</a>
                                <img id="noticeInnerNewIcon"
                                     src="http://merge.mand.co.kr:2189/img/notice/notice_new_btn.png"
                                     title="New"/>
                                <img id="noticeTopNewIcon"
                                     src="http://merge.mand.co.kr:2189/img/notice/notice_new_btn.png"
                                     title="New"/>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:oldNoticePopup();">- ${full.menu_name}</a>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <!-- 공지사항 작업. 2015.5.18 Lucas : END -->
                    </li>
                    <!-- 메뉴 depth-1 경우 -->
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <!-- 메뉴 반복  : END -->
                </ul></li>
                </ul></li>
            </ul>
        </div>
        <div class="user_setting">
            <button type="button" class="user_cont"><img src="../img/common/menu/img_user.gif" width="21" height="21"
                                                         alt=""/><c:out value="${operator.oper_id}"/><span
                    class="arrow"></span></button>
            <div class="user_infolayer">
                <div class="inner">
                    <ul class="user_info">
                        <li>
                            <span class="user_tit">아이디</span>
                            <span class="user_sub"><c:out value="${operator.oper_id}"/></span>
                            <a href="/logout.do" class="logout"><img
                                    src="../img/common/btn/btn_logout.gif" width="87" height="26" alt="로그아웃"/></a>
                        </li>
                        <li>
                            <span class="user_tit"
                                  <c:if test="${operator.group_level < 2}">ondblclick="goAdminBrChangePop();"</c:if>>이름</span>
                            <span class="user_sub"><c:out value="${operator.oper_name}"/></span>
                        </li>
                    </ul>
                    <ul class="user_info tline">
                        <li>
                            <span class="user_tit">현재 패스워드</span>
                            <span class="user_sub02"><input type="password" name="oper_pw" id="oper_pw"
                                                            class="input_txt"/></span>
                        </li>
                        <li>
                            <span class="user_tit">새 패스워드</span>
                            <span class="user_sub02"><input type="password" name="oper_newpw1" id="oper_newpw1"
                                                            class="input_txt"/></span>
                        </li>
                        <li>
                            <span class="user_tit">패스워드 확인</span>
                            <span class="user_sub02"><input type="password" name="oper_newpw2" id="oper_newpw2"
                                                            class="input_txt"/></span>
                        </li>
                    </ul>
                    <div class="user_midifybtn">
                        <a href="javascript:updatePassword();">수 정</a>
                        <a href="javascript:void(0);" class="user_close">닫 기</a>
                    </div>
                </div>
            </div>
        </div>
        <c:if test="${useWidget=='1'}">
            <div class="aside_btn">
                <button type="button">side menu</button>
            </div>
        </c:if>
    </header>
    <!--Top 끝-->

    <input type="hidden" name="cmd" value="forms">
    <input type="hidden" name="changePgmSession" value="Y">
    <%--    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}" />">--%>
    <%--    <input type="hidden" name="ch_key" value="<c:out value="${operator.ch_key}" />">--%>
    <input type="hidden" name="ch_name" value="<c:out value="${operator.ch_name}" />">
    <input type="hidden" name="pgm_name" value="<c:out value="${operator.pgm_name}" />">
    <%--    <input type="hidden" name="pgm_start" value="<c:out value="${operator.pgm_start}" />">--%>
    <script type='text/javascript' src="/js/md5.js"></script>
    <script type='text/javascript' src='/js/cookie.js'></script>
</form>
<script>

    $(function () {
        let menu_order = getCookie("menu_order");
        let top_menu = menu_order.substr(0,1);
        let child_menu = menu_order.substr(1,1);

        $('.gnb .on').removeClass("on");
        $('#top_menu_'+ top_menu).addClass("on");
        $('.cont_topl .path_area li:first-child').text($('#top_menu_' + top_menu).text());
        $('.cont_topl .path_area li:last-child').text($('.m' + top_menu + '_' + child_menu).text());
        //gnb
        $('.gnb>ul>li').bind('mouseover focusin', function(){
            $('select:focus').blur();
            $('.gnb').css({'height':'365px'});
            $(this).removeClass('on');
            $(this).addClass('on');
        });
        $('.gnb>ul>li').bind('mouseleave focusout', function(){
            $('.gnb').css({'height':'40px'});
            $(this).removeClass('on');
        });

        // operator
        $('.user_cont').click(function(){
            $(this).toggleClass('on');
            $(this).next('.user_infolayer').toggle();

            let f = document.topPform;
            f.oper_pw.value="";
            f.oper_newpw1.value="";
            f.oper_newpw2.value="";
        });

        // $('.user_infolayer').bind('mouseleave focusout', function() {
        //     $('.user_cont').removeClass('on');
        //     $('.user_infolayer').hide();
        // });
        $('.user_close').click(function(){
            $('.user_cont').removeClass('on');
            $('.user_infolayer').hide();
        });
    });

    function goURL(url, menu_order){
        setCookie("menu_order", menu_order, "/", "<%=request.getServerName()%>");
        setCookie("top_menu", menu_order, "/", "<%=request.getServerName()%>");
        if(url!=''){
            location.href = url;
        }else{
            alert("경로 설정이 안되어 있습니다.");
        }
    }

    function getTopPgms() {
        const frm = document.topPform;
        const ch_key = frm.ch_key.value;
        const url = "/PgmAction.do/withTime";
        const data = "ch_key=" + ch_key;
        $.ajax({
            url: url,
            type: "post",
            dataType: "html",
            data: data,
            error: ajaxError,
            success: makeTopOptions
            //beforeSend : ajaxOnCreate,
            //complete : ajaxOnComplete
        });
    }

    function ajaxError(e) {
        alert("error : getTopPgms");
        console.log(e.responseText);
    }

    function makeTopOptions(json) {
        const obj = JSON.parse(json);
        let pgm_key = obj[0].pgm_key;

        // TODO fix options
        //$("#pgm_key").empty();
        for (let i = 0; i < obj.length; i++) {
            let option = $("<option value='" + obj[i].pgm_key + "'>" + obj[i].pgm_name + "</option>");
            $("#pgm_key").append(option);
        }

        // tiles - (body -> boardsearch) -> boardlist
        changeProgramSession(pgm_key);
    }

    function changeProgramSession(pgm_key) {
        // if (pgm_key != "") {
            document.topPform.pgm_key.value = pgm_key;
            // if (pgm_start) document.topPform.pgm_start.value = pgm_start;
            // if (pgm_name) document.topPform.pgm_name.value = pgm_name;
            document.topPform.submit();
        // }
    }

    function updatePassword() {
        let f = document.topPform;

        if(isValidPassword()) {
            var url = "<c:url value='/UserAction.do/updateOperPW' />";

            var data = "oper_id=" + '<c:out value="${operator.oper_id}" />' + "&oper_pw=" + hex_md5(f.oper_newpw1.value);
            $.ajax({
                url: url,
                type : "post",
                dataType : "json",
                //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
                data : data,
                error : ajaxError,
                success : resultUpdateOperPW,
                beforeSend : ajaxOnCreate,
                complete : ajaxOnComplete
            });
        }
    }

    function resultUpdateOperPW(result) {
        let f = document.topPform;
        var data = result;
        if(data == "false") {
            alert("패스워드 수정에 실패 하었습니다.");
        } else {
            alert("패스워드가 수정되었습니다.");
            f.oper_pw.value="";
            f.oper_newpw1.value="";
            f.oper_newpw2.value="";
            location.reload();
        }
    }

    function isValidPassword() {
        let f = document.topPform;
        // 세가지 모두 입력했는지
        if(emptyData(f.oper_pw, "현재 패스워드를 입력해주세요.")) return false;
        if(emptyData(f.oper_newpw1, "새 패스워드를 입력해주세요.")) return false;
        if(emptyData(f.oper_newpw2, "패스워드 확인을 입력해주세요.")) return false;
        // 현재 패스워드가 입력한 패스워드가 일치하는지
        // TODO 현재 암호화 안된 패스워드 그대로도 사용하고 있는데 추후 DB에 정보가 바뀌면 수정해야 할듯 (if문 두번째 조건)
        if('<c:out value="${operator.oper_pw}" />' != hex_md5(f.oper_pw.value) && '<c:out value="${operator.oper_pw}" />' != f.oper_pw.value) {
            alert("입력한 패스워드가 일치 하지 않습니다.");
            f.oper_pw.value = "";
            f.oper_pw.focus();
            return false;
        }
        // 새로운 두가지 패스워드가 서로 맞는지
        if(f.oper_newpw1.value != f.oper_newpw2.value) {
            alert("새 패스워드 값이 서로 일치 하지 않습니다.");
            f.oper_newpw1.focus();
            return false;
        }
        return true;
    }

    function oldNoticePopup() {
        var imgUrl = "http://mnstudio.mand.co.kr:2189/page/notice/mnstudio_popup_20150601.jpg";

        var imgObj = new Image();
        imgObj.src = imgUrl;

        var width = 550;
        var height = 528;

        var sw = screen.availWidth;
        var sh = screen.availHeight;
        var px = (sw-width)/2;
        var py = (sh-height)/2;

        var set ='top = '+py+', left='+px;

        set += ',width='+width+',height='+height+',directories=no,titlebar=no,menubar=no,toobar=no,location=no,resizable=no,status=no,scrollbars=yes';

        window.open(imgUrl, 'notice', set);
    }
</script>