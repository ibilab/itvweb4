<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <script type="text/javascript" src="../js/jquery/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
    <link href="../css/common.css" rel="stylesheet" type="text/css">
<%--    <link href="../css/overcast/jquery-ui.min.css" rel="stylesheet" type="text/css"/>--%>
<style>

    .ui-progressbar {
        position: relative;
    }

    .progress-label {
        position: absolute;
        left: 45%;
        top: 4px;
        font-weight: bold;
        text-shadow: 1px 1px 0 #fff;
    }

    #progressbar .ui-progressbar-value {
        background-color: #ccc;
    }
</style>
</head>
<body class="bg_body">
<div>
    <tiles:insertAttribute name="header"/>
    <section id="container" on>
        <article class="contents">
        <tiles:insertAttribute name="body"/>
        </article>
        <aside class="aside" id="aside">
            <tiles:insertAttribute name="right" />
        </aside>
    </section>
    <tiles:insertAttribute name="footer"/>
</div>
<div id="progressbar" style="position:absolute;left:10%;top:500px;width:800px;z-index:999">
    <div class="progress-label">Loading...</div>
</div>
</body>
<script type="text/javascript">

    $(function() {
        try {
            $( "#progressbar" ).progressbar({
                value: false
            });
            $( "#progressbar" ).find( ".ui-progressbar-value" ).css({"background": "#FFFFFF"});
        } catch(e) {

        }
        $('.t_calendar').on('click', function () {
            $(this).toggleClass('on');
            $("#" + $(this).find('input').attr("id")).datepicker( "show" );
            if($(this).find('input').attr("id")) $("#ui-datepicker-div").show();
            $("#ui-datepicker-div").mouseleave(function() {
                $(this).hide();
            });
            return false;
        });
        $('.month_btn').on('click', function(){
            $(this).toggleClass('on');
            $(this).prev().find('input').datepicker('show');
            if($(this).prev().find('input')) $("#ui-datepicker-div").show();
            $("#ui-datepicker-div").mouseleave(function() {
                $(this).hide();
            });
            return false;
        });

        $(window).scroll( function() {
            let auto_scroll_widget_yn = getCookie("auto_scroll_widget_yn");
            let notice_height = $("#top_notice_bar").height() ? $("#top_notice_bar").height() : 0;
            let header_height = $("#header").height();
            let img_height = 70;
            if(auto_scroll_widget_yn == "Y"){
                $("#aside").css({
                    "top" : "0px"
                    ,'position': 'fixed'
                });
            } else {
                let scrolled_val = $(document).scrollTop().valueOf();
                let tops = (notice_height + header_height);

                if(scrolled_val > (tops + img_height)){
                    let moveScroll = scrolled_val - (tops + img_height) + 1;
                    $("#aside").css({
                        "top" : moveScroll+"px"
                        ,'position': 'relative'
                    });
                } else {
                    $("#aside").css({
                        "top" : "0px"
                        ,'position': 'fixed'
                    });
                }
            }
        });
    });
</script>
<script type="text/javascript" src="../js/common.js?v=${System.nanoTime()}"></script>
</html>