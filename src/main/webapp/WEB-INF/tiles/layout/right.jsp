<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <%
        String session_tab = "";
        session_tab = (String)session.getAttribute("session_tabs");
        String[] arrTab = session_tab.split("\\|");
    %>
<script language="JavaScript">
    let asideTab = '0';
    $(document).ready(function() {
        changeWidgetTabs("2");
    });

    function changeWidgetTabs(idx){
        // 탭 처리
        $(".aside_tab li").each(function(index, item) {
            $(item).removeClass("on");
        });

        // 탭 내용 처리
        $("#aside_cont0 div").each(function(index, item) {
            $(item).css("display", "none");
        });

        // 선택 내용 처리
        $("#aside_tab" + idx).addClass("on");
        $("#aside_cont" + idx).css("display", "block");

        // TODO 이것때문에 오류...
        //console.log(idx);
        try {
            if(asideTab != idx) window["frame" + idx].initPage();
        } catch(e) {
        }
        asideTab = idx;
    }

    function setWidgetFrame(tab_idx, data = "") {
        if(tab_idx=="2") {
            if($("#" + tab_idx).css("display") != "none") {
                let url = "showUserInfo";
                if(data!="") {
                    const user_id = data.user_id;
                    const msg_com = data.msg_com;
                    const user_name = data.user_name;
                    const comment_id = data.comment_id;
                    const board_key = data.board_key;
                    const table_name = data.table_name;
                    data = 'user_id=' + escape(encodeURIComponent(user_id)) + '&msg_com=' + msg_com
                        + '&user_name=' + escape(encodeURIComponent(user_name)) + '&comment_id=' + comment_id
                        + '&board_key=' + board_key + '&table_name=' + table_name;
                }
                frame2.location.href =  "<c:url value='/UserAction.do' />" + "/" + url + "?" + data;
                // 시간차 둬야 크롬에서 오류 안남
                //setTimeout("changeWidgetTabs(2)", 1000);
            }
        }
    }
</script>
</head>
<body>
<form>
    <div class="asidelayer_wrap" id="aside_setting" style="display:none;">
        <div class="asidelayer_mask"></div>
        <div class="asidelayer">
            <div class="asidelayer_inner">
                <span class="arrow"></span>
                <div class="asidelayer_content">
                    <p><strong>탭 추가</strong></p>
                    <ul class="aside_tab_list">
                        <li class="aside_t02">
                            <input type="checkbox" class="checkbox" onclick="return false;"  id="aside_t02" name="chktabs" <%if(arrTab[1].equals("1")){%>checked<%} %>/>
                            <label for="aside_t02">유저정보 <span>게시판 등에서 선택한 유저의 정보 확인</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="asidelayer_bottom">
                    <a href="javascript:setWidgetSession();">적용</a>
                    <a href="#" class="aside_close">닫기</a>
                </div>
            </div>
        </div>
    </div>

    <!-- aside tab -->
    <ul class="aside_tab">
        <li id="aside_tab2"><a href="javascript:changeWidgetTabs('2');" class="aside_tab02">유저정보<span
                class="arrow"></span></a></li>
        <!-- <li><a href="#" class="aside_tab04">토표설정<span class="arrow"></span></a></li>
        <li><a href="#" class="aside_tab05">보관함<span class="arrow"></span></a></li> -->
        <!--
        <li class="setting"><a href="#aside_setting" class="aside_layerb">Setting</a></li>
         -->
    </ul>
    <!-- //aside tab -->
    <div id="aside_cont0">
        <!--
        <div id="aside_cont1" class="aside_cont" style="display:none;">
            <%-- <iframe frameborder="0" id="frame1" name="frame1" width="380px" height="900px" scrolling="no" src="<c:url value='WidgetStatGoogle.jsp'/>"></iframe> --%>
            <iframe frameborder="0" id="frame1" name="frame1" width="380px" height="900px" scrolling="no" src="<c:url value='/StatAction.do?cmd=statWidgetMng'/>"></iframe>
        </div>
         -->
        <div id="aside_cont2" class="aside_cont" style="display:none;">
            <iframe frameborder="0" id="frame2" name="frame2" width="380px" height="900px" scrolling="no" src="<c:url value='/UserAction.do/showUserInfo'/>"></iframe>
        </div>
        <!--
        <div id="aside_cont3" class="aside_cont" style="display:none;">
            <iframe frameborder="0" id="frame3" name="frame3" width="380px" height="900px" scrolling="no" src="<c:url value='/BoardAction.do?cmd=boardBox' />"></iframe>
        </div>
         -->
        <div id="aside_cont4" class="aside_cont" style="display:none;">
        </div>
    </div>


    <div id="widget_setting"
         style="position:absolute;left:0px;top:0px;width:500;height:500;display:none;z-index:1;background:#ffffff">
        <table>
            <tr>
                <td>탭추가
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="chktabs_b" value="001" <%if(arrTab[1].equals("1")){%>checked<%} %>
                           onclick="return false;">유저정보
                </td>
                <td>게시판 등에서 선택한 유저의 정보 확인
                </td>
            </tr>
            <%-- 			<tr>
                            <td>
                                <input type="checkbox" name="chktabs" value="003" <%if(arrTab[3].equals("1")){%>checked<%} %>>투표 설정
                            </td>
                            <td>투표 설정/현황을 실시간 모니터링
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="chktabs" value="004" <%if(arrTab[4].equals("1")){%>checked<%} %>>보관함
                            </td>
                            <td>보관함 리스트 통합뷰어
                            </td>
                        </tr> --%>
        </table>
        <input type="button" value="적용" name="setOption" onClick="setWidgetSession();">
        <input type="button" value="닫기" name="close" onClick="$('#widget_setting').fadeOut();">
    </div>
</form>
</body>
</html>