<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>

<div class="table_type01">
    <input type="hidden" name="chkBoxValid" value="true">
    <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
        <caption>문자메시지 리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:45px;" />
            <col style="width:140px;" />
            <col style="width:155px;" />
            <col style="width:auto;" />
            <col style="width:20px;" />
            <col style="width:20px;" />
            <col style="width:127px;" />
            <col style="width:135px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col"><div id="chkAllBox"><a href="#" onclick = "javascript:checkAllBtn('chkAllBox','chk_board_key', true)">선택</a></div></th>
            <th scope="col">N</th>
            <th scope="col">보낸사람</th>
            <th scope="col" colspan="5">내용</th>
            <th scope="col">보낸시간</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${BoardForm.list}" varStatus="i">
            <c:set var="imgMsgCom" value="" />
            <c:set var="imgAlt" value="" />
            <c:set var="setStar" value="" />
            <c:set var="setFirst" value="" />
            <c:choose>
                <c:when test="${row.mo_day>8}">
                    <c:set var="setFontColor" value="#E9001B" />
                    <c:set var="setStar" value="1" />
                </c:when>
                <c:when test="${row.mo_day>5}">
                    <c:set var="setFontColor" value="#FF8A00" />
                </c:when>
                <c:when test="${row.mo_day>2}">
                    <c:set var="setFontColor" value="#002AFF" />
                </c:when>
                <c:otherwise>
                    <c:set var="setFontColor" value="" />
                </c:otherwise>
            </c:choose>
            <!--  새싹 멤버 세팅  -->
            <c:if test="${row.mo_day==row.mo_total}">
                <c:set var="setFirst" value="1" />
            </c:if>
            <!-- SNS 세팅 -->
            <c:set var="imgMsgCom" value="/img/common/btn/btn_sns_${row.msg_com}.png" />
            <c:set var="imgAlt" value="매체" />
            <tr class="msg_table_tr
                        <c:if test="${row.msg_read == 1}"> msg_read_tr </c:if>
<%--                    <c:if test="${row.msg_read == 1}">--%>
<%--                        <c:if test="${BoardForm.read_msg_table == true}">class=""</c:if>--%>
<%--                        <c:if test="${BoardForm.read_msg_table != true}">class="blind_area"</c:if>--%>
<%--                    </c:if>--%>
                    <c:if test="${setStar==1}">focus</c:if>" >
                <!-- 체크 박스 적용 Lucas. 2015.3.11 : START -->
                <c:if test="${param.list_type!='userBox'}">
                    <td class="ac">
                        <input	type="checkbox" name="chk_board_key" value="<c:out value='${row.board_key};${row.msg_userid};${row.table_name};${row.msg_com};${row.msg_key}'/>" />
                    </td>
                </c:if>
                <!-- 체크 박스 적용 Lucas. 2015.3.11 : START -->

                <td class="ac"><c:out value='${BoardForm.start_index-i.index}' /></td>
                <td class="name">
				<span>
				<!-- 유저 아이콘 -->
				<c:choose>
                    <c:when test="${empty row.user_icon}">
                        <img src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                    </c:when>
                    <c:when test="${row.user_icon == '100' }">
                        <img  src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                    </c:when>
                    <c:otherwise>
                        <img src='../icon/position_<c:out value="${row.user_icon}"/>.gif' width="24" height="24" border="0"  name="emoticon" >
                    </c:otherwise>
                </c:choose>
				</span>
                    <!-- 유저 명 -->
                    <c:choose>
                        <c:when test="${row.user_grade!='0'}">
                            <%-- <a href="javascript:popUserInfo('<c:out value='${row.msg_userid}'/>', '<c:out value='${row.msg_com}'/>',  '<c:out value='${row.msg_username}'/>',  '<c:out value='${row.comment_id}'/>')"> --%>
                            <a href="javascript:popUserInfo('<c:out value='${row.msg_userid}'/>', '<c:out value='${row.msg_com}'/>',  '<c:out value='${row.msg_username}'/>',  '<c:out value='${row.comment_id}'/>' ,  '<c:out value='${row.board_key}'/>' ,  '<c:out value='${row.table_name}'/>')" onmouseover="Tip('<c:out value='번호:${row.user_phone_dash}<br>별명:${row.user_nick}<br>금일:${row.mo_day}<br>금월:${row.mo_month}<br>누적:${row.mo_total}<br>'/>'); ">

                                <!-- 4자리 보여주기 Lucas. 2015.3.10 :START -->
                                <c:if test="${BoardForm.show_last_4_number == 1}">
                                    <c:if test="${((row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007) && row.msg_com == 001}">
                                        <c:out value='${row.user_nick}(${row.last_phone_number})' />
                                    </c:if>
                                    <c:if test="${((row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007) && row.msg_com != 001}">
                                        <c:out value='${row.user_nick}(${row.msg_username})' />
                                    </c:if>
                                    <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                                        <c:out value='${row.user_nick}' />
                                    </c:if>
                                    <c:if test="${(row.user_nick == '' || row.user_nick eq null) && row.msg_com == 001}">
                                        <c:out value='${row.last_phone_number}' />
                                    </c:if>
                                    <c:if test="${(row.user_nick == '' || row.user_nick eq null) && row.msg_com != 001}">
                                        <c:out value='${row.msg_username}' />
                                    </c:if>
                                </c:if>

                                <c:if test="${BoardForm.show_last_4_number != 1}">
                                    <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
                                        <c:if test="${row.msg_com == 001}">
                                            <c:out value='${row.user_nick}(${row.user_phone_dash})' />
                                        </c:if>
                                        <c:if test="${row.msg_com != 001}">
                                            <c:out value='${row.user_nick}(${row.msg_username})' />
                                        </c:if>
                                    </c:if>
                                    <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                                        <c:out value='${row.user_nick}' />
                                    </c:if>
                                    <c:if test="${row.user_nick == '' || row.user_nick eq null}">
                                        <c:if test="${row.msg_com == 001}">
                                            <c:out value='${row.user_phone_dash}' />
                                        </c:if>
                                        <c:if test="${row.msg_com != 001}">
                                            <c:out value='${row.msg_username}' />
                                        </c:if>
                                    </c:if>
                                </c:if>
                                <!-- 4자리 보여주기 Lucas. 2015.3.10 :END -->

                                <!--
								<c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
									<c:out value='${row.user_nick}(${row.msg_username})' />
								</c:if>
								<c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
									<c:out value='${row.user_nick}' />
								</c:if>
								<c:if test="${row.user_nick == '' || row.user_nick eq null}">
									<c:out value='${row.msg_username}' />
								</c:if>
								 -->
                            </a>
                        </c:when>
                        <c:otherwise>
                            <c:out value='${row.user_nick}(${row.msg_username})' />
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <li>
                                <c:choose>
                                    <c:when test="${setFirst=='1'}">
                                        <img src="../img/common/icon/icon_first.gif" width="20" height="20" alt="새싹">
                                    </c:when>
                                    <c:when test="${setStar=='1'}">
                                        <img src="../img/common/icon/icon_star.gif" width="20" height="20" alt="별">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="../img/common/btn/btn_star_off.png" width="20" height="20" alt="별" />
                                    </c:otherwise>
                                </c:choose>
                            </li>
                            <li>
                                <!--  2014. 12. 17 Lucas -->
                                <!--  KBS 콩 and K플레이어. 모바일과 PC 아이콘 작업 -->
                                <img src="..
								<c:if test="${row.msg_com == 002 }" >
									<c:if test="${row.comment_id == 002 }" >
									/img/common/btn/btn_sns_002_kong_mobile.png
									</c:if>
									<c:if test="${row.comment_id == 003 }" >
									/img/common/btn/btn_sns_002_kplayer_mobile.png
									</c:if>
									<c:if test="${row.comment_id != 002 && row.comment_id != 003 }" >
									/img/common/btn/btn_sns_002_kong_pc.png
									</c:if>
								</c:if>
								<c:if test="${row.msg_com != 002}" >
									<c:out value='${imgMsgCom}'/>
								</c:if>
								" width="20" height="20" alt="<c:out value='${imgAlt}'/>" />
                            </li>
                        </ul>
                        <div class="iconbox02">
                            <ul>
                                <c:set var="msg_mmsurl" value="" />
                                <c:set var="mmsurl_cnt" value="0" />
                                <c:forTokens var="token" items='${row.msg_mmsurl}' delims="|" varStatus="i">
                                    <c:set var="mmsurl_cnt" value="${i.count }" />
                                </c:forTokens>
                                <c:choose>
                                    <c:when test="${mmsurl_cnt=='2'}">
                                        <c:set var="msg_mmsurl" value="${row.msg_mmsurl}| " />
                                    </c:when>
                                    <c:when test="${mmsurl_cnt=='1'}">
                                        <c:set var="msg_mmsurl" value="${row.msg_mmsurl}| | " />
                                    </c:when>
                                    <c:when test="${mmsurl_cnt=='0'}">
                                        <c:set var="msg_mmsurl" value=" | | |" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="msg_mmsurl" value="${row.msg_mmsurl}" />
                                    </c:otherwise>
                                </c:choose>
                                    <%-- <c:if test="${!empty row.msg_mmsurl}"> --%>
                                <c:forTokens var="token" items='${msg_mmsurl}' delims="|" varStatus="i">
                                    <c:if test="${i.count < 4 }" >
                                        <%
                                            String fname = (String)pageContext.getAttribute("token");
                                            if(fname.length() > 4) {
                                                String ext = fname.substring(fname.length() - 4, fname.length());
                                                pageContext.setAttribute("nonspace_link", fname.replace(" ", "%20"));
                                                pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20"));
                                                int view_size = 30;
                                                if(fname.replace(" ", "%20").length() > view_size) {
                                                    pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20").substring(0, view_size) + "...");
                                                }
                                                if(ext.compareToIgnoreCase(".avi") == 0 || ext.compareToIgnoreCase(".mp4") == 0 ) {
                                        %>
                                        <li>
                                            <!-- <a href="javascript:aviDownloadPop('<c:out value='${row.msg_mmsurl}'/>','<c:out value='${token}'/>')"><img src="../img/common/btn/btn_movie2.png"  width="20" height="20"></a>  -->
                                            <a href="<c:out value='${row.msg_mmsurl}'/>" download><img src="../img/common/btn/btn_movie2.png"  width="20" height="20"></a></li>
                                        <%
                                        } else if(ext.compareToIgnoreCase(".m4a") == 0 ) {
                                        %>
                                        <li>
                                            <!-- <a href="javascript:aviPlayPop('<c:out value='${row.msg_mmsurl}'/>','<c:out value='${token}'/>')"><img src="../img/common/btn/btn_sound.png"  width="20" height="20"></a>  -->
                                            <a href="<c:out value='${row.msg_mmsurl}'/>" download><img src="../img/common/btn/btn_sound.png"  width="20" height="20"></a></li>
                                        </li>
                                        <%
                                        } else if(ext.compareToIgnoreCase("jpeg") == 0 || ext.compareToIgnoreCase(".jpg") == 0 || ext.compareToIgnoreCase(".gif") == 0 || ext.compareToIgnoreCase(".png") == 0) {
                                        %>
                                        <li><a href="#"></a>
                                            <img src="<c:out value='${token}'/>" width='20' height="20" onmouseover="if(this.src.indexOf('btn_noimg.png')!=-1)mmsTip('<img src=../img/common/btn/btn_noimg.png width=350>');else mmsTip('<img src=<c:out value='${nonspace_link}'/> width=350>');"
                                                 onclick="popup_Layer(event,'popup_table', '<c:out value='${row.msg_userid}'/>','<c:out value='${row.msg_com}'/>','<c:out value='${row.pgm_key }'/>', '<c:out value='${token}'/>' )"
                                                 onerror="javascript:this.src='../img/common/btn/btn_noimg.png'"/>
                                            </a>
                                        </li>
                                        <%
                                        } else {
                                            // short url
                                        %>
                                        <li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
                                        <%		}

                                        } else {
                                        %>
                                        <li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
                                        <%  }%>
                                    </c:if>
                                </c:forTokens>
                                    <%-- </c:if>		 --%>
                            </ul>
                        </div>
                    </div>
                </td>
                <td class="brd_txt  <c:if test="${setStar==1}">on</c:if>">
                    <!--
				<a href="javascript:void(0);" class="ellips_text" id="msg_<c:out value='${row.board_key }'/>" onmouseover="showTooltip('<c:out value='${row.board_key }'/>');"><font color="<c:out value='${setFontColor}'/>">
				 -->
                    <!-- 선물 아이콘 작업 2015. 2. 2 Lucas -->
                    <c:if test="${row.user_image != ' ' }">
                        <c:forTokens var="token" items='${row.user_image}' delims=";">
                            <c:if test="${token != '200' }">
                                <img src="/icon/gift_<c:out value='${token}'/>.gif"  border="0"  >
                            </c:if>
                        </c:forTokens>
                    </c:if>

                    <!--
				 <a href="javascript:void(0);" id="msg_<c:out value='${row.board_key }'/>" );"><font color="<c:out value='${setFontColor}'/>">
					<img src="../img/common/btn/btn_noimg.png" width="0" height="0" alt="<c:out value='${row.msg_username}'/> /"  />
					<aof:out value='${row.msg_data}' />
					</font></a>
					-->

                    <div class="p_txtcont" title="클릭하시면 글내용 복사가 가능합니다.">

                        <!-- 클립 보드에 복사 기능. Lucas. 2015.3.9 : START -->
                        <!-- <a href="#" onclick="javascript:window.clipboardData.setData('Text', '클립보드로 복사할 글'); ">Copy</a>  -->
                        <c:if test="${BoardForm.show_last_4_number == 1}">
                            <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007 && row.msg_com != 009}">
                                <input type="hidden" name="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.user_nick}(${row.last_phone_number})' />] <c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>

                            <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007 && row.msg_com == 009}">
                                <input type="hidden" name="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.user_nick}(${row.last_phone_number})' />] <c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>

                            <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                                <input type="hidden" name="original_msg_data_<c:out value='${row.board_key}' />"  value="<c:out value='${row.user_nick}' />] <c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>
                            <!-- 4자리 보기시, 고릴라 이름 짤리는 현상 수정. 2015.11.16. LUCAS : START -->
                            <c:if test="${(row.user_nick == '' || row.user_nick eq null) && row.msg_com == 009 }">
                                <input type="hidden" name="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.msg_username}' />] <c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>

                            <c:if test="${(row.user_nick == '' || row.user_nick eq null) && row.msg_com != 009 }">
                                <input type="hidden" name="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.last_phone_number}' />] <c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>
                            <!-- 4자리 보기시, 고릴라 이름 짤리는 현상 수정. 2015.11.16. LUCAS : END -->
                        </c:if>

                        <c:if test="${BoardForm.show_last_4_number != 1}">
                        <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
                            <!-- 포항MBC 복사시, 번호내에 '-' 추가. 2016.7.1 Lucas : START -->
                            <c:if test="${userBrKey == 'PHMBC'}">
                                <input type="hidden" id="original_msg_data_<c:out value='${row.board_key}' />"  value="<c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste_Dash('<c:out value='${row.user_nick}'/>', '<c:out value='${row.msg_username}'/>', '<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>
                            <c:if test="${userBrKey != 'PHMBC'}">
                                <input type="hidden" id="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.user_nick}(${row.msg_username})' />] <c:out value='${row.msg_data }'/>">
                                <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </a>
                            </c:if>
                            <!-- 포항MBC 복사시, 번호내에 '-' 추가. 2016.7.1 Lucas : END -->
                        </c:if>

                        <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                        <input type="hidden" id="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.user_nick}' />] <c:out value='${row.msg_data }'/>">
                        <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                <c:out value='${row.msg_data }'/>
                            </c:if>

                            <c:if test="${row.user_nick == '' || row.user_nick eq null}">
                            <!-- 포항MBC 복사시, 번호내에 '-' 추가. 2016.7.1 Lucas : START -->
                            <c:if test="${userBrKey == 'PHMBC'}">
                            <input type="hidden" id="original_msg_data_<c:out value='${row.board_key}' />"  value="<c:out value='${row.msg_data }'/>">
                            <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste_Dash('', '<c:out value='${row.msg_username}'/>', '<c:out value='${row.board_key}'/>');">
                                <c:out value='${row.msg_data }'/>
                            </a>
                            </c:if>
                            <c:if test="${userBrKey != 'PHMBC'}">
                            <input type="hidden" id="original_msg_data_<c:out value='${row.board_key}' />"  value="[<c:out value='${row.msg_username}' />] <c:out value='${row.msg_data }'/>">
                                <%-- <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('[<c:out value='${row.msg_username}' />] <c:out value='${row.msg_data }'/>');"> --%>
                            <a href="javascript:void(0);" onclick="javascript:boardClickCopyPaste('<c:out value='${row.board_key}'/>');">
                                <c:out value='${row.msg_data }'/>
                            </a>
                            </c:if>
                            <!-- 포항MBC 복사시, 번호내에 '-' 추가. 2016.7.1 Lucas : END -->
                            </c:if>
                            </c:if>
                            <!-- 클립 보드에 복사 기능. Lucas. 2015.3.9 : END -->
                    </div>
                </td>

                <td class="noline">
                    <c:if test="${row.msg_rtn == 1 }" >
                        <img src="../img/common/icon/icon_msgrtn.jpg" width="20" height="20" title="답장 완료">
                    </c:if>
                </td>

                <td class="noline"></td>
                <td class="noline">
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <li>
                                <a href="javascript:addPrize('<c:out value='${row.msg_com}'/>', '<c:out value='${row.msg_userid}'/>', '<c:out value='${row.pgm_key}'/>' )">
                                    <img src="../img/common/btn/btn_win.png" width="20" height="20" alt="당첨"  title="당첨"/>
                                </a>
                            </li>
                            <c:if test="${row.msg_read != 1}">
                                <li><a href="javascript:setRead('1', '<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>')"><img src="../img/common/btn/btn_blind_off.png" width="20" height="20" title="읽음 처리" /></a></li>
                            </c:if>
                            <c:if test="${row.msg_read == 1}">
                                <li><a href="javascript:setRead('0', '<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>')"><img src="../img/common/btn/btn_blind_on.png" width="20" height="20" title="안읽음 처리"/></a></li>
                            </c:if>
                            <li>
                                <a href="javascript:copyMsg('<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>' )">
                                    <img src="../img/common/btn/btn_down.png" width="20" height="20" title="보관메시지함으로 복사 또는 이동"/>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:showMsg(<c:out value='${row.msg_show}'/>,'<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>' )">
                                    <img src="../img/common/btn/btn_del_on.png" width="20" height="20" title="차단메시지함으로 이동"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
                <td class="ac"><c:out value='${row.msg_time}' /></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="40" align="center"><c:out value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
    </tr>
</table>
<div id='popup_table' style="position:absolute; left:0px; top:0px; z-index:1;display:none;" onmouseover="mouse_anchor('1');" onmouseout="mouse_anchor('0');">
    <table border="1" border-color="#000000" width="130" height="14" bgcolor='#eeeeee'>
        <tr valign=absmiddle align=center><td height="14" valign="top"><a href="javascript:setUserImage();" >개인 이미지로 등록</a><br></td></tr>
    </table>
</div>
<!-- 탭 눌렀을때 개인이미지 출력 되는 것 제거 부분 -->
<!--
<div id='popup_table_' style="position:absolute; left:0px; top:0px; z-index:1;display:none;" onmouseover="mouse_anchor('1');" onmouseout="mouse_anchor('0');">
<table border="1" border-color="#000000" width="130" height="14" bgcolor='#eeeeee'>
    <tr valign=absmiddle align=center><td height="14" valign="top"><a href="javascript:setUserImage();" >개인 이미지로 등록</a><br></td></tr>
</table>
</div>
 -->
<!--
<div id='popup_avi' style="position:absolute; left:0px; top:0px; z-index:1;display:none;" onmouseover="mouse_anchor('1');" onmouseout="mouse_anchor('0');">
<table border="1" border-color="#000000" width="130" height="14" bgcolor='#eeeeee'>
    <tr valign=absmiddle align=center>
    	<td height="14" valign="top">
			<audio controls preload="none" style="width:480px;">
			    <source src="./data/KAKAO_20141124145651726_drBeZPmFtndN.m4a" type="audio/mp4" />
			    <p>Your browser does not support HTML5 audio.</p>
			</audio>
    	</td>
    </tr>
</table>
</div>
 -->

<c:if test="${empty param.st_type}">
    <c:if test="${param.list_type!='userBox'}">
        <script type="text/javascript">
            $("#tot_cnt1").html(<c:out value='${BoardForm.total_row_count}'/>);
            $("#tot_cnt2").html(<c:out value='${BoardForm.tot_cnt2}'/>);
            <%-- 	<c:if test="${BoardForm.total_row_count > 0}">Element.show("button3");</c:if>
                <c:if test="${BoardForm.total_row_count == 0}">Element.hide("button3");</c:if> --%>
        </script>
    </c:if>
</c:if>
<script type="text/javascript">
    $(document).ready(function() {
        // 읽음메시지함
        if(activeTab != '888') {
            $(".msg_table_tr.msg_read_tr").addClass("blind_area");
        } else {
            $(".msg_table_tr.msg_read_tr").removeClass("blind_area");
        }
    });

    function boardClickCopyPaste(msg_key){
        // TODO 계산된 input hidden 보여준다... -> 여기서 계산하자...
        const msgData = $("#original_msg_data_" + msg_key + "").val();

        if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
            window.clipboardData.setData('Text', msgData);
            //alert('해당 글을 복사하였습니다.');
        } else { //IE 가 아닐때
            prompt("Ctrl+C를 눌러 클립보드로 복사하세요", msgData);
        }
    }

    function checkAllBtn(id, checks, isCheck){
        let fobj = $('#list'+activeTab).find("input[name='" + checks + "']");
        if(fobj == null) return;

        let valid = $('#list'+activeTab).find("input[name='chkBoxValid']");
        if(valid.val() == "true"){
            valid.val("false");
            isCheck = true;
        }
        else{
            valid.val("true");
            isCheck = false;
        }

        if(fobj.length){
            for(let i=0; i < fobj.length; i++){
                if(fobj[i].disabled != true ){
                    fobj[i].checked = isCheck;
                }
            }
        }else{
            fobj.checked = isCheck;
        }
    }

    function mmsTip(img) {
        if(Settings.setMmsMiddle()) {
            Tip(img, FIX, CalcFixXY());
        } else {
            Tip(img);
        }

    }
    function CalcFixXY(){
        let top  = window.pageYOffset || document.documentElement.scrollTop;
        let left = window.pageXOffset || document.documentElement.scrollLeft;
        top = top + 100;
        left = left + 350;
        return [left, top];
    }

</script>
</body>
</html>