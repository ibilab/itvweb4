<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
	<caption>문자메시지 리스트</caption>
	<colgroup>
		<col style="width:40px;" />
		<col style="width:77px;" />
		<col style="width:auto;" />
		<col style="width:80px;" />
	</colgroup>
	<thead>	
		<tr>
			<th scope="col">N</th>
			<th scope="col" colspan="2">내용</th>
			<th scope="col">보낸시간</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach var="row" items="${BoardForm.list}" varStatus="i">
		<c:set var="imgMsgCom" value="" />
		<c:set var="imgAlt" value="" />
		<c:set var="setStar" value="" />
		<c:set var="setFirst" value="" />
		<c:choose>
			<c:when test="${row.mo_day>8}">
				<c:set var="setFontColor" value="#E9001B" />
				<c:set var="setStar" value="1" />
			</c:when>
			<c:when test="${row.mo_day>5}">
				<c:set var="setFontColor" value="#FF8A00" />
			</c:when>
			<c:when test="${row.mo_day>2}">
				<c:set var="setFontColor" value="#002AFF" />
			</c:when>
			<c:otherwise>
				<c:set var="setFontColor" value="" />
			</c:otherwise>
		</c:choose>
		<!--  새싹 멤버 세팅  -->
		<c:if test="${row.mo_day==row.mo_total}">
			<c:set var="setFirst" value="1" />
		</c:if>
		<!-- SNS 세팅 -->
		<c:set var="imgMsgCom" value="/common/btn/btn_sns_${row.msg_com}.png" />
		<c:set var="imgAlt" value="매체" />
		<tr <c:if test="${row.msg_read == 1}">class="blind_area"</c:if>>
			<td class="ac"><c:out value='${BoardForm.start_index-i.index}' /></td>
			<td>
				<div class="icon_area">
					<ul class="iconbox01">
						<li>
							<img src="../img/<c:out value='${imgMsgCom}'/>" width="20" height="20" alt="<c:out value='${imgAlt}'/>" />
						</li>
					</ul>
					<div class="iconbox02">
						<ul>
						<c:set var="msg_mmsurl" value="" />
						<c:set var="mmsurl_cnt" value="0" />
						<c:forTokens var="token" items='${row.msg_mmsurl}' delims="|" varStatus="i">
							<c:set var="mmsurl_cnt" value="${i.count }" />
						</c:forTokens>
						<c:choose>
							<c:when test="${mmsurl_cnt=='2'}">
								<c:set var="msg_mmsurl" value="${row.msg_mmsurl}| " />
							</c:when>
							<c:when test="${mmsurl_cnt=='1'}">
								<c:set var="msg_mmsurl" value="${row.msg_mmsurl}| | " />
							</c:when>
							<c:when test="${mmsurl_cnt=='0'}">
								<c:set var="msg_mmsurl" value=" | | |" />
							</c:when>
							<c:otherwise>
								<c:set var="msg_mmsurl" value="${row.msg_mmsurl}" />
							</c:otherwise>
						</c:choose>
						<%-- <c:if test="${!empty row.msg_mmsurl}"> --%>
							<c:forTokens var="token" items='${msg_mmsurl}' delims="|" varStatus="i">
								<c:if test="${i.count < 2 }" >
								<%        
									String fname = (String)pageContext.getAttribute("token");
									if(fname.length() > 4) {
										String ext = fname.substring(fname.length() - 4, fname.length());
										pageContext.setAttribute("nonspace_link", fname.replace(" ", "%20"));
										pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20"));
										int view_size = 30;
										if(fname.replace(" ", "%20").length() > view_size) {
											pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20").substring(0, view_size) + "...");
										}
										if(ext.compareToIgnoreCase(".avi") == 0) {
									%>
										<li><a href="javascript:aviPlayPop('<c:out value='${row.msg_mmsurl}'/>','<c:out value='${token}'/>')"><span class="icon_movie"></span><img src="../img/common/etc/icon_img.png"  width="20" height="20"></a></li>
									<%
										} else if(ext.compareToIgnoreCase("jpeg") == 0 || ext.compareToIgnoreCase(".jpg") == 0 || ext.compareToIgnoreCase(".gif") == 0 || ext.compareToIgnoreCase(".png") == 0) {
									%>
									<li><a href="#"></a><img src="<c:out value='${token}'/>" width='20' height="20" onmouseover="if(this.src.indexOf('btn_noimg.png')!=-1)Tip('<img src=../img/common/btn/btn_noimg.png width=200>');else Tip('<img src=<c:out value='${nonspace_link}'/> width=200>');"
									onclick="popup_Layer(event,'popup_table', '<c:out value='${row.msg_userid}'/>','<c:out value='${row.msg_com}'/>','<c:out value='${row.pgm_key }'/>', '<c:out value='${token}'/>' )" 
									onerror="javascript:this.src='../common/btn/btn_noimg.png'"/></a></li>
								<%
										} else {
									// short url
								%>
									<li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
								<%		}
										
									} else {
								%>
									<li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
								<%  }%>
								</c:if>
							</c:forTokens>
						<%-- </c:if>		 --%>				
						</ul>
					</div>
				</div>
			</td>
			<td class="brd_txt">
				<a href="javascript:void(0);" class="ellips_text" id="msg_<c:out value='${row.board_key }'/>" onmouseover="showTooltip('<c:out value='${row.board_key }'/>');"><font color="<c:out value='${setFontColor}'/>"><aof:out value='${row.msg_data}' /></font></a>
				<div class="p_txtcont">
					<!-- tooltip layer -->
					<div id="tip_<c:out value='${row.board_key }'/>" class="tootip_layer" ><div class="tootip_inner">
						<div class="arrow"></div>
						<p class="tooltip_txt">
						<c:out value='${row.msg_data }'/>
						</p>
					</div></div>
					<!-- //tooltip layer -->
				</div>
			</td>
			<td class="ac"><c:out value='${row.msg_time}' /></td>
		</tr>
	</c:forEach>
	<c:if test="${empty BoardForm.list}">
		<tr>
			<td class="ac" colspan="3">검색된 정보가 없습니다.</td>
		</tr>
	</c:if>		
	</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="40" align="center"><c:out value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
	</tr>
</table>
