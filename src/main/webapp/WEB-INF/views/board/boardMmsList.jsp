<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="multi_board">
    <ul>
    <c:forEach var="row" items="${BoardForm.list}" varStatus="i">
        <c:set var="imgMsgCom" value="" />
        <c:set var="imgAlt" value="" />
        <c:set var="setStar" value="" />
        <c:set var="setFirst" value="" />
        <c:choose>
            <c:when test="${row.mo_day>8}">
                <c:set var="setFontColor" value="#E9001B" />
                <c:set var="setStar" value="1" />
            </c:when>
            <c:when test="${row.mo_day>5}">
                <c:set var="setFontColor" value="#FF8A00" />
            </c:when>
            <c:when test="${row.mo_day>2}">
                <c:set var="setFontColor" value="#002AFF" />
            </c:when>
            <c:otherwise>
                <c:set var="setFontColor" value="" />
            </c:otherwise>
        </c:choose>
        <c:if test="${row.mo_day==row.mo_total}">
            <c:set var="setFirst" value="1" />
        </c:if>
        <c:set var="imgMsgCom" value="../img/common/btn/btn_sns_${row.msg_com}.png" />
        <c:set var="imgAlt" value="매체" />
        <li>
            <div class="multi_num">
                <strong><c:out value='${BoardForm.start_index-i.index}' /></strong>
                <span><c:out value='${row.msg_time}' /></span>
            </div>
            <div class="multi_top">
                <div class="user_infobox">
                    <span class="user_class">
                    <c:choose>
                        <c:when test="${empty row.user_icon}">
                            <img src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                        </c:when>
                        <c:when test="${row.user_icon == '100' }">
                            <img  src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                        </c:when>
                        <c:otherwise>
                            <img src='./icon/position_<c:out value="${row.user_icon}"/>.gif' width="24" height="24" border="0"  name="emoticon" >
                        </c:otherwise>
                    </c:choose>
                    </span>
                    <span class="user_info">
                        <c:choose>
                            <c:when test="${row.user_grade!='0'}">
                                <a href="javascript:popUserInfo('<c:out value='${row.msg_userid}'/>', '<c:out value='${row.msg_com}'/>',  '<c:out value='${row.msg_username}'/>',  '<c:out value='${row.comment_id}'/>')">
                                    <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
                                        <c:out value='${row.user_nick}(${row.msg_username})' />
                                    </c:if>
                                    <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                                        <c:out value='${row.user_nick}' />
                                    </c:if>
                                    <c:if test="${row.user_nick == '' || row.user_nick eq null}">
                                        <c:out value='${row.msg_username}' />
                                    </c:if>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <c:out value='${row.user_nick}'/>
                                <br>
                                <c:out value='(${row.msg_username})' />
                            </c:otherwise>
                        </c:choose>
                    </span>
                </div>
                <div class="user_etc">
                    <span class="star_area">
                        <c:choose>
                            <c:when test="${setFirst=='1'}">
                                <img src="../img/common/icon/icon_first.gif" width="20" height="20" alt="새싹">
                            </c:when>
                            <c:when test="${setStar=='1'}">
                                <img src="../img/common/btn/btn_star_off.png" width="20" height="20" alt="별" />
                            </c:when>
                            <c:otherwise>
                                <img src="../img/common/btn/btn_star_off.png" width="20" height="20" alt="별" />
                            </c:otherwise>
                        </c:choose>
                    </span>
                    <span class="sns_area">
                        <!--  2014. 12. 17 Lucas -->
                        <!--  KBS 콩 and K플레이어. 모바일과 PC 아이콘 작업 -->
						<img src="
                            <c:if test="${row.msg_com == 002 }" >
                                <c:if test="${row.comment_id == 002 }" >
                                ../img/common/btn/btn_sns_002_kong_mobile.png
                                </c:if>
                                <c:if test="${row.comment_id == 003 }" >
                                ../img/common/btn/btn_sns_002_kplayer_mobile.png
                                </c:if>
                                <c:if test="${row.comment_id != 002 && row.comment_id != 003 }" >
                                ../img/common/btn/btn_sns_002_kong_pc.png
                                </c:if>
                            </c:if>
                            <c:if test="${row.msg_com != 002}" >
                                <c:out value='${imgMsgCom}'/>
                            </c:if>
                            " width="20" height="20" alt="<c:out value='${imgAlt}'/>" />
                    </span>
                </div>
            </div>

            <ul class="multi_imglst">
                <c:set var="msg_mmsurl" value="" />
                <c:set var="mmsurl_cnt" value="0" />
                <c:forTokens var="token" items='${row.msg_mmsurl}' delims="|" varStatus="i">
                    <c:set var="mmsurl_cnt" value="${i.count }" />
                </c:forTokens>
                <c:set var="msg_mmsurl" value="${row.msg_mmsurl}" />
                <!-- 수정본 -->
                <c:forTokens var="token" items='${msg_mmsurl}' delims="|" varStatus="i">
                    <%
                        String fname = (String)pageContext.getAttribute("token");

                        String ext = fname.substring(fname.length() - 4, fname.length());
                        pageContext.setAttribute("nonspace_link", fname.replace(" ", "%20"));
                        pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20"));
                        int view_size = 30;

                        if(fname.replace(" ", "%20").length() > view_size) {
                            pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20").substring(0, view_size) + "...");
                        }
                        if(ext.compareToIgnoreCase(".avi") == 0 || ext.compareToIgnoreCase(".mp4") == 0 ) {
                    %>
                    <li>
                        <!-- 다운로드 모드 -->
                        <a href="<c:out value='${row.msg_mmsurl}'/>" download="<c:out value='${row.msg_mmsurl}'/>"><img src="../img/common/btn/btn_movie2.png"  width="20" height="20"></a>
                    </li>
                    <%
                        } else if(ext.compareToIgnoreCase(".m4a") == 0 ) {
                    %>
                    <li>
                        <!-- 다운로드 모드 -->
                        <a href="<c:out value='${row.msg_mmsurl}'/>" download="<c:out value='${row.msg_mmsurl}'/>"><img src="../img/common/btn/btn_sound.png"  width="20" height="20"></a>
                    </li>
                    <%
                        } else if(ext.compareToIgnoreCase("jpeg") == 0 || ext.compareToIgnoreCase(".jpg") == 0 || ext.compareToIgnoreCase(".gif") == 0 || ext.compareToIgnoreCase(".png") == 0) {
                    %>
                    <li>
                        <a href="javascript:changeImage('<c:out value='${row.board_key}'/>', '<c:out value='${token}'/>')" >
                            <img src="<c:out value='${token}'/>" width="20" height="20" alt="이미지없음">
                        </a>
                    </li>
                    <%
                        } else {
                    %>
                    <li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
                    <%
                        }
                    %>
                </c:forTokens>
                <!-- 수정본 -->
            </ul>

            <ul class="mutiimg_box">
                <c:forTokens var="token" items='${msg_mmsurl}' delims="|" varStatus="i">
                    <%
                        String fname = (String)pageContext.getAttribute("token");

                        String ext = fname.substring(fname.length() - 4, fname.length());
                        pageContext.setAttribute("nonspace_link", fname.replace(" ", "%20"));
                        pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20"));
                        int view_size = 30;

                        if(fname.replace(" ", "%20").length() > view_size) {
                            pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20").substring(0, view_size) + "...");
                        }
                    %>

                    <%
                        if(ext.compareToIgnoreCase(".avi") == 0 || ext.compareToIgnoreCase(".mp4") == 0 ) {
                    %>
                    <li <c:if test="${i.count == 1 }" > class="on" </c:if>>
                        <!-- 다운로드 모드 -->
                        <a href="<c:out value='${row.msg_mmsurl}'/>" download><img src="../img/common/btn/thum_movie.png"  width="200" height="200"></a>
                    </li>
                    <%
                        } else if(ext.compareToIgnoreCase(".m4a") == 0 ) {
                    %>
                    <li <c:if test="${i.count == 1 }" > class="on" </c:if>>
                    <!-- 다운로드 모드 -->
                        <a href="<c:out value='${row.msg_mmsurl}'/>" download><img src="../img/common/btn/thum_sound.png"  width="200" height="200"></a>
                    </li>
                    <%
                        } else if(ext.compareToIgnoreCase("jpeg") == 0 || ext.compareToIgnoreCase(".jpg") == 0 || ext.compareToIgnoreCase(".gif") == 0 || ext.compareToIgnoreCase(".png") == 0) {
                    %>
                    <li <c:if test="${i.count == 1 }" > class="on" </c:if>>
                        <a href="<c:out value='${token}'/>"  id="board_tag_<c:out value='${row.board_key}'/>" download>
                            <img id="board_item_<c:out value='${row.board_key}'/>" src="<c:out value='${token}'/>" width="200" height="200" alt="이미지없음">
                        </a>
                    </li>
                    <%
                        } else {
                    %>
                    <li <c:if test="${i.count == 1 }" > class="on" </c:if>>
                        <a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="200" height="200" alt="이미지없음" /></a></li>
                    <%
                        }
                    %>
                </c:forTokens>
            </ul>

            <ul class="multi_btnbox">
                <c:if test="${row.msg_read != 1}">
                    <li class="blind"><a href="javascript:setRead('1', '<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>')"><img src="../img/common/btn/btn_blind_off.png" width="20" height="20" alt="블라인드" title="읽음 처리"/></a></li>
                </c:if>
                <c:if test="${row.msg_read == 1}">
                    <li class="blind"><a href="javascript:setRead('0', '<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>')"><img src="../img/common/btn/btn_blind_on.png" width="20" height="20" alt="블라인드" title="안읽음 처리"/></a></li>
                </c:if>
                <li <c:if test="${row.msg_read == 1}">class="blind"</c:if>><a href="javascript:copyMsg('<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>' )"><img src="../img/common/btn/btn_down.png" width="20" height="20" alt="저장"  title="보관메시지함으로 복사 또는 이동"/></a></li>
                <li <c:if test="${row.msg_read == 1}">class="blind"</c:if>><a href="javascript:showMsg(<c:out value='${row.msg_show}'/>,'<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>' )"><img src="../img/common/btn/btn_del_on.png" width="20" height="20" title="차단메시지함으로 이동"/></a></li>
            </ul>

            <div class="mtlst_txtcont">
                <a href="javascript:void(0);" class="ellips_text" id="msg_<c:out value='${row.board_key }'/>" onmouseover="showTooltip('<c:out value='${row.board_key }'/>');"><font color="<c:out value='${setFontColor}'/>"><c:out value='${row.msg_data}' /></font></a>
                <div class="p_txtcont">
                    <div id="tip_<c:out value='${row.board_key }'/>" class="tootip_layer" ><div class="tootip_inner">
                        <div class="arrow"></div>
                        <p class="tooltip_txt">
                            <c:out value='${row.msg_data }'/>
                        </p>
                    </div>
                </div>
            </div>
        </li>
    </c:forEach>
    </ul>
</div>
<c:if test="${!empty BoardForm.list}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="40" align="center"><c:out value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
    </tr>
</table>
</c:if>
<script type="text/javascript">
    $("#tot_cnt1").html(<c:out value='${BoardForm.total_row_count}'/>);
    $("#tot_cnt2").html(<c:out value='${BoardForm.tot_cnt2}'/>);
</script>







