<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<head>
    <link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <div class="tit_box">
            <h2 class="mo01">멀티미디어 게시판</h2>
        </div>
    </div>
    <div class="cont_topr">
        <p class="total_num">수신건수<br />
            <strong class="sdclr" id="tot_cnt1" >0</strong></p>
        <p class="total_num">참여자수<br />
            <strong class="ptclr" id="tot_cnt2" >0</strong></p>
    </div>
</div>
<form name="BoardForm" action="/BoardAction.do" style="margin-bottom: 0">
    <input type="hidden" name="page_num" value="1">
    <input type="hidden" name="list_type" value="mmslist">
    <input type="hidden" name="row_count" value="15">
    <input type="hidden" id="show_hide_msg" name="show_hide_msg" value="1">
    <input type="hidden" name="start_hh" value="00">
    <input type="hidden" name="start_mi" value="00">
    <input type="hidden" name="end_hh" value="24">
    <input type="hidden" name="end_mi" value="00">

    <div id="top_searchbox" class="top_searchbox">
        <span class="arrow"></span>
        <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
				<button type="button" class="month_btn">달력</button>
			</span>
        </div>
        <span class="swung">~</span>
        <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
				<button type="button" class="month_btn">달력</button>
			</span>
        </div>

        <select id="search_field" name="search_field" class="tsearch select_b select_box" style="height: 26px">
            <option value="BOTH2">보낸사람+내용</option>
            <option value="a.msg_username">보낸사람</option>
            <option value="a.msg_data">내용</option>
        </select>
        <div class="tsearch">
			<span class="select_box select_t">
				<span class="t_search"><input type="text" name="search_kw" id="" onkeydown="onEnter(search);"/></span>
				<button type="button" style="width:62px;" class="btn" onClick="search();"><img src="../img/common/btn/btn_search.gif" width="62" height="24" alt="검색" /></button>
			</span>
        </div>
    </div>
    <div class="top_searchbln"></div>
    <div id="table_topbox" class="table_topbox">
        <div class="tabletb_l">
            <select id="save_box_type" name="save_box_type" class="gray_select select_b">
                <option value="1">보관메시지함1</option>
                <option value="2">보관메시지함2</option>
                <option value="3">보관메시지함3</option>
                <option value="4">보관메시지함4</option>
                <option value="5">보관메시지함5</option>
            </select>
            (으)로
            <select id="save_type" name="save_type" class="gray_select select_b" onchange="changeSaveType(this.value)">
                <option value="copy">복사</option>
                <option value="move">이동</option>
            </select>
        </div>
    </div>

    <div id="list">
        <table cellspacing="0" border="1" summary="사진게시판을 담고 있습니다.">
            <caption>사진 게시판 리스트</caption>
            <colgroup>
                <col style="width:45px;" />
                <col style="width:130px;" />
                <col style="width:155px;" />
                <col style="width:auto;" />
                <col style="width:95px;" />
                <col style="width:135px;" />
            </colgroup>
            <thead>
            <tr>
                <th scope="col">N</th>
                <th scope="col">보낸사람</th>
                <th scope="col" colspan="3">내용</th>
                <th scope="col">보낸시간</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td align="center" colspan="5" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
            </tr>
            </tbody>
        </table>
    </div>

</form>
<script type="text/javascript" src="../js/common.js"></script>
<script language="JavaScript">
let f;
$(document).ready(function() {
    f = document.BoardForm;

    // date picker
    setDatePicker("#start_dt");
    setDatePicker("#end_dt");
    // 초기 날짜 세팅
    $( "#start_dt" ).datepicker( "setDate", "-30" );
    $( "#end_dt" ).datepicker( "setDate", "0" );

    list();
});

function list() {
    const url = "<c:url value='/BoardAction.do/list' />";
    let data = $(f).serialize();
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').empty().html(response);
}

function search(){
    if(isValid()){
        f.page_num.value = "1";
        list();
    }
}

function goPage(pageNum){
    f.page_num.value = pageNum;
    list();
}


function isValid(){
    const start_yy	= f.start_dt.value.substring(0,4);
    const start_mm	= f.start_dt.value.substring(5,7);
    const start_dd	= f.start_dt.value.substring(8,10);

    const end_yy		= f.end_dt.value.substring(0,4);
    const end_mm		= f.end_dt.value.substring(5,7);
    const end_dd		= f.end_dt.value.substring(8,10);

    const sDate	= new Date(start_yy, start_mm, start_dd, f.start_hh.value, f.start_mi.value);
    const eDate	= new Date(end_yy, end_mm, end_dd, f.end_hh.value, f.end_mi.value);

    if( f.end_dt.value < f.start_dt.value ) {
        alert("종료일자는 시작일자 이후로 설정하세요.");
        f.end_dt.focus();
        return;
    }

    const elapsed = (eDate.valueOf()-sDate.valueOf())/(1000*60*60*24);  //기간 (일수)

    if(elapsed > 93){
        alert("기간은 최대 3개월(93일) 이내로 설정해야 합니다.");
        return false;
    }

    return true;
}


function setRead(msg_read, board_key, table_name) {
    const url = "BoardAction.do/setMsgRead";
    let data = {
        msg_read : msg_read,
        board_key : board_key,
        table_name : table_name,
    };

    $.ajax({
        url: url,
        type : "post",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function copyMsg(board_key, table_name)  {
    const url = "BoardAction.do/copyMsg";
    const save_type = f.save_type.value;
    const save_box_type = f.save_box_type.value;
    let data = {
        board_key : board_key,
        table_name : table_name,
        save_type : save_type,
        save_box_type : save_box_type,
    };

    $.ajax({
        url: url,
        type : "post",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function showMsg(msg_show, board_key, table_name)  {
    const url = "BoardAction.do/showMsg";
    msg_show = msg_show == "1" ? "0" : "1";
    let data = {
        board_key : board_key,
        table_name : table_name,
        msg_show : msg_show,
    };

    $.ajax({
        url: url,
        type : "post",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

</script>
</body>
</html>