<%@ page import="net.infobank.itv.common.Constants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<head>
<link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
<style>
    #btnSendMsgDiv.gray_select button {
        height: 20px;
        line-height: 20px;
        padding: 0 15px 0 10px;
        background: url('../../img/common/gray_select_box.png') no-repeat 100% 0;
        color: red;
        font-size: 12px;
    }

    #btnMoveMsgDiv.gray_select button {
        height: 20px;
        line-height: 20px;
        padding: 0 15px 0 10px;
        background: url('../../img/common/gray_select_box.png') no-repeat 100% 0;
        color: #383845;
        font-size: 12px;
    }
</style>
</head>
<body>
<form name="BoardForm" method="post"  action="/BoardAction.do" style="margin-bottom: 0">
    <input type="hidden" name="page_num" value="1">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
    <input type="hidden" name="list_type" value="saveBox"/>
    <input type="hidden" name="send_type"/>
    <input type="hidden" name="st_type" value="<c:out value="${param.st_type }"/>"/>
    <input type="hidden" name="copy_st_type" value="<c:if test="${param.st_type=='1'}">2</c:if><c:if test="${param.st_type!='1'}">1</c:if>"/>
    <input type="hidden" name="st_type_set" value="copy"/>
    <div class="cont_top">
        <div class="cont_topl">
            <!-- path -->
            <ul class="path_area">
                <li class="home"><c:out value="${menu_1_name}"/></li>
                <li class="on"><c:out value="${menu_2_name}"/></li>
            </ul>
            <!-- //path -->
            <!-- tit -->
            <div class="tit_box">
                <h2 class="mo02"><c:out value="${menu_title}"/></h2>
            </div>
            <!-- //tit -->
        </div>
    </div>

    <div class="table_topbox">
        <div class="tabletb_l">
            <!-- gray select -->
    <%--        <div class="gray_select select_b">--%>
    <%--            <button type="button" class="select_t" id="save_box_type"><c:if test="${param.st_type=='1'}">보관메시지함2</c:if><c:if test="${param.st_type!='1'}">보관메시지함1</c:if></button>--%>
    <%--            <div class="select_m" style="width:110px;">--%>
    <%--                <ul class="layer_inner">--%>
    <%--                    <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.copy_st_type, '0', 'save_box_type', this)">전체게시판</a></li>--%>
    <%--                    <c:if test="${param.st_type!='1'}"><li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.copy_st_type, '1', 'save_box_type', this)">보관메시지함1</a></li></c:if>--%>
    <%--                    <c:if test="${param.st_type!='2'}"><li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.copy_st_type, '2', 'save_box_type', this)">보관메시지함2</a></li></c:if>--%>
    <%--                    <c:if test="${param.st_type!='3'}"><li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.copy_st_type, '3', 'save_box_type', this)">보관메시지함3</a></li></c:if>--%>
    <%--                    <c:if test="${param.st_type!='4'}"><li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.copy_st_type, '4', 'save_box_type', this)">보관메시지함4</a></li></c:if>--%>
    <%--                    <c:if test="${param.st_type!='5'}"><li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.copy_st_type, '5', 'save_box_type', this)">보관메시지함5</a></li></c:if>--%>
    <%--                </ul>--%>
    <%--            </div>--%>
    <%--        </div>--%>
            <select id="save_box_type" name="save_box_type" class="gray_select select_b">
                <%-- TODO 코드에서 가져오기로 --%>
                <c:if test="${param.st_type != 1}"><option value="1">보관메시지함1</option></c:if>
                <c:if test="${param.st_type != 2}"><option value="2">보관메시지함2</option></c:if>
                <c:if test="${param.st_type != 3}"><option value="3">보관메시지함3</option></c:if>
                <c:if test="${param.st_type != 4}"><option value="4">보관메시지함4</option></c:if>
                <c:if test="${param.st_type != 5}"><option value="5">보관메시지함5</option></c:if>
            </select>
            <!-- //gray select -->
            (으)로
            <!-- gray select -->
    <%--        <div class="gray_select select_b">--%>
    <%--            <button type="button" class="select_t" id="save_type">복사</button>--%>
    <%--            <div class="select_m" style="width:60px;">--%>
    <%--                <ul class="layer_inner">--%>
    <%--                    <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.st_type_set, 'copy', 'save_type', this)">복사</a></li>--%>
    <%--                    <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.st_type_set, 'move', 'save_type', this)">이동</a></li>--%>
    <%--                </ul>--%>
    <%--            </div>--%>
    <%--        </div>--%>
            <select id="save_type" name="save_type" class="gray_select select_b" onchange="changeSaveType(this.value)">
                <option value="copy">복사</option>
                <option value="move">이동</option>
            </select>
            <span style="color:black;">또는</span>
            <div class="gray_select"  id="btnSendMsgDiv" name="btnSendMsgDiv">
                <button type="button" class="select_t" id="btnSaveMsg" name="btnSaveMsg" onClick="eachSaveMsgAction()" title="선택 항목 이동 또는 복사하기">선택 복사</button>
            </div>
            <span>ㅣ</span>

            <span class="btn_btype01"  id="btnExcel">
                <a href="javascript:excelDown();">엑셀 다운로드</a>
            </span>

        </div>
        <div style="float:right;">
            <span class="btn_btype01"  id="btnSendMsg">
                <a href="javascript:sendBoxMsg();">전체답장</a>
            </span>
            <span style="color: #e2e2e2;">ㅣ</span>
            <!-- 선택 기능 추가 2018.9.6 Lucas Start -->
            <span class="btn_btype01"  id="btnSendMsg">
                <a href="javascript:sendBoxMsgSelect();">선택답장</a>
            </span>
            <!-- 선택 기능 추가 2018.9.6 Lucas End -->
            <span style="color: #e2e2e2;">ㅣ</span>
            <span class="btn_btype02"  id="btnSaveBoxDeleteAllMsg">
                <a href="javascript:deleteSaveBoxAllMsg();">전체삭제</a>
            </span>
        </div>
    </div>
    <div id="list">
        <div class="table_type01">
            <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
                <caption>리스트</caption>
                <colgroup>
                    <col style="width:45px;" />
                    <col style="width:130px;" />
                    <col style="width:155px;" />
                    <col style="width:auto;" />
                    <col style="width:95px;" />
                    <col style="width:135px;" />
                </colgroup>
                <thead>
                <tr>
                    <th scope="col">N</th>
                    <th scope="col">보낸사람</th>
                    <th scope="col" colspan="3">내용</th>
                    <th scope="col">보낸시간</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td align="center" colspan="5" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>
</body>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/wz_tooltip.js"></script>
<script type="text/javascript" src="../js/itv_user.js"></script>
<script language="JavaScript">
    let f;
    $(document).ready(function() {
        f = document.BoardForm;

        // date picker
        setDatePicker("#start_dt");
        setDatePicker("#end_dt");
        // 초기 날짜 세팅
        $( "#start_dt" ).datepicker( "setDate", "0" );
        $( "#end_dt" ).datepicker( "setDate", "0" );

        list();
    });

    function list() {
        const url = "<c:url value='/BoardAction.do/list' />";
        let data = $(f).serialize();

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            data : data,
            error : ajaxError,
            success : endList,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function endList(response, status, request){
        $('#list').html(response);
    }

    function goPage(pageNum){
        f.page_num.value = pageNum;
        list();
    }

    function excelDown() {
        var cnt = 0;
        if($("tot_cnt1") != null) cnt = $("tot_cnt1").innerHTML;
        if(cnt>10000) {
            alert("건수가 많아 다운로드가 불가 합니다. 관리자에게 문의하시기 바랍니다.");
            return;
        }

        f.action = "<c:url value='/BoardAction.do/list?excel=Y' />";
        f.target="_self";
        f.submit();
    }

    function setRead(msg_read, board_key, table_name) {
        const url = "BoardAction.do/setMsgRead";
        let data = {
            msg_read : msg_read,
            board_key : board_key,
            table_name : table_name,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function copyMsg(board_key, table_name)  {
        const url = "BoardAction.do/copyMsg";
        const save_type = f.save_type.value;
        const save_box_type = f.save_box_type.value;
        let data = {
            board_key : board_key,
            table_name : table_name,
            save_type : save_type,
            save_box_type : save_box_type,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function showMsg(msg_show, board_key, table_name)  {
        const url = "BoardAction.do/showMsg";
        msg_show = msg_show == "1" ? "0" : "1";
        let data = {
            board_key : board_key,
            table_name : table_name,
            msg_show : msg_show,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function changeSaveType(value) {
        if(value == 'move') {
            $("#btnSaveMsg").text("선택 이동");
        } else {
            $("#btnSaveMsg").text("선택 복사");
        }
    }

    function eachSaveMsgAction(){
        if(getChks(f.chk_board_key)==""){
            alert("이동 또는 복사 하고자 하는 메세지를 선택 하세요");
            return;
        }else {
            if(!confirm("선택된 메세지를 '"+$("#save_box_type  option:selected").text()+"'(으)로 " + $("#btnSaveMsg").text()+ " 하시겠습니까?")){
                return;
            }

            const url = "BoardAction.do/copySelectMsg";
            const chk_board_key = getChks(f.chk_board_key).split(",");
            const save_type = f.save_type.value;
            const save_box_type = f.save_box_type.value;
            let data = {
                chk_board_key : chk_board_key,
                save_type : save_type,
                save_box_type : save_box_type,
            };

            $.ajax({
                url: url,
                type : "post",
                data : data,
                error : ajaxError,
                success : actionResult,
                beforeSend : ajaxOnCreate,
                complete : ajaxOnComplete
            });
        }
    }

    let _userMsg = null;
    function sendBoxMsg(selected = "N"){
        _userMsg = window.open('about:blank','sendMsgPop','status=yes, scrollbars=yes, resizable=yes, width=620, height=330');
        _userMsg.focus();
        f.target = "sendMsgPop";
        f.action = "<c:url value='/BoardAction.do/sendMsgBox' />?st_type=<c:out value='${param.st_type}'/>&selected=" + selected;
        f.submit();
    }

    function sendBoxMsgSelect(){
        if(getChks(f.chk_board_key)==""){
            alert("선택 답장 하고자 하는 메세지를 선택 하세요");
            return;
        }else {
            sendBoxMsg("Y");
        }
    }

    function deleteSaveBoxAllMsg(){
        if(!confirm("정말로 전체 메세지를 삭제  하시겠습니까?")){
            return;
        }

        const url = "BoardAction.do/deleteSaveBoxAllMsg";
        const pgm_key = f.pgm_key.value;
        const st_type = f.st_type.value;
        let data = {
            pgm_key : pgm_key,
            st_type : st_type,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

</script>
</html>