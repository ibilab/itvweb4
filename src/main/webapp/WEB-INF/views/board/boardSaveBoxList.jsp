<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01">
    <input type="hidden" name="chkBoxValid"  value="true">
    <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
        <caption>문자메시지 리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:45px;" />
            <col style="width:150px;" />
            <col style="width:155px;" />
            <col style="width:auto;" />
            <col style="width:95px;" />
            <col style="width:135px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col"><div id="chkAllBox"><a href="#" onclick = "javascript:checkAllBtn('chkAllBox','chk_board_key', true)">선택</a></div></th>
            <th scope="col">N</th>
            <th scope="col">보낸사람</th>
            <th scope="col" colspan="3">내용</th>
            <th scope="col">보낸시간</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${BoardForm.list}" varStatus="i">
            <c:set var="imgMsgCom" value="" />
            <c:set var="imgAlt" value="" />
            <c:set var="setStar" value="" />
            <c:set var="setFirst" value="" />
            <c:choose>
                <c:when test="${row.mo_day>8}">
                    <c:set var="setFontColor" value="#E9001B" />
                    <c:set var="setStar" value="1" />
                </c:when>
                <c:when test="${row.mo_day>5}">
                    <c:set var="setFontColor" value="#FF8A00" />
                </c:when>
                <c:when test="${row.mo_day>2}">
                    <c:set var="setFontColor" value="#002AFF" />
                </c:when>
                <c:otherwise>
                    <c:set var="setFontColor" value="" />
                </c:otherwise>
            </c:choose>
            <!--  새싹 멤버 세팅  -->
            <c:if test="${row.mo_day==row.mo_total}">
                <c:set var="setFirst" value="1" />
            </c:if>
            <!-- SNS 세팅 -->
            <c:set var="imgMsgCom" value="/common/btn/btn_sns_${row.msg_com}.png" />
            <c:set var="imgAlt" value="매체" />
            <tr <c:if test="${row.msg_read == 1}">class="blind_area"</c:if>>
                <td class="ac">
                    <input	type="checkbox" name="chk_board_key" value="<c:out value='${row.board_key};${row.msg_userid};${row.table_name};${row.msg_com}'/>" />
                </td>
                <td class="ac"><c:out value='${BoardForm.start_index-i.index}' /></td>
                <td class="name">
				<span>
				<!-- 유저 아이콘 -->
				<c:choose>
                    <c:when test="${empty row.user_icon}">
                        <img src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                    </c:when>
                    <c:when test="${row.user_icon == '100' }">
                        <img  src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                    </c:when>
                    <c:otherwise>
                        <img src='./icon/position_<c:out value="${row.user_icon}"/>.gif' width="24" height="24" border="0"  name="emoticon" >
                    </c:otherwise>
                </c:choose>
				</span>
                    <!-- 유저 명 -->
                    <c:choose>
                        <c:when test="${row.user_grade!='0'}">
                            <a href="javascript:popUserInfo('<c:out value='${row.msg_userid}'/>', '<c:out value='${row.msg_com}'/>',  '<c:out value='${row.msg_username}'/>',  '<c:out value='${row.comment_id}'/>')">
                                <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
                                    <%-- <c:out value='${row.user_nick}(${row.msg_username})' /> --%>
                                    <c:if test="${row.msg_com != 001}">
                                        <c:out value='${row.user_nick}(${row.msg_username})' />
                                    </c:if>
                                    <c:if test="${row.msg_com == 001}">
                                        <c:out value='${row.user_nick}(${row.user_phone_dash})' />
                                    </c:if>
                                </c:if>
                                <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                                    <c:out value='${row.user_nick}' />
                                </c:if>
                                <c:if test="${row.user_nick == '' || row.user_nick eq null}">
                                    <c:if test="${row.msg_com != 001}">
                                        <c:out value='${row.msg_username}' />
                                    </c:if>
                                    <c:if test="${row.msg_com == 001}">
                                        <c:out value='${row.user_phone_dash}' />
                                    </c:if>
                                </c:if>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <c:out value='${row.user_nick}(${row.msg_username})' />
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <li>
                                <c:choose>
                                    <c:when test="${setFirst=='1'}">
                                        <img src="../img/common/icon/icon_first.gif" width="20" height="20" alt="새싹">
                                    </c:when>
                                    <c:when test="${setStar=='1'}">
                                        <img src="../img/common/icon/icon_star.gif" width="20" height="20" alt="별">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="../img/common/btn/btn_star_off.png" width="20" height="20" alt="별" />
                                    </c:otherwise>
                                </c:choose>
                            </li>
                            <li>
                                <!--  2014. 12. 17 Lucas -->
                                <!--  KBS 콩 and K플레이어. 모바일과 PC 아이콘 작업 -->
                                <img src="../img
								<c:if test="${row.msg_com == 002 }" >
									<c:if test="${row.comment_id == 002 }" >
									/common/btn/btn_sns_002_kong_mobile.png
									</c:if>
									<c:if test="${row.comment_id == 003 }" >
									/common/btn/btn_sns_002_kplayer_mobile.png
									</c:if>
									<c:if test="${row.comment_id != 002 && row.comment_id != 003 }" >
									/common/btn/btn_sns_002_kong_pc.png
									</c:if>
								</c:if>
								<c:if test="${row.msg_com != 002}" >
									<c:out value='${imgMsgCom}'/>
								</c:if>
								" width="20" height="20" alt="<c:out value='${imgAlt}'/>" />
                            </li>
                        </ul>
                        <div class="iconbox02">
                            <ul>
                                <c:set var="msg_mmsurl" value="" />
                                <c:set var="mmsurl_cnt" value="0" />
                                <c:forTokens var="token" items='${row.msg_mmsurl}' delims="|" varStatus="i">
                                    <c:set var="mmsurl_cnt" value="${i.count }" />
                                </c:forTokens>
                                <c:choose>
                                    <c:when test="${mmsurl_cnt=='2'}">
                                        <c:set var="msg_mmsurl" value="${row.msg_mmsurl}| " />
                                    </c:when>
                                    <c:when test="${mmsurl_cnt=='1'}">
                                        <c:set var="msg_mmsurl" value="${row.msg_mmsurl}| | " />
                                    </c:when>
                                    <c:when test="${mmsurl_cnt=='0'}">
                                        <c:set var="msg_mmsurl" value=" | | |" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="msg_mmsurl" value="${row.msg_mmsurl}" />
                                    </c:otherwise>
                                </c:choose>
                                    <%-- <c:if test="${!empty row.msg_mmsurl}"> --%>
                                <c:forTokens var="token" items='${msg_mmsurl}' delims="|" varStatus="i">
                                    <c:if test="${i.count < 4 }" >
                                        <%
                                            String fname = (String)pageContext.getAttribute("token");
                                            if(fname.length() > 4) {
                                                String ext = fname.substring(fname.length() - 4, fname.length());
                                                pageContext.setAttribute("nonspace_link", fname.replace(" ", "%20"));
                                                pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20"));
                                                int view_size = 30;
                                                if(fname.replace(" ", "%20").length() > view_size) {
                                                    pageContext.setAttribute("nonspace_view", fname.replace(" ", "%20").substring(0, view_size) + "...");
                                                }
                                                if(ext.compareToIgnoreCase(".avi") == 0) {
                                        %>
                                        <li><a href="javascript:aviPlayPop('<c:out value='${row.msg_mmsurl}'/>','<c:out value='${token}'/>')"><span class="icon_movie"></span><img src="../img/common/etc/icon_img.png"  width="20" height="20"></a></li>
                                        <%
                                        } else if(ext.compareToIgnoreCase("jpeg") == 0 || ext.compareToIgnoreCase(".jpg") == 0 || ext.compareToIgnoreCase(".gif") == 0 || ext.compareToIgnoreCase(".png") == 0) {
                                        %>
                                        <li><a href="#"></a><img src="<c:out value='${token}'/>" width='20' height="20" onmouseover="if(this.src.indexOf('btn_noimg.png')!=-1)Tip('<img src=../img/common/btn/btn_noimg.png width=200>');else Tip('<img src=<c:out value='${nonspace_link}'/> width=200>');"
                                                                 onclick="popup_Layer(event,'popup_table', '<c:out value='${row.msg_userid}'/>','<c:out value='${row.msg_com}'/>','<c:out value='${row.pgm_key }'/>', '<c:out value='${token}'/>' )"
                                                                 onerror="javascript:this.src='../img/common/btn/btn_noimg.png'"/></a></li>
                                        <%
                                        } else {
                                            // short url
                                        %>
                                        <li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
                                        <%		}

                                        } else {
                                        %>
                                        <li><a href="javascript:void(0);"><img src="../img/common/btn/btn_noimg.png" width="20" height="20" alt="이미지없음" /></a></li>
                                        <%  }%>
                                    </c:if>
                                </c:forTokens>
                                    <%-- </c:if>		 --%>
                            </ul>
                        </div>
                    </div>
                </td>
                <td class="brd_txt">

                    <!-- 선물 아이콘 작업 2015. 2. 2 Lucas -->
                    <c:if test="${row.user_image != ' ' }">
                        <c:forTokens var="token" items='${row.user_image}' delims=";">
                            <c:if test="${token != '200' }">
                                <img src="/icon/gift_<c:out value='${token}'/>.gif"  border="0"  >
                            </c:if>
                        </c:forTokens>
                    </c:if>

                    <!-- CBS 이모티콘 작업 2018. 5. 24 Lucas -->
                    <c:set var="emoClass" value="${row.emo_class}" />
                    <c:set var="emoCode" value="${row.emo_code}" />
                    <c:if test="${row.emo_type==1 || row.emo_type==2}">
                        <%
//                            EmoticonAction ea = new EmoticonAction();
//                            String emoClass = (String)pageContext.getAttribute("emoClass");
//                            String emoCode = (String)pageContext.getAttribute("emoCode");
//                            String emoticonPath = ea.getPath(emoClass, emoCode);
//                            if(emoticonPath == null || emoticonPath.isEmpty()) {
//                                emoticonPath = "./icon/position_000.gif";
//                            }
//                            request.setAttribute("emoticonPath", emoticonPath);
                        %>
                        <img src="<c:out value="${emoticonPath}"/>" style="width:70px; height:70px;" onerror="./icon/position_000.gif">
                    </c:if>

                    <div class="p_txtcont" title="클릭하시면 글내용 복사가 가능합니다.">

                        <!-- CBS 이모티콘만 있는 케이스는 MSG_DATA 노출 제거. 2018.5.29. Lucas : START -->
                        <c:if test="${row.emo_type != 1}">
                        <!-- 클립 보드에 복사 기능. Lucas. 2015.3.9 : START -->
                        <!-- <a href="#" onclick="javascript:window.clipboardData.setData('Text', '클립보드로 복사할 글'); ">Copy</a>  -->
                        <!--
						<a href="#" onclick="javascript:boardClickCopyPaste('[<c:out value='${row.msg_username}' />] <c:out value='${row.msg_data }'/>');">
							<c:out value='${row.msg_data }'/>
						</a>
						-->
                        <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
                            <a href="#" onclick="javascript:boardClickCopyPaste('[<c:out value='${row.user_nick}(${row.msg_username})' />] <c:out value='${row.msg_data }'/>');">
                                <c:out value='${row.msg_data }'/>
                            </a>
                        </c:if>

                        <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                        <a href="#" onclick="javascript:boardClickCopyPaste('[<c:out value='${row.user_nick}' />] <c:out value='${row.msg_data }'/>');">
                                <c:out value='${row.msg_data }'/>
                            </c:if>

                            <c:if test="${row.user_nick == '' || row.user_nick eq null}">
                            <a href="#" onclick="javascript:boardClickCopyPaste('[<c:out value='${row.msg_username}' />] <c:out value='${row.msg_data }'/>');">
                                    <c:out value='${row.msg_data }'/>
                                </c:if>
                                <!-- 클립 보드에 복사 기능. Lucas. 2015.3.9 : END -->
                                </c:if>
                                <!-- CBS 이모티콘만 있는 케이스는 MSG_DATA 노출 제거. 2018.5.29. Lucas : END -->
                    </div>
                </td>
                <td class="noline" style="float: right">
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <c:if test="${row.msg_read != 1}">
                                <li><a href="javascript:setRead('1', '<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>')"><img src="../img/common/btn/btn_blind_off.png" width="20" height="20"  title="읽음 처리"/></a></li>
                            </c:if>
                            <c:if test="${row.msg_read == 1}">
                                <li><a href="javascript:setRead('0', '<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>')"><img src="../img/common/btn/btn_blind_on.png" width="20" height="20" title="안읽음 처리"/></a></li>
                            </c:if>
                            <li><a href="javascript:copyMsg('<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>' , '<c:out value='${row.msg_date}'/>', '<c:out value='${row.msg_key}'/>')"><img src="../img/common/btn/btn_down.png" width="20" height="20" title="다른 보관메시지함 또는 전체게시판으로 복사 또는 이동"/></a></li>
                            <li><a href="javascript:showMsg(<c:out value='${row.msg_show}'/>,'<c:out value='${row.board_key}'/>', '<c:out value='${row.table_name}'/>' )"><img src="../img/common/btn/btn_del_on.png" width="20" height="20" title="차단메시지함으로 이동"/></a></li>
                        </ul>
                    </div>
                </td>
                <td class="ac"><c:out value='${row.msg_time}' /></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="40" align="center"><c:out value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
    </tr>
</table>
<!-- <div id='popup_table' style="position:absolute; left:0px; top:0px; z-index:1;display:none;" onmouseover="mouse_anchor('1');" onmouseout="mouse_anchor('0');">
<table border="1" width="130" height="14" bgcolor='#eeeeee'>
    <tr valign=absmiddle align=center><td height="14" valign="top"><a href="javascript:setUserImage();" >개인 이미지로 등록</a><br></td></tr>
</table>
</div> -->
<c:if test="${empty param.st_type}">
    <c:if test="${param.list_type!='userBox'}">
        <script type="text/javascript">
            $("#tot_cnt1").html(<c:out value='${BoardForm.total_row_count}'/>);
            $("#tot_cnt2").html(<c:out value='${BoardForm.tot_cnt2}'/>);
            <%-- 	<c:if test="${BoardForm.total_row_count > 0}">Element.show("button3");</c:if>
                <c:if test="${BoardForm.total_row_count == 0}">Element.hide("button3");</c:if> --%>
        </script>
    </c:if>
</c:if>
<script type="text/javascript">
    $(document).ready(function() {
    });

    // TODO 합치기... 공통
    function boardClickCopyPaste(data){
        if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
            window.clipboardData.setData('Text',data);
            alert('해당 글을 복사하였습니다.');
        } else { //IE 가 아닐때
            prompt("Ctrl+C를 눌러 클립보드로 복사하세요", data );
        }
    }

    function checkAllBtn(id, checks, isCheck) {
        let fobj = $("input[name='" + checks + "']");
        if (fobj == null) return;

        let valid = $("input[name='chkBoxValid']");
        if (valid.val() == "true") {
            valid.val("false");
            isCheck = true;
        } else {
            valid.val("true");
            isCheck = false;
        }

        if (fobj.length) {
            for (let i = 0; i < fobj.length; i++) {
                if (fobj[i].disabled != true) {
                    fobj[i].checked = isCheck;
                }
            }
        } else {
            fobj.checked = isCheck;
        }
    }
</script>
</body>
</html>
