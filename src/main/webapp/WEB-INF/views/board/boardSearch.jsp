<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<head>
<link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
<style>
    #btnSendMsgDiv.gray_select button {
        height: 20px;
        line-height: 20px;
        padding: 0 15px 0 10px;
        background: url('../../img/common/gray_select_box.png') no-repeat 100% 0;
        color: red;
        font-size: 12px;
    }

    #btnMoveMsgDiv.gray_select button {
        height: 20px;
        line-height: 20px;
        padding: 0 15px 0 10px;
        background: url('../../img/common/gray_select_box.png') no-repeat 100% 0;
        color: #383845;
        font-size: 12px;
    }
</style>
</head>
<body>
<form name="BoardForm" method="post" action="/BoardAction.do">
    <input type="hidden" name="page_num" value="1">
    <input type="hidden" id="show_hide_msg" name="show_hide_msg" value="1">
    <input type="hidden" id="read_msg_table" name="read_msg_table" value="0">
    <input type="hidden" name="start_hh" value="00">
    <input type="hidden" name="start_mi" value="00">
    <input type="hidden" name="end_hh" value="24">
    <input type="hidden" name="end_mi" value="00">
    <input type="hidden" name="search_new_user" value="0">
    <input type="hidden" name="show_last_4_number" value="0">

    <div class="cont_top">
        <div class="cont_topl">
            <ul class="path_area">
                <li class="home"><c:out value="${menu_1_name}"/></li>
                <%-- 					<li class="on"><c:out value="${menu_2_name}"/></li> --%>

                <span id="mtGuideBtn" class="btn_btype01" style="display: inline-block; line-height: 0px; top: -5px;"><a href="javascript:mtGuide();">문자 참여 방법 ></a></span>
                <span id="kakaoGuideBtn" class="btn_btype02" style="display: inline-block; line-height: 0px; top: -5px;"><a href="javascript:kakaoGuide();" style="color: yellow;">카톡 참여 방법 ></a></span>
            </ul>
            <div class="tab_type01">
                <c:set var="useAPPYN" value="N" />
                <c:set var="useKakaoYN" value="N" />
                <ul>
                    <li id="000" class="on"><a href="javascript:tabs('000');">전체 게시판<span class="arrow"></span></a></li>

                    <c:forTokens var="token" items='${operator.oper_sns}' delims="|" varStatus="i">

                        <li id="<c:out value='${token}'/>" ><a href="javascript:tabs('<c:out value='${token}'/>');">
                            <c:forTokens var="name" items='${sns_lists}' delims="|" varStatus="k">

                                <c:if test="${i.count==k.count}">
                                    <c:out value="${name}"/>

                                    <c:if test="${name=='Kakao'}">
                                        <c:set var="useKakaoYN" value="Y" />
                                    </c:if>

                                    <c:if test="${name=='콩' || name=='고릴라' || name=='반디' || name=='레인보우'}">
                                        <c:set var="useAPPYN" value="Y" />
                                    </c:if>

                                </c:if>
                            </c:forTokens></a>
                        </li>

                    </c:forTokens>

                  <%--  <c:if test="${useKakaoYN=='N'}">
                        <li id="666"><a href="javascript:tabs('666');">방송사 APP</a></li>
                    </c:if>

                    <c:if test="${useKakaoYN=='N'}">
                        <li id="777"><a href="javascript:tabs('777');">카카오 체험</a></li>
                    </c:if>--%>

                    <li id="999"><a href="javascript:tabs('999');">차단 메시지</a></li>
                    <li id="888"><a href="javascript:tabs('888');">읽음 메시지</a></li>

                </ul>
            </div>
        </div>
        <div class="cont_topr">
            <p class="total_num">수신건수<br /><strong id="tot_cnt1" class="sdclr">0</strong></p>
            <p class="total_num">참여자수<br /><strong id="tot_cnt2" class="ptclr">0</strong></p>
        </div>
    </div>

    <!-- 검색 부분  -->
    <div id="top_searchbox" class="top_searchbox">
        <span class="arrow"></span>
        <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
				<button type="button" class="month_btn">달력</button>
			</span>
        </div>
        <span class="swung">~</span>
        <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
				<button type="button" class="month_btn">달력</button>
			</span>
        </div>

        <select id="search_field" name="search_field" class="tsearch select_b select_box" style="height: 26px">
            <option value="BOTH2">보낸사람+내용</option>
            <option value="a.msg_username">보낸사람</option>
            <option value="a.msg_data">내용</option>
        </select>
        <div class="tsearch">
			<span class="select_box select_t">
				<span class="t_search"><input type="text" name="search_kw" id="" onkeydown="onEnter(search);"/></span>
				<button type="button" style="width:62px;" class="btn" onClick="search();"><img src="../img/common/btn/btn_search.gif" width="62" height="24" alt="검색" /></button>
			</span>
        </div>

        <div class="tsearch select_b" id = "excelDownBtn"><span class="btn_btype01"  id="btnExcel"><a href="javascript:excelDown();">엑셀 다운로드</a></span>
        </div>
    </div>

    <div class="top_searchbln"></div>
    <div id="table_topbox" class="table_topbox">
        <div class="tabletb_l">
            <select id="save_box_type" name="save_box_type" class="gray_select select_b">
                <option value="1">보관메시지함1</option>
                <option value="2">보관메시지함2</option>
                <option value="3">보관메시지함3</option>
                <option value="4">보관메시지함4</option>
                <option value="5">보관메시지함5</option>
            </select>
            (으)로
            <select id="save_type" name="save_type" class="gray_select select_b" onchange="changeSaveType(this.value)">
                <option value="copy">복사</option>
                <option value="move">이동</option>
            </select>
            <div class="gray_select"  id="btnMoveMsgDiv" name="btnMoveMsgDiv">
                <button type="button" class="select_t" id="btnMoveMsg" name="btnMoveMsg" onClick="moveMsg()" title="선택 항목 이동/복사하기">선택 복사</button>
            </div>
        </div>
        <div class="tabletb_r">
            <input type="checkbox" class="checkbox" name="showSproutUser" id="showSproutUser" value="1" onClick="checkSproutUser(this);"> <label for="showSproutUser">새싹 모아보기</label> <span>ㅣ</span>
            <input type="checkbox" class="checkbox" name="showlast4number" id="showlast4number" onClick="checkLast4Number(this);"> <label for="showlast4number">4자리보기</label> <span>ㅣ</span>
            <input type="checkbox" class="checkbox" name="autorefresh" id="autorefresh" onClick="checkRefresh(this);"> <label for="autorefresh">자동검색</label>	<span>ㅣ</span>
            <!-- 선택답장 기능 추가 2015.3.11 Lucas Start -->
            <div class="gray_select"  id="btnSendMsgDiv" name="btnSendMsgDiv">
                <button type="button" class="select_t" id="btnSendMsg" name="btnSendMsg" onClick="sendMsg()" title="선택 항목 답장하기">선택 답장</button>
            </div>
            <!-- 선택답장 기능 추가 2015.3.11 Lucas End -->
            <span>ㅣ</span>
            페이지당
            <select id="row_count" name="row_count" class="gray_select select_b" onchange="setRowCount(this.value)">
                <option value="10">10줄</option>
                <option value="15" selected>15줄</option>
                <option value="20">20줄</option>
                <option value="30">30줄</option>
                <option value="50">50줄</option>
                <option value="100">100줄</option>
                <option value="300">300줄</option>
            </select>
        </div>
    </div>

    <div id="tab_contents">
        <div id="tabs-000">
            <div id="list000">
                <div class="table_type01">
                    <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
                        <caption>문자메시지 리스트</caption>
                        <colgroup>
                            <col style="width:35px;" />
                            <col style="width:45px;" />
                            <col style="width:130px;" />
                            <col style="width:155px;" />
                            <col style="width:auto;" />
                            <col style="width:95px;" />
                            <col style="width:135px;" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">선택</th>
                            <th scope="col">N</th>
                            <th scope="col">보낸사람</th>
                            <th scope="col" colspan="3">내용</th>
                            <th scope="col">보낸시간</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" colspan="5" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <c:forTokens var="token" items='${operator.oper_sns}' delims="|" varStatus="i">
            <div id="tabs-<c:out value='${token}'/>" style="display:none;">
                <div id="list<c:out value='${token}'/>">
                    <div class="table_type01">
                        <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
                            <caption>리스트</caption>
                            <colgroup>
                                <col style="width:35px;" />
                                <col style="width:45px;" />
                                <col style="width:130px;" />
                                <col style="width:155px;" />
                                <col style="width:auto;" />
                                <col style="width:95px;" />
                                <col style="width:135px;" />
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope="col">선택</th>
                                <th scope="col">N</th>
                                <th scope="col">보낸사람</th>
                                <th scope="col" colspan="3">내용</th>
                                <th scope="col">보낸시간</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td align="center" colspan="5" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </c:forTokens>
        <div id="tabs-666" style="display:none;">
            <div id="list666" style="" align="center">
                <img src="http://merge.mand.co.kr:2189/img/notice/tabs_app_content.png"  style="width:100%; height:630px;"/>
            </div>
        </div>
        <div id="tabs-777" style="display:none;">
            <div id="list777" style="" align="center">
                <img src="http://merge.mand.co.kr:2189/img/notice/tabs_kakao_content.png"  style="width:100%; height:630px;"/>
                <!-- <img src="http://merge.mand.co.kr:2189/img/notice/tabs_kakao_btn.png"  style="cursor: pointer; width: 250px;" title="확인" onclick="javascript:tabs('007');" /> -->
            </div>
        </div>
        <div id="tabs-999" style="display:none;">
            <div id="list999">
                <div class="table_type01">
                    <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
                        <caption>문자메시지 리스트</caption>
                        <colgroup>
                            <col style="width:35px;" />
                            <col style="width:45px;" />
                            <col style="width:130px;" />
                            <col style="width:155px;" />
                            <col style="width:auto;" />
                            <col style="width:95px;" />
                            <col style="width:135px;" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">선택</th>
                            <th scope="col">N</th>
                            <th scope="col">보낸사람</th>
                            <th scope="col" colspan="3">내용</th>
                            <th scope="col">보낸시간</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" colspan="5" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="tabs-888" style="display:none;">
            <div id="list888">
                <div class="table_type01">
                    <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
                        <caption>문자메시지 리스트</caption>
                        <colgroup>
                            <col style="width:35px;" />
                            <col style="width:45px;" />
                            <col style="width:130px;" />
                            <col style="width:155px;" />
                            <col style="width:auto;" />
                            <col style="width:95px;" />
                            <col style="width:135px;" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">선택</th>
                            <th scope="col">N</th>
                            <th scope="col">보낸사람</th>
                            <th scope="col" colspan="3">내용</th>
                            <th scope="col">보낸시간</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" colspan="5" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript" src="../js/wz_tooltip.js"></script>
<script type="text/javascript" src="../js/itv_user.js"></script>
<jsp:include page="../main/settings.jsp"/>
<script language="JavaScript">
    // 감추기 선택 기본 여부 isHideMessage
    // 자동검색 여부  isAutoRefresh, refreshTime
    // 자동 새로고침 여부 체크
    let activeTab = '000';
    let autoSearch;
    let isAutoRefresh = 10;
    let isAutoRefreshYN = Settings.isAutoRefresh();
    let f;
    $(document).ready(function() {
        f = document.BoardForm;

        // TODO 확인 필요
        $( "#mtGuideBtn" ).hide();
        $( "#kakaoGuideBtn" ).hide();

        // date picker
        setDatePicker("#start_dt");
        setDatePicker("#end_dt");
        // 초기 날짜 세팅
        $( "#start_dt" ).datepicker( "setDate", "-30" );
        $( "#end_dt" ).datepicker( "setDate", "0" );

        $("#tabs li").click(function() {
            activeTab = this.id;
            document.BoardForm.page_num.value = 1;
            list();
        });

        // 초기 불러올 경우, 위젯 제거
        // $('#container').removeClass('on');
        // $('#container>.aside').hide();

        // 현재 페이지 노출 수 쿠키 검색
        chkHistoryBoardRow();

        $(".button").button();
        list();

    });


    // 해당 페이지 노출 수 쿠키 검색
    function chkHistoryBoardRow() {
        const default_row = 15;
        let history_row_count = getCookie("history_row_count");
        if(history_row_count == "") {
            history_row_count = default_row;
            setCookie("history_row_count", default_row, "/", "${pageContext.request.serverName}");
        }
        f.row_count.value = history_row_count;
    }

    // 새싹 사용자 기능
    function checkSproutUser(obj){
        if(obj.checked){
            f.search_new_user.value = 1;
        } else {
            f.search_new_user.value = 0;
        }
        list();
    }

    // 전화번호 4자리 숫자보이기 기능 적용
    function checkLast4Number(obj) {
        if(obj.checked){
            f.show_last_4_number.value = 1;
        } else {
            f.show_last_4_number.value = 0;
        }
        list();
    }

    // 자동 검색
    function checkRefresh(obj) {
        if(obj.checked){
            clearInterval(autoSearch);
            autoSearch = window.setInterval(list, isAutoRefresh * 1000);
            isAutoRefreshYN='1';
        } else {
            isAutoRefreshYN='0';
            clearInterval(autoSearch);
        }
    }

    // 선택 답장
    function sendMsg(){
        if(getChks(f.chk_board_key)==""){
            alert("답장 하고자 하는 메세지를 선택 하세요");
            return;
        }

        let _userMsg = window.open('about:blank','sendMsgPop','status=yes, scrollbars=yes,resizable=yes,width=620,height=400');
        _userMsg.focus();
        f.target="sendMsgPop";
        f.action = "<c:url value='/BoardAction.do/sendMsg' />";
        f.submit();
    }

    function list() {
        clearInterval(autoSearch);
        const url = "<c:url value='/BoardAction.do/list' />";

        // 탭별 option 처리. 삭제, 읽음 등...
        if(activeTab != '999') {
            $('#show_hide_msg').val("1");
        } else {
            $('#show_hide_msg').val("0");
        }
        if(activeTab == '888') {
            $('#read_msg_table').val("1");
        } else {
            $('#read_msg_table').val("0");
        }
        // 선택 답장 버튼
        if( ["000", "001", "007", "013"].includes(activeTab) ) {
            document.getElementById('btnSendMsg').style.display = "block";
        } else {
            document.getElementById('btnSendMsg').style.display = "none";
        }

        let data = $(f).serialize();
        if(activeTab != '000' && activeTab != '999' && activeTab != '888') {
            data += "&msg_com=" + activeTab;
        }

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            data : data,
            error : ajaxError,
            success : endList,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });

        // 자동 새로고침 여부 체크
        if(isAutoRefreshYN == "1"){
            document.BoardForm.autorefresh.checked = true;
            autoSearch = window.setInterval(list, isAutoRefresh * 1000);
        }
    }

    function endList(response, status, request){
        $('#list'+ activeTab).html(response);
        response = null;
    }

    function search() {
        if(isValid()){
            f.page_num.value = "1";
            list();
        }
    }

    function goPage(pageNum){
        document.BoardForm.page_num.value = pageNum;
        list();
    }

    function setRowCount(val) {
        // 페이지 수 저장.
        setCookie("history_row_count", val, "/", "${pageContext.request.serverName}");
        list();
    }

    function excelDown() {
        var cnt = 0;
        if($("tot_cnt1") != null) cnt = $("tot_cnt1").innerHTML;
        if(cnt>10000) {
            alert("건수가 많아 다운로드가 불가 합니다. 관리자에게 문의하시기 바랍니다.");
            return;
        }

        f.action = "<c:url value='/BoardAction.do/list?excel=Y' />";
        f.target="_self";
        f.submit();
    }

    function isValid(){
        const start_yy	= f.start_dt.value.substring(0,4);
        const start_mm	= f.start_dt.value.substring(5,7);
        const start_dd	= f.start_dt.value.substring(8,10);

        const end_yy		= f.end_dt.value.substring(0,4);
        const end_mm		= f.end_dt.value.substring(5,7);
        const end_dd		= f.end_dt.value.substring(8,10);

        const sDate	= new Date(start_yy, start_mm, start_dd, f.start_hh.value, f.start_mi.value);
        const eDate	= new Date(end_yy, end_mm, end_dd, f.end_hh.value, f.end_mi.value);

        if( f.end_dt.value < f.start_dt.value ) {
            alert("종료일자는 시작일자 이후로 설정하세요.");
            f.end_dt.focus();
            return;
        }

        const elapsed = (eDate.valueOf()-sDate.valueOf())/(1000*60*60*24);  //기간 (일수)

        if(elapsed > 93){
            alert("기간은 최대 3개월(93일) 이내로 설정해야 합니다.");
            return false;
        }

        return true;
    }

    function tabs(idx){
        // 탭 처리
        $(".tab_type01 ul li").each(function(index, item) {
            $(item).removeClass("on");
        });

        // 전체 게시판 엑셀 다운로드 기능 제공.
        if(idx == "000") $("#excelDownBtn").css("display", "");
        if(idx != "000") $("#excelDownBtn").css("display", "none");

        // 탭 내용 처리
        $("#tab_contents div").each(function(index, item) {
            if($(item).attr("id")!="popup_table") $(item).css("display", "none");

        });
        $("#tab_contents div div").each(function(index, item) {
            if($(item).attr("id")!="popup_table") $(item).css("display", "");
        });

        // 선택 내용 처리
        $("#" + idx).addClass("on");
        $("#tabs-" + idx).css("display", "block");

        if(idx != "777" && idx != "666") {
            $("#top_searchbox").show();
            $("#table_topbox").show();
            activeTab = idx;
            goPage(1);
        } else {
            $("#top_searchbox").hide();
            $("#table_topbox").hide();
        }
    }

    function moveMsg(){
        if(getChks(f.chk_board_key)==""){
            alert("이동 또는 복사 하고자 하는 메세지를 선택 하세요");
            return;
        } else {
            copySelectMsg();
        }
    }

    function changeSaveType(value) {
        if(value == 'move') {
            $("#btnMoveMsg").text("선택 이동");
        } else {
            $("#btnMoveMsg").text("선택 복사");

        }
    }

    function copySelectMsg() {
        const url = "BoardAction.do/copySelectMsg";
        const chk_board_key = getChks(f.chk_board_key).split(",");
        const save_type = f.save_type.value;
        const save_box_type = f.save_box_type.value;
        let data = {
            chk_board_key : chk_board_key,
            save_type : save_type,
            save_box_type : save_box_type,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function setRead(msg_read, board_key, table_name) {
        const url = "BoardAction.do/setMsgRead";
        let data = {
            msg_read : msg_read,
            board_key : board_key,
            table_name : table_name,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function addPrize(msg_com, user_id, pgm_key) {
        const url = "BoardAction.do/addPrize";
        let data = {
            msg_com : msg_com,
            user_id : user_id,
            pgm_key : pgm_key,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function copyMsg(board_key, table_name)  {
        const url = "BoardAction.do/copyMsg";
        const save_type = f.save_type.value;
        const save_box_type = f.save_box_type.value;
        let data = {
            board_key : board_key,
            table_name : table_name,
            save_type : save_type,
            save_box_type : save_box_type,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function showMsg(msg_show, board_key, table_name)  {
        const url = "BoardAction.do/showMsg";
        msg_show = msg_show == "1" ? "0" : "1";
        let data = {
            board_key : board_key,
            table_name : table_name,
            msg_show : msg_show,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function setUserImage(){
        const url = "UserAction.do/setUserImage";
        let data = {
            pgm_key : szPgm_key,
            user_id : szUser_id,
            msg_com : szMsg_com,
            // user_pic : encodeURIComponent(szImage_url),
            user_pic : szImage_url,
        };

        $.ajax({
            url: url,
            type : "post",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });

        // if(n==-1){
        //     alert("사용자 이미지 업데이트 실패");
        // }else{
        //     list();
        //     if(_popUserInfo!=null) {
        //         try{
        //             _popUserInfo.setUserImageResult(n);
        //         } catch(e) {}
        //     }
        // }
    }
</script>
</body>
</html>