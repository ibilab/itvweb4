<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<form name="BoardForm" action="/BoardAction.do" style="margin-bottom: 0">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
    <c:set var="msg_com_001_cnt" value="0" />
    <c:set var="msg_com_007_cnt" value="0" />
    <c:set var="msg_com_013_cnt" value="0" />

    <c:forEach items="${BoardForm.list}" var="row">
        <!-- Lucas.2015.2.25 - 참여자 관리 페이지에서 전체답장, 선택 답장 적용. : START -->
        <c:if test="${row.msg_username ne null}"><input type="hidden" name="phone_num" value="<c:out value="${row.msg_username }"/>"/></c:if>
<%--            <c:if test="${row.msg_username eq null}"><input type="hidden" name="phone_num" value="<c:out value="${row.user_phone }"/>"/></c:if>--%>
        <!-- Lucas.2015.2.25 - 참여자 관리 페이지에서 전체답장, 선택 답장 적용. : END -->

        <input type="hidden" name="board_key" value="<c:out value="${row.board_key }"/>"/>
        <input type="hidden" name="msg_date" value="<c:out value="${row.msg_date }"/>"/>
        <input type="hidden" name="msg_key" value="<c:out value="${row.msg_key }"/>"/>


        <input type="hidden" name="msg_com" value="<c:out value="${row.msg_com }"/>"/>
        <input type="hidden" name="msg_userid" value="<c:out value="${row.msg_userid }"/>"/>
        <input type="hidden" name="comment_id" value="<c:out value="${row.comment_id }"/>"/>
        <input type="hidden" name="table_name" value="<c:out value="${row.table_name }"/>"/>

        <c:if test="${row.msg_com == '001'}">
            <c:set var="msg_com_001_cnt" value="${msg_com_001_cnt+1}" />
        </c:if>

        <c:if test="${row.msg_com == '007'}">
            <c:set var="msg_com_007_cnt" value="${msg_com_007_cnt+1}" />
        </c:if>

        <c:if test="${row.msg_com == '013'}">
            <c:set var="msg_com_013_cnt" value="${msg_com_013_cnt+1}" />
        </c:if>

    </c:forEach>
    <table width="560" border="0" cellspacing="7" cellpadding="0">
        <tr>
            <td><table width="580" border="0" cellspacing="0" cellpadding="0">
                <tr height="10">

                </tr>

                <tr>
                    <td align="center" >
                        <table width="550" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><img src="../img/popup/popup_right.gif" width="16" height="15" align="absmiddle"> <strong>답장 메시지작성 </strong> [<c:out value="${operator.oper_name}"/>]</td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td class="box_table3">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td valign="top"><table width="385" border="0" cellspacing="1" cellpadding="4">
                                                <tr>
                                                    <td width="80" class="bg_gray2">프로그램명</td>
                                                    <td class="box_bottom1 text_blue"><c:out value="${operator.pgm_name}"/></td>
                                                </tr>

                                                <tr><td height="5"></td></tr>

                                                <tr>
                                                    <td class="bg_gray2">수신대상</td>
                                                    <!-- <td class="box_bottom1 text_blue"><strong>문자 : <c:out value="${BoardForm.total_row_count}"/></strong>명에게 MT를 발송합니다.</td>  -->
                                                    <td class="box_bottom1 text_blue">
                                                        <c:if test="${msg_com_001_cnt > 0}">
                                                            <br><strong>&lt;문자&gt;</strong>
                                                            <br><strong><c:out value="${msg_com_001_cnt}"/></strong>명에게 답장 메시지를 발송합니다.
                                                        </c:if>
                                                        <c:if test="${msg_com_007_cnt > 0}">
                                                            <br><br><strong>&lt;카카오톡&gt;</strong>
                                                            <br><strong><c:out value="${msg_com_007_cnt}"/></strong>명에게 답장 메시지를 발송합니다.
                                                        </c:if>
                                                        <c:if test="${msg_com_013_cnt > 0}">
                                                            <br><br><strong>&lt;레인보우&gt;</strong>
                                                            <c:if test="${rainbow_phone_case_y > 0}">
                                                                <br><strong><c:out value="${rainbow_phone_case_y}"/></strong>명에게 답장 메시지를 발송합니다.
                                                            </c:if>
                                                            <c:if test="${rainbow_phone_case_n > 0}">
                                                                <br>발송목록에서 제외 : <strong><c:out value="${rainbow_phone_case_n}"/></strong>명(유효한 폰번호 X)
                                                            </c:if>
                                                        </c:if>
                                                    </td>
                                                </tr>

                                                <tr><td height="15"></td></tr>

                                                <tr>
                                                    <td class="bg_gray2">상용구</td>
                                                    <td class="box_bottom1 text_blue">
                                                        <select name="phrlist" onChange="javascript:setMtMsg(this.value)">
                                                            <option value="">----------</option>
                                                            <c:forEach items="${phlist}" var="row">
                                                                <option value="<c:out value='${row.msg}'/>"   >
                                                                    <c:out value='${row.title}'/>
                                                                </option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                </tr>

                                                <tr><td height="10"></td></tr>

                                                <tr>
                                                    <td class="bg_gray2">주의사항</td>
                                                    <td class="text_gray" style="padding-top:10px">
                                                        - 발송에는 5~10초의 시간이 소요되므로 발송버튼을<br>
                                                        &nbsp;&nbsp;누른 후 대기하여 주시기 바랍니다.</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="text_gray">- 발송버튼을 중복 클릭시 메시지가 중복 발송될 수<br>&nbsp;&nbsp;있습니다.</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <c:choose>
                                                        <c:when test="${operator.br_key=='MOC'}">
                                                            <td class="text_gray"><strong>- 문자와 카카오톡만 답장이 가능합니다!</strong></td>
                                                        </c:when>
                                                        <c:when test="${operator.br_key=='CBS'}">
                                                            <td class="text_gray">
                                                                <strong>- 문자, 카카오톡, 레인보우 답장만 답장이 가능합니다!
                                                                    <br>
                                                                    - 레인보우의 경우 전화번호가 아이디가 아닌 경우 발송이 제한 됩니다.</strong>
                                                            </td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td class="text_gray"><strong>- 문자와 카카오톡 답장이 가능합니다.</strong></td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </tr>
                                            </table>
                                            </td>
                                            <td>

                                                <!-- 답장하기 영역  : Start -->
                                                <div class="box_lf">
                                                    <div class="user_box">
                                                        <div class="user_top">
                                                            <strong class="t_tit">답장하기[</strong>
                                                            <strong class="t_tit" id="msgType" style="margin-right:1px;">SMS</strong>
                                                            <strong class="t_tit">]</strong>
                                                        </div>

                                                        <div class="user_cont">
                                                            <div class="user_reply">
                                                                <c:set var="setMsgSize"  value="80" />
                                                                <c:set var="setUserType" value="MO" />

                                                                <div class="user_reply_txt"><textarea id=""  name="mt_msg" rows="" cols="" onkeyup="javascript:check_msglen();"></textarea></div>
                                                                <div class="user_reply_byte">
                                                                    <span id="nbytes">0</span>/<span id="msgTypeLength"><c:out value="${setMsgSize}"/></span> Bytes
                                                                </div>
                                                                <div class="set_blk select_b" id="addCount" style="width:120px;padding-left:15px;">
                                                                <select name="phrlist_text" class="select_box select_t" onChange="javascript:setMtMsg(this.value)">
                                                                    <option value="">----------</option>
                                                                    <c:forEach items="${phlist}" var="row">
                                                                        <option value="<c:out value='${row.msg}'/>"   >
                                                                            <c:out value='${row.title}'/>
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                                <c:choose>
                                                                    <c:when test="${setMsgSize=='0'}">
                                                                        <!--
                                                                        <a href="javascript:sendSmsMsg_no();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                                        -->
                                                                        <a href="javascript:sendSmsMsg();" class="user_reply_btn" style="top: 34px"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                                    </c:when>

                                                                    <c:otherwise>
                                                                        <!-- <c:if test="${param.msg_com == '007'}">"javascript:sendSmsMsg_no();"</c:if> -->
                                                                        <!-- <c:if test="${param.msg_com != '007'}">"javascript:sendSmsMsg();"</c:if> -->
                                                                        <a href="javascript:sendSmsMsg();" class="user_reply_btn" style="top: 34px"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                                    </c:otherwise>
                                                                </c:choose>


                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user_bline"></div>
                                                </div>
                                                <!-- 답장하기 영역  : END -->

                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table></td>
        </tr>
    </table>

</form>
<jsp:include page="../main/settings.jsp"/>
<script type="text/javascript">
    let f;
    let nowSendMsg = false;
    const br_key = "<c:out value="${operator.br_key}"/>";
    $(document).ready(function() {
        f = document.BoardForm;
    });

    function sendSmsMsg() {
        if(nowSendMsg){
            alert("메세지 보내는 중입니다.");
            return;
        }
        if(emptyData(f.mt_msg, "메세지를 작성하셔요.")){
            return;
        }
        if(!check_msglen()) return;

        const url = "<c:url value='/SendAction.do/sendMsgSave' />";
        let data = $(f).serialize();

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            data : data,
            error : ajaxError,
            success : sendMsgSaveResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function sendMsgSaveResult(response) {
        const result = JSON.parse(response);
        const result_001 = result.result_001;
        const result_007 = result.result_007;
        const result_013 = result.result_013;

        if(result_001 == -1){
            alert("메세지 발송을 실패 했습니다.");
        } else {
            let showMsg = "";
            if(result_001>0) showMsg = showMsg + "문자 " + result_001 +"건 ";
            if(result_007>0) showMsg = showMsg + "카카오톡 " + result_007 +"건 ";
            if(result_013>0) showMsg = showMsg + "레인보우 " + result_013 +"건 ";
            showMsg = showMsg + "답장 메세지 발송을 성공했습니다.";
            alert(showMsg);
            window.close();
        }
    }

    function setMtMsg(value) {
        document.BoardForm.mt_msg.value = value;
        check_msglen();
    }

    function check_msglen() {
        var length = calculate_msglen(document.BoardForm.mt_msg.value);
        document.all.nbytes.innerText = length;
        f = document.BoardForm;

        // SMS 케이스
        if($("#msgType").html() == "SMS"){

            // LMS 발송 가능한 케이스
            if(Settings.canLmsMt()){
                if ( length > 80 ) {
                    $("#msgType").html("LMS");
                    $("input[name=setMsgSize]").val("1000");
                    $("#msgTypeLength").html("1000");
                }

                // LMS 발송 불 가능한 케이스
            } else {
                maxlength = 80;
                if (length > maxlength) {

                    // noticePopup();

                    alert("LMS발송 문의는 하단의 담당자에게 연락 바랍니다.\r\n메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
                    f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);

                    return false;
                }

            }
        } else if($("#msgType").html() == "LMS"){
            maxlength = 1000;
            if(length < 80){
                $("#msgType").html("SMS");
                $("input[name=setMsgSize]").val("80");
                $("#msgTypeLength").html("80");
            }else if (length > maxlength) {
                alert("메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
                f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);

                return false;
            }
        }
        /*
        maxlengh = 80;
        if (length > maxlengh) {
            alert("무선메시지는 최대 " + maxlengh + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlengh) + "바이트는 자동으로 삭제됩니다.");
            document.BoardForm.mt_msg.value = assert_msglen(document.BoardForm.mt_msg.value, maxlengh);
        } */
        return true;
    }

    function calculate_msglen(message) {
        var nbytes = 0;

        for (i=0; i<message.length; i++) {
            var ch = message.charAt(i);
            if (escape(ch).length > 4) {
                nbytes += 2;
            } else if (ch != '\r') {
                nbytes++;
            }
        }
        return nbytes;
    }

    function assert_msglen(message, maximum) {
        var inc = 0;
        var nbytes = 0;
        var msg = "";
        var msglen = message.length;

        for (i=0; i<msglen; i++) {
            var ch = message.charAt(i);
            if (escape(ch).length > 4) {
                inc = 2;
            } else if (ch != '\r') {
                inc = 1;
            }
            if ((nbytes + inc) > maximum) {
                break;
            }
            nbytes += inc;
            msg += ch;
        }
        document.all.nbytes.innerText = nbytes;
        return msg;
    }

</script>
</body>
</html>