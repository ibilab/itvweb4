<%--
  Created by IntelliJ IDEA.
  User: liam
  Date: 2021-05-10
  Time: 오전 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
<style>
    .table_type01 #userList tbody tr {cursor: pointer}
    .table_type01 #userList tbody tr:hover {background: #ddd;}
    label {cursor:pointer}
</style>
<div class="cont_top">
    <div class="cont_topl">
        <ul class="path_area">
            <li class="home" ></li>
            <li></li>
        </ul>
    </div>
    <div class="tit_box" style="clear: both;">
        <h2 class="ss01">이모티콘 관리</h2>
    </div>
</div>
<div class="table_type01 mt00">
    <table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
        <tr>
            <td bgcolor="#FFFFFF">
                <div id="list">
                    <!-- 목록 -->
                    <form id="emoticonPageForm" name="emoticonPageForm" >
                        <input type="hidden" name="rowCount" value="5"/>
                        <input type="hidden" name="page" id="pageNum" value="1"/>
                    </form>
                    <div class="table_type01 mt00">
                        <table cellspacing="0" border="1" summary="이모티콘 리스트에 대한 내용를 담고 있습니다." id="emoticonList">
                            <caption>이모티콘 리스트</caption>
                            <colgroup>
                                <col style="width:45px;" />
                                <col style="width:150px;" />
                                <col style="width:150px;" />
                                <col style="width:auto;" />
                                <col style="width:40px;" />
                                <col style="width:150px;" />
                                <col style="width:100px;" />
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope="col">NO</th>
                                <th scope="col">대분류 코드</th>
                                <th scope="col">소분류 코드</th>
                                <th scope="col" colspan="2">이모티콘</th>
                                <th scope="col">등록일</th>
                                <th scope="col">사용여부</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" align="center" id="pageNavigator"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br>
                <form action="/EmoticonAction" name="emoticonForm" id="emoticonForm" style="margin-bottom: 0">
                    <input type="hidden" name="cmd" value="insert"/>
                    <input type="hidden" name="emoIdx" value=""/>
                    <div class="box_type01 mt30">
                        <!-- table_type03 -->
                        <div class="table_type03">
                            <table cellspacing="0" border="1" summary="신규 가입자 답장 관리에 대한 내용를 담고 있습니다.">
                                <caption>신규 가입자 답장 관리</caption>
                                <colgroup>
                                    <col style="width:90px;" />
                                    <col style="width:auto;" />
                                </colgroup>
                                <tbody>
                                <tr>
                                    <th>대분류 코드</th>
                                    <td>
                                        <div class="set_blk select_b">
							<span class="select_box">
								<input type="text" id="emo_class" name="emoClass" class="dup_reset" style="width:100px;" maxlength="2" size="2"  value=""/>
							</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>소분류 코드</th>
                                    <td>
                                        <div class="set_blk select_b">
							<span class="select_box">
								<input type="text" id="emo_code" name="emoCode" class="dup_reset" style="width:100px;" maxlength="2" size="2"  value=""/>

							</span>
                                        </div>
                                        <span class="btn_btype02" >
							<a id="btnChkMOKeyword" href="javascript:void(0)" >중복확인</a>
						</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>이모티콘</th>
                                    <td>
                                        <div class="set_blk select_b" id="hidden_div_file">
                                            <input type="hidden" id="emo_path" name="emoPath" style="width:100px;" value="이미지 URL"/>
                                            <input type="hidden" id="isUpload" value="+"/>
                                            <img id="preview_attach_img" src="./icon/position_000.gif" style="width:100px; height:100px;">
                                            <a href="javascript:emoticon.imageUploadPopup()" style="width:10px; height:10px;"><img id="addMediaBtn" name="addMediaBtn" src="../img/common/btn/btn_attach_on.png"   title="이모티콘 첨부 하기"/></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>사용여부</th>
                                    <td>
                                        <input type="checkbox" name="emoUse" id="emo_use" class="checkbox" value="1"><label for="emo_use">사용함</label>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- //table_type03 -->
                    </div>
                    <div class="box_type01_bline"></div>
                    <div class="btn_box">
                        <div class="btn_lf" id="addBtn">
                            <span class="btn_btype01"><a href="javascript:void(0);">신규등록</a></span>
                        </div>
                        <div class="btn_rf" id="modifyBtn" style="display: none;">
                            <span class="btn_btype02" id="modify"><a href="javascript:void(0);">수정</a></span>
                            <span class="btn_btype01" id="addModeBtn"><a href="javascript:void(0);">입력모드</a></span>
                        </div>
                    </div>

                </form>

                <form name="uploadForm" method="post" enctype="multipart/form-data" action="/emoticon/image">
                    <input type="hidden" name="oper_id" value="<c:out value="${operator.oper_id}"/>" >
                    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>" >
                    <input type="hidden" id="uploadFilePath" name="uploadFilePath" value="" >
                    <input type="file" id="reqUploadFileName" name="reqUploadFileName" size=40 style="display:none;">
                </form>
            </td>
        </tr>
    </table>
</div>

<div class="top_searchbln"></div>
<script type="text/javascript">
    const emoticon = {
        getList : function(){
            $.ajax({
                url : '/EmoticonAction.do/emoticon'
                ,type : 'get'
                ,data : $('#emoticonPageForm').serialize()
                ,dataType : 'json'
            }).done(this.makeList)
            .fail(ajaxError);

        }, makeList : function(response){
            $('#emoticonList tbody').empty();
            let no = ($('#pageNum').val() - 1) * $('input[name=rowCount]').val();
            $(response.list).each(function (index) {
                let tr = $('<tr/>', {'data-idx' : this.emoIdx
                    , 'data-class' : this.emoClass
                    , 'data-code' : this.emoCode
                    , 'data-src' : this.emoPath
                    , 'data-use' : this.emoUse
                });
                let idx = $('<td/>',{'class': 'ac', text : ((response.total_row_count - no - 1)  - index + 1)});
                let emoClass = $('<td/>',{'class': 'ac', text : this.emoClass});
                let emoCode = $('<td/>',{'class': 'ac', text : this.emoCode});
                let emoticon = $('<td/>');
                let img = $('<img/>', {style:'width:70px; height:70px;', src : this.emoPath})
                let del = $('<td/>', {'class':'noline'});
                let div = $('<div/>', {'class' : 'icon_area'});
                let ul = $('<ul/>', {'class' : 'iconbox01'});
                let li = $('<li/>');
                let a = $('<a/>', {'href' : 'javascript:void(0);'})
                let delBtn = $('<img/>', {src:'../img/common/btn/btn_del_on.png', style: 'width:20px;height:20px;', 'class' : 'del_btn',alt : '삭제', title : '삭제', 'data-idx' : this.emoIdx, 'onerror' : '/icon/position_000.gif'});
                let regDate = $('<td/>', {'class' : 'ac', text : this.emoRegdate});
                let isUse = $('<td/>', {'class' : 'ac'});
                if(this.emoUse == 1){
                    let strong = $('<strong/>', {'class' : 'pntclr', text : '사용'});
                    isUse.append(strong);
                } else {
                    isUse.append("미사용");
                }
                emoticon.append(img);
                a.append(delBtn)
                li.append(a);
                ul.append(li);
                div.append(ul);
                del.append(div);
                tr.append(idx)
                    .append(emoClass)
                    .append(emoCode)
                    .append(emoticon)
                    .append(del)
                    .append(regDate)
                    .append(isUse);
                $('#emoticonList tbody').append(tr);
            });
            $('#pageNavigator').empty();
            $('#pageNavigator').append(response.pageNavigator);
        }, modifySet : function(obj){
            const data = $(obj).data();
            $('#emo_class').val(data.class);
            $('#emo_code').val(data.code);
            $('#preview_attach_img').attr('src', data.src);
            $('#emo_use').prop('checked', data.use == 1 ? true : false);
            $('input[name=emoIdx]').val(data.idx);
            $('#addBtn').hide();
            $('#modifyBtn').show();
            $("#isUpload").val('-');
            $('#uploadFilePath').val(data.src);
            $('#btnChkMOKeyword').text('중복확인');
            $('#reqUploadFileName').val('')
        }, addMode : function (){
            $('#emo_class').val('');
            $('#emo_code').val('');
            $('#preview_attach_img').attr('src', './icon/position_000.gif');
            $('#emo_use').prop('checked', false);
            $('input[name=emo_idx]').val('');
            $('#addBtn').show();
            $('#modifyBtn').hide();
            $("#isUpload").val('+');
            $("#uploadFilePath").val('');
            $('#emo_path').val('');
            $('#btnChkMOKeyword').text('중복확인');
            $('#reqUploadFileName').val('');
        }, save : function(action, obj) {
            let url = '/EmoticonAction.do/emoticon';
            let method = '';
            let param = '';
            if (action === 'add') {
                method = 'post';
                param = $('#emoticonForm').serialize()
            } else if (action === 'modify') {
                method = 'put';
                param = $('#emoticonForm').serialize();
            } else if (action === 'remove') {
                const data = $($(obj).parents('tr')[0]).data();
                url = url + '/' + data.idx + '?emoPath=' + data.src;
                method = 'put';
            }
            if(action !== 'remove'){
                if(!this.confirmForm()) return false;
            }
            $.ajax({
                url: url
                , type: method
                , dataType: 'json'
                , data: param
            }).done(function (response) {
                if (response.result === 1) {
                    alert(response.message);
                    emoticon.getList();
                }
            }).fail(ajaxError)
            .then(this.addMode);

        }, confirmForm : function () {
            const uploadFilePath = $("#uploadFilePath").val();
            const f = document.emoticonForm;

            if(uploadFilePath === ""){
                alert("이모티콘을 추가해 주세요.");
                return false;
            } else {
                f.emoPath.value = uploadFilePath;
            }

            if(emptyData(f.emoClass, "대분류 코드를 입력해 주세요.")) return false;
            if(emptyData(f.emoCode, "소분류 코드를 입력해 주세요.")) return false;

            // anchor tag 텍스트 가져오기
            var anchor = document.getElementById("btnChkMOKeyword");
            var keyword = anchor.innerHTML;

            if(keyword == "사용불가") {
                alert("중복 키워드 입니다. 다른 키워드를 사용하세요.");
                return false;
            }

            if(keyword == "중복확인") {
                alert("중복확인을 하여주시기 바랍니다.");
                return false;
            }

            nowSubmit=true;
            return true;
        }, imageUploadPopup : function () {
            var currentState = $("#isUpload").val();

            if(currentState == "+"){
                console.log(currentState);
                $("#reqUploadFileName").click();
            }else{
                $("#hidden_div_file").show();
                $("#preview_attach_img").attr("src", "./icon/position_000.gif");

                $("#reqUploadFileName").val("");
                $("#uploadFilePath").val("");

                alert("첨부 이미지 삭제 성공.");
                $("#addMediaBtn").attr("src", "../img/common/btn/btn_attach_on.png");

                $("#isUpload").val('+');
                $("#addMediaBtn").attr("title", "첨부파일 추가");

            }
        }, uploadImage : function (file) {
            let formData = new FormData();
            formData.append("file", file);
            formData.append('operId', document.uploadForm.oper_id.value);
            formData.append('pgmKey', document.uploadForm.pgm_key.value);
            formData.append('uploadFilePath', document.uploadForm.uploadFilePath.value);
            $.ajax({
                url : '/EmoticonAction.do/emoticon/image'
                ,enctype : 'multipart/form-data'
                ,processData : false
                ,contentType : false
                ,type: 'post'
                ,data : formData
            }).done(function(response){
                alert(response.message);
                if(response.result){
                    $('#preview_attach_img').attr('src', response.fullPath);
                    $('#emo_path').val(response.fullPath);
                    $('#uploadFilePath').val(response.fullPath);
                }
            });
        }, checkDuplicate : function () {
            const f = document.emoticonForm;
            let emo_class = f.emoClass.value;
            let emo_code = f.emoCode.value;
            let emo_idx = f.emoIdx.value;
            emo_class = trim(emo_class);
            emo_code = trim(emo_code);
            let url = '/EmoticonAction.do/emoticon/check'
            if(emo_idx !== "") {
                url = url + '/' + emo_idx;
            }

            const data = {
                emoCode : emo_code
                ,emoClass : emo_class
            };
            $.ajax({
                type: "get",
                url: url,
                data: data,
            }).done(function(response){
                if(!response.result){
                    alert(response.message);
                }
                $('#btnChkMOKeyword').text(response.btnText);
            }).fail(ajaxError);
        }
    }
    function goPage(pageNum){
        $('#pageNum').val(pageNum);
        emoticon.getList();
    }
    $(function(){
        $("#progressbar").hide();
        emoticon.getList();
        $('#emoticonList').on('click', 'tbody tr', function () {
           emoticon.modifySet(this);
        });

        $('#addModeBtn').on('click', function(){
            emoticon.addMode();
        });

        $('#addBtn a').on('click', function(){
            emoticon.save('add');
        })

        $('#modify').on('click', function(){
           emoticon.save('modify');
        });

        $(document).on('change', '#reqUploadFileName', function () {
            const file = this.files[0];
            console.log(123123);
            emoticon.uploadImage(file);
        });
        $('.dup_reset').on('keyup', function(){
            $('#btnChkMOKeyword').text('중복확인');
        });
        $('#btnChkMOKeyword').on('click', function(){
           emoticon.checkDuplicate();
        });
        $('#emoticonList').on('click', '.del_btn', function(){
           emoticon.save('remove', this);
        });
    });

</script>