<%--	
  - 작성자: Lucas
  - 일자: 2015. 1. 28
  - 저작권 표시:
  - @(#)
  - 설명:  메세지 게시판 검색 결과 엑셀.
  --%>
<%@ page language="java" contentType="application/vnd.ms-excel; charset=EUC-KR"%>
<%@ page errorPage="../errors/JspError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% response.setHeader("Content-Disposition", "attachment;filename="+java.net.URLEncoder.encode("entire_search_list", "UTF-8")+".xls"); %>


<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="application/vnd.ms-excel; charset=EUC-KR">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="searchList.files/filelist.xml">
<link rel=Edit-Time-Data href="searchList.files/editdata.mso">
<link rel=OLE-Object-Data href="searchList.files/oledata.mso">
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:돋움, monospace;
	mso-font-charset:129;
	border:none;
	mso-protection:locked visible;
	mso-style-name:표준;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:돋움, monospace;
	mso-font-charset:129;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	text-align:center;}
.xl25
	{mso-style-parent:style0;
	mso-number-format:"General Date";}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:돋움, monospace;
	mso-font-charset:129;
	mso-char-type:none;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Sheet1</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Selected/>
     <x:Panes>
      <x:Pane>
       <x:Number>3</x:Number>
       <x:ActiveRow>11</x:ActiveRow>
       <x:ActiveCol>2</x:ActiveCol>
      </x:Pane>
     </x:Panes>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet2</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet3</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>12945</x:WindowHeight>
  <x:WindowWidth>16560</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>90</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 width=769 style='border-collapse:collapse;table-layout:fixed;width:578pt'>
 <col width=56 style='mso-width-source:userset;mso-width-alt:1000;width:10pt'>
 <col width=70 style='mso-width-source:userset;mso-width-alt:2000;width:10pt'>
 <col width=162 style='mso-width-source:userset;mso-width-alt:2000;width:10pt'>
 <col width=397 style='mso-width-source:userset;mso-width-alt:6000;width:10pt'>
 <col width=397 style='mso-width-source:userset;mso-width-alt:6000;width:10pt'>
 <col width=154 style='mso-width-source:userset;mso-width-alt:14000;width:10pt'>
 <col width=154 style='mso-width-source:userset;mso-width-alt:4500;width:10pt'>
 <tr height=18 style='height:13.5pt'>
  <td height=18 class=xl24 width=56 style='height:13.5pt;width:42pt'></td>
  <td height=18 class=xl24 width=56 style='height:13.5pt;width:42pt'>NO</td>
  <td height=18 class=xl24 width=70 style='height:13.5pt;width:56pt'>TYPE</td>
  <td class=xl24 width=50 style='width:50pt'>PGM</td>
  <td class=xl24 width=50 style='width:50pt'>HP</td>
  <td class=xl24 width=397 style='width:298pt'>MSG</td>
  <td class=xl24 width=154 style='width:116pt'>TIME</td>
 </tr>
  <c:forEach var="row" items="${board.list}" varStatus="i">
 <tr height=18 style='height:13.5pt'>
	<td height=18 align=right style='height:13.5pt' x:num></td>
  <td height=18 align=right style='height:13.5pt' x:num>
  <c:out value='${board.start_index-i.index}' />
  </td>
  
  <td height=18 align=right style='height:13.5pt' x:num>
  <c:if test="${row.msgCom == 001}">문자</c:if>
  <c:if test="${row.msgCom == 002}">콩</c:if>
  <c:if test="${row.msgCom == 003}">게시판</c:if>
  <c:if test="${row.msgCom == 004}">MN</c:if>
  <c:if test="${row.msgCom == 005}">트위터</c:if>
  <c:if test="${row.msgCom == 006}">페이스북</c:if>
  <c:if test="${row.msgCom == 007}">카카오톡</c:if>
  <c:if test="${row.msgCom == 008}">Mini</c:if>
  <c:if test="${row.msgCom == 009}">고릴라</c:if>
  <c:if test="${row.msgCom == 010}">반디</c:if>
  <c:if test="${row.msgCom == 011}">MNET</c:if>
  <c:if test="${row.msgCom == 012}">NS</c:if>
  <c:if test="${row.msgCom == 013}">레인보우</c:if>
  </td>
  <td>
  	<c:out value='${row.pgmName}' />
  </td>
  <td>
  <c:if test="${!empty row.userNick}">
		<c:if test="${row.msgCom == 007}">
  			<c:out value='[${row.msgUsername}]${row.userNick}' />
  		</c:if>
  		<c:if test="${row.msgCom != 007}">
  			<c:out value='[${row.msgUsername}]${row.userNick}' />
  		</c:if>
	</c:if> 
	<c:if test="${empty row.userNick}">
		<c:out value='${row.msgUsername}' />
	</c:if> 
  </td>
  <td><c:out value='${row.msgData}'/></td>
  <td class=xl25 align=right ><c:out value='${row.msgTime}'/></td>
 </tr>
 </c:forEach>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=56 style='width:42pt'></td>
  <td width=162 style='width:122pt'></td>
  <td width=397 style='width:298pt'></td>
  <td width=154 style='width:116pt'></td>
 </tr>
 <![endif]>
</table>

</body>

</html>


