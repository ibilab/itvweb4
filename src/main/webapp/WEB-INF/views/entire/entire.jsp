<%--
  Created by IntelliJ IDEA.
  User: tjddusx
  Date: 2021/05/20
  Time: 1:39 오후
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="/js/wz_tooltip.js"></script>
<script type="text/javascript" src="/js/itv_user.js"></script>
<link href="/css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>

<style>
    #btnSendMsgDiv.gray_select button {
        height: 20px;
        line-height: 20px;
        padding: 0 25px 0 10px;
        background: url('/img/common/gray_select_box.png') no-repeat 100% 0;
        color: #383845;
        font-size: 12px;
    }

    #btnMoveMsgDiv.gray_select button {
        height: 20px;
        line-height: 20px;
        padding: 0 25px 0 10px;
        background: url('/img/common/gray_select_box.png') no-repeat 100% 0;
        color: #383845;
        font-size: 12px;
    }
	#listTbody .brd_txt {cursor: pointer;}
</style>
<!-- Tabs -->

<form action="/EntireBoardAction" name="EntireBoardForm">
	<input type="hidden" name="page_num" value="1">
	<input type="hidden" name="row_count" value="15">
	<input type="hidden" name="st_type_set" value="move">
	<input type="hidden" name="copy_st_type" value="1">
	<input type="hidden" name="search_field" value="BOTH2">

	<input type="hidden" id="show_hide_msg" name="show_hide_msg" value="on">
	<!-- 메세지 읽음 탭 처리. 2017.9.18. Lucas -->
	<input type="hidden" id="read_msg_table" name="read_msg_table" value="off">
	<input type=hidden name="show_last_4_number" value="0">

	<input type="hidden" name="send_type" value="P">

	<!-- 선택답장 매체별 변수. 2015.11.20. Lucas -->
	<input type="hidden" name="msg_com_type" value="DEFAULT">

	<!-- Excel 다운로드 추가. 2015. 1. 27 Lucas -->
	<!-- <input type="hidden" name="excel" value="N">  -->
	<!-- <input type="hidden" name="cmd" value=""> -->

	<input type="hidden" name="camp_key"/>

	<div class="cont_top">
		<div class="cont_topl">
			<ul class="path_area">
				<li class="home"></li>
				<%-- 					<li class="on"><c:out value="${menu_2_name}"/></li> --%>
			</ul>
			<div class="tab_type01">
				<ul id="tabList">

					<li id="000" data-id="000" class="on"><a href="javascript:void(0);">전체 게시판<span class="arrow"></span></a></li>

					<!-- <li id="999"><a href="javascript:tabs('999');">차단 메시지</a></li> -->
					<!-- <li id="888"><a href="javascript:tabs('888');">읽음 메시지</a></li> -->
				</ul>
			</div>
		</div>
		<div class="cont_topr">
			<p class="total_num">수신건수<br/><strong id="tot_cnt1" class="sdclr">0</strong></p>
		</div>
	</div>

	<!-- 검색 부분  -->
	<div class="top_searchbox">
		<div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar">
					<strong>시작</strong> ㅣ
					<input type="text" name="startDate" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly>
				</span>
				<button type="button" class="month_btn">달력</button>
			</span>
		</div>
		<span class="swung">~</span>
		<div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar">
					<strong>종료</strong> ㅣ
					<input type="text" name="endDate" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly>
				</span>
				<button type="button" class="month_btn">달력</button>
			</span>
		</div>

		<div class="tsearch select_b" style="width:140px;">
			<span class="select_box select_t">
				<span class="t_select" id="search_field_name"></span>
				<select id="searchCondition" name="searchCondition" style="width:120px;border:0px;">
					<option value="NAME">보낸사람</option>
					<option value="DATA">내용</option>
					<option value="BOTH" selected>보낸사람+내용</option>
				</select>
			</span>
		</div>

		<div class="tsearch">
			<span class="select_box select_t">
				<span class="t_search"><input type="text" name="searchValue" id="" onkeydown="onEnter(board.list);"/></span>
				<button type="button" style="width:62px;" class="btn" onClick="board.list()">
					<img src="../img/common/btn/btn_search.gif" width="62" height="24" alt="검색"/>
				</button>
			</span>
		</div>
		<!-- 엑셀다운로드 기능 추가 2015.1.27 Lucas Start -->
		<div class="tsearch select_b" id="excelDownBtn">
			<span class="btn_btype01" id="btnExcel">
				<a href="javascript:excelDown();">엑셀 다운로드</a>
			</span>
		</div>
		<!-- 엑셀다운로드 기능 추가 2015.1.27 Lucas End -->
	</div>
	<div class="top_searchbln"></div>
	<div class="table_topbox">
		<div class="tabletb_r">
			<input type="checkbox" class="checkbox" name="autorefresh" onClick="checkRefresh();"> 자동검색
			<span>ㅣ</span>
			페이지당
			<select id="searchRowCount" style="font-size:12px;">
				<option value="15" selected>15줄</option>
				<option value="20">20줄</option>
				<option value="30">30줄</option>
				<option value="50">50줄</option>
			</select>
		</div>
	</div>
	<div id="tab_contents">
		<div id="listArea">
			<div id="list">
				<div class="table_type01">
					<table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
						<caption>문자메시지 리스트</caption>
						<colgroup>
							<col style="width:60px;"/>
							<col style="width:130px;"/>
							<col style="width:155px;"/>
							<col style="width:155px;"/>
							<col style="width:auto;"/>
							<col style="width:135px;"/>
						</colgroup>
						<thead>
						<tr>
							<th scope="col">N</th>
							<th scope="col">프로그램명</th>
							<th scope="col">보낸사람</th>
							<th scope="col" colspan="2">내용</th>
							<th scope="col">보낸시간</th>
						</tr>
						</thead>
						<tbody id="listTbody">
							<tr>
								<td align="center" colspan="6" class="brd_txt">조건 설정후 검색 버튼을 클릭하세요.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="40" align="center" id="page">

						</td>
					</tr>
				</table>
				<div id='popup_table' style="position:absolute; left:0px; top:0px; z-index:1;display:none;" onmouseover="mouse_anchor('1');" onmouseout="mouse_anchor('0');">
					<table border="1" border-color="#000000" width="130" height="14" bgcolor='#eeeeee'>
						<tr valign=absmiddle align=center><td height="14" valign="top"><a href="javascript:setUserImage();" >개인 이미지로 등록</a><br></td></tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</form>

<script>
    var activeTab = '000';
    var autoSearch;
    var domainInfo;
    var f;

    const board = {
        info : function () {
            $.ajax({
				url : '/EntireBoardAction.do/info'
				,type : 'get'
				,dataType : 'json'
                ,beforeSend: ajaxOnCreate
                ,complete: ajaxOnComplete
			}).done(function(response){
				$(response.sns_lists).each(function(){
                    let li = $('<li/>', {'data-id' : this.code, id : this.code});
                    let a = $('<a/>', {text : this.codeNm, href : 'javascript:void(0);'})
					li.append(a);
                    $('#tabList').append(li);
				});
				board.list();
			}).fail(ajaxError);
		}, list : function (){
            if(board.searchValid()){
                clearInterval(autoSearch);
				let data = $(document.EntireBoardForm).serialize();
				if (activeTab != '000' && activeTab != '999' && activeTab != '888') {
					data += "&msgCom=" + activeTab;
				}
				$.ajax({
					url : '/EntireBoardAction.do/list'
					,type : 'get'
					,dataType : 'json'
					,data : data
					,beforeSend: ajaxOnCreate
					,complete: ajaxOnComplete
				}).done(function(response){
					if(response.result) {
					    $('#tot_cnt1').text(response.board.total_row_count);
					    $('#listTbody').empty();
                        $('#page').empty();
                        if(response.board.total_row_count > 0 ){
					        $('#page').append(response.board.pageNavigator);
                            $(response.board.list).each(function(i){
                                let tr = $('<tr/>');
                                let no = $('<td/>', {text : response.board.start_index - i, 'class' : 'ac'});
                                let programName = $('<td/>', {text: this.pgmName, 'class' : 'ac'});
                                let name = $('<td/>', {'class': 'name'});
                                let nameSpan = $('<span/>');
                                let nameImg = $('<img/>',{style : 'width:24; height:24;'});
                                if(this.userIcon == null || this.userIcon == '100') {
                                    nameImg.attr('src', '/img/common/icon/icon_level_' + (this.userGrade == null ? '0' : this.userGrade) + '.png');
                                } else {
                                    nameImg.attr('src', '/img/icon/position_' + this.userIcon + '.gif');
                                }
                                let nameA = $('<a/>',{href : 'javascript:void(0);', 'data-pgm' : this.pgmName});
                                nameSpan.append(nameImg);
                                name.append(nameSpan);
                                tr.append(no);
                                tr.append(programName);
                                // ?
                                if(this.userGrade != 0) {
                                    if(document.EntireBoardForm.show_last_4_number.value == 1) {
                                        if(this.userNick != '') {
                                            if(this.msgCom == '001'){
                                                nameA.text(this.userNick + '(' + this.lastPhoneNumber + ')');
                                            } else if(this.msgCom == '007'){
                                                nameA.text(this.userNick);
                                            } else {
                                                nameA.text(this.userNick + '(' + this.msgUserName + ')');
                                            }
                                        } else {
                                            if(this.msgCom == '001') {
                                                nameA.text(this.lastPhoneNumber);
                                            } else {
                                                nameA.text(this.msgUsername);
                                            }
                                        }
                                    } else {
                                        if(this.userNick != '') {
                                            if(this.msgCom == '007'){
                                                nameA.text(this.userNick);
                                            } else
                                                nameA.text(this.userNick + '(' + this.msgUserName + ')');
                                        } else {
                                            nameA.text(this.msgUsername);
                                        }
                                    }
                                    name.append(nameA);
                                } else {
                                    name.append(this.userNick + '(' + this.msgUserName + ')');
                                }
                                tr.append(name);

                                let setStar = false;
                                let fontColor = '';
                                let first = false;
                                if(this.moDay > 8){
                                    setStar = true;
                                    fontColor = '#E9001B';
                                } else if (this.moDay > 5){
                                    fontColor = '#FF8A00';
                                } else if (this.moDay > 2){
                                    fontColor = '#002AFF';
                                }
                                if(this.moDay == this.moTotal){
                                    first = true;
                                }
                                let src = '/img/common/btn/btn_sns_' + this.msgCom + '.png';
                                let iconTd = $('<td/>');
                                let iconDiv = $('<div/>', {'class' : 'icon_area'});
                                let iconUl1 = $('<ul/>', {'class' : 'iconbox01'});
                                let starLi = $('<li/>');
                                let starImg = $('<img/>', {'style': 'width:20px;height:20px;'});
                                if(first){
                                    starImg.attr('src', '/img/common/icon/icon_first.gif');
                                } else if(setStar) {
                                    starImg.attr('src', '/img/common/icon/icon_start.gif')
                                } else {
                                    starImg.attr('src', '/img/common/btn/btn_star_off.png');
                                }
                                starLi.append(starImg);
                                iconUl1.append(starLi);
                                let li2 = $('<li/>');
                                let img2 = $('<img/>', {alt:'매체', style : 'width:20px;height:20px;'});
                                if(this.msgCom == '002') {
                                    if(this.commentId == '002'){
                                        img2.attr('src', '/img/common/btn/btn_sns_002_kong_mobile.png');
                                    } else if(this.commentId == '003'){
                                        img2.attr('src', '/img/common/btn/btn_sns_002_kplayer_mobile.png');
                                    } else {
                                        img2.attr('src', '/img/common/btn/btn_sns_002_kong_pc.png');
                                    }
                                } else {
                                    img2.attr('src', src);
                                }
                                li2.append(img2);
                                iconUl1.append(li2);
                                iconDiv.append(iconUl1);

                                //-------------------- lms시작
                                let iconBox2 = $('<div/>', {'class': 'iconbox02'});
                                let iconBoxUl = $('<ul/>');
                                let lmsList = this.msgMmsurl.split('|');
                                if(lmsList.length < 4){
                                    for(let idx = 0, len = 3 ; idx < len ; idx ++){
                                        let lms = lmsList[idx] == undefined ? '' : lmsList[idx];
                                        let tmp = lms.split('.');
                                        let ext = tmp[tmp.length - 1].toLowerCase();
                                        let img = $('<img/>', {style: 'width:20px; height:20px;', 'class' : 'tool_tip_image', onerror: 'this.src = /img/common/btn/btn_noimg.png'})
                                        let li = $('<li/>');
                                        let a = $('<a/>');
                                        if(ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'gif'){
                                            //tooltip
                                            img.attr('src', lms);
                                            a.attr('href', 'javascript:void(0)');
                                            a.addClass('tooltip_img');
                                        } else if (ext == 'm4a') {
                                            img.attr('src', '/img/common/btn/btn_sound.png');
                                            a.attr('href', lms);
                                            a.attr('download', 'download');
                                        } else if (ext == 'avi' || ext == 'mp4'){
                                            img.attr('src', '/img/common/btn/btn_movie2.png');
                                            a.attr('href', lms);
                                            a.attr('download', 'download');
                                        } else {
                                            img.attr('src', '/img/common/btn/btn_noimg.png');
                                            a.attr('href', 'javascript:void(0)');
                                        }
                                        a.append(img);
                                        li.append(a);
                                        iconBoxUl.append(li);
                                    }
                                } else {
                                    for(let i = 0, len = 3 ; i < len ; i++){
                                        let li = $('<li/>');
                                        let img = $('<img/>', {style: 'width:20px; height:20px;', src : '/img/common/btn/btn_noimg.png'});
                                        let a = $('<a/>',{href : 'javascript:void(0);'});
                                        a.append(img);
                                        li.append(a);
                                        iconBoxUl.append(li);
                                    }
                                }
                                iconBox2.append(iconBoxUl);
                                iconDiv.append(iconBox2);
                                iconTd.append(iconDiv);
                                tr.append(iconTd);
                                let starTd = $('<td/>', {'class': 'brd_txt'});
                                if(setStar){
                                    starTd.addClass('on');
                                }
                                //user image 시작
                                if( this.userImage != null && this.userImage != ''){
                                    let userImages = this.userImage.split(';');
                                    for(let image in userImages){
                                        if(image != '200'){
                                            let img = $('<img/>', {src : '/img/icon/gift_' + image, border : 0});
                                            starTd.append(img);
                                        }
                                    }
                                }
                                let copyDiv = $('<div/>',{'class': 'p_txtcont', text: this.msgData
                                    , 'data-msg' : this.msgData
                                    , 'data-com' : this.msgCom
                                    , 'data-last' : this.lastPhoneNumber
                                    , 'data-dash' : this.userPhoneDash
                                    , 'data-name' : this.msgUsername
									, 'data-nick' : this.userNick
                                });
                                starTd.append(copyDiv);
                                tr.append(starTd);
                                let lastTd = $('<td/>', {'class': 'ac', text: this.msgTime});
                                tr.append(lastTd);
                                $('#listTbody').append(tr);
                            });
						} else {
					        let tr = $('<tr/>');
					        let td = $('<td/>', {colspan: 6, text : '검색된 데이터가 없습니다.', style :'text-align:center;'});
					        tr.append(td);
                            $('#listTbody').append(tr);
						}

                        if (document.EntireBoardForm.autorefresh.checked == true) {
                            autoSearch = window.setInterval(board.list, 10 * 1000);
                        }
					} else {
						alert(response.message);
					}
				}).fail(ajaxError);
            }
		}, setTab : function (obj){
            const idx = $(obj).data('id');
            activeTab = idx
            document.EntireBoardForm.page_num.value = 1;
            $('#tabList .on').removeClass('on');
            $(obj).addClass('on');
            $('#tabList').remove('span');
			let span = $('<span/>',{'class': 'arrow'});
            $(obj).find('a').append(span);
            if (idx == '000') $("#excelDownBtn").show();
            if (idx != '000') $("#excelDownBtn").hide();
            this.list();
		}, searchValid : function (){

            const splitStart = f.startDate.value.split('-');
            const splitEnd = f.endDate.value.split('-');
            const sDate = new Date(splitStart[0], splitStart[1], splitStart[2]);
            const eDate = new Date(splitEnd[0], splitEnd[1], splitEnd[2]);

            if (eDate < sDate) {
                alert("종료일자는 시작일자 이후로 설정하세요.");
                f.endDate.focus();
                return;
            }

            let elapsed = (eDate.valueOf() - sDate.valueOf()) / (1000 * 60 * 60 * 24);//기간 (일수)

            if (elapsed > 3) {
                alert("기간은 최대 3일 이내로 설정해야 합니다.");
                return false;
            }
            return true;
		}
	}
    $(document).ready(function () {
		$('.home').text('통합게시판 - 전체 프로그램 조회');
        alert("전체 프로그램이 검색 가능한 기간은 3일입니다.");
		board.info();
        f = document.EntireBoardForm;
        domainInfo = document.domain;

        if (domainInfo == 'nshome.mand.co.kr') {
            $("#999").hide();
        }
        $('#tabList').on('click', 'li', function(){
           board.setTab(this);
		});

        $("#start_dt").datepicker({
            dateFormat: "yy-mm-dd",
            altField: "#start_dt",
            showOtherMonths: true,
            selectOtherMonths: true,
            nextText: '다음 달', // next 아이콘의 툴팁.
            prevText: '이전 달', // prev 아이콘의 툴팁.
            monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
            dayNamesMin: ["일", "월", "화", "수", "목", "금", "토",]
        });
        $("#end_dt").datepicker({
            dateFormat: "yy-mm-dd",
            altField: "#end_dt",
            showOtherMonths: true,
            selectOtherMonths: true,
            nextText: '다음 달', // next 아이콘의 툴팁.
            prevText: '이전 달', // prev 아이콘의 툴팁.
            monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
            dayNamesMin: ["일", "월", "화", "수", "목", "금", "토",]
        });
        $("#start_dt").datepicker("setDate", "0");
        $("#end_dt").datepicker("setDate", "0");

        $(".button").button();
        // list();

        // 초기 불러올 경우, 위젯 제거. 2015.11.16. Lucas
        $('#container').removeClass('on');
        $('#listTbody').on('click', '.name a', function(){
			noPopUserInfo($(this).data('pgm'));
		});

        $('#listTbody').on('click', '.brd_txt', function () {
        	const data = $(this).find('div').data();
        	const fullPhoneNum = data.dash;
        	const userName = data.name;
        	const msgCom = data.com;
        	const lastPhoneNum = data.last;
        	const nick = data.nick;
        	const msg = data.msg;
			let copyData = '';
			if(nick == "" || nick === undefined || nick === null){
				copyData = "[" + userName + "]" + msg;
			}else{
				copyData = "[" + nick + "(" + userName + ")]" + msg;
			}

			if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
				window.clipboardData.setData('Text', copyData);
				//alert('해당 글을 복사하였습니다.');
			} else { //IE 가 아닐때
				temp = prompt("Ctrl+C를 눌러 클립보드로 복사하세요", copyData);
			}
		});
        $('#listTbody').on('mouseover', '.tool_tip_image', function (){
        	Tip('<img src="'+$(this).attr('src')+'" width=300/>');
		});
        $('#searchRowCount').on('change', function (){
        	$('input[name=row_count]').val($(this).val());
		});
    });

    function search() {
        if (isValid()) {
            f.page_num.value = "1";
            list();
        }
    }

    function goPage(pageNum) {
        document.EntireBoardForm.page_num.value = pageNum;
        board.list();
    }


    /* 기능 부분 */
    // 자동 검색기능
    function checkRefresh() {
        if (document.EntireBoardForm.autorefresh.checked == true) {
            clearInterval(autoSearch);
            autoSearch = window.setInterval(board.list, 10 * 1000);
        } else {
            clearInterval(autoSearch);
        }
    }

    // 감추기
    function checkHide() {
        list();
    }

    // 사용자 이미지 업데이트
    function setUserImage() {
        alert("통합게시판 메뉴는 사용자의 이미지 수정이 불가능합니다.\n해당 '" + pgm_name + "' 프로그램으로 \n이동하여 수정 부탁드립니다.");
    }

    function noPopUserInfo(pgm_name) {
        alert("통합게시판 메뉴는 사용자의 정보 확인이 불가능합니다.\n해당 '" + pgm_name + "' 프로그램으로 \n이동하여 확인 부탁드립니다.");
    }

    function showTooltip(board_key) {
        $("#tip_" + board_key).css("display", "block");
        $("#msg_" + board_key).mouseleave(function () {
            $("#tip_" + board_key).css("display", "none");
        });
    }


    // 엑셀다운로드 기능 추가. 2015.1.27 Lucas
    function excelDown() {
        var cnt = 0;
        if ($("tot_cnt1") != null) cnt = $("tot_cnt1").innerHTML;
        if (cnt > 10000) {
            alert("건수가 많아 다운로드가 불가 합니다. 관리자에게 문의하시기 바랍니다.");
            return;
        }

        f.action = "/EntireBoardAction.do/list/excel?"+$(document.EntireBoardForm).serialize() + '&excel=Y';
        f.target = "_self";
        f.submit();
    }

</script>