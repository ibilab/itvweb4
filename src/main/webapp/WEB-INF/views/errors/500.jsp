<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>500 Error Page!</title></head>
<body>
</body>
<script language="JavaScript">
    alert("<c:out value="${requestScope['javax.servlet.error.status_code']}"/>" + " : " + "<c:out value="${requestScope['javax.servlet.error.exception']}"/>");
    history.back();
</script>
</html>
