<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page!</title></head>
<body>
    <div>
        <p>exception : <c:out value="${requestScope['javax.servlet.error.exception']}"/></p>
    </div>
</body>
<script language="JavaScript">
    // TODO 페이지 없을 경우 404로 가도록 수정 필요...
    alert("<c:out value="${requestScope['javax.servlet.error.status_code']}"/>");
    //history.back();
</script>
</html>
