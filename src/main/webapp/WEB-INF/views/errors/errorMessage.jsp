<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.lang.Throwable" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page!</title>
</head>
<%
    Throwable error = (Throwable)request.getAttribute("msg");
    String msg = null;
    if(error != null) {
        msg = error.toString().replaceAll("\n","").replaceAll("\r","").replaceAll("\"","'");
    }else{
        msg = (String)request.getAttribute("msg");
    }
%>
<script language="javascript">
    alert("<%= msg %>");
    if("${next}") {
        location.href = "${next}";
    } else {
        history.back();
    }
</script>
</html>
