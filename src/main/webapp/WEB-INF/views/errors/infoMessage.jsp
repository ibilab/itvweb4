<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page!</title>
</head>
<script language="javascript">
    alert("${msg}");
    if("${next}") {
        location.href = "${next}";
    } else {
        history.back();
    }
</script>
</html>
