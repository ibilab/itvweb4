<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01 mt00">
    <table cellspacing="0" border="1" summary="내용별 자동 답장 리스트에 대한 내용를 담고 있습니다.">
        <caption>내용별 자동 답장 리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:auto;" />
            <col style="width:40px;" />
            <col style="width:150px;" />
            <col style="width:100px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">N</th>
            <th scope="col" colspan="2">회신내용</th>
            <th scope="col">등록일</th>
            <th scope="col">사용여부</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${KeywordForm.list}" varStatus="i">
            <tr>
                <td class="ac"><c:out value="${KeywordForm.total_row_count - i.index}"/></td>
                <td><a href="javascript:keywordWrite('<c:out value="${row.keyword_idx}"/>')"><c:out value="${row.mt_msg}"/></a></td>
                <td class="noline">
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <li><a href="javascript:deleteMsg('<c:out value="${row.keyword_idx}"/>');"><img src="../../img/common/btn/btn_del_on.png" width="20" height="20" alt="삭제"  title="삭제"/></a></li>
                        </ul>
                    </div>
                </td>
                <td class="ac"><c:out value="${row.regdate}"/></td>
                <td class="ac">
                    <c:if test="${row.mt_use==1}"><strong class='pntclr'>사용</strong></c:if>
                    <c:if test="${row.mt_use!=1}">미사용</c:if>
                </td>
            </tr>
        </c:forEach>
        <c:if test="${empty KeywordForm.list}">
            <tr>
                <td class="ac" colspan="5" align="center">등록된 내용별 자동 답장이 없습니다.</td>
            </tr>
        </c:if>
        </tbody>
    </table>
</div>

<script type="text/javascript">
$(function() {

});

</script>
</body>
</html>