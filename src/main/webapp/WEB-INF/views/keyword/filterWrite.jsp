<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link href="../css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<div leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="KeywordForm" action="/KeywordAction.do" style="margin-bottom: 0">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
    <input type="hidden" name="oper_id" value="<c:out value="${operator.oper_id}"/>"/>
    <input type="hidden" name="keyword_idx" value="<c:out value="${param.keyword_idx}"/>"/>
    <div class="box_type01 mt30">
        <!-- table_type03 -->
        <div class="table_type03">
            <table cellspacing="0" border="1" summary="내용별 자동 답장 관리에 대한 내용를 담고 있습니다.">
                <caption>내용별 자동 답장 관리</caption>
                <colgroup>
                    <col style="width:90px;" />
                    <col style="width:auto;" />
                </colgroup>
                <tbody>
                <tr>
                    <th>회신종류</th>
                    <td>
                        <span class="dftclr" id="nbytes_default_name">내용별 자동 답장</span>
                    </td>
                </tr>
                <tr>
                    <th>필터링 단어</th>
                    <td>
                        <div class="set_blk select_b">
						<span class="select_box">
							<input type="text" id="mo_word" name="mo_word" style="width:150px;" maxlength="10" onkeyup="javascript:resetDupButton();" value="<c:out value='${Keyword.mo_word}'/>"/>
						</span>
                        </div>
                        <span class="btn_btype02" >
							<a id="btnChkMOKeyword" href="javascript:chkFilterDup();" >중복확인</a>
						</span>
                    </td>
                </tr>
                <tr id="mt_subject_div">
                    <th>회신제목</th>
                    <td>
                        <div class="set_blk select_b">
							<span class="select_box">
								<input type="text" id="mt_subject" name="mt_subject" style="width:540px;" onkeyup="javascript:check_mt_subject_msglen();" value="<c:out value='${Keyword.mt_subject}'/>"/>
							</span>
                        </div>
                        <!-- // -->
                        <span class="set_txt"><span class="dftclr" id="mt_subject_nbytes">0</span> / 40 Bytes</span>
                    </td>
                </tr>
                <tr>
                    <th>회신메시지</th>
                    <td>
                        <!--  -->
                        <div class="set_blk select_b">
							<span class="select_box" style="height:80px;">
								<textarea name="mt_msg" cols="60" rows="5" onkeyup="javascript:check_msglen();" style="border-style:none; outline:0 none; resize: none;width:500px;height: 100%"><c:out value='${Keyword.mt_msg}'/></textarea>
								<%-- <input type="text" id="mt_msg" name="mt_msg" style="width:540px;" onkeyup="javascript:check_msglen();" value="<c:out value='${Keyword.mt_msg}'/>"/> --%>
							</span>
                        </div>
                        <!-- // -->
                        <span class="set_txt"><span class="dftclr" id="nbytes">0</span> / <span class="dftclr" id="nbytes_default">80</span> Bytes</span>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <input type="checkbox" name="mt_use" id="mt_use" class="checkbox" <c:if test="${Keyword.mt_use==1}">checked</c:if>><label for="auto_use">사용함</label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- //table_type03 -->
    </div>
    <div class="box_type01_bline"></div>
    <div class="btn_box">
        <div class="btn_lf">
            <c:if test="${empty param.keyword_idx}">
                <span class="btn_btype01"><a href="javascript:keywordSave();">신규등록</a></span>
            </c:if>
        </div>
        <div class="btn_rf">
            <c:if test="${!empty param.keyword_idx}">
                <span class="btn_btype02"><a href="javascript:keywordSave();">수정</a></span>
                <span class="btn_btype01"><a href="javascript:view();">입력모드</a></span>
            </c:if>
        </div>
    </div>
</form>
</div>
<jsp:include page="/WEB-INF/views/main/settings.jsp"/>
<script language="JavaScript">
let currentMsgType = "SMS";
let nowSubmit = false;
$(function() {
    initWritePage();
});

function initWritePage() {
    var f = document.KeywordForm;
    $("#mt_subject_div").hide();
    <c:if test="${!empty param.keyword_idx}">
    check_msglen();
    check_mt_subject_msglen();
    var anchor = document.getElementById("btnChkMOKeyword");
    anchor.innerHTML = "사용가능"
    </c:if>
}
function view() {
    this.location.href = "<c:url value='/KeywordAction.do/filterWrite' />";
}

function keywordSave() {
    if(nowSubmit){
        return;
    }
    if(!chkForm()) return;

    let f = document.KeywordForm;
    let url = "<c:url value='/KeywordAction.do/save' />";
    const keyword_idx = f.keyword_idx.value ? f.keyword_idx.value : 0;
    const pgm_key = f.pgm_key.value;
    const oper_id = f.oper_id.value;
    const mo_word = trim(f.mo_word.value);
    const mt_subject = f.mt_subject.value;
    const mt_msg = f.mt_msg.value;
    const mt_use = $('#mt_use').is(':checked') ? 1 : 0;

    let data = {
        keyword_idx : keyword_idx,
        pgm_key : pgm_key,
        oper_id : oper_id,
        mo_word : mo_word,
        mt_subject : mt_subject,
        mt_msg : mt_msg,
        mt_use : mt_use,
    };

    if(mo_word == "기본" || mo_word == "처음"){
        alert("해당 필터링 단어로는 설정이 불가능합니다.");
        return;
    }

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });

    nowSubmit = false;
}

function chkForm() {
    let f = document.KeywordForm;
    if(emptyData(f.mo_word, "필터링 단어를 입력 하셔요")) return false;
    if(emptyData(f.mt_msg, "회신 메세지를 입력 하셔요")) return false;

    // anchor tag 텍스트 가져오기
    var anchor = document.getElementById("btnChkMOKeyword");
    var keyword = anchor.innerHTML;

    if(keyword == "사용불가") {
        alert("중복 키워드 입니다. 다른 키워드를 사용하세요.");
        return false;
    }

    if(keyword == "중복확인") {
        alert("중복확인을 하여주시기 바랍니다.");
        return false;
    }

    nowSubmit = true;
    return true;
}

function check_mt_subject_msglen() {
    var f = document.KeywordForm;
    var length = calculate_msglen(f.mt_subject.value);
    $("#mt_subject_nbytes").html(length);
    if (length > 40) {
        alert("회신 제목은 최대 40 바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - 40) + "바이트는 자동으로 삭제됩니다.");
        f.mt_subject.value = assert_msglen(f.mt_subject.value, 40, "mt_subject");
    }
}

function check_msglen() {
    var f = document.KeywordForm;
    var length = calculate_msglen(f.mt_msg.value);
    $("#nbytes").html(length);
    //document.all.nbytes.innerText = length;

    if (length <= 80) {
        // 회신 제목 숨김
        $("#mt_subject_div").hide();
        $("#mt_subject_div").val("");
        currentMsgType = "SMS";
    }

    if(currentMsgType == "SMS" && length > 80) {
        if(Settings.canLmsMt()){
            if(confirm("LMS(1000바이트)로 자동답장을 설정 하시겠습니까?")){
                $("#mt_subject_div").show();
                currentMsgType = "LMS";
                $("#nbytes_default").html("1000");
                $("#nbytes_default_name").html("장문 기본 자동 답장");
                $("#mt_subject_div").show();
            } else {
                currentMsgType = "SMS";
                $("#nbytes_default").html("80");
                $("#nbytes_default_name").html("단문 기본 자동 답장");
                alert("SMS 단문 메시지는 최대 80 바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - 80) + "바이트는 자동으로 삭제됩니다.");
                f.mt_msg.value = assert_msglen(f.mt_msg.value, 80, "mt_msg");
                $("#mt_subject_div").hide();

            }
        } else {
            currentMsgType = "SMS";
            $("#nbytes_default").html("80");
            $("#nbytes_default_name").html("단문 기본 자동 답장");
            alert("SMS 단문 메시지는 최대 80 바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - 80) + "바이트는 자동으로 삭제됩니다.");
            f.mt_msg.value = assert_msglen(f.mt_msg.value, 80, "mt_msg");
            $("#mt_subject_div").hide();
        }
    }

    if(currentMsgType == "LMS" && length > 1000){
        alert("LMS 단문 메시지는 최대 1000 바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - 1000) + "바이트는 자동으로 삭제됩니다.");
        f.mt_msg.value = assert_msglen(f.mt_msg.value, 1000, "mt_msg");
        $("#mt_subject_div").show();
        $("#nbytes_default").html("1000");
        $("#nbytes_default_name").html("장문 기본 자동 답장");
        $("#mt_subject_div").show();
    }else if(currentMsgType == "LMS" && length < 80){
        currentMsgType = "SMS";
        $("#nbytes_default").html("80");
        $("#nbytes_default_name").html("단문 기본 자동 답장");
        $("#mt_subject_div").hide();
    }

}

function calculate_msglen(message) {
    var nbytes = 0;

    for (var i=0; i<message.length; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            nbytes += 2;
        } else if (ch != '\r') {
            nbytes++;
        }
    }
    return nbytes;
}

function assert_msglen(message, maximum, type) {
    var inc = 0;
    var nbytes = 0;
    var msg = "";
    var msglen = message.length;

    for (i=0; i<msglen; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            inc = 2;
        } else if (ch != '\r') {
            inc = 1;
        }
        if ((nbytes + inc) > maximum) {
            break;
        }
        nbytes += inc;
        msg += ch;
    }

    if(type == "mt_subject"){
        $('#mt_subject_nbytes').html(nbytes);
    } else {
        $('#nbytes').html(nbytes);
    }

    return msg;
}

function chkFilterDup() {
    const f = document.KeywordForm;
    const pgm_key = f.pgm_key.value;
    const mo_word = trim(f.mo_word.value);
    const keyword_idx = f.keyword_idx.value ? f.keyword_idx.value : 0;
    const url = "<c:url value='/KeywordAction.do/filterDupCount' />";
    const data = {
        pgm_key : pgm_key,
        mo_word : mo_word,
        keyword_idx : keyword_idx
    }

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        error : ajaxError,
        success: chkFilterDupResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function chkFilterDupResult(response) {
    const result = JSON.parse(response);
    var anchor = document.getElementById("btnChkMOKeyword");
    var data = trim(result.result);
    if(data == "true") { // 바뀌지 않았을 경우
        alert("중복된 키워드 입니다.");
        anchor.innerHTML = "사용불가"
    } else { // 기존과 다르게 바꿀 경우
        anchor.innerHTML = "사용가능"
    }
}

function resetDupButton(){
    var anchor = document.getElementById("btnChkMOKeyword");
    anchor.innerHTML = "중복확인";
}

</script>
</body>
</html>