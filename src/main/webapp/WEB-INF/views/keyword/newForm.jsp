<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="ss02">신규 가입자 답장</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<div class="table_type01 mt00" id="list">
    <table cellspacing="0" border="1" summary="신규 가입자 답장 리스트에 대한 내용를 담고 있습니다.">
        <caption>신규 가입자 답장</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:auto;" />
            <col style="width:40px;" />
            <col style="width:150px;" />
            <col style="width:100px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col" colspan="2">회신내용</th>
            <th scope="col">등록일</th>
            <th scope="col">사용여부</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="5" align="center">정보를 불러 오는 중입니다.</td>
        </tr>
        </tbody>
    </table>
</div>
<iframe frameborder="0" width="100%"  scrolling="no" height="570" src="<c:url value='/KeywordAction.do/newWrite' />" name="writeframe"></iframe>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript">
$(function() {
    initFormPage();
});

function initFormPage(){
    list();
    keywordWrite();
}

function list(){
    let url = "<c:url value='/KeywordAction.do/newList' />";
    let data = {};
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').html(response);
}

function keywordWrite(idx = ""){
    writeframe.location.replace("<c:url value='/KeywordAction.do/newWrite' />?keyword_idx="+idx);
}

function deleteMsg(idx){
    if(confirm("정말로 삭제 하시겠습니까?")) {
        let url = "<c:url value='/KeywordAction.do/delete' />";
        let data = {
            keyword_idx: idx
        };
        $.ajax({
            url: url,
            type: "post",
            dataType: "html",
            data: data,
            error: ajaxError,
            success: actionResult,
            beforeSend: ajaxOnCreate,
            complete: ajaxOnComplete
        });
    }
}
</script>
</body>
</html>