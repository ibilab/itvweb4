<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>M&Studio</title>
<link href="../css/common.css" rel="stylesheet" type="text/css">
<link href="../css/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css">

<style>
    body.login{background-color:#fafafa;}
    #login_wrap{width:1100px; margin:100px auto 0;}
    #login_wrap .login_box{border:1px solid #d1d1d6; background:#fff;}
    #login_wrap .login_bline{height:1px; overflow:hidden; margin:0 2px; border:1px solid #eaeaea; border-top:0 none;}
    .login_content{width:1000px; overflow:hidden; padding:40px 0 20px 60px;}
    .login_content .login_lfbox{float:left; width:320px;}
    .login_content .login_rfbox{float:right; width:650px; left:10px;}
    .login_lfbox h1{padding-bottom:5px; border-bottom:1px solid #d1d1d6;}
    .login_lfbox h1 img{vertical-align:top;}

    .login_lfbox .login_form{position:relative; margin-top:138px;}
    .login_form li.login_id,.login_form li.login_pw{width:222px; height:25px; overflow:hidden; padding:5px 0; margin-bottom:5px; background:url('../../img/common/login/login_form_bg.gif') no-repeat 0 0;}
    .login_form li.login_id label,.login_form li.login_pw label{float:left; width:45px; padding:2px 0; background:url('../../img/common/login/login_form_line.gif') no-repeat 100% 50%; text-align:center; font-family:Arial; font-size:16px; color:#21a5df; font-weight:bold;}
    .login_form li.login_id input,.login_form li.login_pw input{width:150px; padding:2px 10px 0; border:0 none; outline:0 none; font-size:16px; line-height:20px; color:#686872;}
    .login_form li.login_btn{position:absolute; top:0; right:0; width:90px;}
    .login_form li.login_btn button{display:block; width:90px; height:75px; background:url('../../img/common/login/login_form_btn.gif') no-repeat 0 0; text-align:center; font-family:Arial; font-size:18px; color:#fff; font-weight:bold;}
    .login_form li.login_save{color:#686872;}

    .login_rfbox .cast_logo{text-align:right;}
    .login_rfbox .login_img{padding-top:30px; padding-left:10px; text-align:center;}

    .login_footer{width:100%; padding:15px 0; border-top:1px solid #f3f3f4; background:#f8f8f8; zoom:1;}
    .login_footer:after{content:""; display:block; height:0; clear:both; visibility:hidden;}
    .login_footer .footer_logo{float:left; width:122px; padding:7px 0 0 57px;}
    .login_footer .footer_info{margin-left:190px; width:300px; font-size:11px; line-height:16px; color:#9b9ba2;}
    .login_footer .footer_banner{float:right; width:122px; margin-top:-5px;}
</style>
</head>
<body class="login">
<form name="f" method="post" action="login.do" style1="margin-top: 0;margin-bottom: 0">
    <div id="login_wrap">
        <div class="login_box">
            <div class="login_content">
                <div class="login_lfbox">
                    <h1><img src="../img/common/login/login_logo.gif" width="173" height="54" alt="m&amp;studio" /></h1>
                    <ul class="login_form">
                        <li class="login_id"><label for="logid">ID</label><input type="text" id="logid" name="oper_id" maxlength="20" value=""/></li>
                        <li class="login_pw"><label for="logpw">PW</label><input type="password" id="logpw" name="oper_pw" maxlength="25" value="" onkeydown="onEnter(login);"/></li>
                        <li class="login_btn"><button type="button" onclick="javascript:login();">Login</button></li>
                    </ul>
                </div>
                <div class="login_rfbox">
                    <ul class="bxslider">
                        <li>
                            <a href="mailto:imedia@infobank.net" target="_top">
                                <img src="http://mnstudio.mand.co.kr:2189/page/main_banner/banner_1.png"  style="width:100%;" />
                            </a>
                        </li>
                        <li>
                            <a href="mailto:imedia@infobank.net" target="_top">
                                <img src="http://mnstudio.mand.co.kr:2189/page/main_banner/banner_2.png"  style="width:100%;" />
                            </a>
                        </li>
<%--                        <li>--%>
<%--                            <a href="mailto:imedia@infobank.net" target="_top">--%>
<%--                                <img src="http://mnstudio.mand.co.kr:2189/page/main_banner/banner_3.png"  style="width:100%;" />--%>
<%--                            </a>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <a href="mailto:imedia@infobank.net" target="_top">--%>
<%--                                <img src="http://mnstudio.mand.co.kr:2189/page/main_banner/banner_4.png"  style="width:100%;" />--%>
<%--                            </a>--%>
<%--                        </li>--%>
                    </ul>
                </div>
            </div>
            <!-- footer -->
            <div class="login_footer">
                <div class="footer_logo"><img src="../img/common/img_footer_logo.gif" width="112" height="37" alt="m&amp;studio" /></div>
<%--                <div class="footer_banner">--%>
<%--                    <a href="http://mnstudio.mand.co.kr:2189/page/main_banner/mnstudio_introduction.pdf" download class="buttonFixed" id="dataDownBtn"><img src="http://mnstudio.mand.co.kr:2189/page/main_banner/main_section_download_btn.png" alt="소개자료 다운받기"  style=""></a>--%>
<%--                </div>--%>
                <div id="mainContactAddr" class="footer_info">
                    E-mail: van@infobank.net<br>
                    Tel: 031-628-1520<br>
                    Copyright ⓒ Infobank.Corp. All Right Reserved.
                </div>

            </div>
            <!-- //footer -->
        </div>
        <div class="login_bline"></div>
    </div>
</form>
<script type="text/javascript" src="../js/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/md5.js"></script>

<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        f.oper_id.value = "admin";
        f.oper_pw.value = "mediapw";

        $('.bxslider').bxSlider({
            speed: 500,
            pause: 2500,
            auto: true,
            autoControls: false,
            hideControlOnEnd: false,
            controls: false,
            slideWidth: 600,
        });
    });

    function initPage(){
        f.oper_id.focus();
        //login();
    }

    function isValid(){
        if(emptyData(f.oper_id, "아이디를 입력해주세요.")) return false;
        if(emptyData(f.oper_pw, "비밀번호를 입력해주세요.")) return false;

        return true;
    }

    function login(){
        if(isValid()) {
            f.oper_pw.value = hex_md5(f.oper_pw.value);
            f.submit();
        }
    }

</script>

</body>
</html>
