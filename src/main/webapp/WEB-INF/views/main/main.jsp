<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<script type='text/javascript' src='/js/cookie.js'></script>
<script type="text/javascript">
    <c:forEach var="row" items="${topMenus}" varStatus="i">
    <c:if test="${i.count == 1}">
    setCookie("menu_order", "<c:out value='${row.menu_order}'/>", "/", "<%=request.getServerName()%>");
    window.location.href = "<c:out value='${row.menu_url}'/>"
    </c:if>
    </c:forEach>
</script>
</html>