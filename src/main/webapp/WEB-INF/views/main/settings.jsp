<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script language="JavaScript">
let Settings = {
    sendSMSMaxLength : function () {
        // 장문 전송가능 범위 판단
        const br_key = "<c:out value='${operator.br_key}'/>";
        let returnVal = 80;		// 기본 80byte
        if(br_key == "TBS") {
            returnVal = 230;
        }
        return returnVal;
    },
    canLmsMt : function () {
        // 창원극동방송, 울산극동방송 LMS 추가. 창원 MBC LMS 추가. 2018.6.7
        let result = false;
        const br_key = "<c:out value='${operator.br_key}'/>";
        const list = ["CBS", "JIBS", "FEBCCW", "FEBCUS", "MBCNEWCW"];

        if (list.includes(br_key)) {
            result = true;
        }
        return result;
    },
    isAutoRefresh : function () {
        // 자동 새로고침 여부
        // 미사용으로 보이나 billing_code로 분리가 필요할듯해서 일단.
        let result = "0";
        const billing_code = "<c:out value='${operator.billing_code}'/>";
        const list = ["merge"];
        if (list.includes(billing_code)) {
            result = "1";
        }
        return result;
    },
    setMmsMiddle : function () {
        // MBC 광주만 화면 중앙 및 MAX 세로 길이 지정 처리. 2020.02.04. LUCAS
        const br_key = "<c:out value='${operator.br_key}'/>";
        const ch_key = "<c:out value='${operator.ch_key}'/>";
        let result = false;
        const br_list = ["MBC"];
        const ch_list = ["2626"];
        if(br_list.includes(br_key) && ch_list.includes(ch_key)) {
            result = true;
        }
        return result;
    },
}
</script>