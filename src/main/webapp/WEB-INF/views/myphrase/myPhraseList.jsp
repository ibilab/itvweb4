<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01 mt00">
    <table cellspacing="0" border="1" summary="상용구 리스트에 대한 내용를 담고 있습니다.">
        <caption>상용구 리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:auto;" />
            <col style="width:40px;" />
            <col style="width:350px;" />
            <col style="width:130px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col" colspan="2">제목</th>
            <th scope="col">메시지</th>
            <th scope="col">등록일</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${list}" varStatus="i">
            <tr>
                <td class="ac"><c:out value="${row.myphrase_idx}"/></td>
                <td><a href="javascript:update('<c:out value="${row.myphrase_idx}"/>')"><c:out value="${row.title}"/></a></td>
                <td class="noline">
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <li><a href="javascript:deleteMsg('<c:out value="${row.myphrase_idx}"/>');"><img src="../../img/common/btn/btn_del_on.png" width="20" height="20" alt="삭제" title="삭제"/></a></li>
                        </ul>
                    </div>
                </td>
                <td class="ac"><c:out value="${row.msg}"/></td>
                <td class="ac"><c:out value="${row.regdate}"/></td>
                    <%-- <td class="ac"><aof:out value="<strong class='pntclr'>사용</strong>" test="${row.mt_use==1}" >미사용</aof:out></td> --%>
            </tr>
        </c:forEach>
        <c:if test="${empty list}">
            <tr>
                <td colspan="5" class="ac">등록된 상용구 정보가  없습니다.
                </td>
            </tr>
        </c:if>
        </tbody>
    </table>
</div>
</body>
</html>