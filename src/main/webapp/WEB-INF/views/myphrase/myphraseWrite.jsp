<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<form name="MyPhraseForm" action="/MyPhraseAction.do" style="margin-bottom: 0">
    <input type="hidden" name="myphrase_idx" value="<c:out value="${MyPhrase.myphrase_idx}"/>"/>
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
    <input type="hidden" name="oper_id" value="<c:out value="${operator.oper_id}"/>"/>
    <div class="box_type01 mt30">
        <div class="table_type03">
            <table cellspacing="0" border="1" summary="기본 자동답장 관리에 대한 내용를 담고 있습니다.">
                <caption>기본 자동답장 관리</caption>
                <colgroup>
                    <col style="width:90px;" />
                    <col style="width:auto;" />
                </colgroup>
                <tbody>
                <tr>
                    <th>제목</th>
                    <td>
                        <div class="set_blk select_b">
							<span class="select_box">
								<input type="text" name="title" value="${MyPhrase.title}" maxlength="16"  style="width:125px"/>
							</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>메시지</th>
                    <td>
                        <div class="set_blk select_b">
							<span class="select_box">
								<input type="text" name="msg" value="${MyPhrase.msg}" style="width:540px" onkeyup="javascript:check_msglen();" />
							</span>
                        </div>
                        <span class="set_txt"><span class="dftclr" id="nbytes">80</span> / 80 Bytes</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box_type01_bline"></div>
    <div class="btn_box">
        <div class="btn_lf">
            <c:if test="${empty param.myphrase_idx}">
                <span class="btn_btype01"><a href="javascript:save();">신규등록</a></span>
            </c:if>
        </div>
        <div class="btn_rf">
            <c:if test="${!empty param.myphrase_idx}">
                <span class="btn_btype02"><a href="javascript:save();">수정</a></span>
                <span class="btn_btype01"><a href="javascript:view();">입력모드</a></span>
            </c:if>
        </div>
    </div>
</form>
<script language="JavaScript">
var f;
var nowSubmit=false;
$(document).ready(function(){
    initPage();
});

function initPage(){
    f = document.MyPhraseForm;
    <c:if test="${!empty param.myphrase_idx}">
    check_msglen();
    </c:if>
}
function save(){
    if(nowSubmit){
        return;
    }

    check_msglen();
    if(chkForm()){
        let url = "<c:url value='/MyPhraseAction.do/save' />";
        const myphrase_idx = f.myphrase_idx.value ? f.myphrase_idx.value : 0;
        const pgm_key = f.pgm_key.value;
        const oper_id = f.oper_id.value;
        const title = f.title.value;
        const msg = f.msg.value;

        let data = {
            myphrase_idx : myphrase_idx,
            pgm_key : pgm_key,
            oper_id : oper_id,
            title : title,
            msg : msg,
        };

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
    nowSubmit = false;
}

function chkForm(){
    if(emptyData(f.title, "제목을 입력 하셔요")) return false;
    // 제목은 10바이트까지만 입력 가능 UI 때문
    var length = calculate_msglen(f.title.value);
    if(length > 10) {
        f.title.value = assert_msglen(f.title.value, 10);
        alert("제목은 최대 10 바이트까지 입력할 수 있습니다." );
        return false;
    }

    if(emptyData(f.msg, "메세지를 입력 하셔요")) return false;
    nowSubmit=true;
    return true;
}


function check_msglen() {
    var length = calculate_msglen(f.msg.value);
    $("#nbytes").html(length);
    //document.all.nbytes.innerText = length;
    if (length > 80) {
        alert("무선메시지는 최대 80 바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - 80) + "바이트는 자동으로 삭제됩니다.");
        f.msg.value = assert_msglen(f.msg.value, 80);
    }
}

function calculate_msglen(message) {
    var nbytes = 0;

    for (i=0; i<message.length; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            nbytes += 2;
        } else if (ch != '\r') {
            nbytes++;
        }
    }
    return nbytes;
}

function assert_msglen(message, maximum) {
    var inc = 0;
    var nbytes = 0;
    var msg = "";
    var msglen = message.length;

    for (i=0; i<msglen; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            inc = 2;
        } else if (ch != '\r') {
            inc = 1;
        }
        if ((nbytes + inc) > maximum) {
            break;
        }
        nbytes += inc;
        msg += ch;
    }
    $("#nbytes").html(nbytes);
    //document.all.nbytes.innerText = nbytes;
    return msg;
}

function view(){
    this.location.href = "<c:url value='/MyPhraseAction.do/write' />";
}
</script>
</body>
</html>