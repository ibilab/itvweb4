<%--
  - 작성자: 권봉진
  - 일자: 2007. 12. 05
  - 저작권 표시:
  - @(#)
  - 설명: 계정관리 페이지
  --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
<style>
	.table_type01 #userList tbody tr {cursor: pointer}
	.table_type01 #userList tbody tr:hover {background: #ddd;}
	label {cursor:pointer}
</style>
<div class="cont_top">
	<div class="cont_topl">
		<ul class="path_area">
			<li class="home" ></li>
			<li></li>
		</ul>
	</div>
	<div class="tit_box" style="clear: both;">
		<h2 class="ss01">계정관리</h2>
	</div>
</div>
<div style="clear: both;"></div>
<div class="top_searchbox pr">
	<span class="arrow" ondblclick="reloadSystem();"></span>
	<div class="tsearch select_b">
		<span class="select_box select_t" style="min-width:110px;">
			<span class="t_select">
				<strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span>
			</span>
			<select id="channelList" style="border: 0px;">

			</select>
		</span>

	</div>
	<!-- // -->
</div>
<div class="top_searchbln"></div>
<div class="table_type01 mt10">
	<table cellspacing="0" border="1" summary="계정 리스트에 대한 내용를 담고 있습니다." id="userList">
		<caption>계정 리스트</caption>
		<colgroup>
			<col style="width:60px;" />
			<col style="width:auto;" />
			<col style="width:24%;" />
			<col style="width:24%;" />
			<col style="width:24%;" />
		</colgroup>
		<thead>
		<tr>
			<th scope="col">N</th>
			<th scope="col">ID</th>
			<th scope="col">이름</th>
			<th scope="col">프로그램</th>
			<th scope="col">소속그룹</th>
		</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
</div>

<div class="top_searchbln"></div>

<form name="OperatorForm" action="/OperatorAction" style="margin-bottom: 0">
	<input type="hidden" name="page_num" value="1">
	<input type="hidden" name="row_count" value="15">
	<input type="hidden" name="ch_key" value="<c:out value="${operator.ch_key}" />">
</form>

<form name="modifyForm" action="/OperatorAction">
	<input type="hidden" name="cmd" value="insert">
	<input type="hidden" name="oper_id_db" value="">
	<input type="hidden" name="oper_use" value="1">
	<input type="hidden" name="oper_pw" value="">
	<input type="hidden" name="pgm_key" value="">
	<input type="hidden" name="group_key" value="">
	<input type="hidden" name="sns_list" value="">
	<input type="hidden" name="sns_count" value="">
	<input type="hidden" name="oper_info" value="">

	<div class="box_type01 mt30">
	<!-- table_type03 -->
	<div class="table_type03">
		<table cellspacing="0" border="1" summary="계정관리 설정에 대한 내용를 담고 있습니다." id="userTable">
			<caption>계정관리 설정</caption>
			<colgroup>
			<col style="width:90px;" />
			<col style="width:auto;" />
			</colgroup>
			<tbody>
			<tr>
				<th>유저정보</th>
				<td>
					<div class="set_blk select_b">
						<span class="select_box">
							<strong class="gryclr">ID</strong> <span class="gryclr">ㅣ</span>
							<input type="text" name="oper_id" style="width:100px" onchange="makeChkOperID();"/>
							<span id="btnChkOperID" class="btn_btype01"><a href="javascript:chkOperID();"><span>중복확인</span></a></span>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<th>소속그룹</th>
				<td>
					<div class="set_blk select_b">
						<span class="select_box">
							<strong class="pntclr">이름</strong> <span class="pntclr">ㅣ</span>
							<input type="text" name="oper_name" style="width:80px"/>
						</span>
					</div>
					<div class="set_blk select_b">
						<span class="select_box">
							<strong class="pntclr">패스워드</strong> <span class="pntclr">ㅣ</span>
							<input type="password" name="oper_newpw" style="width:100px"/>
						</span>
					</div>
					<a href="javascript:updatePW();" class="set_btn"><img src="../../img/common/btn/btn_write.png" width="20" height="20" alt="수정" /></a>
				</td>
			</tr>
			<tr>
				<th>그룹설정</th>
				<td>
					<div class="set_blk select_b">
						<span class="select_box select_t">
							<span class="t_select">
								<strong class="pntclr">소속</strong>
								<span class="pntclr">ㅣ</span>
								<span id="search_group_name"><c:out value='${operater.group_name}'/></span>
							</span>
							<select style="min-width: 110px;border: 0px;" id="groupList">
								<option value="">선택</option>
								<c:forEach items="${groupList}" var="row">
									<option value="<c:out value='${row.group_key}'/>"><c:out value='${row.group_name}'/></option>
								</c:forEach>
							</select>
<%--							<button type="button" class="arrow_btn">화살표</button>--%>
						</span>

<%--						<div class="select_m" style="width:150px;">--%>
<%--							<ul class="layer_inner">--%>
<%--								<c:forEach items="${groupList}" var="row">--%>
<%--									<li><a href="#" onclick="javascript:setSelectBox(document.OperatorForm.group_key, '<c:out value='${row.group_key}'/>', 'search_group_name', this);"><c:out value='${row.group_name}'/></a></li>--%>
<%--								</c:forEach>--%>
<%--							</ul>--%>
<%--						</div>--%>
					</div>
						<!-- // -->
						<!--  -->
					<div class="set_blk select_b">
						<span class="select_box select_t">
							<span class="t_select">
								<strong class="pntclr">프로그램</strong>
								<span class="pntclr">ㅣ</span>
								<span id="search_pgm_name"></span>
							</span>
<%--							<button type="button" class="arrow_btn">화살표</button>--%>
							<select style="min-width: 110px;border: 0px;" id="programList">
								<c:forEach items="${pgmTopList}" var="row">
									<option value="<c:out value='${row.pgm_key}'/>"><c:out value='${row.pgm_name}'/></option>
								</c:forEach>
							</select>
						</span>
						<%--<div class="select_m" style="width:175px;">
							<ul class="layer_inner" style="height:180px;">
								<c:forEach items="${pgmTopList}" var="row">
									<li><a href="#" onclick="javascript:setSelectBox(document.OperatorForm.pgm_key, '<c:out value='${row.pgm_key}'/>', 'search_pgm_name', this);"><c:out value='${row.pgm_name}'/></a></li>
								</c:forEach>
							</ul>
						</div>--%>
					</div>
				</td>
			</tr>
			<tr>
				<th>방송매체</th>
				<td>
					<c:forTokens var="sns_code_num" items='${sns_lists}' delims="|" varStatus="i"> <!-- SNS CODE NUM : 001, 002, 003 ~ -->
						<c:forTokens var="sns_code_name" items='${sns_lists_nm}' delims="|" varStatus="k"> <!-- SNS CODE NAME : 문자, 콩, 게시판~ -->
							<c:if test="${i.count == k.count}">
								<input type="checkbox" value="${sns_code_num}" id="check_${sns_code_num}" name="sns_lists" />
								<label for="check_${sns_code_num}">${sns_code_name}</label>

								<%--<c:if test="${sns_code_num == '001'}"><input type="checkbox" name="sns_lists"   value="001" /></c:if>
								<c:if test="${sns_code_num == '002'}"><input type="checkbox" name="sns_lists"   value="002" /></c:if>
								<c:if test="${sns_code_num == '003'}"><input type="checkbox" name="sns_lists"   value="003" /></c:if>
								<c:if test="${sns_code_num == '004'}"><input type="checkbox" name="sns_lists"   value="004" /></c:if>
								<c:if test="${sns_code_num == '005'}"><input type="checkbox" name="sns_lists"   value="005" /></c:if>
								<c:if test="${sns_code_num == '006'}"><input type="checkbox" name="sns_lists"   value="006" /></c:if>
								<c:if test="${sns_code_num == '007'}"><input type="checkbox" name="sns_lists"   value="007" /></c:if>
								<c:if test="${sns_code_num == '008'}"><input type="checkbox" name="sns_lists"   value="008" /></c:if>
								<c:if test="${sns_code_num == '009'}"><input type="checkbox" name="sns_lists"   value="009" /></c:if>
								<c:if test="${sns_code_num == '010'}"><input type="checkbox" name="sns_lists"   value="010" /></c:if>
								<c:if test="${sns_code_num == '011'}"><input type="checkbox" name="sns_lists"   value="011" /></c:if>
								<c:if test="${sns_code_num == '012'}"><input type="checkbox" name="sns_lists"   value="012" /></c:if>
								<c:if test="${sns_code_num == '013'}"><input type="checkbox" name="sns_lists"   value="013" /></c:if>

								<!-- LUCAS. 코드 여분. 20150212 : START -->
								<c:if test="${sns_code_num == '014'}"><input type="checkbox" name="sns_lists"   value="014" /></c:if>
								<c:if test="${sns_code_num == '015'}"><input type="checkbox" name="sns_lists"   value="015" /></c:if>
								<c:if test="${sns_code_num == '016'}"><input type="checkbox" name="sns_lists"   value="016" /></c:if>
								<c:if test="${sns_code_num == '017'}"><input type="checkbox" name="sns_lists"   value="017" /></c:if>
								<c:if test="${sns_code_num == '018'}"><input type="checkbox" name="sns_lists"   value="018" /></c:if>
								<c:if test="${sns_code_num == '019'}"><input type="checkbox" name="sns_lists"   value="019" /></c:if>
								<c:if test="${sns_code_num == '020'}"><input type="checkbox" name="sns_lists"   value="020" /></c:if>
								<c:if test="${sns_code_num == '021'}"><input type="checkbox" name="sns_lists"   value="021" /></c:if>
								<c:if test="${sns_code_num == '022'}"><input type="checkbox" name="sns_lists"   value="022" /></c:if>
								<c:if test="${sns_code_num == '023'}"><input type="checkbox" name="sns_lists"   value="023" /></c:if>
								<c:if test="${sns_code_num == '024'}"><input type="checkbox" name="sns_lists"   value="024" /></c:if>
								<c:if test="${sns_code_num == '025'}"><input type="checkbox" name="sns_lists"   value="025" /></c:if>
								<c:if test="${sns_code_num == '026'}"><input type="checkbox" name="sns_lists"   value="026" /></c:if>
								<c:if test="${sns_code_num == '027'}"><input type="checkbox" name="sns_lists"   value="027" /></c:if>
								<c:if test="${sns_code_num == '028'}"><input type="checkbox" name="sns_lists"   value="028" /></c:if>
								<c:if test="${sns_code_num == '029'}"><input type="checkbox" name="sns_lists"   value="029" /></c:if>
								<c:if test="${sns_code_num == '030'}"><input type="checkbox" name="sns_lists"   value="030" /></c:if>--%>
								<!-- LUCAS. 코드 여분. 20150212 : END -->


							</c:if>
						</c:forTokens>
					</c:forTokens>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<!-- //table_type03 -->
	</div>
	<div class="box_type01_bline"></div>
	<div class="btn_box">
	<div class="btn_lf" id="addBtnArea">
		<span class="btn_btype01"><a href="javascript:save();">신규등록</a></span>
	</div>
	<div class="btn_rf" style="display:none;" id="modifyBtnArea">
		<span class="btn_btype02"><a href="javascript:save();">수정</a></span>
		<span class="btn_btype02"><a href="javascript:del()">삭제</a></span>
		<span class="btn_btype01"><a href="javascript:view();">입력모드</a></span>
	</div>
</div>
</form>
	<script language="JavaScript" src="/js/md5.js"></script>
	<script type="text/javascript">
	let pgmSns  ="${operator.pgm_sns}";
	$(document).ready(function(){
		$("#progressbar").hide();
		// list();
		channel();
	});

	let setCheckBox = function (sns) {
		$('input[name=sns_lists]').prop('checked', false);
		sns.split('|').forEach(value => $('#check_'+value).prop('checked', true));
	}

	function list(){
		var url = "<c:url value='/OperatorAction.do' />";
		var data = "cmd=list&"+$(document.OperatorForm).serialize().replace(/%/g,'%25');
		data += "&br_key=<c:out value="${user.br_key}" />";
		$.ajax({
			url: url + 'list',
			type : "post",
			dataType : "html",
			data : data,
			error : ajaxError,
			success : endList,
			beforeSend : ajaxOnCreate,
			complete : ajaxOnComplete
		});
	}

	function endList(response, status, request){
		$('#list').html(response);
		ifr1.location.href = "<c:url value='/OperatorAction.do?cmd=write&ch_key='/>" + document.OperatorForm.ch_key.value;
	}

	function goPage(pageNum){
		document.OperatorForm.page_num.value = pageNum;
		users();
	}

	function reloadSystem() {
		<c:if test="${user.oper_id eq 'admin'}">
		var url = "/servlet/InitSystem";
		var data = "";

		$.ajax({
			url: url,
			type : "post",
			dataType : "html",
			data : data,
			error : ajaxError,
			success : reloadOK,
			beforeSend : ajaxOnCreate,
			complete : ajaxOnComplete
		});
		</c:if>
	}

	function reloadOK() {
		alert("시스템 초기화 완료.");
	}


	function setSelectBox(val_field, val, text_field, obj) {
		val_field.value = val;
		$("#" + text_field).html(obj.outerText);
		$(obj).parent().parent().parent().toggle();
	}

	let channel = function () {
		$.ajax({
			url : 'OperatorMng.do/channel'
			,type : 'get'
			,dataType : 'json'
			,async : false
		}).done(function (response){
			$(response).each(function(){
				const option = $('<option/>',{text : this.ch_info,value : this.ch_key, 'data-key' : this.ch_key, 'data-brkey' : this.br_key, 'data-billcode' : this.billing_code,'data-chkey' : 'search_ch_key'});
				$('#channelList').append(option);
				users();
			});
		}).fail(ajaxError);
	}

	let users = function () {
		let param = {
			pageNum : $('input[name=page_num]').val()
			,rowCount : $('input[name=row_count]').val()
			,ch_key : $('input[name=ch_key]').val()
		}
		$.ajax({
			url : 'OperatorMng.do/users'
			,type : 'get'
			,dataType : 'json'
			,data : param
		}).done(function (response){
			$('.paging').remove();
			$('#userList tbody').empty();
			if(response.total_row_count > 0) {
				$(response.list).each(function(index){
					let tr = $('<tr/>', {'data-id' : this.oper_id, 'data-groupkey' : this.group_key, 'data-name' : this.oper_name
						, 'data-pgm' : this.pgm_key, 'data-sns' : this.oper_sns , 'data-old' : this.oper_pw });
					let td_no = $('<td/>', { text : index + 1});
					let td_id = $('<td/>', { text : this.oper_id});
					let td_name = $('<td/>', { text : this.oper_name});
					let td_program = $('<td/>', { text : this.pgm_name});
					let td_group = $('<td/>', { text : this.group_name});
					tr.append(td_no)
					.append(td_id)
					.append(td_name)
					.append(td_program)
					.append(td_group);
					$('#userList tbody').append(tr);
				});
			} else {
				let tr = $('<tr/>');
				let td = $('<td/>', {colspan : '5', text : '등록된 계정이 없습니다.'});
				tr.append(td);
				$('#userList tbody').append(tr);
			}
			$('#userList').parent().next().after(response.pageNavigator);

		}).fail(ajaxError);
	}

	let program = function (chKey) {
		$.ajax({
			url : '/OperatorMng.do/program/' + chKey
			,type : 'get'
			,dataType : 'json'
		}).done(function(response){
			$('#programList').empty();
			$(response).each(function(){
				let option = $('<option/>', {value : this.pgmKey, text : this.pgmName});
				$('#programList').append(option);
			});
		}).fail(ajaxError);
	};

	$('#channelList').on('change', function(){
		const text = $(this).text();
		const text_field = $(this).data('chkey');
		$('input[name=ch_key]').val($(this).val());
		$("#" + text_field).html(text);
		// $(this).parent().parent().parent().toggle();
		users();
		view();
		program($(this).val());
	});



	$('#userList').on('click', 'tbody tr', function(){
		$('#btnChkOperID').hide();
		$('#addBtnArea').hide();
		$('#modifyBtnArea').show();
		let data = $(this).data();
		$('input[name=oper_id]').val(data.id);
		$('input[name=oper_id]').prop('readonly', 'readonly');
		$('input[name=oper_name]').val(data.name);
		$('#groupList').val(data.groupkey);
		$('input[name=oper_pw]').val(data.old);
		$('#programList').val(data.pgm);
		$('input[name=pgm_key]').val(data.pgm);
		$('input[name=group_key]').val(data.groupkey);
		$('input[name=cmd]').val('update');
		$('input[name=oper_newpw]').val('');
		setCheckBox(data.sns);
		$.ajax({
			url : '/OperatorMng.do/users/'+data.id
			,type : 'post'
			,dataType : 'json'
		}).done(function(){

		}).fail(ajaxError);

	});

	$('#groupList').on('change', function(){
		$('input[name=group_key]').val($(this).val());
	});

	$('#pgm_key').on('change', function(){
		$('input[name=pgm_key]').val($(this).val());
	});

	function view(){
		$('input[name=sns_lists]').prop('checked', false);
		$('input[name=oper_id]').prop('readonly', false);
		$('#btnChkOperID').show();
		$('#addBtnArea').show();
		$('#modifyBtnArea').hide();
		$('input[name=oper_pw]').val('');
		$('input[name=cmd]').val('insert');
		$('#userTable input[type=text]').val('');
		$('#userTable select').val('');
		$('#programList').val(1);
		$('input[name=pgm_key]').val(1);
		$('#btnChkOperID').val('중복확인');
		$('input[name=oper_newpw]').val('');
	}

	function initPage(){
		f = document.modifyForm;
		// parent.resize_frame("ifr1");

		if(f.oper_id_db.value == ''){
			f.cmd.value = "insert";

			$("#btnInsert").show();

			$("#btnDel").hide();
			$("#btnInit").hide();
			$("#btnUpdate").hide();
			$("#btnPWUpdate").hide();

			//document.getElementById("oper_id").readOnly = false;
			$('input[name=oper_id]').prop('readonly', '');
		}else{
			f.cmd.value = "update";

			$("#btnInsert").hide();
			$("#btnChkOperID").hide();

			$("#btnDel").show();
			$("#btnInit").show();
			$("#btnUpdate").show();

			//document.getElementById("oper_id").readOnly = true;
			$('input[name=oper_id]').prop('readonly', 'readonly');
		}
	}

	function isValid(){
		let f = document.modifyForm;
		if(emptyData(f.oper_id, "ID를 입력해주세요.")) return false;
		if(emptyData(f.oper_name, "이름을 입력해주세요.")) return false;
		if(emptyData(f.group_key, "소속 그룹을 입력해주세요.")) return false;
		if(f.cmd.value == "insert") {
			if(emptyData(f.oper_newpw, "패스워드를 입력해주세요.")) return false;

			if(document.getElementById("btnChkOperID").value=="사용불가") {
				alert("사용 불가한 아이디 입니다.");
				return false;
			}
			if(document.getElementById("btnChkOperID").value=="중복확인") {
				alert("중복확인을 하여주시기 바랍니다.");
				return false;
			}
		}
		f.sns_list.value = getChks(f.sns_lists,"|");
		f.sns_count.value = f.sns_list.value.split("|").length;
		$('input[name=pgm_key]').val($('#programList').val());
		// 운영자 일경우 기본값 추가
		if( f.group_key.value != "1" && f.pgm_key.value == "0"  ) {
			alert("프로그램을 선택해 주세요" );
			return false;
		}
		return true;
	}

	function save(param){
		let f = document.modifyForm;
		if(isValid()){
			let method = 'post';
			let url = '/OperatorMng.do/users';
			const cmd = param == undefined ? $('input[name=cmd]').val() : param;
			if(cmd == 'update'){
				method = 'put';
			} else if (cmd === 'delete'){
				method = 'put';
				url = url + '/' + $('input[name=oper_id]').val()
			} else if (cmd === 'insert'){
				method = 'post';
			}
			if(f.oper_newpw.value!= "" && f.oper_pw.value != hex_md5(f.oper_newpw.value)) f.oper_pw.value = hex_md5(f.oper_newpw.value);
			$.ajax({
				url : url
				,type : method
				,dataType : 'json'
				,data : $(document.modifyForm).serialize()
			}).done(function(response){
				if(response.result == 1) {
					alert(response.message);
					users();
					view();
				} else {
					alert('사용자 추가에 실패하였습니다.');
				}
			}).fail(ajaxError);
		}
	}

	function del(){
		if(confirm("삭제하시겠습니까?")){
			let f = document.modifyForm;
			f.cmd.value = "delete";
			save("delete");
		}
	}

	function updatePW(){
		if(f.oper_newpw.value != "") {

			var url = "<c:url value='/OperatorAction.do' />";
			var data = "cmd=updateOperPW&oper_id=" + f.oper_id.value + "&oper_pw=" + hex_md5(f.oper_newpw.value);

			$.ajax({
				url: url,
				type : "post",
				dataType : "html",
				data : data,
				error : ajaxError,
				success : resultUpdateOperPW,
				beforeSend : ajaxOnCreate,
				complete : ajaxOnComplete
			});
		}
	}

	function ajaxError() {
	}

	function resultUpdateOperPW(result) {
		var data = trim(result);
		if(data == "false") {

		} else {
			alert("패스워드가 수정되었습니다.");
			f.oper_pw.value = hex_md5(f.oper_newpw.value);
		}
	}

	/* 	function chkOperID() {
            if(f.oper_id.value != "") {
                var url = "<c:url value='/OperatorAction.do' />";

			var data = "cmd=chkOperID&oper_id=" + f.oper_id.value;

			var ajax_request = new Ajax.Request(
				url,
				{method: "post", asynchronous:true, parameters: data, onSuccess: function(transport) {resultChkOperID(transport.responseText);}}
				);
		}
	} */

	function chkOperID(){
		let operId = $('input[name=oper_id]').val();
		if(operId != "") {
			var url = "<c:url value='/OperatorMng.do/users/check/' />";

			$.ajax({
				url: url + operId,
				type : "get",
				dataType : "html",
				error : ajaxError,
				success : resultChkOperID,
				beforeSend : ajaxOnCreate,
				complete : ajaxOnComplete
			});
		}
	}

	function resultChkOperID(result) {
		var data = trim(result);
		if(data < 1) {
			alert("사용할 수 있습니다.");
			document.getElementById("btnChkOperID").value="사용가능";
		} else {
			alert("사용할 수 없습니다.");
			document.getElementById("btnChkOperID").value="사용불가";
		}
	}

	function makeChkOperID() {
		document.getElementById("btnChkOperID").value="중복확인";
	}
</script>
