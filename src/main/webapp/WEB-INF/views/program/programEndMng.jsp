<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="ss01">종료 프로그램</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<form name="PgmForm" action="/PgmAction" style="margin-bottom: 0">
    <div class="top_searchbox pr">
        <span class="arrow"></span>
        <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_select"><strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span></span>
            <select name="ch_key" class="select_m" onchange="list()" style="width:170px;">
                <c:forEach var="row" items="${Channels}" varStatus="i">
                    <option value="<c:out value='${row.ch_key}'/>" <c:if test="${operator.ch_key == row.ch_key}">selected</c:if> ><c:out value="${row.ch_name }"/></option>
                </c:forEach>
            </select>
        </span>
        </div>
    </div>
</form>
<div class="top_searchbln"></div>
<table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
    <tr>
        <td bgcolor="#FFFFFF">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td id="list" valign="top" class="box_table01">
                        <!-- 리스트 -->
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" align="center"></td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
</table>
<script type="text/javascript" src="../js/common.js"></script>
<script language="Javascript">
    $(document).ready(function() {
        list();
    });

    function list(){
        let url = "<c:url value='/PgmAction.do/endList' />";
        let data = $(document.PgmForm).serialize().replace(/%/g,'%25');

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : endList,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
    function endList(response, status, request){
        $('#list').html(response);

    }

</script>
</body>
</html>