<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01 mt20">
    <table cellspacing="0" border="1" summary="편성프로그램에 대한 내용를 담고 있습니다.">
        <caption>편성프로그램 리스트</caption>
        <colgroup>
            <col style="width:auto;" />
            <col style="width:60px;" />
            <col style="width:60px;" />
            <col style="width:60px;" />
            <col style="width:20%;" />
            <col style="width:20%;" />
            <col style="width:200px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">프로그램</th>
            <th scope="col">KEY</th>
            <th scope="col">시작</th>
            <th scope="col">종료</th>
            <th scope="col">방송일</th>
            <th scope="col">방송기간</th>
            <th scope="col">SNS사용</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${list}" varStatus="i">
            <tr>
                <td><a href="javascript:view(${row.pgm_key});"><c:out value="${row.pgm_name}"/></a></td>
                <td class="ac"><c:out value="[${row.pgm_key}]"/></td>
                <td class="ac"><c:out value="${row.pgm_stm}"/></td>
                <td class="ac"><c:out value="${row.pgm_etm}"/></td>
                <td class="ac"><c:out value="${fn:replace(row.pgm_week, '|', '/')}" /></td>
                <td class="ac"><c:out value="${row.pgm_sdate}"/> ~ <c:out value="${row.pgm_edate}"/></td>
                <td class="aL">
                    <c:forTokens var="token" items='${row.sns_list}' delims="|">
                        <img src="../img/common/btn/btn_sns_<c:out value='${token}'/>.png" />
                    </c:forTokens>
                </td>
            </tr>
        </c:forEach>
        <c:if test="${empty list}">
            <tr>
                <td colspan="6" align="center">등록된 편성 프로그램이 없습니다.</td>
            </tr>
        </c:if>
        </tbody>
    </table>
</div>

</body>
</html>