<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<form name="PgmForm" action="/PgmAction.do">
    <input type="hidden" name="pgm_key" value="<c:out value="${Program.pgm_key}"/>"/>
    <input type="hidden" name="pgm_week" value="">
    <input type="hidden" name="pgm_stm" value="">
    <input type="hidden" name="pgm_etm" value="">
    <input type="hidden" name="sns_list" value="">
    <input type="hidden" name="sns_count" value="">
    <input type="hidden" name="mac_use" value="0">
    <div class="box_type01 mt30">
        <!-- table_type03 -->
        <div class="table_type03">
            <table cellspacing="0" border="1" summary="편성프로그램 설정에 대한 내용를 담고 있습니다." id="program_table">
                <caption>편성프로그램 설정</caption>
                <colgroup>
                    <col style="width:90px;" />
                    <col style="width:auto;" />
                </colgroup>
                <tbody>
                <tr>
                    <th>프로그램</th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_select"><strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span></span>
                                <select name="ch_key" class="select_m" style="width:170px;">
                                <c:forEach items="${chTopList}" var="row">
                                    <option value="<c:out value='${row.ch_key}'/>" <c:if test='${Program.ch_key == row.ch_key}'> selected </c:if> ><c:out value='${row.ch_name}'/></option>
                                </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box">
                                <strong class="pntclr">프로그램</strong> <span class="pntclr">ㅣ</span>
                                <input type="text" name="pgm_name" value="${Program.pgm_name}" style="width:200px" maxlength="200"/>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box">
                                <strong class="pntclr">MO번호</strong> <span class="pntclr">ㅣ</span>
                                <input type="text" name="pgm_mo" value="${Program.pgm_mo.concat(Program.pgm_emo)}" style="width:80px" maxlength="20"/>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>방송기간</th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="pgm_sdate" id="pgm_sdate" style="border:none;cursor:pointer;width:80px;" readonly value="<c:out value='${Program.pgm_sdate }'/>"> </span>
                                <button type="button" class="month_btn">달력</button>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="pgm_edate" id="pgm_edate" style="border:none;cursor:pointer;width:80px;" readonly value="<c:out value='${Program.pgm_edate }'/>"> </span>
                                <button type="button" class="month_btn">달력</button>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>방송시간<c:out value="${Program.start_mi}"/></th>
                    <td>
                        <span class="set_txt">시작</span>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="start_hh" name="start_hh" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${hh_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Program.start_hh == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>시</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="start_mi" name="start_mi" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${mi_1_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Program.start_mi == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>분</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <span class="swung">~</span>
                        <span class="set_txt">시작</span>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="end_hh" name="end_hh" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${hh_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Program.end_hh == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>시</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="end_mi" name="end_mi" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${mi_1_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Program.end_mi == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>분</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>방송요일</th>
                    <td>
                    <c:forEach var="week" items="월,화,수,목,금,토,일">
                        <input type="checkbox" name="pgm_weeks" <c:if test="${fn:indexOf(Program.pgm_week, week) != -1}">checked</c:if> value="<c:out value="${week}"/>"><c:out value="${week}"/>&nbsp;&nbsp;
                    </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>매체</th>
                    <td>
                    <c:forTokens var="oper_sns" items='${operator.oper_sns}' delims="|" varStatus="i">
                        <c:forEach var="sns_code" items="${sns_lists}" varStatus="k">
                            <c:if test="${oper_sns == sns_code.code}">
                                <input type="checkbox" name="sns_lists" value="${sns_code.code}" onclick="toggleInfo(this, '<c:out value="${sns_code.code_nm}"/>')" <c:if test="${fn:contains(Program.sns_list, sns_code.code)}">checked</c:if> ><c:out value="${sns_code.code_nm}"/>
                            </c:if>
                        </c:forEach>
                    </c:forTokens>
                    </td>
                </tr>
                <c:forEach var="row" items="${Program.imBoardInfoList}">
                    <c:forEach var="sns_code" items="${sns_lists}" varStatus="k">
                        <c:if test="${row.msg_com == sns_code.code}">
                        <c:set var="sns_name" value="${sns_code.code_nm}"/>
                        </c:if>
                    </c:forEach>
                <tr id="prog_tr_<c:out value='${row.msg_com}'/>">
                    <th></th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box">
                                <strong class="pntclr"><c:out value="${sns_name}"/> 연동 코드</strong> <span class="ntclr">ㅣ</span>
                                <input type="text" name="prog_cd_<c:out value='${row.msg_com}'/>" value="<c:out value='${row.prog_cd}'/>" style="width:200px" maxlength="200"/>
                            </span>
                        </div>
                        <div class="set_blk select_b" style="display:<c:if test="${row.msg_com != '013'}">none</c:if> ">
                            <span class="select_box">
                                <strong class="pntclr"><c:out value="${sns_name}"/> 연동 코드 2</strong> <span class="ntclr">ㅣ</span>
                                <input type="text" name="multi_id_<c:out value='${row.msg_com}'/>" value="<c:out value='${row.multi_id}'/>" style="width:200px" maxlength="200"/>
                            </span>
                        </div>
                    </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box_type01_bline"></div>
    <div class="btn_box">
        <div class="btn_lf">
            <c:if test="${empty param.pgm_key}">
                <span class="btn_btype01"><a href="javascript:save();">신규등록</a></span>
            </c:if>
        </div>
        <div class="btn_rf">
            <c:if test="${!empty param.pgm_key}">
                <span class="btn_btype02"><a href="javascript:save();">수정</a></span>
                <span class="btn_btype02"><a href="javascript:del();">삭제</a></span>
                <span class="btn_btype01"><a href="javascript:view();">입력모드</a></span>
            </c:if>
        </div>
    </div>
</form>
<script language="JavaScript">
$(function() {
    setDatePicker("#pgm_sdate");
    setDatePicker("#pgm_edate");

    $( "#pgm_sdate" ).datepicker("option", {
        changeYear: true,
        yearRange: "2018:2099"
    } );
    $( "#pgm_edate" ).datepicker("option", {
        changeYear: true,
        yearRange: "2018:2099"
    } );

    initPage();
});

let f;
function initPage() {
    f = document.PgmForm;
    parent.resize_frame("ifr1", 110);

    // 문자는 기본 선택
    $("input[name=sns_lists]").get(0).checked = "checked";

    if(f.pgm_key.value == '') {
        var d = new Date();
        f.pgm_sdate.value = d.getFullYear() + "-" + fillString(d.getMonth()+1,2,'0') + "-" + fillString(d.getDate(),2,'0');
        f.pgm_edate.value = getDateStr((d.getFullYear()+ 10) + '1231', '-');
    } else {
        if(f.pgm_sdate.value == "0000-00-00") {
            f.pgm_sdate.value = getDateStr('20000101', '-');
        }
        if(f.pgm_edate.value == "0000-00-00") {
            f.pgm_edate.value = getDateStr('29991231', '-');
        }
    }
};

function save(){
    if(isValid()){

        f.pgm_key.value = f.pgm_key.value ? f.pgm_key.value : 0;

        var url = "<c:url value='/PgmAction.do/getPgmTimeDup' />";
        var data = "ch_key=" + f.ch_key.value + "&pgm_stm=" + f.pgm_stm.value + "&pgm_etm=" + f.pgm_etm.value
            + "&pgm_week=" + f.pgm_week.value + "&pgm_key=" + f.pgm_key.value + "&pgm_sdate=" + f.pgm_sdate.value
            + "&pgm_edate=" + f.pgm_edate.value;
        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : resultPgmTimeDup,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
}

function resultPgmTimeDup(response){
    const result = JSON.parse(response);
    const data = trim(result.data);

    if(data == "true") {
        alert("방송시간이 중복됩니다.");
        f.end_hh.focus();
    } else if(data == "error") {
        alert("오류가 발생하였습니다.\n" + trim(result.msg));
    } else {
        let url = "<c:url value='/PgmAction.do/save' />";
        let data = $(f).serialize();
        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
}

function reload() {
    parent.list();
    view();
}

function isValid(){
    if(emptyData(f.pgm_name, "프로그램명을 입력해주세요.")) return false;

    f.pgm_week.value = getChks(f.pgm_weeks,"|");
    f.sns_list.value = getChks(f.sns_lists,"|");
    f.sns_count.value = f.sns_list.value.split("|").length;
    if( f.pgm_mo.value.trim() == '') {
        alert('MO번호를 입력해 주세요.');
        $(f.pgm_mo).focus();
        return false;
    }

    if(getChks(f.pgm_weeks) == ""){
        alert("방송요일을 선택해주세요.");
        $(f.pgm_week).focus();
        return false;
    }


    f.pgm_stm.value = f.start_hh.value + f.start_mi.value;
    f.pgm_etm.value = f.end_hh.value + f.end_mi.value;
    if(f.pgm_etm.value <= f.pgm_stm.value) {
        //alert("방송 시간을 확인하세요.");
        //return false;
    }

    return true;
}

function del(){

    if(confirm("삭제하시겠습니까?")){
        let url = "<c:url value='/PgmAction.do/delete' />";
        let data = {
            pgm_key : f.pgm_key.value
        };
        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
}


function view() {
    this.location.href = "<c:url value='/PgmAction.do/write' />";
}

function toggleInfo(chk, name) {
    let list = ["002", "003", "007", "009", "010", "013", "017", "019", "021"];
    let value = chk.value;

    if(!list.includes(value)) {
        return;
    }
    if(chk.checked) {
        let diaplay = "none";
        let multi_list = ["013"];
        if(multi_list.includes(value)) {
            diaplay = "block";
        }
        // add
        let tr = '<tr id="prog_tr_' + chk.value + '">';
        tr += '<th></th>';
        tr += '<td>';
        tr += '<div class="set_blk select_b">';
        tr += '<span class="select_box">';
        tr += '<strong class="pntclr">' + name + ' 연동 코드</strong> <span class="ntclr">ㅣ</span>';
        tr += '<input type="text" name="prog_cd_' + value + '" value="" style="width:200px" maxlength="200"/>';
        tr += '</span>';
        tr += '</div>';
        tr += '<div class="set_blk select_b" style="display:' + diaplay + '">';
        tr += '<span class="select_box">';
        tr += '<strong class="pntclr">' + name + ' 연동 코드 2</strong> <span class="ntclr">ㅣ</span>';
        tr += '<input type="text" name="multi_id_' + value + '" value="" style="width:200px" maxlength="200"/>';
        tr += '</span>';
        tr += '</div>';
        tr += '</td>';
        tr += '</tr>';
        $('#program_table > tbody:last').append(tr);
    } else {
        // remove
        let tr_id = "prog_tr_" + value;
        $('tr[id^=' + tr_id + ']').remove();
    }
}
</script>
</body>
</html>