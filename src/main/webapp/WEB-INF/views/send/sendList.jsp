<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="table_type01">
    <table cellspacing="0" border="1" summary="보낸 메시지를 담고 있습니다.">
        <caption>리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:155px;" />
            <col style="width:35px;" />
            <col style="width:auto;" />
            <col style="width:135px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">N</th>
            <th scope="col">아이디</th>
            <th scope="col" colspan="2" >내용</th>
            <th scope="col">보낸 시간</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${SendForm.list}" varStatus="i">
            <!-- SNS 세팅 -->
            <c:set var="imgMsgCom" value="/common/btn/btn_sns_${row.msg_com}.png" />
            <c:set var="imgAlt" value="매체" />
            <tr>
                <td class="ac"><c:out value='${SendForm.start_index-i.index}' /></td>
                <td class="name">
						<span>
						<c:choose>
                            <c:when test="${empty row.user_icon}">
                                <img src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                            </c:when>
                            <c:when test="${row.user_icon == '100' }">
                                <img  src="../img/common/icon/icon_level_<c:out value='${row.user_grade}'/>.png" width="24" height="24">
                            </c:when>
                            <c:otherwise>
                                <img src='./icon/position_<c:out value="${row.user_icon}"/>.gif' width="24" height="24" border="0"  name="emoticon" >
                            </c:otherwise>
                        </c:choose>
						</span>
                    <c:choose>
                        <c:when test="${row.user_grade!='0'}">
                            <a href="javascript:popUserInfo('<c:out value='${row.user_id}'/>', '<c:out value='${row.msg_com}'/>',  '<c:out value='${row.user_name}'/>',  '<c:out value='${row.comment_id}'/>' , '<c:out value='${row.send_key}'/>', '<c:out value='${row.table_name}'/>')">
                                <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com != 007}">
                                    <c:out value='${row.user_nick}(${row.user_name})' />
                                </c:if>
                                <c:if test="${(row.user_nick != '' && row.user_nick ne null ) && row.msg_com == 007}">
                                    <c:out value='${row.user_nick}' />
                                </c:if>
                                <c:if test="${row.user_nick == '' || row.user_nick eq null}">
                                    <c:out value='${row.user_name}' />
                                </c:if>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <c:out value='${row.user_nick}(${row.user_name})' />
                        </c:otherwise>
                    </c:choose>
                </td>
                <td class="ac"><img src="../img<c:out value='${imgMsgCom}'/>" width="20" height="20" alt="<c:out value='${imgAlt}'/>" /></td>
                <td class="brd_txt" >
                    <c:if test="${row.msg_com == '007'}"></c:if>
                    <a href="javascript:void(0);" class="ellips_text" id="msg_<c:out value='${row.send_key }'/>" onmouseover="showTooltip('<c:out value='${row.send_key }'/>');"><c:out value='${row.msg}' /></a>
                    <div class="p_txtcont">
                        <!-- tooltip layer -->
                        <div id="tip_<c:out value='${row.send_key }'/>" class="tootip_layer" ><div class="tootip_inner">
                            <div class="arrow"></div>
                            <p class="tooltip_txt">
                                <c:out value='${row.msg }'/>
                            </p>
                        </div></div>
                        <!-- //tooltip layer -->
                    </div>
                </td>
                <td class="ac"><c:out value='${row.regdate}' /></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="40" align="center"><c:out value="${SendForm.pageNavigator}" escapeXml="false" /></td>
    </tr>
</table>
<script type="text/javascript">
    $("#tot_cnt1").html(<c:out value='${SendForm.total_row_count}'/>);
</script>
