<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table cellspacing="0" border="1" summary="발신메시지를 담고 있습니다." style="font-size:12px;">
	<caption>발신메시지 리스트</caption>
	<colgroup>
		<col style="width:45px;" />
		<col style="width:25px;" />
		<col style="width:auto;" />
		<col style="width:80px;" />
	</colgroup>
	<thead>	
		<tr>
			<th scope="col">N</th>
			<th scope="col" colspan="2">내용</th>
			<th scope="col">보낸시간</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="row" items="${BoardForm.list}" varStatus="i">
		<!-- SNS 세팅 -->
		<c:set var="imgMsgCom" value="/img/common/btn/btn_sns_${row.msg_com}.png" />
		<c:set var="imgAlt" value="매체" />
		<tr>
			<td class="ac"><c:out value='${BoardForm.start_index-i.index}' /></td>
			<td>
				<div class="icon_area">
					<ul class="iconbox01">
						<li>
							<img src="<c:out value='${imgMsgCom}'/>" width="20" height="20" alt="<c:out value='${imgAlt}'/>" />
						</li>
					</ul>
				</div>
			</td>
			<td class="brd_txt">
			<c:set var="msg_data" value="${row.msg }" />
					<%
						String msg_data = (String)pageContext.getAttribute("msg_data");
						String tip_msg_data = msg_data;
						int msg_size = 20;
						if(msg_data.length() > msg_size) {
							pageContext.setAttribute("msg_data", msg_data.substring(0, msg_size) + "...");
						}
						for(int i=20 ; i<tip_msg_data.length() ; i+=20) {
							//tip_msg_data = tip_msg_data.substring(0,i) + "<br/>" + tip_msg_data.substring(i, tip_msg_data.lengh);
						}
						pageContext.setAttribute("tip_msg_data", tip_msg_data);
					%>
				<a id="msg_<c:out value='${i.index }'/>" onmouseover="showTooltip('<c:out value='${i.index }'/>');"><c:out value='${msg_data}' /></a>
				<div class="p_txtcont">
					<!-- tooltip layer -->
					<div id="tip_<c:out value='${i.index }'/>" class="tootip_layer" ><div class="tootip_inner">
						<div class="arrow"></div>
						<p class="tooltip_txt">
						<c:out value='${tip_msg_data }'/>
						</p>
					</div></div>
					<!-- //tooltip layer -->
				</div>
			</td>
			<td class="ac"><c:out value='${row.regdate}' /></td>
		<tr>
		</c:forEach>
		<c:if test="${empty BoardForm.list}">
			<tr>
				<td class="ac" colspan="3">검색된 정보가 없습니다.</td>
			</tr>
		</c:if>			
	</tbody>
</table>
<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top" class="box_table01">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" style="table-layout:fixed">
			<tr>
                <td width="8%" align="center" class="bg_gray2">N</td>
                <td width="" align="center" class="bg_gray2">MSG</td>
                <td width="32%" align="center" class="bg_gray2">Time</td>
            </tr>
			<c:forEach var="row" items="${BoardForm.list}" varStatus="i">
				<tr height="26"  onMouseOver="mouseOver(this)" onMouseOut="mouseOut(this)">
					<td class="box_bottom1 box_right1" align="center" ><c:out value='${BoardForm.start_index-i.index}' /></td>
					<td class="box_bottom1 box_right1" style="word-wrap:break-word;">		
						<aof:out value='${row.content}' />
					</td>
					<td align="center" class="box_bottom1 box_right1"><c:out value='${row.msg_time}' /></td>
				</tr>
			</c:forEach>
			<c:if test="${empty BoardForm.list}">
				<tr>
					<td colspan="4" align="center">검색된 정보가 없습니다.</td>
				</tr>
			</c:if>
		</table>
		</td>
	</tr>
</table>
 --%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="40" align="center"><c:out
			value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
	</tr>
</table>
