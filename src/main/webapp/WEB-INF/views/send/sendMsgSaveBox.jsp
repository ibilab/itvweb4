<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<style type="text/css">
    .box_table3 table table td {
        padding-top: 10px;
    }
</style>
</head>
<body>
<form name="SendForm" action="/SendAction.do" style="margin-bottom: 0">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
    <c:forEach items="${BoardForm.list}" var="row">
        <input type="hidden" name="msg_com" value="<c:out value="${row.msg_com }"/>"/>
        <input type="hidden" name="msg_userid" value="<c:out value="${row.msg_userid }"/>"/>
        <input type="hidden" name="board_key" value="<c:out value="${row.board_key }"/>"/>
        <input type="hidden" name="table_name" value="<c:out value="${row.table_name }"/>"/>
    </c:forEach>


    <table width="550" border="0" cellspacing="0" cellpadding="20" style="margin-top: 30px;margin-left: 10px;">
        <tr>
            <td><img src="/img/popup/popup_right.gif" width="16" height="15" align="absmiddle"> 보관게시판 <strong><c:out value="${sendMsgType}"/></strong> 답장 메시지작성 [<c:out value="${operator.oper_name}"/>]</td>
        </tr>
        <tr>
            <td height="10"></td>
        </tr>
        <tr>
            <td class="box_table3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top"><table width="385" border="0" cellspacing="1" cellpadding="4">
                            <tr>
                                <td width="80" align="center" class="bg_gray2">프로그램명</td>
                                <td class="box_bottom1 text_blue"><c:out value="${operator.pgm_name}"/></td>
                            </tr>
                            <tr>
                                <td align="center" class="bg_gray2">수신대상</td>
                                <!-- <td class="box_bottom1 text_blue"><strong>문자 : <c:out value="${BoardForm.total_row_count}"/></strong>명에게 MT를 발송합니다.</td>  -->
                                <td class="box_bottom1 text_blue">
                                    <c:if test="${BoardForm.total_row_count > 0}">
                                        문자 : <strong><c:out value="${BoardForm.total_row_count}"/></strong>명에게 답장 메시지를 발송합니다.<br>
                                    </c:if>
                                    <c:if test="${BoardForm.total_row_count == 0}">
                                    <strong>답장을 보낼 사용자가 없습니다.<br>
                                        </c:if>
                                </td>

                            </tr>
                            <tr>
                                <td align="center" class="bg_gray2">상용구</td>
                                <td class="box_bottom1 text_blue">
                                    <select name="phrlist" onChange="javascript:setMtMsg(this.value)">
                                        <option value="">----------</option>
                                        <c:forEach items="${phlist}" var="row">
                                            <option value="<c:out value='${row.msg}'/>"   >
                                                <c:out value='${row.title}'/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="bg_gray2">주의사항</td>
                                <td class="text_gray">- 발송에는 5~10초의 시간이 소요되므로 발송버튼을<br>&nbsp;&nbsp;누른 후 대기하여 주시기 바랍니다.</td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;</td>
                                <td class="text_gray">- 발송버튼을 중복 클릭시 메시지가 중복 발송될 수<br>&nbsp;&nbsp;있습니다.</td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;</td>
                                <td class="text_gray"><strong>- 문자만 답장이 가능합니다.</strong></td>
                            </tr>
                        </table>
                        </td>
                        <td>
                            <!-- 답장하기 영역  : Start -->
                            <div class="box_lf" >
                                <div class="user_box">
                                    <div class="user_cont">
                                        <div class="user_reply">
                                            <c:set var="setMsgSize"  value="80" />
                                            <c:set var="setUserType" value="MO" />

                                            <div class="user_reply_txt"><textarea id=""  name="mt_msg" rows="" cols="" onkeyup="javascript:check_msglen();"></textarea></div>
                                            <div class="user_reply_byte"><span id="nbytes">0</span>/<c:out value="${setMsgSize}"/> Bytes</div>
                                            <div class="set_blk select_b" id="addCount" style="width:120px;padding-left:15px;">
<%--                                            <span class="select_box select_t">--%>
<%--                                                <span class="t_select" id="mt_msg_name">-------</span>--%>
<%--                                                <button type="button" class="arrow_btn">화살표</button>--%>
<%--                                            </span>--%>

<%--                                                <div class="select_m">--%>
<%--                                                    <ul class="layer_inner" style="height:80px;width:116px;">--%>
<%--                                                        <li><a href="javascript:void(0);" onclick="javascript:setSelectBox(document.MsgForm.mt_msg, '<c:out value='${row.msg}'/>', 'mt_msg_name', this);setMtMsg2('');">-------</a></li>--%>
<%--                                                        <c:forEach items="${phlist}" var="row">--%>
<%--                                                            <li><a href="javascript:void(0);" onclick="javascript:setSelectBox(document.MsgForm.mt_msg, '<c:out value='${row.msg}'/>', 'mt_msg_name', this);setMtMsg2('<c:out value='${row.msg}'/>');"><c:out value='${row.title}'/></a></li>--%>
<%--                                                        </c:forEach>--%>
<%--                                                    </ul>--%>
<%--                                                </div>--%>
                                                <select name="phrlist_text" class="select_box select_t" onChange="javascript:setMtMsg(this.value)">
                                                    <option value="">----------</option>
                                                    <c:forEach items="${phlist}" var="row">
                                                        <option value="<c:out value='${row.msg}'/>"   >
                                                            <c:out value='${row.title}'/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <c:choose>
                                                <c:when test="${setMsgSize=='0'}">
                                                    <!--
                                                    <a href="javascript:sendSmsMsg_no();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                    -->
                                                    <a href="javascript:sendSmsMsg();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                </c:when>

                                                <c:otherwise>
                                                    <!-- <c:if test="${param.msg_com == 007}">"javascript:sendSmsMsg_no();"</c:if> -->
                                                    <!-- <c:if test="${param.msg_com != 007}">"javascript:sendSmsMsg();"</c:if> -->
                                                    <a href="javascript:sendSmsMsg();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                </c:otherwise>
                                            </c:choose>


                                        </div>
                                    </div>
                                </div>
                                <div class="user_bline"></div>
                            </div>
                            <!-- 답장하기 영역  : END -->
                        </td>
                    </tr>
                </table></td>
        </tr>
        <tr>
            <td height="10"></td>
        </tr>
    </table>


</form>
</body>
<script language="JavaScript">
let nowSendMsg = false;

$(document).ready(function() {
    f = document.SendForm;

});

function sendSmsMsg() {
    if(nowSendMsg){
        alert("메세지 보내는 중입니다.");
        return;
    }
    if(emptyData(f.mt_msg, "메세지를 작성하셔요.")){
        return;
    }
    if(!check_msglen()) return;

    const url = "<c:url value='/SendAction.do/sendMsgSave' />";
    let data = $(f).serialize();

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
        data : data,
        error : ajaxError,
        success : sendMsgSaveResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function sendMsgSaveResult(response) {
    const result = JSON.parse(response);
    const result_001 = result.result_001;

    if(result_001 == -1){
        alert("메세지 발송을 실패 했습니다.");
    } else {
        let showMsg = "";
        if(result_001>0) showMsg = showMsg + "문자 " + result_001 +"건 ";
        showMsg = showMsg + "답장 메세지 발송을 성공했습니다.";
        alert(showMsg);
        window.close();
    }
}

function setMtMsg(value) {
    document.SendForm.mt_msg.value = value;
    check_msglen();
}


function check_msglen() {
    var length = calculate_msglen(document.SendForm.mt_msg.value);
    document.all.nbytes.innerText = length;
    let maxlengh = 80;
    if (length > maxlengh) {
        alert("단문 메시지는 최대 " + maxlengh + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlengh) + "바이트는 자동으로 삭제됩니다.");
        document.SendForm.mt_msg.value = assert_msglen(document.SendForm.mt_msg.value, maxlengh);
        return false;
    }
    return true;
}

function calculate_msglen(message) {
    var nbytes = 0;

    for (i=0; i<message.length; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            nbytes += 2;
        } else if (ch != '\r') {
            nbytes++;
        }
    }
    return nbytes;
}

function assert_msglen(message, maximum) {
    var inc = 0;
    var nbytes = 0;
    var msg = "";
    var msglen = message.length;

    for (i=0; i<msglen; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            inc = 2;
        } else if (ch != '\r') {
            inc = 1;
        }
        if ((nbytes + inc) > maximum) {
            break;
        }
        nbytes += inc;
        msg += ch;
    }
    document.all.nbytes.innerText = nbytes;
    return msg;
}

</script>
</html>