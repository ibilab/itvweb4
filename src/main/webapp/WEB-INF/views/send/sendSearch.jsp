<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<head>
<link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<form name="SendForm" method="post"  action="/SendAction.do" style="margin-bottom: 0">
    <input type="hidden" name="page_num" value="1">
    <input type="hidden" name="start_hh" value="00">
    <input type="hidden" name="start_mi" value="00">
    <input type="hidden" name="end_hh" value="24">
    <input type="hidden" name="end_mi" value="00">
    <div class="cont_top">
        <div class="cont_topl">
            <!-- path -->
            <ul class="path_area">
                <li class="home"><c:out value="${menu_1_name}"/></li>
                <li class="on"><c:out value="${menu_2_name}"/></li>
            </ul>
            <!-- //path -->
            <!-- tit -->
            <div class="tit_box">
                <h2 class="mo01">발송문자게시판</h2>
            </div>
            <!-- //tit -->
        </div>
        <div class="cont_topr">
            <!-- total num -->
            <p class="total_num">발신건수<br />
                <strong id="tot_cnt1" class="sdclr">0</strong></p>
            <!-- //total num -->
        </div>
    </div>
    <!-- 검색 부분  -->
    <div class="top_searchbox">
        <span class="arrow"></span>
        <div class="tsearch select_b">
		<span class="select_box select_t">
			<span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
			<button type="button" class="month_btn">달력</button>
		</span>
        </div>
        <span class="swung">~</span>
        <div class="tsearch select_b">
            <span class="select_box select_t">
                <span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
                <button type="button" class="month_btn">달력</button>
            </span>
        </div>
        <select id="search_field" name="search_field" class="tsearch select_b select_box" style="height: 26px">
            <option value="BOTH">받은사람+내용</option>
            <option value="a.user_id">받은사람</option>
            <option value="a.msg">내용</option>
        </select>
<%--        <div class="tsearch select_b" style="width:140px;">--%>
<%--		<span class="select_box select_t">--%>
<%--			<span class="t_select" id="search_field_name">받은사람+내용</span>--%>
<%--			<button type="button" class="arrow_btn">화살표</button>--%>
<%--		</span>--%>
<%--            <div class="select_m" style="width:120px;">--%>
<%--                <ul class="layer_inner">--%>
<%--                    <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.search_field, 'a.user_id', 'search_field_name', this)">받은사람</a></li>--%>
<%--                    <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.search_field, 'a.msg', 'search_field_name', this)">내용</a></li>--%>
<%--                    <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.search_field, 'BOTH', 'search_field_name', this)">받은사람+내용</a></li>--%>
<%--                </ul>--%>
<%--            </div>--%>
<%--        </div>--%>
        <!-- // -->
        <!--  -->
        <div class="tsearch">
		<span class="select_box select_t">
			<span class="t_search"><input type="text" name="search_kw" id="" onkeydown="onEnter(search);"/></span>
			<button type="button" style="width:62px;" class="btn" onClick="search();"><img src="../img/common/btn/btn_search.gif" width="62" height="24" alt="검색" /></button>
		</span>
        </div>
        <!-- // -->
        <!-- 엑셀다운로드 기능 추가 2015. 2. 10 Lucas Start -->
        <div class="tsearch select_b" id = "excelDownBtn"><span class="btn_btype01"  id="btnExcel"><a href="javascript:excelDown();">엑셀 다운로드</a></span>
        </div>
        <!-- 엑셀다운로드 기능 추가 2015. 2. 10 Lucas END -->


    </div>
    <div class="top_searchbln"></div>
    <div class="table_topbox">
        <div class="tabletb_r">
            <!-- <input type="checkbox" id="" class="checkbox" /> 자동검색 <span>ㅣ</span> -->
            페이지당
            <select id="row_count" name="row_count" class="gray_select select_b">
                <option value="10">10줄</option>
                <option value="15" selected>15줄</option>
                <option value="20">20줄</option>
                <option value="30">30줄</option>
                <option value="50">50줄</option>
                <option value="100">100줄</option>
                <option value="300">300줄</option>
            </select>
            <!-- gray select -->
<%--            <div class="gray_select select_b">--%>
<%--                <button type="button" class="select_t" id="row_count_name">15줄</button>--%>
<%--                <div class="select_m" style="width:62px;">--%>
<%--                    <ul class="layer_inner">--%>
<%--                        <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.row_count, '10', 'row_count_name', this)">10줄</a></li>--%>
<%--                        <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.row_count, '15', 'row_count_name', this)">15줄</a></li>--%>
<%--                        <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.row_count, '20', 'row_count_name', this)">20줄</a></li>--%>
<%--                        <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.row_count, '30', 'row_count_name', this)">30줄</a></li>--%>
<%--                        <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.row_count, '50', 'row_count_name', this)">50줄</a></li>--%>
<%--                        <li><a href="#" onclick="javascript:setSelectBox(document.BoardForm.row_count, '100', 'row_count_name', this)">100줄</a></li>--%>
<%--                    </ul>--%>
<%--                </div>--%>
<%--            </div>--%>
            <!-- //gray select -->
        </div>
    </div>
    <div id="list">
        <div class="table_type01">
            <table cellspacing="0" border="1" summary="보낸 메시지를 담고 있습니다.">
                <caption>리스트</caption>
                <colgroup>
                    <col style="width:45px;" />
                    <col style="width:155px;" />
                    <col style="width:35px;" />
                    <col style="width:auto;" />
                    <col style="width:135px;" />
                </colgroup>
                <thead>
                <tr>
                    <th scope="col">N</th>
                    <th scope="col">아이디</th>
                    <th scope="col" colspan="2" >내용</th>
                    <th scope="col">보낸 시간</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td align="center" colspan="5">검색 하고 있는 중 입니다.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>
<script type="text/javascript" src="../js/common.js"></script>
<script language="JavaScript">
let f;
$(document).ready(function() {
    f = document.SendForm;
    // date picker
    setDatePicker("#start_dt");
    setDatePicker("#end_dt");
    // 초기 날짜 세팅
    $("#start_dt").datepicker("setDate", "0");
    $("#end_dt").datepicker("setDate", "0");

    list();
});

function list() {
    const url = "<c:url value='/SendAction.do/list' />";
    let data = $(f).serialize();

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function search() {
    if(isValid()) {
        f.page_num.value = "1";
        list();
    }
}

function excelDown(){
    f.action = "<c:url value='/SendAction.do/list?excel=Y' />";
    f.target="_self";
    f.submit();
}

function endList(response, status, request){
    $('#list').html(response);
}

function goPage(pageNum){
    f.page_num.value = pageNum;
    list();
}

function isValid(){
    var start_yy	= f.start_dt.value.substring(0,4);
    var start_mm	= f.start_dt.value.substring(5,7);
    var start_dd	= f.start_dt.value.substring(8,10);

    var end_yy		= f.end_dt.value.substring(0,4);
    var end_mm		= f.end_dt.value.substring(5,7);
    var end_dd		= f.end_dt.value.substring(8,10);

    var sDate	= new Date(start_yy, start_mm, start_dd, f.start_hh.value, f.start_mi.value);
    var eDate	= new Date(end_yy, end_mm, end_dd, f.end_hh.value, f.end_mi.value);


    if( eDate < sDate ) {
        alert("종료일자는 시작일자 이후로 설정하세요.");
        f.end_dt.focus();
        return;
    }


    let elapsed = (eDate.valueOf()-sDate.valueOf())/(1000*60*60*24);//기간 (일수)

    if(elapsed > 93){
        alert("기간은 최대 3개월(93일) 이내로 설정해야 합니다.");
        return false;
    }

    return true;
}
</script>
</body>