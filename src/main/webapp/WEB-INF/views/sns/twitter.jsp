<%--
  Created by IntelliJ IDEA.
  User: liam
  Date: 2021-05-13
  Time: 오전 9:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form action="/SnsAction" style="margin-bottom: 0">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}" />">
    <input type="hidden" name="ch_key" value="<c:out value="${operator.ch_key}" />">
    <div class="cont_top">
        <div class="cont_topl">
            <!-- path -->
            <ul class="path_area">
                <li class="home"><c:out value="${menu_1_name}"/></li>
                <li class="on"><c:out value="${menu_2_name}"/></li>
            </ul>
            <!-- //path -->
            <!-- tit -->
            <div class="tit_box">
                <h2 class="sm02">Twitter 관리</h2>
            </div>
            <!-- //tit -->
        </div>
    </div>
    <div class="top_searchbox pr">
        <span class="arrow"></span>

        <!--  -->
        <c:choose>
            <c:when test="${operator.group_level <= 2}">
                <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_select">
					<strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span><span id="sns_ch_name"><c:out value='${operator.ch_name}'/></span>
				</span>
			</span>
                </div>
                <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_select" id="sns_pgm_name"><c:out value='${operator.pgm_name}'/></span>
			</span>
                </div>
            </c:when>
            <c:otherwise>
                <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_select">
					<strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span><span id="sns_ch_name"><c:out value='${operator.ch_name}'/></span>
				</span>
			</span>
                </div>
                <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_select" id="sns_pgm_name"><c:out value='${operator.pgm_name}'/></span>
			</span>
                </div>
            </c:otherwise>
        </c:choose>
        <!-- // -->
    </div>
</form>
<div class="top_searchbln"></div>
<table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
    <tr>
        <td bgcolor="#FFFFFF" >
            <form action="/SnsAction" style="margin-bottom: 0">
                <input type="hidden" name="page_num" value="1">
                <input type="hidden" name="row_count" value="20">
                <input type="hidden" name="ch_key" value="<c:out value="${operator.ch_key}" />">
                <input type="hidden" name="pgm_key" id="pgmKey" value="<c:out value="${operator.pgm_key}" />">
            </form>
            <div id="list">
                <!-- 리스트 -->

                <%--<div class="table_type01 mt10">
                    <table cellspacing="0" border="1" summary="Facebook 관리 리스트에 대한 내용를 담고 있습니다.">
                        <caption>Facebook 관리 리스트</caption>
                        <colgroup>
                            <col style="width:60px;" />
                            <col style="width:auto;" />
                            <col style="width:150px;" />
                            <col style="width:80px;" />
                            <!--
                            <col style="width:120px;" />
                            <col style="width:120px;" />
                            <col style="width:120px;" />
                            <col style="width:120px;" />
                            -->
                            <col style="width:180px;" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">N</th>
                            <th scope="col">프로그램</th>
                            <th scope="col">로그인 ID</th>
                            <th scope="col">유저 ID</th>
                            <!--  <th scope="col">customer_key</th>
                            <th scope="col">customer_secret</th>
                            <th scope="col">access_token</th>
                            <th scope="col">access_token_secret</th>
                            -->
                            <th scope="col">수정일</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="row" items="${list}" varStatus="i">
                            <tr>
                                <td class="ac"><c:out value='${i.count}'/></td>
                                <td><span class="ellips_text"><aof:link href="javascript:view(${row.pgm_key});" value="${row.pgm_name}"/></span></td>
                                <td class="ac"><span class="ellips_text"><c:out value='${row.tw_user_id}'/></span></td>
                                <td class="ac"><span class="ellips_text"><c:out value='${row.tw_user_name}'/></span></td>
                                <!--
                                    <td class="ac"><span class="ellips_text"><c:out value='${row.tw_customer_key}'/></span></td>
                                    <td class="ac"><span class="ellips_text"><c:out value="${row.tw_customer_secret}"/></span></td>
                                    <td class="ac"><span class="ellips_text"><c:out value="${row.tw_access_token}"/></span></td>
                                    <td class="ac"><span class="ellips_text"><c:out value="${row.tw_access_token_secret}"/></span></td>
				                 -->
                                <td class="ac"><c:out value="${row.last_update_date}"/></td>
                            </tr>
                        </c:forEach>
                        <c:if test="${empty list}">
                            <tr>
                                <td colspan="5" align="center">등록된 Twitter 정보가 없습니다.</td>
                            </tr>
                        </c:if>
                        </tbody>
                    </table>
                </div>--%>
            </div>
            <br>
            <form action="/SnsAction" id="writeForm">
                <input type="hidden" name="cmd" value="">
                <input type="hidden" name="pgm_key" value="">
                <div class="box_type01 mt30">
                    <!-- table_type03 -->
                    <div class="table_type03">
                        <table cellspacing="0" border="1" summary="twitter 관리 설정에 대한 내용를 담고 있습니다.">
                            <caption>twitter 관리 설정</caption>
                            <colgroup>
                                <col style="width:160px;" />
                                <col style="width:auto;" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>로그인 이메일</th>
                                <td>
                                    <!--  -->
                                    <div class="set_blk select_b">
										<span class="select_box">
											<input type="text" name="twUserId" id="twitterId" style="width:250px;" value=""/>
										</span>
                                    </div>
                                    <span id="btnTwLogin" class="btn_btype01"><a href="javascript:void(0)"><span>로그인</span></a></span>
                                    <!-- // -->
                                </td>
                            </tr>
                            <tr>
                                <th>유저 ID</th>
                                <td>
                                    <!--  -->
                                    <div class="set_blk select_b">
										<span class="select_box">
											<input type="text" name="twUserName" id="twitterUserName" style="width:250px;" value=""/>
										</span>
                                    </div>
                                    <span id="ninfo">예: @MnStudio</span>
                                    <!-- // -->
                                </td>
                            </tr>
                            <tr>
                                <th>customer_key</th>
                                <td>
                                    <!--  -->
                                    <div class="set_blk select_b">
										<span class="select_box">
											<input type="text" name="twCustomerKey" id="twitterCustomerKey" style="width:560px;" value="" readonly/>
										</span>
                                    </div>
                                    <!-- // -->
                                </td>
                            </tr>
                            <tr>
                                <th>customer_secret</th>
                                <td>
                                    <!--  -->
                                    <div class="set_blk select_b">
										<span class="select_box">
											<input type="text" name="twCustomerSecret" id="twitterCustomerSecret" style="width:560px;" value="" readonly/>
										</span>
                                    </div>
                                    <!-- // -->
                                </td>
                            </tr>
                            <tr>
                                <th>access_token</th>
                                <td>
                                    <!--  -->
                                    <div class="set_blk select_b">
										<span class="select_box">
											<input type="text" name="twAccessToken" id="accessToken" style="width:560px;" value="" readonly/>
										</span>
                                    </div>
                                    <!-- // -->
                                </td>
                            </tr>
                            <tr>
                                <th>access_token_secret</th>
                                <td>
                                    <!--  -->
                                    <div class="set_blk select_b">
										<span class="select_box">
											<input type="text" name="twAccessTokenSecret" id="accessTokenSecret" style="width:560px;" value="" readonly/>
										</span>
                                    </div>
                                    <!-- // -->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- //table_type03 -->
                </div>
                <div class="box_type01_bline"></div>
                <div class="btn_box">
                    <div class="btn_lf" id="leftButton">
                        <span class="btn_btype01" id="btnInsert"><a href="javascript:void(0);">신규등록</a></span>
                    </div>
                    <div class="btn_rf" style="display: none;" id="rightButton">
                        <span class="btn_btype02" id="btnUpdate"><a href="javascript:void(0);">수정</a></span>
                        <span class="btn_btype02" id="btnDel"><a href="javascript:void(0);">삭제</a></span>
                        <!-- <span class="btn_btype01"><a href="javascript:view();">입력모드</a></span> -->
                    </div>
                </div>
            </form>
        </td>
    </tr>
</table>
<script type="text/html" id="listTemplate">
    <tr data-idx="{pgmKey}" data-program="{pgmName}" data-id="{userId}" data-name="{userName}">
        <td class="ac">{count}</td>
        <td><span class="ellips_text">{pgmName}</span></td>
        <td class="ac"><span class="ellips_text">{userId}</span></td>
        <td class="ac"><span class="ellips_text">{userName}</span></td>
        <td class="ac">{updateDate}</td>
    </tr>
</script>
<script type="text/javascript">
    $.ajaxSetup({})
    const twitter = {
        get : function(){
            $.ajax({
                url : '/SnsAction.do/twitter'
                ,type : 'get'
                ,dataType : 'json'
            }).done(function(response){
                if(response.result){
                    if(response.twitter !== null) {
                        const data = response.twitter;
                        $('#twitterId').val(data.twUserId);
                        $('#twitterUserName').val(data.twUserName);
                        $('#twitterCustomerKey').val(data.twCustomerKey);
                        $('#twitterCustomerSecret').val(data.twCustomerSecret);
                        $('#accessToken').val(data.twAccessToken);
                        $('#accessTokenSecret').val(data.twAccessTokenSecret);
                        $('#rightButton').show();
                        $('#leftButton').hide();
                    }
                }
            }).fail(ajaxError);
        }, remove : function (){
            if(confirm('삭제하시겠습니까?')){
                $.ajax({
                    url : '/SnsAction.do/twitter/'+ $('#pgmKey').val()
                    ,type : 'put'
                    ,dataType : 'json'
                }).done(function (response) {
                    alert(response.message);
                    $('#twitterId').val('');
                    $('#twitterUserName').val('');
                    $('#twitterCustomerKey').val('');
                    $('#twitterCustomerSecret').val('');
                    $('#accessToken').val('');
                    $('#accessTokenSecret').val('');
                    $('#rightButton').hide();
                    $('#leftButton').show();
                }).fail(ajaxError);
            }
        }, valid : function () {
            if ( $('#twitterUserName').val() === '' ) {
                alert("유저 ID를 입력해 주세요. 예: @MnStudio" );
                return false;
            }
            const pgmKey = $('#pgmKey').val();
            if(pgmKey === '0') {
                alert('프로그램을 선택해 주세요.');
                return false;
            }
            return true;
        }, save : function(command){
            if(this.valid()){
                let method = 'post';
                if(command === 'modify'){
                    method = 'put'
                }
                $.ajax({
                    url : '/SnsAction.do/twitter'
                    ,type : method
                    ,dataType : 'json'
                    ,data : $('#writeForm').serialize()
                }).done(function(response){
                    alert(response.message);
                    if(response.result === true) {
                        $('#rightButton').show();
                        $('#leftButton').hide();
                    }
                });
            }
        }, login : function (){
            $.ajax({
                url : '/SnsAction.do/twitter/url'
                ,type : 'get'
                ,dataType : 'json'
            }).done(function(response){
                window.open(response.url, '_popupImg','status=yes, scrollbars=yes,resizable=yes,width=620,height=450');
            });
        }
    };
    $(document).ready(function(){
        ajaxOnComplete();
        twitter.get();
        $('#btnInsert').on('click', function(){
            twitter.save('add');
        });

        $('#btnUpdate').on('click', function(){
            twitter.save('modify');
        });

        $('#btnDel').on('click', function(){
            twitter.remove();
        });

        $('#btnTwLogin').on('click', function(){
            twitter.login();
        });
    });

</script>