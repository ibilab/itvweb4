<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01 mt00">
    <table cellspacing="0" border="1" summary="유튜브 설정에 대한 내용를 담고 있습니다.">
        <caption>신규 가입자 답장 리스트</caption>
        <colgroup>
            <col style="width:40px;" />
            <col style="width:50px;" />
            <col style="width:100px;" />
            <col style="width:140px;" />
            <col style="width:140px;" />
            <col style="width:50px;" />
            <col style="width:140px;" />
            <col style="width:140px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">N</th>
            <th scope="col">Live Key</th>
            <th scope="col">Video Code</th>
            <th scope="col">시작시간</th>
            <th scope="col">종료시간</th>
            <th scope="col">상태</th>
            <th scope="col">마지막 수신 시간</th>
            <th scope="col">등록시간</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${YoutubeForm.list}" varStatus="i">
            <tr>
                <td class="ac"><c:out value='${YoutubeForm.start_index-i.index}' /></td>
                    <%-- <td class="ac"><aof:link href="javascript:view('${row.live_key}');" value='${row.live_key}'/></td> --%>
                <td class="ac"><c:out value='${row.live_key}'/></td>
                <td class="ac"><c:out value='${row.video_code}' /></td>
                <td class="ac"><c:out value='${row.live_stm}'/></td>
                <td class="ac"><c:out value='${row.live_etm}'/></td>
                <td class="ac"><c:out value='${row.live_status_name}'/></td>
                <td class="ac"><c:out value='${row.msg_ltm}'/></td>
                <td class="ac"><c:out value='${row.live_regdate}'/></td>
            </tr>
        </c:forEach>
        <c:if test="${empty YoutubeForm.list}">
            <tr>
                <td class="ac" colspan="8" align="center">등록된 유튜브 생방송이 없습니다.</td>
            </tr>
        </c:if>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="40" align="center"><c:out value="${YoutubeForm.pageNavigator}" escapeXml="false"/></td>
        </tr>
    </table>
</div>

<script type="text/javascript">
$(function() {

});

</script>
</body>
</html>