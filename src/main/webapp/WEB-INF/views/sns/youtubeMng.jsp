<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="mo03">생방송 설정</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
    <tr>
        <td bgcolor="#FFFFFF">
            <form name="SnsForm" action="/SnsAction">
                <input type="hidden" name="page_num" value="1">
                <input type="hidden" name="row_count" value="15">
            </form>
            <div id="list">
                <!-- 목록 -->
            </div>
            <br>
            <iframe name="ifr1" id="ifr1" src="about:blank" width="100%" height="326" border="0" frameborder="0" scrolling="no" marginwidth="1" marginheight="0"> </iframe>
        </td>
    </tr>
</table>
<script type="text/javascript" src="../js/common.js"></script>
<script language="Javascript">
    $(document).ready(function() {
        list();
    });

    function list(){
        let url = "<c:url value='/SnsAction.do/youtubeList' />";
        let data = $(document.SnsForm).serialize().replace(/%/g,'%25');

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : endList,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
    function endList(response, status, request){
        $('#list').html(response);

    }
    function goPage(pageNum){
        document.SnsF.page_num.value = pageNum;
        list();
    }

</script>
</body>
</html>