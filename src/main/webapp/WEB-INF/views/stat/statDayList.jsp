<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01 mt20">
    <table cellspacing="0" border="1" summary="일별통계 리스트에 대한 내용를 담고 있습니다.">
        <caption>일별통계 리스트</caption>
        <colgroup>
            <col style="width:auto;" />
            <col style="width:13%;" />
            <col style="width:13%;" />
            <col style="width:13%;" />
            <col style="width:13%;" />
            <col style="width:13%;" />
            <col style="width:13%;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">날짜</th>
            <th scope="col">수신수</th>
            <th scope="col">전송수</th>
            <th scope="col">1인당전송</th>
            <th scope="col">신규가입</th>
            <th scope="col">기존회원</th>
            <th scope="col">이용자수</th>
        </tr>
        </thead>
        <c:if test="${!empty list}">
            <tbody>
            <c:forEach var="row" items="${list}" varStatus="i">
                <tr>
                    <td class="ac"><c:out value="${row.reg_day}"/></td>
                    <td class="ac"><c:out value="${row.mo_cnt}"/></td>
                    <td class="ac"><c:out value="${row.mt_cnt}"/></td>
                    <td class="ac"><c:out value="${row.mo_avg_cnt}"/></td>
                    <td class="ac"><c:out value="${row.new_mem_cnt}"/></td>
                    <td class="ac"><c:out value="${row.old_mem_cnt}"/></td>
                    <td class="ac"><c:out value="${row.mo_dis_cnt}"/></td>
                </tr>
            </c:forEach>
            </tbody>
            <tfoot>
            <tr>
                <th>총합계</th>
                <th><c:out value="${totallist[0].mo_cnt}"/></th>
                <th><c:out value="${totallist[0].mt_cnt}"/></th>
                <th><c:out value="${totallist[0].mo_avg_cnt}"/></th>
                <th><c:out value="${totallist[0].new_mem_cnt}"/></th>
                <th><c:out value="${totallist[0].old_mem_cnt}"/></th>
                <th><c:out value="${totallist[0].mo_dis_cnt}"/></th>
            </tr>
            </tfoot>
        </c:if>
        <c:if test="${empty list}">
            <td align="center" colspan="7">검색 결과가 없습니다.</td>
        </c:if>
    </table>
</div>
</body>
</html>