<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="ss01">일별통계</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<form name="StatForm" action="/StatAction" style="margin-bottom: 0">
<div class="top_searchbox pr">
    <span class="arrow"></span>
    <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_select"><strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span></span>
            <select name="ch_key" class="select_m" onchange="getPgms(this.value)" style="width:170px;">
                <c:forEach var="row" items="${Channels}" varStatus="i">
                    <option value="<c:out value='${row.ch_key}'/>" <c:if test="${operator.ch_key == row.ch_key}">selected</c:if> ><c:out value="${row.ch_name }"/></option>
                </c:forEach>
            </select>
        </span>
    </div>
    <div class="tsearch select_b">
        <span class="select_box select_t">
            <select name="pgm_key" id="search_pgm" class="select_m" style="width:170px;">
                <c:forEach var="row" items="${pgmTopList}" varStatus="i">
                    <option value="<c:out value='${row.pgm_key}'/>" <c:if test="${operator.pgm_key == row.pgm_key}">selected</c:if> ><c:out value="${row.pgm_name }"/></option>
                </c:forEach>
            </select>
        </span>
    </div>


    <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
            <button type="button" class="month_btn">달력</button>
        </span>
    </div>
    <span class="swung">~</span>
    <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
            <button type="button" class="month_btn">달력</button>
        </span>
    </div>
    <div class="tsearch">
        <a href="javascript:search();"><img src="../../img/common/btn/btn_search01.gif" width="55" height="26" alt="검색" /></a>
    </div>
</div>
</form>
<div class="box_type01" id="chart" style="height:300px;"></div>
<div class="box_type01_bline"></div>
<div id="list">
    <div class="table_type01 mt20">
        <table cellspacing="0" border="1" summary="일별통계 리스트에 대한 내용를 담고 있습니다.">
            <caption>일별통계 리스트</caption>
            <colgroup>
                <col style="width:auto;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
            </colgroup>
            <thead>
            <tr>
                <th scope="col">날짜</th>
                <th scope="col">수신수</th>
                <th scope="col">전송수</th>
                <th scope="col">1인당전송</th>
                <th scope="col">신규가입</th>
                <th scope="col">기존회원</th>
                <th scope="col">이용자수</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script type="text/javascript" src="../js/common.js"></script>
<script language="Javascript">
$(function(){
    $("#start_dt").datepicker({
        dateFormat: "yy-mm-dd",
        altField: '#start_dt',
        showOtherMonths: true,
        selectOtherMonths: true,
        nextText: '다음 달', // next 아이콘의 툴팁.
        prevText: '이전 달', // prev 아이콘의 툴팁.
        monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayNamesMin: ["일", "월", "화", "수", "목", "금", "토",]
    });
    $("#end_dt").datepicker({
        dateFormat: "yy-mm-dd",
        altField: '#end_dt',
        showOtherMonths: true,
        selectOtherMonths: true,
        nextText: '다음 달', // next 아이콘의 툴팁.
        prevText: '이전 달', // prev 아이콘의 툴팁.
        monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayNamesMin: ["일", "월", "화", "수", "목", "금", "토",]
    });
    // 초기 날짜 세팅
    $( "#start_dt" ).datepicker( "setDate", "-7" );
    $( "#end_dt" ).datepicker( "setDate", "0" );

    list();
    $('.t_calendar').on('click', function () {
        $(this).toggleClass('on');
        $("#" + $(this).find('input').attr("id")).datepicker( "show" );
        if($(this).find('input').attr("id")) $("#ui-datepicker-div").show();
        $("#ui-datepicker-div").mouseleave(function() {
            $(this).hide();
        });
        return false;
    });
    $('.month_btn').on('click', function(){
        $(this).toggleClass('on');
        $(this).prev().find('input').datepicker('show');
        console.log($(this).prev().find('input'));
        if($(this).prev().find('input')) $("#ui-datepicker-div").show();
        $("#ui-datepicker-div").mouseleave(function() {
            $(this).hide();
        });
        return false;
    });
});
function list(){
    let url = "<c:url value='/StatAction.do/statDay' />";
    let data = $(document.StatForm).serialize().replace(/%/g,'%25');

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').html(response);
}

function search(){
    if(isValid()){
        list();
    }
}

function isValid() {
    // TODO 급해서...
    return true;
}

function getPgms(ch_key){
    let url = "<c:url value='/PgmAction.do/withTime' />";
    let data = "ch_key=" + ch_key;
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : makeOptions
    });
}

function makeOptions(json) {
    const obj = JSON.parse(json);
    let pgm_key = obj[0].pgm_key;

    $("#search_pgm").empty();
    for (let i = 0; i < obj.length; i++) {
        let option = $("<option value='" + obj[i].pgm_key + "'>" + obj[i].pgm_name + "</option>");
        $("#search_pgm").append(option);
    }
}

</script>
</body>
</html>