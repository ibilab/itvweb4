<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="ss01">프로그램통계</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<form name="StatForm" action="/StatAction" style="margin-bottom: 0">
<div class="top_searchbox pr">
    <span class="arrow"></span>
    <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_select"><strong class="pntclr">채널</strong> <span class="pntclr">ㅣ</span></span>
            <select name="ch_key" class="select_m" style="width:170px;">
                <c:forEach var="row" items="${Channels}" varStatus="i">
                    <option value="<c:out value='${row.ch_key}'/>" <c:if test="${operator.ch_key == row.ch_key}">selected</c:if> ><c:out value="${row.ch_name }"/></option>
                </c:forEach>
            </select>
        </span>
    </div>

    <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
            <button type="button" class="month_btn">달력</button>
        </span>
    </div>
    <span class="swung">~</span>
    <div class="tsearch select_b">
        <span class="select_box select_t">
            <span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
            <button type="button" class="month_btn">달력</button>
        </span>
    </div>
    <div class="tsearch">
        <a href="javascript:search();"><img src="../../img/common/btn/btn_search01.gif" width="55" height="26" alt="검색" /></a>
    </div>
</div>
</form>
<div class="box_type01" id="chart" style="height:300px;"></div>
<div class="box_type01_bline"></div>
<div id="list">
    <div class="table_type01 mt20">
        <table cellspacing="0" border="1" summary="일별통계 리스트에 대한 내용를 담고 있습니다.">
            <caption>프로그램통계 리스트</caption>
            <colgroup>
                <col style="width:auto;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
                <col style="width:13%;" />
            </colgroup>
            <thead>
            <tr>
                <th scope="col">프로그램</th>
                <th scope="col">수신수</th>
                <th scope="col">전송수</th>
                <th scope="col">1인당전송</th>
                <th scope="col">신규가입</th>
                <th scope="col">기존회원</th>
                <th scope="col">이용자수</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script type="text/javascript" src="../js/common.js"></script>
<script language="Javascript">
$(document).ready(function() {
    // date picker
    setDatePicker("#start_dt");
    setDatePicker("#end_dt");
    // 초기 날짜 세팅
    $( "#start_dt" ).datepicker( "setDate", "-7" );
    $( "#end_dt" ).datepicker( "setDate", "0" );

    list();
});

function list(){
    let url = "<c:url value='/StatAction.do/statPgm' />";
    let data = $(document.StatForm).serialize().replace(/%/g,'%25');

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').html(response);
}

function search(){
    if(isValid()){
        list();
    }
}

function isValid() {
    // TODO 급해서...
    return true;
}

</script>
</body>
</html>