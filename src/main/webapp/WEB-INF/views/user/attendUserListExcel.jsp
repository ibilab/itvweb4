<%--	
  - 작성자: Lucas
  - 일자: 2015. 10. 06
  - 저작권 표시:
  - @(#)
  - 설명:  참여자 검색 결과 엑셀.
  --%>
<%@ page language="java" contentType="application/vnd.ms-excel; charset=EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<% 
        Date now = new Date(); 	
        SimpleDateFormat ymd = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String  FName = "UserList_" + ymd.format(now) ;
		response.setHeader("Content-Disposition", "attachment;filename="+java.net.URLEncoder.encode(FName, "UTF-8")+".xls"); 
%>

<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv=Content-Type content="application/vnd.ms-excel; charset=EUC-KR">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="searchList.files/filelist.xml">
<link rel=Edit-Time-Data href="searchList.files/editdata.mso">
<link rel=OLE-Object-Data href="searchList.files/oledata.mso">
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:돋움, monospace;
	mso-font-charset:129;
	border:none;
	mso-protection:locked visible;
	mso-style-name:표준;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:돋움, monospace;
	mso-font-charset:129;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	text-align:center;}
.xl25
	{mso-style-parent:style0;
	mso-number-format:"General Date";}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:돋움, monospace;
	mso-font-charset:129;
	mso-char-type:none;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Sheet1</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Selected/>
     <x:Panes>
      <x:Pane>
       <x:Number>3</x:Number>
       <x:ActiveRow>11</x:ActiveRow>
       <x:ActiveCol>2</x:ActiveCol>
      </x:Pane>
     </x:Panes>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet2</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet3</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>12945</x:WindowHeight>
  <x:WindowWidth>16560</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>90</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 width=769 style='border-collapse:collapse;table-layout:fixed;width:578pt'>
<col width=50 style='mso-width-source:userset;mso-width-alt:1592;width:50pt'>
<col width=200 style='mso-width-source:userset;mso-width-alt:4608;width:200pt'>
<col width=397 style='mso-width-source:userset;mso-width-alt:11292;width:298pt'>
<col width=154 style='mso-width-source:userset;mso-width-alt:4380;width:116pt'>
<tr height=18 style='height:13.5pt'>
	<td height=18 class=xl24 width=56 style='height:13.5pt;width:42pt'>NO</td>
	<td class=xl24 width=150 style='width:150pt'>매체</td>
	<td class=xl24 width=150 style='width:150pt'>LV</td>
	<td class=xl24 width=150 style='width:150pt'>ID</td>
	<td class=xl24 width=150 style='width:150pt'>닉네임</td>
	<td class=xl24 width=150 style='width:150pt'>User 정보</td>
	
	<td class=xl24 width=150 style='width:150pt'>금일</td>
	<td class=xl24 width=150 style='width:150pt'>금월</td>
	<td class=xl24 width=150 style='width:150pt'>누적</td>
	<td class=xl24 width=150 style='width:150pt'>가입일</td>
	<td class=xl24 width=150 style='width:150pt'>최종참여일</td>
</tr>
<c:forEach var="row" items="${user.list}" varStatus="i">
<tr height=18 style='height:13.5pt'>
	<td height=18 align=right style='height:13.5pt' x:num><c:out value='${(user.page_num-1)*user.row_count+i.count}'/></td>
	<td class="ac">
		${row.codeName}
	</td>
	<td><c:out value='${row.userGrade}' /></td>
	<td>
	  	<c:if test="${row.msgCom != 007}">
			<c:out value='${row.userId}' />
		</c:if>
		<c:if test="${row.msgCom == 007}">
			<c:out value='${row.userNick}' />
		</c:if>
	</td>
	<td>
		<c:if test="${row.msgCom != 007}">
			<c:out value='${row.userName}' />
		</c:if>
		<c:if test="${row.msgCom == 007}">
			<c:out value='${row.userName}' />
		</c:if>
	</td>
	<td><c:out value='${row.userInfo}' /></td>
	  
	<td><c:out value='${row.moDay}' /></td>
	<td><c:out value='${row.moMonth}' /></td>
	<td><c:out value='${row.moTotal}' /></td>
	<td><c:out value='${row.userRegDt}' /></td>
	<td><c:out value='${row.lastloginDt}' /></td>
</tr>
</c:forEach>
<![if supportMisalignedColumns]>
<tr height=0 style='display:none'>
	<td width=56 style='width:42pt'></td>
	<td width=162 style='width:122pt'></td>
	<td width=397 style='width:298pt'></td>
	<td width=154 style='width:116pt'></td>
</tr>
<![endif]>
</table>

</body>

</html>
