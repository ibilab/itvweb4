<%--
  Created by IntelliJ IDEA.
  User: liam
  Date: 2021-05-14
  Time: 오후 3:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css">
<!-- Tabs -->
<form action="/UserMngAction" name="UserForm">
    <input type="hidden" name="page_num" id="pageNum" value="1">
    <input type="hidden" name="row_count" id="rowCount" value="15">
    <input type="hidden" name="excel" value="N">
    <input type="hidden" name="cmd" value="">
    <input type="hidden" name="showMsg" value="0"/>
    <!-- Navigation : Start -->
    <div class="cont_top">
        <div class="cont_topl">
            <!-- path -->
            <ul class="path_area">
                <li class="home"></li>
                <li class="on"></li>
            </ul>
            <div class="tit_box">
                <h2 class="sm01">블랙리스트 관리 </h2>
            </div>
        </div>
    </div>
    <!-- Navigation : End -->


    <!-- 검색 부분  -->
    <div class="top_searchbox">
        <span class="arrow"></span>
<%--        <div class="tsearch select_b" id="searchMsgComDiv">--%>
<%--		<span class="select_box select_t">매체구분:--%>
<%--            <select style="border: 0px;min-width: 60px;" name="searchMsgCom" id="searchMsgCom">--%>
<%--                <option value="ALL">전체</option>--%>
<%--                <c:forEach items="${sns_lists}" var="row" varStatus="status">--%>
<%--                    <option value="<c:out value='${row.code}'/>"><c:out value='${row.codeNm}'/></option>--%>
<%--                </c:forEach>--%>
<%--            </select>--%>
<%--		</span>--%>
<%--        </div>--%>

        <div class="tsearch select_b">
            <span class="select_box select_t">
                <span class="t_select" id="search_period_name">
                    <select style="border: 0px;width:120px" name="searchPeriodName" id="searchPeriodName">
                        <option value="DAY">기간</option>
                        <option value="ALL">전체</option>
                    </select>
                </span>
            </span>
        </div>

        <div id ="dateSearchField">
            <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_select" id="search_login_name">
                   <select style="width: 120px;border: 0px;" name="searchLogin">
                       <option value="LAST">최종참여일</option>
                       <option value="REG">가입일</option>
                   </select>
                </span>
			</span>
            </div>
            <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar" id="datePicker"><strong>시작</strong> ㅣ<input type="text" name="startDate" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
				<button type="button" class="month_btn">달력</button>
			</span>
            </div>
            <span class="swung">~</span>
            <div class="tsearch select_b">
			<span class="select_box select_t">
				<span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="endDate" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly> </span>
				<button type="button" class="month_btn">달력</button>
			</span>
            </div>
        </div>
        <div class="tsearch select_b">
            <span class="select_box select_t">
                <span class="t_select">
                    <select style="border: 0px;width:120px">
                        <option value="USERID">참여자 번호</option>
                    </select>
                </span>
            </span>
        </div>
        <div class="tsearch">
		<span class="select_box select_t">
			<span class="t_search"><input type="text" name="searchKeyword" id="" onkeydown="onEnter(search);"/></span>
			<button type="button" style="width:62px;" class="btn" id="searchButton"><img src="../img/common/btn/btn_search.gif" width="62" height="24" alt="검색" /></button>
		</span>
        </div>

        <div class="tsearch select_b">
            <span class="btn_btype01"  id="btnExcel"><a href="javascript:excelDown();">엑셀 다운로드</a></span>
        </div>
    </div>
</form>

<!-- List 영역 : Start -->
<table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
    <tr>
        <td bgcolor="#FFFFFF" >
            <div id="list">
                <div class="table_type01 mt10">
                    <table cellspacing="0" border="1" summary="계정 리스트에 대한 내용를 담고 있습니다.">
                        <caption>계정 리스트</caption>
                        <colgroup>
                            <col style="width:60px;" />
                            <col style="width:120px;" />
                            <col style="width:120px;" />
                            <col style="width:120px;" />

                            <col style="width:auto;" />
                            <col style="width:140px" />

                            <col style="width:140px" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">번호</th>
                            <th scope="col">LV</th>
                            <th scope="col">ID</th>
                            <th scope="col">닉네임</th>

                            <th scope="col">User 정보</th>
                            <th scope="col">가입일</th>

                            <th scope="col">최종 참여일</th>
                        </tr>
                        </thead>
                        <tbody id="listTbody">

                        <tr><td colspan="7"   class="ac">등록된 계정이 없습니다.</td></tr>

                        </tbody>
                    </table>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="40" align="center" id="paging"></td>
                    </tr>
                </table>
            </div>
            <iframe name="ifr1" src="" width="100%" height="336" border="0" frameborder="0" scrolling="no" marginwidth="1" marginheight="0"> </iframe>
        </td>
    </tr>
</table>
<!-- List 영역 : Start -->
<script type="text/html" id="listTemplate">
    <tr>
        <td class="ac">{index}</td>
        <td class="ac">{userGrade}</td>
        <td class="ac">
            <a href="javascript:{isGrade}">{userId}</a>
        </td>
        <td class="ac">
            {userName}
        </td>
        <td class="ac">{userInfo}</td>
        <td>{userRegDate}</td>
        <td class="ac">{lastLoginDate}</td>
    </tr>
</script>
<script>
    const attendUser = {
        getList : function (){
            const data = $("form[name=UserForm]").serialize();
            $.ajax({
                url: '/UserMngAction.do/users',
                type : "get",
                dataType : "json",
                data : data,
                beforeSend : ajaxOnCreate,
                complete : ajaxOnComplete
            }).done(function(response){
                $('#listTbody').empty();
                $('#paging').empty();
                let no = ($('#pageNum').val() - 1) * $('#rowCount').val();
                if(response.user.total_row_count > 0) {
                    $(response.user.list).each(function(index){
                        let html = $('#listTemplate').html();
                        const showIndex = (response.user.total_row_count - no - 1)  - index + 1;
                        html = html.split("{index}").join(showIndex);
                        html = html.split("{userGrade}").join(this.userGrade);
                        if(this.userGrade != '0'){
                            html = html.split("{isGrade}").join('popUserInfo(\''+ this.userId + '\', \''+ this.msgCom +'\', \''+this.userName+'\', \''+this.userInfo+'\')');
                        } else {
                            html = html.split("{isGrade}").join(`void(0)`);
                        }
                        html = html.split("{userId}").join(this.userId );
                        html = html.split("{userName}").join(this.userName);
                        html = html.split("{userInfo}").join(this.userInfo);

                        // html = html.split("{moDay}").join(this.moDay);
                        // html = html.split("{moMonth}").join(this.moMonth);
                        // html = html.split("{moTotal}").join(this.moTotal);
                        html = html.split("{userRegDate}").join(this.userRegDt);
                        html = html.split("{lastLoginDate}").join(this.lastloginDt);
                        $('#listTbody').append(html);
                    });

                } else {
                    let tr = $('<tr/>');
                    const td = $('<td/>', {colspan : 7, 'class' : 'ac', text : '등록된 계정이 없습니다.'});
                    tr.append(td);
                    $('#listTbody').append(tr);
                }
                $('#paging').append(response.user.pageNavigator);
            }).fail(ajaxError);
        }
    }
    var f;
    $(document).ready(function(){
        f = document.UserForm;

        $( "#start_dt" ).datepicker({
            dateFormat:  "yy-mm-dd",
            altField:   "#start_dt",
            showOtherMonths: true,
            selectOtherMonths: true,
            nextText: '다음 달', // next 아이콘의 툴팁.
            prevText: '이전 달', // prev 아이콘의 툴팁.
            monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
            dayNamesMin:   ["일", "월", "화", "수", "목", "금", "토",]
        });
        $( "#end_dt" ).datepicker({
            dateFormat:  "yy-mm-dd",
            altField:   "#end_dt",
            showOtherMonths: true,
            selectOtherMonths: true,
            nextText: '다음 달', // next 아이콘의 툴팁.
            prevText: '이전 달', // prev 아이콘의 툴팁.
            monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
            dayNamesMin:   ["일", "월", "화", "수", "목", "금", "토",]
        });
        $( "#start_dt" ).datepicker( "setDate", "0" );
        $( "#end_dt" ).datepicker( "setDate", "0" );
        ajaxOnComplete();

        attendUser.getList();
        // list();
        $('#searchButton').on('click', function (){
            attendUser.getList();
        });

        $('#searchPeriodName').on('change', function () {
            val = $(this).val();
            if(val == "ALL"){
                $("#dateSearchField").hide();
            } else if (val == "DAY"){
                $("#dateSearchField").show();
            }
        });
    });


    function list(){
        <%--var url = "<c:url value='/UserMngAction.do' />";--%>
        var data = "cmd=UserMngList&"+$("form[name=UserForm]").serialize().replace(/%/g,'%25');
        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            data : data,
            error : ajaxError,
            success : endList,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
        if(f.search_msg_com.value == "001"){
            $('#btnSendAllMsg').show();
            // $('#btnSendAllMsg').hide();
        }else{
            $('#btnSendAllMsg').hide();
        }
        // 	$('#btnExcel').hide();
    }

    function endList(response, status, request){
        $('#list').html(response);
    }

    function isValid()
    {
        var start_yy	= f.startDate.value.substring(0,4);
        var start_mm	= f.startDate.value.substring(5,7);
        var start_dd	= f.startDate.value.substring(8,10);

        var end_yy		= f.endDate.value.substring(0,4);
        var end_mm		= f.endDate.value.substring(5,7);
        var end_dd		= f.endDate.value.substring(8,10);

        var sDate	= new Date(start_yy, start_mm, start_dd, f.start_hh.value, f.start_mi.value);
        var eDate	= new Date(end_yy, end_mm, end_dd, f.end_hh.value, f.end_mi.value);

        return true;
    }



    function search() {
        if(isValid()){
            f.excel.value = "N";
            f.page_num.value = "1";
            attendUser.getList();
        }
    }

    function goPage(pageNum){
        document.UserForm.page_num.value = pageNum;
        attendUser.getList();
    }


    function setSelectBox(val_field, val, text_field, obj) {

        console.log("val_field : " + val_field);
        console.log("val : " + val);
        console.log("text_field : " + text_field);
        console.log("obj : " + obj);


        // 전체를 선택한 경우, 날짜 검색 필드 제거
        if(text_field == "search_period_name" && val == "ALL"){
            console.log("* 검색 구간 선택에서 전체인경우");
            $("#dateSearchField").hide();
        } else if (text_field == "search_period_name" && val == "DAY"){
            console.log("* 검색 구간 선택에서 기간인경우");
            $("#dateSearchField").show();
        }

        val_field.value = val;
        $("#" + text_field).html(obj.outerText);
        $(obj).parent().parent().parent().toggle();
    }

    function excelDown() {
        let f = document.UserForm;
        f.action = "/UserMngAction.do/users/excel";
        f.method = "get";
        f.cmd.value = "UserMngList";
        f.excel.value = "Y";
        f.target="_self";
        f.submit();
    }

    var _userMsg=null;

    // 전체 답장 적용. 2015.11.23 Lucas
    function sendAllMsg(){
        let f = document.UserForm;
        _userMsg =window.open('/UserMngAction.do/users/message','sendMsgPop','status=yes, scrollbars=yes,resizable=yes,width=620,height=300');
        _userMsg.focus();
        f.target="sendMsgPop";
        f.action = '/UserMngAction.do/users/message';
        <%--f.action = "<c:url value='/UserMngAction.do?cmd=sendAllMsg' />";--%>
        // f.submit();
    }

</script>