<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<form name="BoardForm" action="/BoardAction.do" style="margin-bottom: 0">
    <input type="hidden" name="pgmKey" id="pgmKey" value=""/>
    <input type="hidden" name="totalCount" id="totalCount" value=""/>
        <!-- Lucas.2015.2.25 - 참여자 관리 페이지에서 전체답장, 선택 답장 적용. : START -->
        <input type="hidden" name="phone_num" value=""/>
        <%--            <c:if test="${row.msg_username eq null}"><input type="hidden" name="phone_num" value="<c:out value="${row.user_phone }"/>"/></c:if>--%>
        <!-- Lucas.2015.2.25 - 참여자 관리 페이지에서 전체답장, 선택 답장 적용. : END -->
        <input type="hidden" name="board_key" value=""/>
        <input type="hidden" name="msg_date" value=""/>
        <input type="hidden" name="msg_com" value=""/>
        <input type="hidden" name="msg_userid" value=""/>
        <input type="hidden" name="comment_id" value=""/>
        <input type="hidden" name="table_name" value=""/>

    <table width="560" border="0" cellspacing="7" cellpadding="0">
        <tr>
            <td>
                <table width="580" border="0" cellspacing="0" cellpadding="0">
                    <tr height="10">

                    </tr>

                    <tr>
                        <td align="center" >
                            <table width="550" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><img src="/img/popup/popup_right.gif" width="16" height="15" align="absmiddle"> <strong>답장 메시지작성 </strong> <span id="operName"></span></td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td class="box_table3">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td valign="top"><table width="385" border="0" cellspacing="1" cellpadding="4">
                                                    <tr>
                                                        <td width="80" class="bg_gray2">프로그램명</td>
                                                        <td class="box_bottom1 text_blue" id="programName"></td>
                                                    </tr>

                                                    <tr><td height="5"></td></tr>

                                                    <tr>
                                                        <td class="bg_gray2">수신대상</td>
                                                        <td class="box_bottom1 text_blue">MO사용자 : <span class="total_count_text"></span>명에게 MT를 발송합니다.</td>
                                                    </tr>

                                                    <tr><td height="15"></td></tr>

                                                    <tr>
                                                        <td class="bg_gray2">상용구</td>
                                                        <td class="box_bottom1 text_blue">
                                                            <select name="phrlist" class="set_msg" onChange="javascript:setMtMsg(this.value)">
                                                                <option value="">----------</option>

                                                            </select>
                                                        </td>
                                                    </tr>

                                                    <tr><td height="10"></td></tr>

                                                    <tr>
                                                        <td class="bg_gray2">주의사항</td>
                                                        <td class="text_gray" style="padding-top:10px">
                                                            - 발송에는 5~10초의 시간이 소요되므로 발송버튼을<br>
                                                            &nbsp;&nbsp;누른 후 대기하여 주시기 바랍니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td class="text_gray">- 발송버튼을 중복 클릭시 메시지가 중복 발송될 수<br>&nbsp;&nbsp;있습니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td class="text_gray">
                                                            <strong>
                                                                - 문자만 청취자 전체답장이 가능합니다.<br/>
                                                                - 해당 프로그램 청취자 <span class="total_count_text"></span>명 에게 전체 발송 되므로, 신중하게 발송해 주세요.
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </td>
                                                <td>
                                                    <!-- 답장하기 영역  : Start -->
                                                    <div class="box_lf">
                                                        <div class="user_box">
                                                            <div class="user_top">
                                                                <strong class="t_tit">답장하기[</strong>
                                                                <strong class="t_tit" id="msgType" style="margin-right:1px;">SMS</strong>
                                                                <strong class="t_tit">]</strong>
                                                            </div>

                                                            <div class="user_cont">
                                                                <div class="user_reply">

                                                                    <div class="user_reply_txt"><textarea id=""  name="mt_msg" rows="" cols="" onkeyup="javascript:check_msglen();"></textarea></div>
                                                                    <div class="user_reply_byte">
                                                                        <span id="nbytes">0</span>/<span id="msgTypeLength"></span> Bytes
                                                                    </div>
                                                                    <div class="set_blk select_b" id="addCount" style="width:120px;padding-left:15px;">
                                                                        <select name="phrlist_text" class="select_box select_t set_msg" onChange="javascript:setMtMsg(this.value)">
                                                                            <option value="">----------</option>
                                                                        </select>
                                                                        <!--
                                                                        <a href="javascript:sendSmsMsg_no();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                                        -->
                                                                        <a href="javascript:void(0);" class="user_reply_btn" style="top: 34px"><img src="/img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="user_bline"></div>
                                                        </div>
                                                        <!-- 답장하기 영역  : END -->
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</form>
<jsp:include page="../main/settings.jsp"/>
<script type="text/javascript">
    let f;
    let nowSendMsg = false;

    function sendSmsMsg() {
        if(nowSendMsg){
            alert("메세지 보내는 중입니다.");
            return;
        }
        if(emptyData(f.mt_msg, "메세지를 작성하셔요.")){
            return;
        }
        if(!check_msglen()) return;

        const url = "";
        let data = $(f).serialize();

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            data : data,
            error : ajaxError,
            success : sendMsgSaveResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }

    function sendMsgSaveResult(response) {
        const result = JSON.parse(response);
        const result_001 = result.result_001;
        const result_007 = result.result_007;
        const result_013 = result.result_013;

        if(result_001 == -1){
            alert("메세지 발송을 실패 했습니다.");
        } else {
            let showMsg = "";
            if(result_001>0) showMsg = showMsg + "문자 " + result_001 +"건 ";
            if(result_007>0) showMsg = showMsg + "카카오톡 " + result_007 +"건 ";
            if(result_013>0) showMsg = showMsg + "레인보우 " + result_013 +"건 ";
            showMsg = showMsg + "답장 메세지 발송을 성공했습니다.";
            alert(showMsg);
            window.close();
        }
    }

    function setMtMsg(value) {
        document.BoardForm.mt_msg.value = value;
        check_msglen();
    }

    function check_msglen() {
        var length = calculate_msglen(document.BoardForm.mt_msg.value);
        document.all.nbytes.innerText = length;
        f = document.BoardForm;

        // SMS 케이스
        maxlength = 80;
        if (length > maxlength) {

            // noticePopup();

            alert("LMS발송 문의는 하단의 담당자에게 연락 바랍니다.\r\n메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
            f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);

            return false;
        }

        return true;
    }

    function calculate_msglen(message) {
        var nbytes = 0;

        for (i=0; i<message.length; i++) {
            var ch = message.charAt(i);
            if (escape(ch).length > 4) {
                nbytes += 2;
            } else if (ch != '\r') {
                nbytes++;
            }
        }
        return nbytes;
    }

    function assert_msglen(message, maximum) {
        var inc = 0;
        var nbytes = 0;
        var msg = "";
        var msglen = message.length;

        for (i=0; i<msglen; i++) {
            var ch = message.charAt(i);
            if (escape(ch).length > 4) {
                inc = 2;
            } else if (ch != '\r') {
                inc = 1;
            }
            if ((nbytes + inc) > maximum) {
                break;
            }
            nbytes += inc;
            msg += ch;
        }
        document.all.nbytes.innerText = nbytes;
        return msg;
    }
    const message = {
        isLoaded : false
        ,getBoilerplate : function () {
            $.ajax({
                url : '/MyPhraseAction.do/boilerplate'
                ,type : 'get'
                ,dataType : 'json'
                ,beforeSend : ajaxOnCreate
                ,complete : ajaxOnComplete
            }).done(function (response) {
                if(response.result){
                    $(response.list).each(function(){
                        let option = $('<option/>', {text: this.title, value : this.msg});
                        $('.set_msg').append(option);
                    });
                } else {
                    alert(response.message);
                }
            }).fail(ajaxError);
            this.isLoaded = true;
        }, getSendInfo : function () {
            $.ajax({
                url : '/UserMngAction.do/users/message/info'
                ,type : 'get'
                ,data : $(window.opener.document.UserForm).serialize()
                ,dataType : 'json'
                ,beforeSend : ajaxOnCreate
                ,complete : ajaxOnComplete
            }).done(function(response){
                $('.total_count_text').append(response.totalCount);
                $('#pgmKey').val(response.info.pgm_key);
                $('#totalCount').val(response.totalCount);
                $('#operName').text('[' + response.info.oper_id + ']');
                $('#msgTypeLength').text(response.msgSize);
                $('#programName').text(response.info.pgm_name);
            }).fail(ajaxError);
        }, send : function () {
            const f = document.BoardForm;
            if(nowSendMsg){
                alert("메세지 보내는 중입니다.");
                return;
            }
            if(emptyData(f.mt_msg, "메세지를 작성해 주세요.")){
                return;
            }
            if(!check_msglen()) return;
            nowSendMsg = true;
            const url = "";

            let data = $(window.opener.document.UserForm).serialize();
            data += '&message=' + $('textarea[name=mt_msg]').val();
            $.ajax({
                url: '/UserMngAction.do/users/message',
                type : "post",
                dataType : "json",
                //contentType : "application/x-www-form-urlencoded; charset=UTF-8",
                data : data,
                beforeSend : ajaxOnCreate,
                complete : ajaxOnComplete,
                async : false
            }).done(function(response){
                if(response.totalCount == 0){
                    alert('메세지 전송에 실패하였습니다.');
                } else {
                    let showMsg = "문자 " + response.totalCount +"건 답장 메세지 발송을 성공했습니다.";
                    alert(showMsg);
                    window.close();
                }
            }).fail(function(){
                nowSendMsg = true;
            });
        }
    }
    $(document).ready(function() {
        // f = document.BoardForm;
        message.getBoilerplate();
        message.getSendInfo();
        $('.user_reply_btn').on('click', function () {
            message.send();
        })
    });
</script>
</body>
</html>
