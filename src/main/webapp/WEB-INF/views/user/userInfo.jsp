<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<form name="UserForm" action="/UserAction" style="margin-bottom: 0">
    <input type="hidden" name="page_num" value="1">
    <input type="hidden" name="row_count" value="5">
    <input type="hidden" name="imoti"	value="<c:out value="${userInfo.user_icon}"/>" >
    <input type="hidden" name="gift"  value="<c:out value="${userInfo.user_image}"/>" >
    <input type="hidden" name="gift1" value="">
    <input type="hidden" name="gift2" value="">
    <input type="hidden" name="gift2" value="">

    <input type="hidden" name="user_gift_1_value" value="">
    <input type="hidden" name="user_gift_2_value" value="">
    <input type="hidden" name="user_gift_3_value" value="">



    <!-- 기본 데이터 세팅 : Start -->
    <c:set var="setStar" value="" />
    <c:set var="setFirst" value="" />

    <c:if test="${userInfo.mo_day==userInfo.mo_total}">
        <c:set var="setFirst" value="1" />
    </c:if>

    <c:if test="${userInfo.mo_day>8}">
        <c:set var="setStar" value="1" />
    </c:if>


    <c:set var="setMsgSize"  value="0" />
    <c:set var="setUserType" value="MO" />

    <c:choose>
        <c:when test="${operator.br_key=='EBSTV'}">
            <input type="hidden" name="setMsgSize" value="0">
            <c:set var="setUserType" value="발신불가" />
        </c:when>
        <c:when test="${userInfo.msg_com=='005'}">
            <input type="hidden" name="setMsgSize" value="140">
            <c:set var="setMsgSize"  value="140" />
            <c:set var="setUserType" value="Twitter" />
        </c:when>

        <c:when test="${userInfo.msg_com=='006'}">
            <input type="hidden" name="setMsgSize" value="200">
            <c:set var="setMsgSize"  value="200" />
            <c:set var="setUserType" value="FaceBook" />
        </c:when>

        <c:when test="${userInfo.msg_com=='007' && (operator.br_key=='CBS' || operator.br_key=='TBS' || operator.br_key=='MOC' || operator.br_key=='FEBCSU')}">
            <input type="hidden" name="setMsgSize" value="80">
            <c:set var="setMsgSize"  value="80" />
            <c:set var="setUserType" value="Kakao" />
        </c:when>

        <c:when test="${userInfo.msg_com=='007' && (operator.br_key=='FEBCMP' || operator.br_key=='FEBCDJ' || operator.br_key=='FEBCCW' || operator.br_key=='FEBCUS')}">
            <input type="hidden" name="setMsgSize" value="1000">
            <c:set var="setMsgSize"  value="1000" />
            <c:set var="setUserType" value="Kakao" />
        </c:when>

        <c:when test="${userInfo.msg_com=='007' && (operator.br_key != 'CBS' || operator.br_key != 'TBS' || operator.br_key != 'MOC' || operator.br_key!='FEBCSU' || operator.br_key!='FEBCMP' || operator.br_key!='FEBCDJ' || operator.br_key!='FEBCCW' || operator.br_key!='FEBCUS' )}">
            <input type="hidden" name="setMsgSize" value="0">
            <c:set var="setUserType" value="발신불가" />
        </c:when>

        <c:when test="${userInfo.msg_com=='001'}">
            <input type="hidden" name="setMsgSize" value="80">
            <c:set var="setMsgSize"  value="80" />
            <c:set var="setUserType" value="문자" />
        </c:when>

        <c:when test="${userInfo.msg_com=='013'}">
            <input type="hidden" name="setMsgSize" value="80">
            <c:set var="setMsgSize"  value="80" />
            <c:set var="setUserType" value="레인보우" />
        </c:when>

        <c:otherwise>
            <input type="hidden" name="setMsgSize" value="0">
            <c:set var="setUserType" value="발신불가" />
        </c:otherwise>
    </c:choose>
    <!-- 기본 데이터 세팅 : END -->



    <div class="aside_top" style="padding:7px 0;">
        <p class="aside_tit">유저정보</p>
        <ul class="aside_btnarea">
            <!-- <li class="setting"><a href="#aside_user" class="aside_layerb">설정</a></li> -->
            <li>
                <input type="checkbox" class="checkbox" id="autoScrollWidgetYN" name="autoScrollWidgetYN" onClick="chkAutoScrollWidget(this);">상단 고정
            </li>
        </ul>
    </div>



    <ul class="user_info">
        <li>
            <div class="box_lf">
                <div class="user_box">

                    <!-- User :  이름 , 새삭 여부 -->
                    <div class="user_top">
                        <c:if test="${param.msg_com == 007}">
                            <strong class="t_tit"><c:out value="${userInfo.user_nick}"/> <span>님</span></strong>
                        </c:if>
                        <c:if test="${param.msg_com != 007}">
                            <strong class="t_tit"><c:out value="${userInfo.user_name}"/> <span>님</span></strong>
                        </c:if>

                        <span class="t_icon">
					<c:choose>
                        <c:when test="${setFirst=='1'}">
                            <img src="../img/common/btn/btn_sprout_on.png" width="20" height="20" alt="새싹">
                        </c:when>

                        <c:when test="${setStar=='1'}">
                            <img src="../img/common/icon/icon_star.gif" width="20" height="20" alt="별">
                        </c:when>

                        <c:otherwise>
                            <img src="../img/common/btn/btn_star_off.png" width="20" height="20" alt="별" />
                        </c:otherwise>
                    </c:choose>
					</span>
                    </div>


                    <div class="user_cont">

                        <!-- User :  레벨  -->
                        <div class="user_level">
                            <a class="aside_layerb" href="javascript:setUserIcon();"><img src="../img/common/btn/btn_write.png" width="14" height="14" alt="저장" /></a>

                            <a href="javascript:view_layer(this.form);" onclick="javascript:ch_pos();">
                                <c:choose>
                                    <c:when test="${empty userInfo.user_icon}">
                                        <c:if test="${empty userInfo.user_id}">
                                            <img name="emoticon" src='../icon/position_000.gif' width="58" height="58">
                                        </c:if>
                                        <c:if test="${!empty userInfo.user_id}">
                                            <img name="emoticon" src='../img/common/icon/icon_level_<c:out value='${userInfo.user_grade}'/>.png' width="58" height="58" alt="이미지를 클릭하시면 변경가능합니다.">
                                        </c:if>
                                    </c:when>
                                    <c:when test="${userInfo.user_icon == '100'}">
                                        <img name="emoticon" src='../img/common/icon/icon_level_<c:out value='${userInfo.user_grade}'/>.png' width="58" height="58" alt="이미지를 클릭하시면 변경가능합니다.">
                                    </c:when>
                                    <c:otherwise>
                                        <img name="emoticon" src='../icon/position_<c:out value="${userInfo.user_icon}"/>.gif' width="58" height="58" alt="이미지를 클릭하시면 변경가능합니다.">
                                    </c:otherwise>
                                </c:choose>
                            </a>

                        </div>

                        <!-- User 가입 정보 -->
                        <div class="user_join" style="height:83px;">
                            <ul>
                                <!--
							<li><strong>가입일</strong><span><c:out value="${userInfo.user_reg_dt}"/></span></li>
							<li><strong>아이디</strong><span><c:out value="${userInfo.user_id}"/></span></li>
-->
                                <li><strong>가입일</strong><span><c:out value="${userInfo.user_reg_dt}"/></span></li>
                                <li><strong>최종일</strong><span><c:out value="${userInfo.lastlogin_dt}"/></span></li>

                                <c:if test="${param.msg_com == 007}">
                                    <li><strong>아이디</strong><span><c:out value="${userInfo.user_nick}"/></span></li>
                                </c:if>
                                <c:if test="${param.msg_com != 007}">
                                    <li><strong>아이디</strong><span><c:out value="${userInfo.user_id}"/></span></li>
                                </c:if>


                            </ul>
                        </div>
                    </div>
                </div>

                <!-- 이중 라인 -->
                <div class="user_bline"></div>
            </div>

            <div class="box_rf">
                <div class="user_box">
                    <div class="user_top">
                        <strong class="t_tit">유저설정</strong>
                        <span class="t_icon">

					<a title="이미지를 클릭하시면 변경가능합니다." onclick="javascript:gift_Pos()" href="javascript:gift_Layer(1)">
						<img name="user_gift_1"  src="../icon/gift_200.png" width="25" height="25" alt="새싹">
					</a>
					<a title="이미지를 클릭하시면 변경가능합니다." onclick="javascript:gift_Pos()" href="javascript:gift_Layer(2)">
						<img name="user_gift_2" src="../icon/gift_200.png" width="25" height="25" alt="새싹">
					</a>
					<a title="이미지를 클릭하시면 변경가능합니다." onclick="javascript:gift_Pos()" href="javascript:gift_Layer(3)">
						<img name="user_gift_3" src="../icon/gift_200.png" width="25" height="25" alt="새싹">
					</a>

					<a class="aside_layerb" href="javascript:setUserGift();"><img src="../img/common/btn/btn_write.png" width="14" height="14" alt="저장" /></a>
					</span>
                    </div>
                    <div class="user_cont">
                        <ul class="user_infochg" style="height: 160px;">	<!-- 이 클래스에서 사이즈 처리 -->
                            <li>
                                <strong style="float:left; width:40px; padding-top:5px; color:#9aa4a9; font-weight:normal;">닉네임</strong>
                                <c:if test="${param.msg_com == 007}">
                                    <input type="text" name="user_nick" id="user_nick" class="input_txt"  onkeydown="kakaoNickAlert();" value="<c:out value="${param.user_name}"/>"  style="width:130px; padding:0 15px 0 5px;" readonly="readonly" />
                                </c:if>

                                <c:if test="${param.msg_com != 007}">
							<span  style="float:left;">
								<input type="text" name="user_nick" id="user_nick" class="input_txt" onkeydown="if(event.keyCode == 13)saveUserNick();" value="<c:out value="${userInfo.user_nick}"/>"  style="width:130px; padding:0 15px 0 5px;"/>
								<a style="position:absolute; top:1px; right:-50px;" href="javascript:saveUserNick()" class="btn"><img src="../img/aside/btn_user_name.png" width="24" height="24" alt="" /></a>
							</span>
                                </c:if>
                            </li>
                            <li>
                                <strong style="float:left; width:40px; padding-top:5px; color:#9aa4a9; font-weight:normal;" >정보</strong>
                                <span style="float:left;">
								<input type="text" name="user_info" id="user_info" class="input_txt" onkeydown="if(event.keyCode == 13)saveUserInfo();" value="<c:out value="${userInfo.user_info}"/>" style="width:130px; padding:0 15px 0 5px;"/>
								<a style="position:absolute; top:1px; right:-50px;" href="javascript:saveUserInfo()" class="btn"><img src="../img/aside/btn_user_name.png" width="24" height="24" alt="" /></a>
							</span>
                            </li>
                            <!-- 닉 변경 여부 설정 -->
                            <li>
                                <strong>닉네임변경권한</strong>

                                <c:if test="${param.msg_com == 007}">
								<span class="switch off">
								</span>
                                </c:if>

                                <c:if test="${param.msg_com != 007}">
							<span <c:if test="${userInfo.edit_nick==1}">class="switch on"</c:if> <c:if test="${userInfo.edit_nick!=1}">class="switch off"</c:if>>
								<input type="radio" id="nick_off" name="nick" onClick="javascript:setEditNick('0')"/><label for="nick_off">권한off</label>
								<input type="radio" id="nick_on" name="nick" onClick="javascript:setEditNick('1')"/><label for="nick_on">권한on</label>
							</span>
                                </c:if>
                            </li>


                            <!-- 닉 변경 여부 설정 -->
                            <li>
                                <strong>회신메시지</strong>
                                <span <c:if test="${userInfo.allow_mt==1}">class="switch on"</c:if> <c:if test="${userInfo.allow_mt!=1}">class="switch off"</c:if>>
								<input type="radio" id="mt_off" name="mt" onClick="javascript:setAllowMT(0)"/><label for="mt_off">회신메시지 off</label>
								<input type="radio" id="mt_on" name="mt" onClick="javascript:setAllowMT(1)"/><label for="mt_on">회신메시지 on</label>
							</span>
                            </li>
                            <li>
                                <strong>자동차단</strong>
                                <span <c:if test="${userInfo.show_msg==1}">class="switch off"</c:if> <c:if test="${userInfo.show_msg!=1}">class="switch on"</c:if>>
								<input type="radio" id="hide_off" name="hide" onClick="javascript:setAutoHide(1)"/><label for="hide_off">자동숨김on</label>
								<input type="radio" id="hide_on" name="hide" onClick="javascript:setAutoHide(0)"/><label for="hide_on">자동차단off</label>
							</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="user_bline"></div>
            </div>
        </li>

        <!-- 2단 구성 -->
        <li>
            <!-- 답장하기 영역  : Start -->
            <div class="box_lf" >
                <div class="user_box">
                    <div class="user_top">
                        <c:if test="${userInfo.msg_com != 007}">
                            <strong class="t_tit">답장하기 [<c:out value="${setUserType}"/>]</strong><strong class="t_tit" id="msgType" style="margin-right:1px;">SMS</strong>
                        </c:if>
                        <c:if test="${userInfo.msg_com == 007}">
                            <strong class="t_tit">답장 [카카오]</strong>
                            <!--
                            <strong class="t_tit">답장[<c:out value="${setUserType}"/>]
                            <c:if test="${userInfo.max_cnt == 0}"><span style="color: red; font-size: 8pt">[<c:out value="${userInfo.current_cnt}"/>/200]</c:if>
                            <c:if test="${userInfo.max_cnt != 0}"><span style="color: red; font-size: 8pt">[<c:out value="${userInfo.current_cnt}"/>/<c:out value="${userInfo.max_cnt}"/>]</c:if>

                            </span></strong>
                            -->

                        </c:if>
                    </div>
                    <div class="user_cont">
                        <div class="user_reply">
                            <div class="user_reply_txt"><textarea id="" name="mt_msg" rows="" cols="" onkeyup="javascript:check_msglen();"></textarea></div>
                            <div class="user_reply_byte">
                                <span id="nbytes">0</span>/<span id="msgTypeLength"><c:out value="${setMsgSize}"/></span> Bytes
                            </div>

                            <div class="set_blk select_b" id="addCount" style="width:120px;padding-left:15px;">
                                <select name="phrlist" onChange="javascript:setMtMsg(this.value)" class="select_box select_t" style="width:120px;padding-left:15px;">
                                    <option value="">----------</option>
                                    <c:forEach items="${phlist}" var="row">
                                        <option value="<c:out value='${row.msg}'/>"   >
                                            <c:out value='${row.title}'/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
<%--                            <div class="set_blk select_b" id="addCount" style="width:120px;padding-left:15px;">--%>
<%--							<span class="select_box select_t">--%>
<%--								<span class="t_select" id="mt_msg_name">-------</span>--%>
<%--								<button type="button" class="arrow_btn">화살표</button>--%>
<%--							</span>--%>

<%--                                <div class="select_m">--%>
<%--                                    <ul class="layer_inner" style="height:55px;width:116px;">--%>
<%--                                        <c:forEach items="${phlist}" var="row">--%>
<%--                                            <li><a href="javascript:void(0);" onclick="javascript:setSelectBox(document.BoardForm.mt_msg, '<c:out value='${row.msg}'/>', 'mt_msg_name', this);setMtMsg('<c:out value='${row.msg}'/>');"><c:out value='${row.title}'/></a></li>--%>
<%--                                        </c:forEach>--%>
<%--                                    </ul>--%>
<%--                                </div>--%>

<%--                            </div>--%>

                            <c:choose>
                                <c:when test="${setMsgSize=='0'}">
                                    <a href="javascript:sendSmsMsg_no();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                </c:when>

                                <c:otherwise>
                                    <!--
                                    <c:if test="${param.msg_com == 007}">
                                        <a href="javascript:sendSmsMsg_no();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                    </c:if>
                                    <c:if test="${param.msg_com != 007}">
                                        <a href="javascript:sendSmsMsg();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>
                                    </c:if>
                                    -->
                                    <a href="javascript:sendSmsMsg();" class="user_reply_btn"><img src="../img/aside/btn_user_reply.gif" width="144" height="33" alt="문자보내기" /></a>

                                </c:otherwise>
                            </c:choose>


                        </div>
                    </div>
                </div>
                <div class="user_bline"></div>
            </div>
            <!-- 답장하기 영역  : END -->


            <!-- 이미지 및 활동 내역 영역 : Start -->
            <div class="box_rf">
                <!-- 이미지 영영 -->
                <div class="user_box">
                    <!-- 선물 아이콘 작업. 2015. 2. 2 Lucas START -->
                    <div class="user_top">
                        <strong class="t_tit"><a href="javascript:showImageTap(1);">이미지</a></strong>
                        <!-- KBS에는 적용 -->
                        <!--
                        <strong class="t_tit"> | </strong>
                        <strong class="t_tit"><a href="javascript:showImageTap(2);">선물아이콘</a></strong>
                         -->
                    </div>
                    <div id = "user_pic" class="user_cont">
                        <ul class="user_graph" style="">
                            <c:if test="${empty userInfo.user_pic}">
                                <img src='../img/common/no_picture_image.gif' border="0"  width="70" height="70">
                            </c:if>
                            <c:if test="${!empty userInfo.user_pic}">
                                <img src="<c:out value='${userInfo.user_pic}'/>" border=0 width="70"  height="70" onmouseover="Tip('<img src=<c:out value='${userInfo.user_pic}'/> width=300>')">
                            </c:if>
                        </ul>
                    </div>
                    <div id = "user_image" class="user_cont" style = "height : 81px;">
                        <ul class="user_graph" style="">
                            <c:if test="${empty userInfo.user_image}">
                                <!-- <img src='/img/no_picture_image.gif' border="0"  width="70" height="70">  -->
                                <a href="javascript:gift_Layer(1)" onclick="javascript:gift_Pos()" title="이미지를 클릭하시면 변경가능합니다.">
                                    <img src='../icon/gift_200.gif' border="0"  width="50" height="50" name="giftcon1" >
                                </a>
                                <a href="javascript:gift_Layer(2)" onclick="javascript:gift_Pos()" title="이미지를 클릭하시면 변경가능합니다.">
                                    <img src='../icon/gift_200.gif' border="0" width="50" height="50" name="giftcon2" >
                                </a>
                                <a class="aside_layerb" href="javascript:setUserGift();"><img src="../img/common/btn/btn_write.png" width="14" height="14" alt="저장"  title="저장"/></a>
                                <!-- <input name="button4" type="button" class="btn_gen40" value="설정" onFocus='blur()' onClick="javascript:setUserGift()">  -->
                            </c:if>
                            <c:if test="${!empty userInfo.user_image}">
                                <!--  유저 이미지가 있는 경우. -->
                                <a href="javascript:gift_Layer(1)" onclick="javascript:gift_Pos()" title="이미지를 클릭하시면 변경가능합니다.">
                                    <img src='../icon/gift_200.gif' border="0"  width="50" height="50" name="giftcon1" >
                                </a>
                                <a href="javascript:gift_Layer(2)" onclick="javascript:gift_Pos()" title="이미지를 클릭하시면 변경가능합니다.">
                                    <img src='../icon/gift_200.gif' border="0" width="50" height="50" name="giftcon2" >
                                </a>
                                <a class="aside_layerb" href="javascript:setUserGift();"><img src="../img/common/btn/btn_write.png" width="14" height="14" alt="저장"  title="저장"/></a>
                            </c:if>
                        </ul>
                        <!-- 유저 선물 아이콘  -->
                    </div>
                </div>
                <div class="user_bline"></div>
                <!-- 선물 아이콘 작업. 2015. 2. 2 Lucas END-->

                <!-- MO 활동 영역 -->
                <div class="user_box">
                    <div class="user_top">
                        <strong class="t_tit"><a href="javascript:showCount(1);">MO 활동내역</a></strong>
                        <strong class="t_tit"> | </strong>
                        <strong class="t_tit"><a href="javascript:showCount(2);">당첨내역</a></strong>
                    </div>

                    <div id="count_mo" class="user_cont">
                        <ul class="user_graph">
                            <li>
                                <strong>금일</strong>
                                <span><em class="graph01" style="width:<c:out value="${userInfo.mo_day}"/>0%;"><c:out value="${userInfo.mo_day}"/></em></span>

                            </li>
                            <li>
                                <strong>금월</strong>
                                <span><em class="graph02" style="width:<c:out value="${userInfo.mo_month}"/>0%;"><c:out value="${userInfo.mo_month}"/></em></span>

                            </li>
                            <li>
                                <strong>누적</strong>
                                <span><em class="graph03" style="width:<c:out value="${userInfo.mo_total}"/>0%;"><c:out value="${userInfo.mo_total}"/></em></span>

                            </li>
                        </ul>
                    </div>
                    <div id="count_pz" class="user_cont">
                        <ul class="user_graph">
                            <li>
                                <strong>금일</strong>
                                <span><em class="graph01" style="width:<c:out value="${userInfo.pz_day}"/>0%;"><c:out value="${userInfo.pz_day}"/></em></span>
                            </li>
                            <li>
                                <strong>금월</strong>
                                <span><em class="graph02" style="width:<c:out value="${userInfo.pz_month}"/>0%;"><c:out value="${userInfo.pz_month}"/></em></span>
                            </li>
                            <li>
                                <strong>누적</strong>
                                <span><em class="graph03" style="width:<c:out value="${userInfo.pz_total}"/>0%;"><c:out value="${userInfo.pz_total}"/></em></span>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="user_bline"></div>
            </div>
            <!-- 이미지 및 활동 내역 영??: END -->
        </li>


        <!-- 메시지 검색 영역 -->
        <li>
            <div class="user_box">
                <div class="user_top">
                    <strong class="t_tit">메시지검색</strong>
                </div>
                <div class="user_cont02">
                    <div class="top_searchbox" style="padding: 10px 10px;">
                        <select name="recent" class="tsearch select_b">
                            <option value="last">최근3일</option>
                            <c:forEach items="${BackList}" var="row">
                            <option value="${row}"><c:out value='${row}'/></option>
                            </c:forEach>
                        </select>
                        <div class="tsearch_search" style="padding-left:0px;width:230px;">
                            <input type="radio" id="mo" name="search_type" class="radiobox" value="mo" checked/><label for="mo">수신</label>
                            <input type="radio" id="mt" name="search_type" class="radiobox" value="mt"/><label for="mt">발신</label>
                            <!-- 2015.01.16 Lucas 당첨내역 검색 기능 제거 -->
                            <!--  <input type="radio" id="prize" name="search_type" class="radiobox" value="prize"/><label for="prize">당첨내역</label>  -->
                            <button type="button" style="width:34px;" class="btn" onClick="userlist();"><img src="../img/aside/btn_user_search.gif" width="34" height="26" alt="검색" /></button>
                        </div>
                    </div>
                    <!-- <div class="top_searchbln"></div> -->
                    <div id="userListMsg" class="table_type01">
                        <table cellspacing="0" border="1" summary="문자메시지를 담고 있습니다.">
                            <caption>문자메시지 리스트</caption>
                            <colgroup>
                                <col style="width:45px;" />
                                <col style="width:130px;" />
                                <col style="width:auto;" />
                                <col style="width:80px;" />
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope="col">N</th>
                                <th scope="col" colspan="2">내용</th>
                                <th scope="col">보낸시간</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" align="center"><c:out value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
                            </tr>
                        </table>
                    </div>
                    <table width="100%"  height="150px" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="100%" align="center"><c:out value="${BoardForm.pageNavigator}" escapeXml="false" /></td>
                        </tr>
                    </table>

                </div>
            </div>
        </li>
    </ul>

    <!-- 사용자 화면 설정 : Start -->
    <div class="asidelayer_wrap" id="aside_user" style="display:none;">
        <div class="asidelayer_mask"></div>
        <div class="asidelayer">
            <div class="asidelayer_inner">
                <span class="arrow"></span>
                <div class="asidelayer_content">
                    <p class="use_tit"><strong>탭 사용 여부</strong></p>
                    <ul class="aside_tabuse">
                        <li><input type="radio" class="radiobox" id="us_use" name="tab_use" checked/> <label for="us_use">사용</label></li>
                        <li><input type="radio" class="radiobox" id="us_no_use" name="tab_use" onclick="javascript:setWidgetOption();"/> <label for="us_no_use">미사용(탭 목록에서 제외)</label></li>
                    </ul>
                </div>
                <div class="asidelayer_bottom">
                    <a href="#" class="aside_close" style="padding-left:95px;padding-right:95px;">닫기</a>
                </div>
            </div>
        </div>
    </div>
    <!-- 사용자 화면 설정 : End -->


</form>

</body>
<jsp:include page="../main/settings.jsp"/>
<script type="text/javascript" src="../js/itv_icon.js"></script>
<script type="text/javascript" src="../js/wz_tooltip.js"></script>
<script type="text/javascript" src="../js/cookie.js"></script>
<script language="JavaScript">
$(function() {
    // $('.aside_layerb').click(function(){
    //     var layerlink = $(this).attr('href');
    //     $(layerlink).show();
    //     return false;
    // });
    // $('.aside_close').click(function(){
    //     $(this).parent().parent().parent().parent().hide();
    //     return false;
    // });

    $('.switch>label:eq(0),.switch>label:eq(2),.switch>label:eq(4)').click(function(){
        $(this).parent().removeClass('on');
        $(this).parent().addClass('off');
    });
    $('.switch>label:eq(1),.switch>label:eq(3),.switch>label:eq(5)').click(function(){
        $(this).parent().removeClass('off');
        $(this).parent().addClass('on');
    });

    initPage();
    showCount(1);
    showImageTap(1); // 이미지, 선물아이콘 디폴트 설정.

    var userMsgCom = "<c:out value='${userInfo.msg_com}'/>";
    if(userMsgCom != "001" && userMsgCom != "013" ){
        $("#msgType").html("");
    }

    // 위젯 상단 고정
    let auto_scroll_widget_yn = getCookie("auto_scroll_widget_yn");
    if (auto_scroll_widget_yn != 'Y' && auto_scroll_widget_yn != 'N') {
        auto_scroll_widget_yn = "N";
        setCookie("auto_scroll_widget_yn", auto_scroll_widget_yn);
    }
    if(auto_scroll_widget_yn == "Y") {
        $("#autoScrollWidgetYN").prop("checked", true);
    } else {
        $("#autoScrollWidgetYN").prop("checked", false);
    }
});

function initPage() {
    userlist();
    InitGiftIcon();
}

function userlist() {
    let f = document.UserForm;
    let url = "<c:url value='/BoardAction.do' />";
    let data = "msg_userid=<c:out value='${param.user_id}'/>&msg_com=<c:out value='${param.msg_com}'/>&start_dt="+f.recent.value+"&page_num="+f.page_num.value + "&row_count="+f.row_count.value;

    if(f.search_type[0].checked){
        url = "<c:url value='/BoardAction.do' />" + "/listBoardEachUser";
    } else if(f.search_type[1].checked){
        url = "<c:url value='/SendAction.do' />" + "/listSendMsgEachUser";
    } else if(f.search_type[2].checked){
        data = data+"/prizeListEachUser";
    } else {
        return;
    }

    if("<c:out value='${param.user_id}'/>" =="") return;

    $("userListMsg").innerHTML="";
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request) {
    $('#userListMsg').html(response);
}

function InitGiftIcon() {
    gift_update( document.UserForm.gift.value );
}

// MO 활동 내역, 당첨내역
function showCount( no ) {
    $("#count_mo").css("display","none");
    $("#count_pz").css("display","none");

    if( no == '1' ){
        $("#count_mo").css("display","block");
    } else if( no == '2' ) {
        $("#count_pz").css("display","block");
    }
}

// 이미지, 선물 아이콘 탭.
function showImageTap( no ) {
    $("#user_pic").css("display","none");
    $("#user_image").css("display","none");

    if( no == '1' ){
        $("#user_pic").css("display","block");
    } else if( no == '2' ) {
        $("#user_image").css("display","block");
    }
}

function setEditNick(edit_nick){

    let url = "<c:url value='/UserAction.do/updateUserEditNick' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        edit_nick : edit_nick,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function setAllowMT(allow_mt){
    let url = "<c:url value='/UserAction.do/updateUserAllowMt' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        allow_mt : allow_mt,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function setAutoHide(show_msg){
    let url = "<c:url value='/UserAction.do/updateUserShowMsg' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        show_msg : show_msg,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function kakaoNickAlert(){
    alert("- 카카오 유저는 닉네임 변경이 불가능합니다. \n- 사용자의 정보를 저장 하실려면 '정보' 필드를 이용하세요^^.");
}

function saveUserNick() {
    const f = document.UserForm;
    const user_nick = f.user_nick.value;
    let length = calculate_msglen(user_nick);
    if ( length < 0 ) {
        alert("닉네임을 작성해 주세요. " );
        return false;
    }

    const nMaxNick = 30;
    if (length > nMaxNick ) {
        alert("메시지는 최대"+nMaxNick+" 바이트까지  작성하실수 있습니다. \r\n초과된 " + (length - nMaxNick) + "바이트는 자동으로 삭제됩니다.");
        f.user_nick.value = assert_msglen(user_nick, nMaxNick);
        return false;
    }

    let url = "<c:url value='/UserAction.do/updateUserNick' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        user_nick : user_nick,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function saveUserInfo(){
    if(!check_infomsglen()) return;
    let f = document.UserForm;
    let user_info = f.user_info.value;
    let url = "<c:url value='/UserAction.do/updateUserInfo' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        user_info : user_info,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function check_infomsglen() {
    let f = document.UserForm;
    var length = calculate_msglen(f.user_info.value);
    // $("#nInfo").html(length);
    if (length > 80) {
        alert("사용자 정보 입력은  최대 80 바이트까지 전송하실 수 있습니다.");
        // f.user_info.value = assert_msglen(f.user_info.value, 80);
        // length = calculate_msglen(f.user_info.value);
        // $("#nInfo").html(length);
        return false;
    }
    return true;
}

function reloadBoardList(){
    try{
        parent.list();
    } catch(e) {}
}

function setMtMsg(val) {
    const f = document.UserForm;
    f.mt_msg.value = val;
    check_msglen();
}
var limitSendMt = "해당 매체는 메시지 전송이 불가능한 매체입니다. 관리자에게 문의 하세요." ;
function check_msglen() {
    const f = document.UserForm;
    var length = calculate_msglen(f.mt_msg.value);
    $("#nbytes").html(length);

    var maxlength = f.setMsgSize.value;
    var userMsgCom = "<c:out value="${param.msg_com}"/>";

    if ( maxlength == 0 ) {
        alert( limitSendMt );
    } else if(userMsgCom == "001") {

        // SMS 케이스
        if($("#msgType").html() == "SMS") {

            // LMS 발송 가능한 케이스
            if(Settings.canLmsMt()){
                if ( length > 80 ) {
                    $("#msgType").html("LMS");
                    $("input[name=setMsgSize]").val("1000");
                    $("#msgTypeLength").html("1000");
                }
            } else {
                // LMS 발송 불 가능한 케이스
                maxlength = 80;
                if (length > maxlength) {
                    alert("LMS발송 문의는 하단의 담당자에게 연락 바랍니다.\r\n메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
                    f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);
                    return false;
                }
            }
        } else if($("#msgType").html() == "LMS") {
            maxlength = 1000;
            if(length < 80){
                $("#msgType").html("SMS");
                $("input[name=setMsgSize]").val("80");
                $("#msgTypeLength").html("80");
            } else if (length > maxlength) {
                alert("메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
                f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);

                return false;
            }
        }
    } else if(userMsgCom == "013") {
        // SMS 케이스
        if($("#msgType").html() == "SMS") {

            // LMS 발송 가능한 케이스
            if(Settings.canLmsMt()) {
                if ( length > 80 ) {
                    $("#msgType").html("LMS");
                    $("input[name=setMsgSize]").val("1000");
                    $("#msgTypeLength").html("1000");
                }
            } else {
                // LMS 발송 불 가능한 케이스
                maxlength = 80;
                if (length > maxlength) {
                    alert("LMS발송 문의는 하단의 담당자에게 연락 바랍니다.\r\n메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
                    f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);
                    return false;
                }
            }
        } else if($("#msgType").html() == "LMS") {
            maxlength = 1000;
            if(length < 80){
                $("#msgType").html("SMS");
                $("input[name=setMsgSize]").val("80");
                $("#msgTypeLength").html("80");
            }else if (length > maxlength) {
                alert("메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
                f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);

                return false;
            }
        }
    } else if (length > maxlength) {
        alert("메시지는 최대 " + maxlength + "바이트까지 전송하실 수 있습니다.\r\n초과된 " + (length - maxlength) + "바이트는 자동으로 삭제됩니다.");
        f.mt_msg.value = assert_msglen(f.mt_msg.value, maxlength);
        return false;
    }
    return true;
}

function setUserIcon() {
    let f = document.UserForm;
    let user_icon = f.imoti.value;
    let url = "<c:url value='/UserAction.do/updateUserIcon' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        user_icon : user_icon,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function setUserGift() {
    let f = document.UserForm;
    let g1 = f.user_gift_1_value.value;
    let g2 = f.user_gift_2_value.value;
    let g3 = f.user_gift_3_value.value;
    let user_image = g1 + ";" + g2 + ";" + g3;

    let url = "<c:url value='/UserAction.do/updateUserGift' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        user_image : user_image,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

let nowSendMsg = false;
function sendSmsMsg() {
    let f = document.UserForm;
    let br_key = "<c:out value='${operator.br_key}'/>";
    let board_key = "<c:out value='${param.board_key}'/>";
    let table_name = "<c:out value='${param.table_name}'/>";

    if( "<c:out value='${userInfo.allow_mt}'/>" != "1" ) {
        alert( "메시지 수신이 차단된 사용자 입니다." );
        return;
    }

    if( nowSendMsg ) {
        alert("메세지 보내는 중입니다.");
        return;
    }

    if( emptyData(f.mt_msg, "메세지를 작성하셔요.")) {
        return;
    }

    if( !check_msglen() ) {
        return;
    }

    var msgCom = '<c:out value="${param.msg_com}"/>';
    if(msgCom  == '007') {
        var kakaoUserPhone = '<c:out value="${param.user_name}"/>';

        // 카카오 사용자 핸드폰 등록 번호 유효성 체크
        if(kakaoUserPhone != "") {
            var rgEx = /(01[016789])(\d{4}|\d{3})\d{4}$/g;
            var strValue = kakaoUserPhone;
            var chkFlg = rgEx.test(strValue);
            if(!chkFlg){
                alert("올바른 휴대폰번호가 아닙니다.");
                return;
            }
        }

        // if(br_key == "FEBCDJ" || br_key == "FEBCMP" || br_key == "FEBCCW" || br_key == "FEBCUS"){
        if(br_key == "FEBCDJ"){

            if(!isKakaoFriendTalkTime()){
                alert("[카카오톡 야간발송제한]\n20:00~익일 08:00까지는 카카오톡 발송이 불가합니다.\n해당 시간에 문자로 대체 발송하길 원하시면 담당자에게 연락주세요.");
                return;
            }

            // 목포 극동 방송, 울산 극동 방송
        } else if(br_key == "FEBCMP" || br_key == "FEBCUS"  || br_key == "FEBCCW"){
            // 2018.9.28 Lucas.
            // 해당 방송사는 제한 시간의 경우 SMS 또는 LMS 가 발송 되도록 발송 데몬 ( itvSendTrlist )에서 billing_code(mt_refkey)
            // 목포 극동 방송 -> 'mpfebc' 분기 처리
            // 울산 극동 방송 -> 'usfebc' 분기 처리
            // 창원 극동 방송 -> 'cwfebc' 분기 처리 - 2018.10.15 Lucas.

        }

    } else if(msgCom  == '013'){
        var rainbowUserPhone = '<c:out value="${userInfo.user_id}"/>';
        if(rainbowUserPhone != "") {
            var rgEx = /(01[016789])(\d{4}|\d{3})\d{4}$/g;
            var strValue = rainbowUserPhone;
            var chkFlg = rgEx.test(strValue);
            if(!chkFlg){
                alert("전화번호로 등록된 유저에게만 발송이 가능합니다.");
                return;
            }
        } else {
            return;
        }
    }

    nowSendMsg = true;

    let mt_msg = f.mt_msg.value;
    let comment_id = '<c:out value="${param.comment_id}"/>';
    let user_name = '<c:out value="${param.user_name}"/>';
    let url = "<c:url value='/SendAction.do/sendSmsMsg' />";
    let data = {
        pgm_key : '<c:out value="${operator.pgm_key}"/>',
        user_id : '<c:out value="${param.user_id}"/>',
        msg_com : '<c:out value="${param.msg_com}"/>',
        mt_msg : mt_msg,
        board_key : board_key,
        table_name : table_name,
        user_name : user_name,
        comment_id : comment_id,
    };
    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}
function goPage(pageNum){
    let f = document.UserForm;
    f.page_num.value = pageNum;
    userlist();
}

function chkAutoScrollWidget(obj) {
    let value = obj.checked? "Y" : "N";
    setCookie("auto_scroll_widget_yn", value);
}
</script>
</html>