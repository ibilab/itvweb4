<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="table_type01">
    <table cellspacing="0" border="1" summary="문자투표에 대한 내용를 담고 있습니다.">
        <caption>문자투표 리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:auto;" />
            <col style="width:140px;" />
            <col style="width:140px;" />
            <col style="width:80px;" />
        </colgroup>
        <thead>
        <tr>
            <th scope="col">N</th>
            <th scope="col">투표제목</th>
            <th scope="col">시작시간</th>
            <th scope="col">종료시간</th>
            <th scope="col">상태</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${VoteForm.list}" varStatus="i">
            <tr>
                    <%-- <td class="ac"><c:out value='${(VoteForm.page_num-1)*VoteForm.row_count+i.count}'/></td> --%>
                <td class="ac"><c:out value='${VoteForm.start_index-i.index}' /></td>
                <td><a href="javascript:view('${row.vote_key}');"><c:out value='${row.vote_title}'/></a></td>
                <td class="ac"><c:out value='${row.vote_stm}'/></td>
                <td class="ac"><c:out value='${row.vote_etm}'/></td>
                <td class="ac"><c:out value='${row.vote_status_name}'/><aof:code type="print" group_cd="EL003" selected='${row.vote_status}'/></td>
            </tr>
        </c:forEach>
        <c:if test="${empty VoteForm.list}">
            <tr>
                <td colspan="5" class="ac">등록된 투표가 없습니다..</td>
            </tr>
        </c:if>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="40" align="center"><c:out value="${VoteForm.pageNavigator}" escapeXml="false"/></td>
        </tr>
    </table>
</div>
