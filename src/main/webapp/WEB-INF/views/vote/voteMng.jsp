<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="mo03">투표 설정</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
    <tr>
        <td bgcolor="#FFFFFF">
            <form name="VoteForm" action="/VoteAction.do">
                <input type="hidden" name="pgm_key" value="<c:out value='${operator.pgm_key}'/>">
                <input type="hidden" name="page_num" value="1">
                <input type="hidden" name="row_count" value="10">
            </form>
            <div id="list">
                <!-- 목록 -->
            </div>
            <br>
            <iframe name="ifr1" id="ifr1" src="/VoteAction.do/write" width="100%" height="326" border="0" frameborder="0" scrolling="no" marginwidth="1" marginheight="0"> </iframe>
        </td>
    </tr>
</table>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    list();
});

function list(){
    const url = "<c:url value='/VoteAction.do/list' />";
    let data = $(document.VoteForm).serialize().replace(/%/g,'%25');  // Form.serialize("BoardForm");

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').html(response);
}

function view(vote_key){
    ifr1.location.href = "<c:url value='/VoteAction.do/write?vote_key=' />"+vote_key;
}

function goPage(pageNum){
    let f = document.VoteForm;
    f.page_num.value = pageNum;
    list();
}
</script>
</body>
</html>