<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="mo03">투표 집계결과</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<table width="100%" border="0" cellpadding="10" cellspacing="20" bgcolor="F6F6F6">
    <tr>
        <td bgcolor="#FFFFFF">
            <form name="VoteForm" action="/VoteAction.do">
                <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
                <input type="hidden" name="page_num" value="1">
                <input type="hidden" name="row_count" value="10">
                <input type="hidden" id= "vote_items" name="vote_items" value="">

                <div class="top_searchbox pr">
                    <span class="arrow"></span>
                    <!--  -->
                    <span class="tsearch_txt">방송진행일</span>
                    <div class="tsearch select_b">
					<span class="select_box select_t">
						<span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:70px;" readonly onchange="directSearch();"> </span>
						<button type="button" class="month_btn">달력</button>
					</span>
                    </div>
                    <span class="swung">~</span>
                    <div class="tsearch select_b">
					<span class="select_box select_t">
						<span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:70px;" readonly onchange="directSearch();"> </span>
						<button type="button" class="month_btn">달력</button>
					</span>
                    </div>
                    <div id="excel_layout" style="display:none;">
                        <div class="tsearch select_b" style="float:right;">
                            <span class="btn_btype01"><a href="#" onFocus="blur()" onClick="excelDown();">엑셀 다운</a></span>
                        </div>

                        <div class="tsearch select_b" style="float:right; width:170px;">
						<span class="select_box select_t">
							<strong>항목</strong> ㅣ<span class="t_select" id="vote_item_title"></span>
							<button type="button" class="arrow_btn">화살표</button>
						</span>
                            <div class="select_m" style="width:170px;">
                                <ul class="layer_inner" id="append_test">
                                    <!-- <li><a href="#" onclick="javascript:setSelectBox(document.VoteForm.memo, '전체1', 'vote_item_title', this)">전체1</a></li> -->

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <p style="margin-top:5px"/>
            <div id="list">
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript" src="../js/common.js"></script>
<script language="JavaScript">
let f;
$(document).ready(function() {
    // date picker
    setDatePicker("#start_dt");
    setDatePicker("#end_dt");
    $( "#start_dt" ).datepicker( "setDate", "0" );
    $( "#end_dt" ).datepicker( "setDate", "0" );

    initPage();
});

function initPage(){
    f = document.VoteForm;
    list();
}

function list(){
    $("#excel_layout").hide();
    const url = "<c:url value='/VoteAction.do/list' />";
    let data = $(document.VoteForm).serialize().replace(/%/g,'%25');  // Form.serialize("BoardForm");

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').html(response);
}

//날짜 setting 시 바로 검색.
function directSearch() {
    list();
}

function goPage(pageNum){
    f.page_num.value = pageNum;
    list();
}


function view(vote_key){
    const url = "<c:url value='/VoteAction.do/resultList' />";
    let data = {
        vote_key : vote_key
    }

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}
</script>
</body>
</html>