<%@ page import="java.text.DecimalFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="box_type01 mt30">
    <div class="vote_titbox">
        <span class="vote_tit">투표제목</span>
        <strong class="vote_tit_txt"><c:out value="${vote_info.vote_title}"/></strong>
        <span class="vote_data">(<c:out value="${vote_info.vote_stm}"/> ~ <c:out value="${vote_info.vote_etm}"/>)</span>

    </div>

    <div class="vote_contbox">
        <!-- table_type02 -->
        <div class="table_type02">
            <table cellspacing="0" border="0" summary="투표결과 상세보기에 대한 내용을 담고 있습니다">
                <caption>투표결과 상세보기</caption>
                <colgroup>
                    <col style="width:10%;">
                    <col style="width:8%;">
                    <c:forTokens var="name" items='${sns_lists}' delims="|" varStatus="k">
                        <col style="width:8%;">
                    </c:forTokens>
                    <col style="width:15%;">
                    <col style="width:15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>아이템 명</th>
                    <th>순위</th>
                    <c:forTokens var="name" items='${sns_lists}' delims="|" varStatus="k">
                        <th width="5%" align="center" class="bg_darkgray"><c:out value="${name}"/></th>
                    </c:forTokens>
                    <th>집계결과</th>
                    <th>득표율</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="row" items="${results}">
                    <tr>
                        <td class="ac"><c:out value="${row.vote_item}"/></td>
                        <td align="center" class="ac"><c:out value="${row.rank}"/></td>
                        <c:forTokens var="token" items='${row.vote_result}' delims="|" varStatus="i">
                            <td align="center" class="ac"><c:out value="${token}"/></td>
                        </c:forTokens>
                        <td align="center" class="ac"><c:out value="${row.sum}"/></td>
                        <td align="center" class="ac">
                            <c:out value="${row.ratio}"/>
                        </td>
                    </tr>
                </c:forEach>
                <c:if test="${!empty results}">
                    <tr>
                        <td align="center" class="ac">총 집계결과</td>
                        <td colspan="5" align="center" class="ac"><strong><c:out value="${results[0].total}"/></strong></td>
                    </tr>
                </c:if>
                <c:if test="${empty results}">
                    <tr>
                        <td colspan="5" align="center" class="ac">결과가 없습니다.</td>
                    </tr>
                </c:if>
                </tbody>
            </table>
        </div>
        <!-- //table_type02 -->
    </div>
    <div class="box_type01_bline"></div>
</div>
<div class="btn_box">
    <div class="btn_rf">
        <span class="btn_btype01"><a href="javascript:view(<c:out value="${vote_info.vote_key}"/>);">새로고침</a></span>
        <span class="btn_btype01"><a href="javascript:directSearch();">목록</a></span>
    </div>
</div>
</body>
</html>