<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <link href="../css/custom/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="VoteForm" action="/VoteAction.do" style="margin-bottom: 0">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>"/>
    <input type="hidden" name="oper_id" value="<c:out value="${operator.oper_id}"/>"/>
    <input type="hidden" name="vote_key" value="<c:out value="${param.vote_key}"/>"/>
    <input type="hidden" name="vote_count" value="<c:out value="${Vote.vote_count}"/>"/>
    <input type="hidden" name="vote_status" value="<c:out value="${Vote.vote_status}"/>"/>
    <input type="hidden" name="sub_key" value="<c:out value="${Vote.sub_key}"/>"/>
    <input type="hidden" name="vote_etm" value=""/>
    <input type="hidden" name="vote_stm" value=""/>
    <input type="hidden" name="sns_list" value=""/>
    <input type="hidden" name="sns_count" value=""/>
    <div class="box_type01 mt30">
        <!-- table_type03 -->
        <div class="table_type03">
            <table cellspacing="0" border="1" summary="투표정보 및 투표시간에 대한 내용를 담고 있습니다.">
                <caption>투표정보 및 투표시간</caption>
                <colgroup>
                    <col style="width:150px;" />
                    <col style="width:auto;" />
                </colgroup>
                <tbody>
                <tr>
                    <th>투표정보</th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box">
                                <strong class="pntclr">제목</strong> <span class="pntclr">ㅣ</span>
                                <input type="text" name="vote_title" value="${Vote.vote_title}" style="width:200px" maxlength="100"/>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_select"><strong class="pntclr">데이터</strong> <span class="pntclr">ㅣ</span></span>
                                <select id="vote_emo" name="vote_emo" class="select_m" style="width:170px;">
                                    <c:forEach var="row" items="${vote_emo}" varStatus="i">
                                        <option value="<c:out value='${row.code}'/>" <c:if test="${Vote.vote_emo == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/></option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_select"><strong class="pntclr">방식</strong> <span class="pntclr">ㅣ</span></span>
                                <select id="vote_user_dup" name="vote_user_dup" class="select_m" style="width:170px;">
                                    <c:forEach var="row" items="${vote_user_dup}" varStatus="i">
                                        <option value="<c:out value='${row.code}'/>" <c:if test="${'1인 다중 투표' eq row.code_nm}">selected</c:if> ><c:out value="${row.code_nm }"/></option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>시작 방식</th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="start_type" name="start_type" class="select_m" style="width:100px;">
                                    <option value="RESV">예약 시작</option>
                                    <option value="NOW">바로 시작</option>
                                </select>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>투표시작</th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_calendar"><strong>시작</strong> ㅣ<input type="text" name="start_dt" id="start_dt" style="border:none;cursor:pointer;width:80px;" readonly value="<c:out value='${Vote.start_dt }'/>"> </span>
                                <button type="button" class="month_btn">달력</button>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="start_hh" name="start_hh" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${hh_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Vote.start_hh == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>시</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="start_mi" name="start_mi" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${mi_1_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Vote.start_mi == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>분</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="start_ss" name="start_ss" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${mi_1_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Vote.start_ss == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>초</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>투표종료</th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <span class="t_calendar"><strong>종료</strong> ㅣ<input type="text" name="end_dt" id="end_dt" style="border:none;cursor:pointer;width:80px;" readonly value="<c:out value='${Vote.end_dt }'/>"> </span>
                                <button type="button" class="month_btn">달력</button>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="end_hh" name="end_hh" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${hh_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Vote.end_hh == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>시</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="end_mi" name="end_mi" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${mi_1_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Vote.end_mi == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>분</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                        <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="end_ss" name="end_ss" class="select_m" style="width:70px;">
                                    <c:forEach var="row" items="${mi_1_gcd}" varStatus="i">
                                        <option value='<c:out value="${row.code}"/>' <c:if test="${Vote.end_ss == row.code}">selected</c:if> ><c:out value="${row.code_nm }"/>초</option>
                                    </c:forEach>
                                </select>
                            </span>
                        </div>
                    </td>
                </tr>

                <!-- 동적으로 SNS코드 체크박스 불러오기. 2015. 2. 9. Lucas. -->
                <tr>
                    <th>매체</th>
                    <td>
                        <c:forTokens var="oper_sns" items='${operator.oper_sns}' delims="|" varStatus="i">
                            <c:forEach var="sns_code" items="${sns_lists}" varStatus="k">
                                <c:if test="${oper_sns == sns_code.code}">
                                    <input type="checkbox" name="sns_lists" value="${sns_code.code}" <c:if test="${fn:contains(Vote.sns_list, sns_code.code)}">checked</c:if> ><c:out value="${sns_code.code_nm}"/>
                                </c:if>
                            </c:forEach>
                        </c:forTokens>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- //table_type03 -->

        <!-- table_type03 -->
        <div class="table_type03 line">
            <table cellspacing="0" border="1" summary="투표정보 및 투표시간에 대한 내용를 담고 있습니다." id="tbl1">
                <caption>투표정보 및 투표시간</caption>
                <colgroup>
                    <col style="width:90px;" />
                    <col style="width:auto;" />
                </colgroup>
                <tbody>
                <c:if test="${empty Vote.vote_items}">
                    <tr id="item_1">
                        <td class="tdth">항 &nbsp;목 &nbsp;1</td>
                        <td>
                            <div class="set_blk select_b" style="width:135px;">
                            <span class="select_box">
                                <strong class="pntclr">이름</strong> <span class="pntclr">ㅣ</span>
                                <input type="text" name="vote_item" style="width:70px" onkeyup="check_msglen(this)"/>
                            </span>
                            </div>
                            <div class="set_blk select_b"  style="width:315px;">
                                <span class="select_box">
                                    <strong class="pntclr">조건</strong> <span class="pntclr">ㅣ</span>
                                    <input type="text" name="vote_keyword" style="width:250px;" onkeyup="check_msglen(this)"/>
                                </span>
                            </div>
                            <div class="set_blk select_b">
                            <span class="select_box select_t">
                                <select id="add_count" name="add_count" class="select_m" style="width:100px;" onchange="addItem(this);">
                                    <option value="" >항목추가</option>
                                    <c:forEach var="row" begin="1" end="5">
                                        <option value='<c:out value="${row}"/>'><c:out value="${row}"/>개</option>
                                    </c:forEach>
                                </select>
                            </span>
                            </div>
                        </td>
                    </tr>
                </c:if>
                <c:if test="${!empty Vote.vote_items}">
                <c:set var="items" value="${fn:split(Vote.vote_items,'|')}" />
                <c:set var="keywords" value="${fn:split(Vote.vote_keywords,'|')}" />
                <c:forEach var="row" items="${items}" varStatus="i">
                    <tr id="item_<c:out value='${i.count}'/>">
                        <td class="tdth">항 &nbsp;목 &nbsp;<c:out value='${i.count}'/></td>
                        <td>
                            <div class="set_blk select_b" style="width:135px;">
                            <span class="select_box">
                                <strong class="pntclr">이름</strong> <span class="pntclr">ㅣ</span>
                                <input type="text" name="vote_item" style="width:70px" value="<c:out value="${row}"/>" onkeyup="check_msglen(this)"/>
                            </span>
                            </div>
                            <div class="set_blk select_b"  style="width:315px;">
                                <span class="select_box">
                                    <strong class="pntclr">조건</strong> <span class="pntclr">ㅣ</span>
                                    <input type="text" name="vote_keyword" style="width:250px;" value="<c:out value="${keywords[i.index]}"/>" onkeyup="check_msglen(this)"/>
                                </span>
                            </div>
                            <c:if test="${i.count > 1}">
                                <a href="javascript:delItem(<c:out value="${i.count}"/>)" class="set_btn"><img src="../../img/common/btn/btn_del_on.png" width="20" height="20" alt="삭제" /></a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </c:if>
                </tbody>
            </table>
        </div>
        <!-- //table_type03 -->
    </div>
    <div class="box_type01_bline"></div>
    <div class="btn_box">
        <div class="btn_lf">
            <span class="btn_btype01" id="btnNew"><a href="javascript:view();">입력모드</a></span>
            <span class="btn_btype01" id="btnInsert"><a href="javascript:save();">신규등록</a></span>
            <span class="btn_btype02" id="btnDel"><a href="javascript:del();">삭제</a></span>
            <span class="btn_btype02" id="btnEnd"><a href="javascript:ending();">종료</a></span>
        </div>
        <div class="btn_rf">
            <div class="tabletb_r" id="btns">
                <span class="btn_btype01"  id="btnUpdate"><a href="javascript:save();">저장</a></span>
            </div>
        </div>
    </div>
</form>
</div>
<script type="text/javascript" src="../js/common.js"></script>
<script language="JavaScript">
$(document).ready(function() {
    // date picker
    setDatePicker("#start_dt");
    setDatePicker("#end_dt");

    let f = document.VoteForm;

    if( f.vote_key.value ) {
    } else {
        // 초기 날짜 세팅
        $( "#start_dt" ).datepicker( "setDate", "0" );
        $( "#end_dt" ).datepicker( "setDate", "0" );
        initDate();
    }

    initWritePage();
});

function initDate() {
    const add_min = 30;     // 최초 30분 투표
    let f = document.VoteForm;

    let sDate = new Date();

    let hh = _get2ByteStr( sDate.getHours() );
    let mm = _get2ByteStr( sDate.getMinutes() );
    let ss = _get2ByteStr( sDate.getSeconds() );
    f.start_hh.value = hh;
    f.start_mi.value = mm;
    f.start_ss.value = ss;

    sDate = new Date(sDate.getTime() + (1000*60*add_min));

    hh = _get2ByteStr( sDate.getHours() );
    mm = _get2ByteStr( sDate.getMinutes() );
    ss = _get2ByteStr( sDate.getSeconds() );
    f.end_hh.value = hh;
    f.end_mi.value = mm;
    f.end_ss.value = ss;
}

function _get2ByteStr( tm ) {
    let str = ( tm > '9' ) ? tm : "0" + tm;
    return str;
}

function initWritePage() {
    let f = document.VoteForm;
    parent.resize_frame("ifr1", 180);

    $("#btnInsert").hide();
    $("#btnDel").hide();
    $("#btnInit").hide();
    $("#btnUpdate").hide();
    $("#btnRecount").hide();
    $("#btnEnd").hide();
    $("#btnNew").hide();

    if(f.vote_key.value) {
        $("#btnDel").show();
        $("#btnInit").show();
        $("#btnNew").show();
        $("#btnUpdate").show();
    } else {
        $("#btnInsert").show();
    }

    if(f.vote_status.value == "3" || f.vote_status.value == "7"){
        noEdit();
    } else if(f.vote_status.value == "2" || f.vote_status.value == "4"){
        $("#btnEnd").show();
    } else{
        $("#btnEnd").hide();
    }
}

function noEdit(){
    let f = document.VoteForm;
    <c:if test="${operator.group_level < 2}">
    if(f.vote_status.value != "7"){
        //Element.show('btnRecount');
    }
    </c:if>

    $("#btnInsert").hide();
    $("#btnEnd").hide();
    $("#itemAdd").hide();
    $("#addCount").hide();
    $("#btnUpdate").hide();
    $('#btns').html("* 종료된 투표입니다. 수정할수 없습니다.");
}


function addItem(obj) {
    let add = obj.value;
    if(add == "") return;
    let max_no = $("tr[id^='item_']").length;

    if((Number(add) + Number(max_no)) > 20){
        alert("항목은 최대 20개까지 설정할수 있습니다.");
        return;
    }

    for(let i=0 ; i < add ; i++) {
        let oTR = tbl1.insertRow(-1);
        oTR.id = "item_"+(max_no+1);
        let oCell = oTR.insertCell(-1);
        oCell.className = "tdth";
        oCell.innerHTML = "항 &nbsp;목 &nbsp;<span id='item_num_"+(max_no+1)+"'></span>";

        oCell = oTR.insertCell(-1);
        oCell.innerHTML = "<div class=\"set_blk select_b\" style=\"width:135px;\"><span class=\"select_box\"><strong class=\"pntclr\">이름</strong> <span class=\"pntclr\">ㅣ</span><input type=\"text\" style=\"width:70px\" name=\"vote_item\" value=\"\" onkeyup=\"check_msglen(this)\"></span></div>"
            + "<div class=\"set_blk select_b\" style=\"width:315px;\"><span class=\"select_box\"><strong class=\"pntclr\">조건</strong> <span class=\"pntclr\">ㅣ</span><input style=\"width:250px\" name=\"vote_keyword\" value=\"\" onkeyup=\"check_msglen(this)\"></span></div>"
            + "<a href='javascript:delItem("+(max_no+1)+")'class=\"set_btn\"><img src=\"../../img/common/btn/btn_del_on.png\" width=\"20\" height=\"20\" alt=\"삭제\" /></a>";

        max_no++;
    }
    orderNo();
    parent.resize_frame("ifr1", 100);
    obj.value = "";
}

function delItem(index){
    let row = $("#item_" + index);
    row.remove();
    orderNo();
    parent.resize_frame("ifr1");
}

function orderNo() {
    let f = document.VoteForm;
    let obj;
    let index = 2;
    let max_no = $("tr[id^='item_']").length;

    for(let i=1; i <= max_no ; i++){
        obj = $("tr[id^='item_']")[i];
        if($(obj) != null && $(obj).length >0) {
            $(obj).find("span[id^='item_num_']").html(index++);
        }
    }
    f.vote_count.value = index-1;
}

function view() {
    this.location.href = "<c:url value='/VoteAction.do/write' />";
}

function ending() {
    if(!confirm("투표를 종료하시겠습니까?")){
        return;
    }
    let f = document.VoteForm;
    let url = "<c:url value='/VoteAction.do/ending' />";
    const vote_key = f.vote_key.value;
    const sub_key = f.sub_key.value;
    let data = {
        vote_key : vote_key,
        sub_key : sub_key,
    };

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function reload() {
    parent.list();
    location.reload();
}

function del() {
    let f = document.VoteForm;
    if(!confirm("투표를 삭제하시겠습니까?")) {
        return;
    }
    if(f.vote_status.value == 2) {
        alert("진행중인 투표는 삭제하실 수 없습니다.");
        return;
    }
    let url = "<c:url value='/VoteAction.do/delete' />";
    const vote_key = f.vote_key.value;
    const sub_key = f.sub_key.value;
    let data = {
        vote_key : vote_key,
        sub_key : sub_key,
    };

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : actionResult,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function reloadInsert() {
    parent.list();
    view();
}

function save() {
    let f = document.VoteForm;
    if(isValid()) {
        let url = "<c:url value='/VoteAction.do/save' />";
        let resultCheckKeywordDup =  checkKeywordDup();
        // 키워드 중복 확인
        if(resultCheckKeywordDup != -1) {
            alert(resultCheckKeywordDup + 1 + "번째 키워드가 중복이 됩니다. 확인바랍니다.");
            f.vote_keyword[resultCheckKeywordDup].select();
            f.vote_keyword[resultCheckKeywordDup].focus();
            return false;
        }


        f.vote_stm.value = f.start_dt.value.replace(/-/g,"") + f.start_hh.value + f.start_mi.value+f.start_ss.value;
        f.vote_etm.value = f.end_dt.value.replace(/-/g,"") + f.end_hh.value + f.end_mi.value+f.end_ss.value;

        // 재시작 요건 확인
        if(f.vote_key.value != "" && isRestart()) {
            if(confirm("투표가 재시작 됩니다. 계속 하시겠습니까?"))
                f.vote_status.value = "4";
            else return;
        }

        f.vote_key.value = f.vote_key.value ? f.vote_key.value : 0;
        f.sub_key.value = f.sub_key.value ? f.sub_key.value : 0;

        let data = $(f).serialize();
        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
}


function isValid() {
    let f = document.VoteForm;
    f.pgm_key.value = <c:out value="${operator.pgm_key}"/>;

    if(emptyData(f.pgm_key, "프로그램을 선택해주세요.")) return false;
    if(emptyData(f.vote_title, "투표 제목을 입력해주세요.")) return false;
    if(emptyData(f.start_dt, "투표 시작일을 입력해주세요.")) return false;
    if(emptyData(f.end_dt, "투표 종료일을 입력해주세요.")) return false;


    f.sns_list.value = getChks(f.sns_lists,"|");
    f.sns_count.value = f.sns_list.value.split("|").length;

    if( f.sns_list.value < 1 ) {
        alert("매체를 선택해 주세요");
        return;
    }

    var start_yy	= f.start_dt.value.substring(0,4);
    var start_mm	= f.start_dt.value.substring(5,7);
    var start_dd	= f.start_dt.value.substring(8,10);

    var end_yy		= f.end_dt.value.substring(0,4);
    var end_mm		= f.end_dt.value.substring(5,7);
    var end_dd		= f.end_dt.value.substring(8,10);

    var sDate	= new Date(start_yy, start_mm-1, start_dd, f.start_hh.value, f.start_mi.value);
    var eDate	= new Date(end_yy, end_mm-1, end_dd, f.end_hh.value, f.end_mi.value);

    if( f.start_type.value == "NOW" )  {
        sDate = new Date();
    }

    //alert( eDate + " : " + sDate );
    //return;

    if( eDate < sDate ) {
        alert("종료일자는 시작일자 이후로 설정하세요.");
        f.end_dt.focus();
        return;
    }

    if(f.vote_item.length == null){
        alert("항목을 입력하세요.");
        return false; // 아무것도 입력하지 않은 경우.
    }
    for(let i=0; i < f.vote_item.length; i++){
        if(trim(f.vote_item[i].value) == ''){
            alert((i+1)+"번 항목의 이름을 입력해주세요.");
            f.vote_item[i].focus();
            return false;
        } else if(!isUseFulVoteString(f.vote_item[i].value)) {
            alert((i+1)+"번 항목의 이름의 특수문자( " + specialChars + " )를 빼주세요.");
            f.vote_item[i].focus();
            return false;
        }
        if(trim(f.vote_keyword[i].value) == ''){
            alert((i+1)+"번 항목의 조건을 입력해주세요.");
            f.vote_keyword[i].focus();
            return false;
        } else if(!isUseFulVoteString(f.vote_keyword[i].value)) {
            alert((i+1)+"번 항목의 조건의 특수문자( " + specialChars + " )를 빼주세요.");
            f.vote_keyword[i].focus();
            return false;
        }
    }

    return true;
}

// 투표 항목/키워드 길이 제한 30 byte. 2015.10.19 LUCAS : START
function check_msglen( obj ) {
    var valueText = obj.value;
    var valueByte = calculate_msglen(valueText);

    if(valueByte > 30){
        alert("최대 30 바이트까지 설정 가능합니다.\r\n초과된 " + (valueByte - 30) + "바이트는 자동으로 삭제됩니다.");
        obj.value = assert_msglen(valueText, 30);
    }
}

function calculate_msglen(message) {
    var nbytes = 0;

    for (i=0; i<message.length; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            nbytes += 2;
        } else if (ch != '\r') {
            nbytes++;
        }
    }
    return nbytes;
}

function assert_msglen(message, maximum) {
    var inc = 0;
    var nbytes = 0;
    var msg = "";
    var msglen = message.length;

    for (let i=0; i<msglen; i++) {
        var ch = message.charAt(i);
        if (escape(ch).length > 4) {
            inc = 2;
        } else if (ch != '\r') {
            inc = 1;
        }
        if ((nbytes + inc) > maximum) {
            break;
        }
        nbytes += inc;
        msg += ch;
    }
    return msg;
}

function checkKeywordDup() {
    let f = document.VoteForm;
    // 전체 목록 구하기
    var row_cnt = f.vote_keyword.length;
    var total_keyword = "";

    for(var i=0 ; i<row_cnt ; i++) {
        total_keyword += f.vote_keyword[i].value;
        if(i+1 != row_cnt) total_keyword += ";";
    }

    var keywords = total_keyword.split(';');

    // for 2개로 중복 확인
    for(var i=0 ; i<keywords.length ; i++) {
        var dup_cnt = 0;
        for(var j=0 ; j<row_cnt ; j++) {
            var row_data = f.vote_keyword[j].value.split(';');
            for(var k=0 ; k<row_data.length ; k++) {
                if(keywords[i] == row_data[k]) dup_cnt++;
                if(dup_cnt > 1) return j;
            }
        }
    }
    return -1;
}

function isRestart() {
    let f = document.VoteForm;
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minute = date.getMinutes();
    month = month > '9' ? month : "0" + month;
    day = day > '9' ? day : "0" + day;
    hour = hour > '9' ? hour : "0" + hour;
    minute = minute > '9' ? minute : "0" + minute;
    var nowDate = year + month + day + hour + minute + "00";

    // 항목 수정시
    var arrItems = '<c:out value="${Vote.vote_items}"/>'.split('|');
    var arrKeywords = '<c:out value="${Vote.vote_keywords}"/>'.split('|');
    if(arrItems.length != f.vote_item.length) return true;
    for(var i=0; i < f.vote_item.length; i++){
        if(arrItems[i] != f.vote_item[i].value || arrKeywords[i] != f.vote_keyword[i].value){
            return true;
        }
    }

    // 시작시간 수정
    if('<c:out value="${Vote.start_dt}"/>' != f.start_dt.value || '<c:out value="${Vote.start_hh}"/>' != f.start_hh.value || '<c:out value="${Vote.start_mi}"/>' != f.start_mi.value) {
        return true;
    }
    // 종료시간 수정
    if('<c:out value="${Vote.end_dt}"/>' != f.end_dt.value || '<c:out value="${Vote.end_hh}"/>' != f.end_hh.value || '<c:out value="${Vote.end_mi}"/>' != f.end_mi.value) {
        return true;
    }
}

</script>
</body>
</html>