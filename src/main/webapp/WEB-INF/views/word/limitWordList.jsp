<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="table_type01 mt00">
    <table cellspacing="0" border="1" summary="금칙어 리스트에 대한 내용를 담고 있습니다.">
        <caption>금칙어 리스트</caption>
        <colgroup>
            <col style="width:45px;" />
            <col style="width:auto;" />
            <col style="width:40px;" />
            <%-- <col style="width:150px;" /> --%>
        </colgroup>
        <thead>
        <tr>
            <th scope="col">N</th>
            <th scope="col" colspan="2">단어</th>
            <!-- <th scope="col">대체 단어</th> -->
        </tr>
        </thead>
        <tbody>
        <c:forEach var="row" items="${WordForm.list}" varStatus="i">
            <tr>
                <td class="ac"><c:out value='${(WordForm.page_num-1)*WordForm.row_count+i.count}'/></td>
                <td><c:out value='${row.word_limt}'/></td>
                <td class="noline">
                    <div class="icon_area">
                        <ul class="iconbox01">
                            <li><a href="javascript:del('<c:out value="${row.lword_key}"/>');"><img src="../../img/common/btn/btn_del_on.png" width="20" height="20" alt="삭제" title="삭제"/></a></li>
                        </ul>
                    </div>
                </td>
                    <%-- <td class="ac"><c:out value='${row.word_replace}'/></td> --%>
            </tr>
        </c:forEach>
        <c:if test="${empty WordForm.list}">
            <tr>
                <td colspan="3" align="center">등록된 단어가 없습니다.
                </td>
            </tr>
        </c:if>
        </tbody>
    </table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="40" align="center"><c:out value="${WordForm.pageNavigator}" escapeXml="false"/></td>
    </tr>
</table>
</body>
</html>