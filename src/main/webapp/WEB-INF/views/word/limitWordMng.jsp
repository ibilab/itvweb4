<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
</head>
<body>
<div class="cont_top">
    <div class="cont_topl">
        <!-- path -->
        <ul class="path_area">
            <li class="home"><c:out value="${menu_1_name}"/></li>
            <li class="on"><c:out value="${menu_2_name}"/></li>
        </ul>
        <!-- //path -->
        <!-- tit -->
        <div class="tit_box">
            <h2 class="ss02">금칙어 관리</h2>
        </div>
        <!-- //tit -->
    </div>
</div>
<form name="WordForm" action="/WordMng.do">
    <input type="hidden" name="pgm_key" value="<c:out value="${operator.pgm_key}"/>">
    <input type="hidden" name="word_replace" value="">
    <input type="hidden" name="word_use" value="1">
    <input type="hidden" name="page_num" value="1">
    <input type="hidden" name="row_count" value="15">

    <div id="list">
        <!-- 단어 목록 -->
    </div>
    <div class="box_type01 mt00">
        <!-- table_type03 -->
        <div class="table_type03">
            <table cellspacing="0" border="1" summary="금칙어 관리에 대한 내용를 담고 있습니다.">
                <caption>금칙어 관리</caption>
                <colgroup>
                    <col style="width:90px;" />
                        <%-- <col style="width:90px;" /> --%>
                    <col style="width:90px;" />
                </colgroup>
                <tbody>
                <tr>
                    <th>새 금칙어 : </th>
                    <td>
                        <!--  -->
                        <div class="set_blk select_b">
							<span class="select_box">
								<input type="text" name="word_limt" style="width:80px" onkeydown="onEnter(save);">
							</span>
                        </div>
                        <!-- // -->
                    </td>
                    <!-- <th>대체단어 : </th>
                    <td>
                        <div class="set_blk select_b">
                            <span class="select_box">
                                <input type="text" name="word_replace" style="width:80px" onkeydown="onEnter(save);">
                            </span>
                        </div>
                    </td> -->
                    <td>
                        <span id="btnChkOperID" class="btn_btype01"><a href="javascript:save();"><span>추가</span></a></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- //table_type03 -->
    </div>
</form>
<script type="text/javascript" src="../js/common.js"></script>
<script language="JavaScript">
var f;
$(document).ready(function(){
    initPage();
});
function initPage(){
    f = document.WordForm;
    f.word_limt.value = "";
    //f.word_replace.value = "";
    list();
}
function list(){
    var url = "<c:url value='/WordMng.do/limitWordList' />";
    var data = $(document.WordForm).serialize().replace(/%/g,'%25');

    $.ajax({
        url: url,
        type : "post",
        dataType : "html",
        data : data,
        error : ajaxError,
        success : endList,
        beforeSend : ajaxOnCreate,
        complete : ajaxOnComplete
    });
}

function endList(response, status, request){
    $('#list').html(response);
}

function goPage(pageNum){
    f.page_num.value = pageNum;
    list();
}

function del(key) {
    if(confirm("삭제하시겠습니까?")){
        var url = "<c:url value='/WordMng.do/deleteLimitWord' />";
        var data = {
            lword_key : key
        }

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });
    }
}

function save(){
    if(isValid()){
        f.page_num.value = "1";

        const url = "<c:url value='/WordMng.do/insertLimitWord' />";
        const data = $(document.WordForm).serialize();

        $.ajax({
            url: url,
            type : "post",
            dataType : "html",
            data : data,
            error : ajaxError,
            success : actionResult,
            beforeSend : ajaxOnCreate,
            complete : ajaxOnComplete
        });

    }
}

function isValid(){
    if(emptyData(f.word_limt, "새 금칙어를 입력해주세요.")) return false;
    //if(emptyData(f.word_replace, "대체단어를 입력해주세요.")) return false;

    return true;
}
</script>
</body>
</html>