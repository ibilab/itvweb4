<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String oauth_token = request.getParameter("oauth_token");
    String oauth_verifier = request.getParameter("oauth_verifier");

    request.setAttribute("token", oauth_token);
    request.setAttribute("verifier", oauth_verifier);
%>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/js/jquery/jquery-1.9.1.js"></script>
</head>
<body>
<script>
    const token = '${token}';
    const verifier = '${verifier}';
    $(function(){
       $.ajax({
           url : '/SnsAction.do/twitter/oauth'
           ,type : 'post'
           ,dataType : 'json'
           ,data : {'oauth_token' : token, 'oauth_verifier' : verifier}
       }).done(function(response){
           if(response.result){
               window.opener.document.getElementById('twitterCustomerKey').value = response.consumerKey;
               window.opener.document.getElementById('twitterCustomerSecret').value = response.consumerSecret;
               window.opener.document.getElementById('accessToken').value = response.accessToken;
               window.opener.document.getElementById('accessTokenSecret').value = response.accessTokenSecret;
               window.close();
           } else {
               alert(response.message);
               window.close();
           }
       });
    });
</script>
</body>
</html>
